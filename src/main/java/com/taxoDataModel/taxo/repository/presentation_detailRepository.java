package com.taxoDataModel.taxo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.taxoDataModel.taxo.model.presentation_detail;

public interface presentation_detailRepository extends JpaRepository<presentation_detail, Long> {
	
	List<presentation_detail> findByParentIdIsNull();
	
	presentation_detail findByDataItem(String data_item);

}

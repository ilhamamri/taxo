package com.taxoDataModel.taxo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.taxoDataModel.taxo.model.tax_enum;
import com.taxoDataModel.taxo.model.tax_sme_enum;

public interface tax_sme_enumRepository extends JpaRepository<tax_sme_enum, Long> {

	List<tax_sme_enum> findByParentIdIsNull();
	
	@Query("SELECT c FROM tax_sme_enum c WHERE c.parentId is null and c.modalId = :modalId")
	List<tax_sme_enum> findByParentIdAndModalId(@Param("modalId") Long mid);
	
	List<tax_sme_enum> findByModalId(Long id);
	
	tax_sme_enum findTopByName(String name);
}

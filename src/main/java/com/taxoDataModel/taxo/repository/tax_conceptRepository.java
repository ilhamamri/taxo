package com.taxoDataModel.taxo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.taxoDataModel.taxo.model.tax_concept;

public interface tax_conceptRepository extends JpaRepository<tax_concept, Long> {

	List<tax_concept> findByPrefix(String prefix);
	
	List<tax_concept> findByModalId(Long id);
	
	List<tax_concept> findByWorkingSheet(String workingSheet);
	
	@Query(nativeQuery = true, value = "SELECT TOP 1 * FROM tax_concept WHERE name = :name and modal_id = :modalId")
	tax_concept findTopByNameAndModalId(@Param("name") String name, @Param("modalId") Long id);
	
}

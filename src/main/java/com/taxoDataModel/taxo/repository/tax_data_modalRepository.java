package com.taxoDataModel.taxo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.taxoDataModel.taxo.model.tax_data_modal;
import com.taxoDataModel.taxo.model.tax_form;

public interface tax_data_modalRepository extends JpaRepository<tax_data_modal, Long> {

	tax_data_modal findByFilename(String filename);
}

package com.taxoDataModel.taxo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.taxoDataModel.taxo.model.presentation_formula;

public interface presentation_formulaRepository extends JpaRepository<presentation_formula, Long> {

	List<presentation_formula> findByDataItem(String data_item);
}

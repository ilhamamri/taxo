package com.taxoDataModel.taxo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.taxoDataModel.taxo.model.tax_form_header;

public interface tax_form_headerRepository extends JpaRepository<tax_form_header, Long> {
	
	List<tax_form_header> findByParentIdIsNull();

}

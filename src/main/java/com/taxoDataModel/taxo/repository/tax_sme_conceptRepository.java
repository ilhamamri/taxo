package com.taxoDataModel.taxo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.taxoDataModel.taxo.model.tax_sme_concept;

public interface tax_sme_conceptRepository extends JpaRepository<tax_sme_concept, Long> {
	
	List<tax_sme_concept> findByPrefix(String prefix);
	
	List<tax_sme_concept> findByModalId(Long id);
	
	List<tax_sme_concept> findByWorkingSheet(String workingSheet);
	
	@Query("SELECT c FROM tax_sme_concept c WHERE c.name = :name and c.modalId = :modalId")
	tax_sme_concept findTopByName(@Param("name") String name, @Param("modalId") Long id);

}

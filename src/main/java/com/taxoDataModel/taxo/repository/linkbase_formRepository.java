package com.taxoDataModel.taxo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.taxoDataModel.taxo.model.linkbase_form;

public interface linkbase_formRepository extends JpaRepository<linkbase_form, Long> {
	
	linkbase_form findByName(String name);
	
	linkbase_form findByModalIdAndName(Long id, String name);
	
	linkbase_form findByDefinition(String definition);
	
	@Query("SELECT c FROM linkbase_form c WHERE c.namespace = :namespace and c.modalId = :modalId")
	linkbase_form findByNamespaceAndModalId(@Param("modalId") Long id, @Param("namespace") String namespace);
	
	@Query("SELECT c FROM linkbase_form c WHERE c.category is not null and c.modalId = :modalId")
	List<linkbase_form> findByCategoryAndModalId(@Param("modalId") Long id);
	
	List<linkbase_form> findByModalId(Long id);

}

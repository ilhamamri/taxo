package com.taxoDataModel.taxo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.taxoDataModel.taxo.model.tax_form;

public interface tax_formRepository extends JpaRepository<tax_form, Long> {
	
	tax_form findByColumnIndex(Long columnIndex);
	
//	tax_form findByColumnIndexAndTaxDataModalId(Long columnIndex, Long taxDataModalId);
	
	tax_form findByFormName(String formName);
	
	

}

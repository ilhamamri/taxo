package com.taxoDataModel.taxo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.taxoDataModel.taxo.model.tax_form_linkbase;

public interface tax_form_linkbaseRepository extends JpaRepository<tax_form_linkbase, Long> {

}

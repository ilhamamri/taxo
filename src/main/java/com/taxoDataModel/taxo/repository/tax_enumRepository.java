package com.taxoDataModel.taxo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.taxoDataModel.taxo.model.tax_enum;

public interface tax_enumRepository extends JpaRepository<tax_enum, Long> {
	
	List<tax_enum> findByParentIdIsNull();
	
	@Query("SELECT c FROM tax_enum c WHERE c.parentId is null and c.modalId = :modalId")
	List<tax_enum> findByParentIdAndModalId(@Param("modalId") Long mid);
	
	List<tax_enum> findByModalId(Long id);
	
	tax_enum findTopByName(String name);

}

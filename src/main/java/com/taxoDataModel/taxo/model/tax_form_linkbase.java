package com.taxoDataModel.taxo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "tax_form_linkbase")
public class tax_form_linkbase {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	
	@Column(name = "tax_form_id")
	private long tax_form_id;
	
	@Column(name = "linkbase_form_id")
	private long linkbase_form_id;
	
//	@ManyToOne
//    @JoinColumn(name = "tax_form_id")
//	tax_form form;
//	
//	@ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "linkbase_form_id", insertable = false, updatable = false)
//	private linkbase_form linkbaseForm;

	public tax_form_linkbase() {
	}

	public tax_form_linkbase(long id, long tax_form_id, long linkbase_form_id) {
		this.id = id;
		this.tax_form_id = tax_form_id;
		this.linkbase_form_id = linkbase_form_id;
	}

	public long getId() {
		return id;
	}

	public long getTax_form_id() {
		return tax_form_id;
	}

	public long getLinkbase_form_id() {
		return linkbase_form_id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setTax_form_id(long tax_form_id) {
		this.tax_form_id = tax_form_id;
	}

	public void setLinkbase_form_id(long linkbase_form_id) {
		this.linkbase_form_id = linkbase_form_id;
	}

//	public tax_form getForm() {
//		return form;
//	}
//
//	
//	public linkbase_form getLinkbaseForm() {
//		return linkbaseForm;
//	}

//	public void setForm(tax_form form) {
//		this.form = form;
//	}
//
//	@JsonIgnore
//	public void setLinkbaseForm(linkbase_form linkbaseForm) {
//		this.linkbaseForm = linkbaseForm;
//	}
	
	
	
}

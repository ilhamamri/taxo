package com.taxoDataModel.taxo.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "tax_enum")
public class tax_enum {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "rno", unique = true, nullable = false)
	private long rno;
	
	@Column(name = "enum_id", length = 500)
	private String enum_id;

	@Column(name = "linkrole", length = 5000)
	private String linkrole;
	
	@Column(name = "definition_en", length = 5000)
	private String definition_en;
	
	@Column(name = "definiton_ms", length = 5000)
	private String definiton_ms;
	
	@Column(name = "prefix", length = 5000)
	private String prefix;
	
	@Column(name = "name", length = 5000)
	private String name;
	
	@Column(name = "label_en", length = 5000)
	private String label_en;
	
	@Column(name = "label_ms", length = 5000)
	private String label_ms;
	
	@Column(name = "usable")
	private String usable;
	
	@Column(name = "parent_id")
	private Long parentId;
	
	@Column(name = "modal_id")
	private Long modalId;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="parent_id", insertable = false, updatable = false)
    private tax_enum parent;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "parent_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<tax_enum> childrens = new ArrayList<tax_enum>();
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "parent")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<tax_enum> childList = new ArrayList<tax_enum>();

	public tax_enum() {
	}

	public tax_enum(long rno, String enum_id, String linkrole, String definition_en, String definiton_ms, String prefix,
			String name, String label_en, String label_ms, String usable, Long modalId) {
		this.enum_id = enum_id;
		this.linkrole = linkrole;
		this.definition_en = definition_en;
		this.definiton_ms = definiton_ms;
		this.prefix = prefix;
		this.name = name;
		this.label_en = label_en;
		this.label_ms = label_ms;
		this.usable = usable;
		this.modalId = modalId;
	}

	public tax_enum(tax_enum obj) {
		this.enum_id = obj.enum_id;
		this.linkrole = obj.linkrole;
		this.definition_en = obj.definition_en;
		this.definiton_ms = obj.definiton_ms;
		this.prefix = obj.prefix;
		this.name = obj.name;
		this.label_en = obj.label_en;
		this.label_ms = obj.label_ms;
		this.usable = obj.usable;
		this.modalId = obj.getModalId();
	}

	public long getRno() {
		return rno;
	}

	public String getEnum_id() {
		return enum_id;
	}

	public String getLinkrole() {
		return linkrole;
	}

	public String getDefinition_en() {
		return definition_en;
	}

	public String getDefiniton_ms() {
		return definiton_ms;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getName() {
		return name;
	}

	public String getLabel_en() {
		return label_en;
	}

	public String getLabel_ms() {
		return label_ms;
	}

	public String getUsable() {
		return usable;
	}

	public void setRno(long rno) {
		this.rno = rno;
	}

	public void setEnum_id(String enum_id) {
		this.enum_id = enum_id;
	}

	public void setLinkrole(String linkrole) {
		this.linkrole = linkrole;
	}

	public void setDefinition_en(String definition_en) {
		this.definition_en = definition_en;
	}

	public void setDefiniton_ms(String definiton_ms) {
		this.definiton_ms = definiton_ms;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLabel_en(String label_en) {
		this.label_en = label_en;
	}

	public void setLabel_ms(String label_ms) {
		this.label_ms = label_ms;
	}

	public void setUsable(String usable) {
		this.usable = usable;
	}
	
	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	@JsonIgnore
	public tax_enum getParent() {
		return parent;
	}

	public void setParent(tax_enum parent) {
		this.parent = parent;
	}

	public void setChildren(List<tax_enum> childrens) {
		this.childrens = childrens;
	}
	
	@JsonIgnore
    public List<tax_enum> getChildren() {
        return childrens;
    }
    
    public List<tax_enum> getChildList() {
        return childList;
    }
    
    public void addChild(tax_enum children) {
        this.childrens.add(children);
    }

	public Long getModalId() {
		return modalId;
	}

	public void setModalId(Long modalId) {
		this.modalId = modalId;
	}

	@Override
	public String toString() {
		return "tax_enum [rno=" + rno + ", enum_id=" + enum_id + ", linkrole=" + linkrole + ", definition_en="
				+ definition_en + ", definiton_ms=" + definiton_ms + ", prefix=" + prefix + ", name=" + name
				+ ", label_en=" + label_en + ", label_ms=" + label_ms + ", usable=" + usable + ", parent=" + parent
				+ ", childrens=" + childrens + "]";
	}
	
}

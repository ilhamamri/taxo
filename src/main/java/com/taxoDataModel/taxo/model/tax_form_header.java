package com.taxoDataModel.taxo.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "tax_form_header")
public class tax_form_header {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	
//	@Column(name = "form_id")
//	private long form_id;
	
	@Column(name = "prefix")
	private String prefix;
	
	@Column(name = "name", length = 5000)
	private String name;
	
	@Column(name = "label_en", length = 5000)
	private String label_en;
	
	@Column(name = "label_ms", length = 5000)
	private String label_ms;
	
	@Column(name = "label_en_form", length = 5000)
	private String label_en_form;

	@Column(name = "label_ms_form", length = 5000)
	private String label_ms_form;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="parent_id", insertable = false, updatable = false)
    private tax_form_header parent;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "parent_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<tax_form_header> childrens = new ArrayList<tax_form_header>();
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "parent")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<tax_form_header> childList = new ArrayList<tax_form_header>();

	public tax_form_header() {
	}

	public tax_form_header(long form_id, String prefix, String name, String label_en, String label_ms,
			String label_en_form, String label_ms_form) {
		super();
//		this.form_id = form_id;
		this.prefix = prefix;
		this.name = name;
		this.label_en = label_en;
		this.label_ms = label_ms;
		this.label_en_form = label_en_form;
		this.label_ms_form = label_ms_form;
	}

	public long getId() {
		return id;
	}

//	public long getForm_id() {
//		return form_id;
//	}

	public String getPrefix() {
		return prefix;
	}

	public String getName() {
		return name;
	}

	public String getLabel_en() {
		return label_en;
	}

	public String getLabel_ms() {
		return label_ms;
	}

	public String getLabel_en_form() {
		return label_en_form;
	}

	public String getLabel_ms_form() {
		return label_ms_form;
	}

	public void setId(long id) {
		this.id = id;
	}

//	public void setForm_id(long form_id) {
//		this.form_id = form_id;
//	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLabel_en(String label_en) {
		this.label_en = label_en;
	}

	public void setLabel_ms(String label_ms) {
		this.label_ms = label_ms;
	}

	public void setLabel_en_form(String label_en_form) {
		this.label_en_form = label_en_form;
	}

	public void setLabel_ms_form(String label_ms_form) {
		this.label_ms_form = label_ms_form;
	}
		
	@JsonIgnore
	public tax_form_header getParent() {
		return parent;
	}

	public void setParent(tax_form_header parent) {
		this.parent = parent;
	}

	public void setChildren(List<tax_form_header> childrens) {
		this.childrens = childrens;
	}
	
	@JsonIgnore
    public List<tax_form_header> getChildren() {
        return childrens;
    }
    
    public List<tax_form_header> getChildList() {
        return childList;
    }
    
    public void addChild(tax_form_header children) {
        this.childrens.add(children);
    }

}

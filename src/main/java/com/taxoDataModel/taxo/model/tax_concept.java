package com.taxoDataModel.taxo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tax_concept")
public class tax_concept {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "rno", unique = true, nullable = false)
	private long rno;
	
	@Column(name = "working_sheet", length = 500)
	private String workingSheet;

	@Column(name = "prefix", length = 500)
	private String prefix;

	@Column(name = "name", length = 5000)
	private String name;
	
	@Column(name = "occurence", length = 500)
	private String occurence;
	
	@Column(name = "concept_id", length = 5000)
	private String concept_id;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "substitution_group")
	private String substitution_group;
	
	@Column(name = "enum_headUsable")
	private String enum_headUsable;
	
	@Column(name = "enum_domain")
	private String enum_domain;
	
	@Column(name = "enum_linkrole", length = 5000)
	private String enum_linkrole;
	
	@Column(name = "typedDomainRef")
	private String typedDomainRef;
	
	@Column(name = "balance")
	private String balance;
	
	@Column(name = "period_type")
	private String period_type;

	@Column(name = "abstract")
	private boolean abstract_col;
	
	@Column(name = "nillable")
	private boolean nillable;
	
	@Column(name = "label_en", length = 5000)
	private String label_en;
	
	@Column(name = "label_ms", length = 5000)
	private String label_ms;
	
	@Column(name = "reference_name", length = 5000)
	private String reference_name;
	
	@Column(name = "reference_name2", length = 5000)
	private String reference_name2;
	
	@Column(name = "reference_section", length = 5000)
	private String reference_section;
	
	@Column(name = "reference_section2", length = 5000)
	private String reference_section2;
	
	@Column(name = "reference_subsection")
	private String reference_subsection;
	
	@Column(name = "reference_subsection2")
	private String reference_subsection2;
	
	@Column(name = "reference_paragraph")
	private String reference_paragraph;
	
	@Column(name = "reference_paragraph2")
	private String reference_paragraph2;
	
	@Column(name = "reference_subparagraph")
	private String reference_subparagraph;
	
	@Column(name = "reference_subparagraph2")
	private String reference_subparagraph2;
	
	@Column(name = "remarks", length = 5000)
	private String remarks;
	
	@Column(name = "modal_id")
	private Long modalId;

	public tax_concept() {
	}
	
	public tax_concept(String working_sheet, String prefix, String name, String occurence, String concept_id, String type,
			String substitution_group, String enum_headUsable, String enum_domain, String enum_linkrole,
			String typedDomainRef, String balance, String period_type, boolean abstract_col, boolean nillable,
			String label_en, String label_ms, String reference_name, String reference_name2, String reference_section,
			String reference_section2, String reference_subsection, String reference_subsection2,
			String reference_paragraph, String reference_paragraph2, String reference_subparagraph,
			String reference_subparagraph2, String remarks, Long modalId) {
		this.workingSheet = working_sheet;
		this.prefix = prefix;
		this.name = name;
		this.occurence = occurence;
		this.concept_id = concept_id;
		this.type = type;
		this.substitution_group = substitution_group;
		this.enum_headUsable = enum_headUsable;
		this.enum_domain = enum_domain;
		this.enum_linkrole = enum_linkrole;
		this.typedDomainRef = typedDomainRef;
		this.balance = balance;
		this.period_type = period_type;
		this.abstract_col = abstract_col;
		this.nillable = nillable;
		this.label_en = label_en;
		this.label_ms = label_ms;
		this.reference_name = reference_name;
		this.reference_name2 = reference_name2;
		this.reference_section = reference_section;
		this.reference_section2 = reference_section2;
		this.reference_subsection = reference_subsection;
		this.reference_subsection2 = reference_subsection2;
		this.reference_paragraph = reference_paragraph;
		this.reference_paragraph2 = reference_paragraph2;
		this.reference_subparagraph = reference_subparagraph;
		this.reference_subparagraph2 = reference_subparagraph2;
		this.remarks = remarks;
		this.modalId = modalId;
	}

	public tax_concept(tax_concept Obj) {
		this.workingSheet = Obj.getWorkingSheet();
		this.prefix = Obj.getPrefix();
		this.name = Obj.getName();
		this.occurence = Obj.getOccurence();
		this.concept_id = Obj.getConcept_id();
		this.type = Obj.getType();
		this.substitution_group = Obj.getSubstitution_group();
		this.enum_headUsable = Obj.getEnum_headUsable();
		this.enum_domain = Obj.getEnum_domain();
		this.enum_linkrole = Obj.getEnum_linkrole();
		this.typedDomainRef = Obj.getTypedDomainRef();
		this.balance = Obj.getBalance();
		this.period_type = Obj.getPeriod_type();
		this.abstract_col = Obj.isAbstract_col();
		this.nillable = Obj.isNillable();
		this.label_en = Obj.getLabel_en();
		this.label_ms = Obj.getLabel_ms();
		this.reference_name = Obj.getReference_name();
		this.reference_name2 = Obj.getReference_name2();
		this.reference_section = Obj.getReference_section();
		this.reference_section2 = Obj.getReference_section2();
		this.reference_subsection = Obj.getReference_subsection();
		this.reference_subsection2 = Obj.getReference_subsection2();
		this.reference_paragraph = Obj.getReference_paragraph();
		this.reference_paragraph2 = Obj.getReference_paragraph2();
		this.reference_subparagraph = Obj.getReference_subparagraph();
		this.reference_subparagraph2 = Obj.getReference_subparagraph2();
		this.remarks = Obj.getRemarks();
		this.modalId = Obj.getModalId();
	}

	public long getRno() {
		return rno;
	}
	public String getWorkingSheet() {
		return workingSheet;
	}

	public void setWorkingSheet(String working_sheet) {
		this.workingSheet = working_sheet;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getName() {
		return name;
	}

	public String getOccurence() {
		return occurence;
	}

	public String getConcept_id() {
		return concept_id;
	}

	public String getType() {
		return type;
	}

	public String getSubstitution_group() {
		return substitution_group;
	}

	public String getEnum_headUsable() {
		return enum_headUsable;
	}

	public String getEnum_domain() {
		return enum_domain;
	}

	public String getEnum_linkrole() {
		return enum_linkrole;
	}

	public String getTypedDomainRef() {
		return typedDomainRef;
	}

	public String getBalance() {
		return balance;
	}

	public String getPeriod_type() {
		return period_type;
	}

	public boolean isAbstract_col() {
		return abstract_col;
	}

	public boolean isNillable() {
		return nillable;
	}

	public String getLabel_en() {
		return label_en;
	}

	public String getLabel_ms() {
		return label_ms;
	}

	public String getReference_name() {
		return reference_name;
	}

	public String getReference_name2() {
		return reference_name2;
	}

	public String getReference_section() {
		return reference_section;
	}

	public String getReference_section2() {
		return reference_section2;
	}

	public String getReference_subsection() {
		return reference_subsection;
	}

	public String getReference_subsection2() {
		return reference_subsection2;
	}

	public String getReference_paragraph() {
		return reference_paragraph;
	}

	public String getReference_paragraph2() {
		return reference_paragraph2;
	}

	public String getReference_subparagraph() {
		return reference_subparagraph;
	}

	public String getReference_subparagraph2() {
		return reference_subparagraph2;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRno(long rno) {
		this.rno = rno;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOccurence(String occurence) {
		this.occurence = occurence;
	}

	public void setConcept_id(String concept_id) {
		this.concept_id = concept_id;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setSubstitution_group(String substitution_group) {
		this.substitution_group = substitution_group;
	}

	public void setEnum_headUsable(String enum_headUsable) {
		this.enum_headUsable = enum_headUsable;
	}

	public void setEnum_domain(String enum_domain) {
		this.enum_domain = enum_domain;
	}

	public void setEnum_linkrole(String enum_linkrole) {
		this.enum_linkrole = enum_linkrole;
	}

	public void setTypedDomainRef(String typedDomainRef) {
		this.typedDomainRef = typedDomainRef;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public void setPeriod_type(String period_type) {
		this.period_type = period_type;
	}

	public void setAbstract_col(boolean abstract_col) {
		this.abstract_col = abstract_col;
	}

	public void setNillable(boolean nillable) {
		this.nillable = nillable;
	}

	public void setLabel_en(String label_en) {
		this.label_en = label_en;
	}

	public void setLabel_ms(String label_ms) {
		this.label_ms = label_ms;
	}

	public void setReference_name(String reference_name) {
		this.reference_name = reference_name;
	}

	public void setReference_name2(String reference_name2) {
		this.reference_name2 = reference_name2;
	}

	public void setReference_section(String reference_section) {
		this.reference_section = reference_section;
	}

	public void setReference_section2(String reference_section2) {
		this.reference_section2 = reference_section2;
	}

	public void setReference_subsection(String reference_subsection) {
		this.reference_subsection = reference_subsection;
	}

	public void setReference_subsection2(String reference_subsection2) {
		this.reference_subsection2 = reference_subsection2;
	}

	public void setReference_paragraph(String reference_paragraph) {
		this.reference_paragraph = reference_paragraph;
	}

	public void setReference_paragraph2(String reference_paragraph2) {
		this.reference_paragraph2 = reference_paragraph2;
	}

	public void setReference_subparagraph(String reference_subparagraph) {
		this.reference_subparagraph = reference_subparagraph;
	}

	public void setReference_subparagraph2(String reference_subparagraph2) {
		this.reference_subparagraph2 = reference_subparagraph2;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public Long getModalId() {
		return modalId;
	}

	public void setModalId(Long modalId) {
		this.modalId = modalId;
	}

	@Override
	public String toString() {
		return "tax_concept [rno=" + rno + ", prefix=" + prefix + ", name=" + name + ", occurence=" + occurence
				+ ", concept_id=" + concept_id + ", type=" + type + ", substitution_group=" + substitution_group
				+ ", enum_headUsable=" + enum_headUsable + ", enum_domain=" + enum_domain + ", enum_linkrole="
				+ enum_linkrole + ", typedDomainRef=" + typedDomainRef + ", balance=" + balance + ", period_type="
				+ period_type + ", abstract_col=" + abstract_col + ", nillable=" + nillable + ", label_en=" + label_en
				+ ", label_ms=" + label_ms + ", reference_name=" + reference_name + ", reference_name2="
				+ reference_name2 + ", reference_section=" + reference_section + ", reference_section2="
				+ reference_section2 + ", reference_subsection=" + reference_subsection + ", reference_subsection2="
				+ reference_subsection2 + ", reference_paragraph=" + reference_paragraph + ", reference_paragraph2="
				+ reference_paragraph2 + ", reference_subparagraph=" + reference_subparagraph
				+ ", reference_subparagraph2=" + reference_subparagraph2 + ", remarks=" + remarks + "]";
	}
	
}

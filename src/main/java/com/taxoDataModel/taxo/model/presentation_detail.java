package com.taxoDataModel.taxo.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "presentation_detail")
public class presentation_detail {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	
//	@Column(name = "linkbase_form_id")
//	private long linkbase_form_id;
	
	@Column(name = "prefix")
	private String prefix;
	
	@Column(name = "name", length = 5000)
	private String name;
	
	@Column(name = "label_en", length = 5000)
	private String label_en;
	
	@Column(name = "label_ms", length = 5000)
	private String label_ms;
	
	@Column(name = "label_en_form", length = 5000)
	private String label_en_form;

	@Column(name = "label_ms_form", length = 5000)
	private String label_ms_form;
	
	@Column(name = "label_en_document", length = 5000)
	private String label_en_document;
	
	@Column(name = "label_ms_document", length = 5000)
	private String label_ms_document;
	
	@Column(name = "data_item", length = 5000)
	private String dataItem;
	
	@Column(name = "test_expression")
	private String test_expression;
	
	@Column(name = "error_message_en")
	private String error_message_en;
	
	@Column(name = "error_message_ms")
	private String error_message_ms;
	
//	@Column(name = "parent_id")
//	private Long parentId;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="parent_id", insertable = false, updatable = false)
    private presentation_detail parent;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "parent_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<presentation_detail> childrens = new ArrayList<presentation_detail>();
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "parent")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<presentation_detail> childList = new ArrayList<presentation_detail>();
    
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "linkbase_form_id", insertable = false, updatable = false)
//    @JsonIgnore
//    private linkbase_form form;

	public presentation_detail() {
	}

	public presentation_detail(long id, long linkbase_form_id, String prefix, String name, String label_en,
			String label_ms, String label_en_form, String label_ms_form, String label_en_document,
			String label_ms_document, String data_item) {
		this.id = id;
//		this.linkbase_form_id = linkbase_form_id;
		this.prefix = prefix;
		this.name = name;
		this.label_en = label_en;
		this.label_ms = label_ms;
		this.label_en_form = label_en_form;
		this.label_ms_form = label_ms_form;
		this.label_en_document = label_en_document;
		this.label_ms_document = label_ms_document;
		this.dataItem = data_item;
	}

	public long getId() {
		return id;
	}

//	public long getLinkbase_form_id() {
//		return linkbase_form_id;
//	}

	public String getPrefix() {
		return prefix;
	}

	public String getName() {
		return name;
	}

	public String getLabel_en() {
		return label_en;
	}

	public String getLabel_ms() {
		return label_ms;
	}

	public String getLabel_en_form() {
		return label_en_form;
	}

	public String getLabel_ms_form() {
		return label_ms_form;
	}

	public String getLabel_en_document() {
		return label_en_document;
	}

	public String getLabel_ms_document() {
		return label_ms_document;
	}

	public void setId(long id) {
		this.id = id;
	}

//	public void setLinkbase_form_id(long linkbase_form_id) {
//		this.linkbase_form_id = linkbase_form_id;
//	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLabel_en(String label_en) {
		this.label_en = label_en;
	}

	public void setLabel_ms(String label_ms) {
		this.label_ms = label_ms;
	}

	public void setLabel_en_form(String label_en_form) {
		this.label_en_form = label_en_form;
	}

	public void setLabel_ms_form(String label_ms_form) {
		this.label_ms_form = label_ms_form;
	}

	public void setLabel_en_document(String label_en_document) {
		this.label_en_document = label_en_document;
	}

	public void setLabel_ms_document(String label_ms_document) {
		this.label_ms_document = label_ms_document;
	}
	
	@JsonIgnore
	public presentation_detail getParent() {
		return parent;
	}

	public void setParent(presentation_detail parent) {
		this.parent = parent;
	}

	public void setChildren(List<presentation_detail> childrens) {
		this.childrens = childrens;
	}
	
	@JsonIgnore
    public List<presentation_detail> getChildren() {
        return childrens;
    }
    
    public List<presentation_detail> getChildList() {
        return childList;
    }
    
    public void addChild(presentation_detail children) {
        this.childrens.add(children);
    }

	public String getDataItem() {
		return dataItem;
	}

	public void setDataItem(String data_item) {
		this.dataItem = data_item;
	}

	public String getTest_expression() {
		return test_expression;
	}

	public void setTest_expression(String test_expression) {
		this.test_expression = test_expression;
	}

	public String getError_message_en() {
		return error_message_en;
	}

	public void setError_message_en(String error_message_en) {
		this.error_message_en = error_message_en;
	}

	public String getError_message_ms() {
		return error_message_ms;
	}

	public void setError_message_ms(String error_message_ms) {
		this.error_message_ms = error_message_ms;
	}

//	public linkbase_form getForm() {
//		return form;
//	}
//
//	public void setForm(linkbase_form form) {
//		this.form = form;
//	}
    
	
}

package com.taxoDataModel.taxo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "presentation_formula")
public class presentation_formula {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	
//	@Column(name = "linkbase_form_id")
//	private long linkbase_form_id;
	
	@Column(name = "data_item", length = 5000)
	private String dataItem;
	
	@Column(name = "field_name", length = 5000)
	private String fieldName;
	
	@Column(name = "value_assertion", length = 5000)
	private String value_assertion;
	
	@Column(name = "test_expression", length = 5000)
	private String test_expression;
	
	@Column(name = "test_item", length = 5000)
	private String testItem;
	
	@Column(name = "error_message_en", length = 5000)
	private String error_message_en;
	
	@Column(name = "error_message_ms", length = 5000)
	private String error_message_ms;
	
	@Column(name = "value_assertion_label", length = 5000)
	private String value_assertion_label;

	public presentation_formula() {
	}

	public presentation_formula(String data_item, String fieldName, String value_assertion, String test_expression,
			String error_message_en, String error_message_ms, String testItem, String label) {
		super();
		this.dataItem = data_item;
		this.fieldName = fieldName;
		this.value_assertion = value_assertion;
		this.test_expression = test_expression;
		this.error_message_en = error_message_en;
		this.error_message_ms = error_message_ms;
		this.testItem = testItem;
		this.value_assertion_label = label;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDataItem() {
		return dataItem;
	}

	public void setDataItem(String data_item) {
		this.dataItem = data_item;
	}

	public String getValue_assertion() {
		return value_assertion;
	}

	public void setValue_assertion(String value_assertion) {
		this.value_assertion = value_assertion;
	}

	public String getTest_expression() {
		return test_expression;
	}

	public void setTest_expression(String test_expression) {
		this.test_expression = test_expression;
	}

	public String getError_message_en() {
		return error_message_en;
	}

	public void setError_message_en(String error_message_en) {
		this.error_message_en = error_message_en;
	}

	public String getError_message_ms() {
		return error_message_ms;
	}

	public void setError_message_ms(String error_message_ms) {
		this.error_message_ms = error_message_ms;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getTestItem() {
		return testItem;
	}

	public void setTestItem(String testItem) {
		this.testItem = testItem;
	}

	public String getValue_assertion_label() {
		return value_assertion_label;
	}

	public void setValue_assertion_label(String value_assertion_label) {
		this.value_assertion_label = value_assertion_label;
	}
	
}

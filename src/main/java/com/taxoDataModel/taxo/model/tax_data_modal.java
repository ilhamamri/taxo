package com.taxoDataModel.taxo.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@Entity
@Table(name = "tax_data_modal")
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class tax_data_modal {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	
	@Column(name = "filename", length = 5000)
	private String filename;
	
	@Column(name = "upload_date")
	private Date uploadDate;
	
	@Column(name = "size")
	private double size;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "tax_data_modal_id")
	private List<tax_form> taxFormList = new ArrayList<tax_form>();

	public tax_data_modal() {
	}

	public tax_data_modal(String filename, Date uploadDate) {
		this.filename = filename;
		this.uploadDate = uploadDate;
	}

	public long getId() {
		return id;
	}

	public String getFilename() {
		return filename;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public List<tax_form> getTaxFormList() {
		return taxFormList;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public void setTaxFormList(List<tax_form> taxFormList) {
		this.taxFormList = taxFormList;
	}
	
	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	@JsonIgnore
    public List<tax_form> getChildren() {
        return taxFormList;
    }

}

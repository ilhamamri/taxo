package com.taxoDataModel.taxo.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@Entity
@Table(name = "linkbase_form")
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class linkbase_form {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "frm_id", unique = true, nullable = false)
	private long frm_id;
	
	@Column(name = "namespace", length = 5000)
	private String namespace;

	@Column(name = "linkrole", length = 5000)
	private String linkrole;
	
	@Column(name = "definition", length = 5000)
	private String definition;
	
	@Column(name = "definition_ms", length = 5000)
	private String definition_ms;
	
	@Column(name = "name", length = 5000)
	private String name;
	
	@Column(name = "modal_id")
	private Long modalId;
	
	@Column(name = "category", length = 5000)
	private String category;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "linkbase_form_id")
    private List<presentation_detail> presentationList = new ArrayList<presentation_detail>();
    
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "linkbase_form_id")
    private List<presentation_formula> formulaList = new ArrayList<presentation_formula>();
    
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "linkbase_form_id")
	private List<tax_form_linkbase> linkbaseList = new ArrayList<tax_form_linkbase>();

	public linkbase_form() {
	}

	public linkbase_form(long frm_id, String namespace, String linkrole, String definition, String definition_ms, String name, long modal_id, String category) {
		this.frm_id = frm_id;
		this.namespace = namespace;
		this.linkrole = linkrole;
		this.definition = definition;
		this.definition_ms = definition_ms;
		this.name = name;
		this.modalId = modal_id;
		this.category = category;
	}

	public long getFrm_id() {
		return frm_id;
	}

	public String getNamespace() {
		return namespace;
	}

	public String getLinkrole() {
		return linkrole;
	}

	public String getDefinition() {
		return definition;
	}

	public String getName() {
		return name;
	}
	
	@JsonIgnore
    public List<presentation_detail> getChildren() {
        return presentationList;
    }
    
    @JsonIgnore
    public List<presentation_formula> getFormulaChildren() {
        return formulaList;
    }

	public void setFrm_id(long frm_id) {
		this.frm_id = frm_id;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public void setLinkrole(String linkrole) {
		this.linkrole = linkrole;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<presentation_detail> getPresentationList() {
		return presentationList;
	}

	public void setPresentationList(List<presentation_detail> presentationList) {
		this.presentationList = presentationList;
	}
	
	public List<tax_form_linkbase> getLinkbaseList() {
		return linkbaseList;
	}

	public void setLinkbaseList(List<tax_form_linkbase> linkbaseList) {
		this.linkbaseList = linkbaseList;
	}

	public String getDefinition_ms() {
		return definition_ms;
	}

	public void setDefinition_ms(String definition_ms) {
		this.definition_ms = definition_ms;
	}

	public List<presentation_formula> getFormulaList() {
		return formulaList;
	}

	public void setFormulaList(List<presentation_formula> formulaList) {
		this.formulaList = formulaList;
	}

	public Long getModal_id() {
		return modalId;
	}

	public void setModal_id(Long modalId) {
		this.modalId = modalId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}	
	
}

package com.taxoDataModel.taxo.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "tax_form")
public class tax_form {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	
	@Column(name = "template_name", length = 5000)
	private String templateName;
	
	@Column(name = "form_name", length = 5000)
	private String formName;
	
	@Column(name = "name", length = 5000)
	private String name;
	
	@Column(name = "column_index")
	private long columnIndex;
	
	@Column(name = "linkrole", length = 5000)
	private String linkRole;
	
	@Column(name = "definition", length = 5000)
	private String definition;
	
	@Column(name = "form_type", length = 5000)
	private String formType;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "tax_form_id")
	private List<tax_form_linkbase> linkbaseList = new ArrayList<tax_form_linkbase>();
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "form_id")
    private List<tax_form_header> formHeaderList = new ArrayList<tax_form_header>();

	public tax_form() {
	}
	
	public tax_form(String templateName, String formName, String name, long columnIndex, String linkRole, String definition, String formType) {
		this.templateName = templateName;
		this.formName = formName;
		this.name = name;
		this.columnIndex = columnIndex;
		this.linkRole = linkRole;
		this.definition = definition;
		this.formType = formType;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getColumnIndex() {
		return columnIndex;
	}

	public void setColumnIndex(long columnIndex) {
		this.columnIndex = columnIndex;
	}

	public List<tax_form_linkbase> getLinkbaseList() {
		return linkbaseList;
	}

	public void setLinkbaseList(List<tax_form_linkbase> linkbaseList) {
		this.linkbaseList = linkbaseList;
	}

	public String getTemplateName() {
		return templateName;
	}

	public String getLinkRole() {
		return linkRole;
	}

	public String getDefinition() {
		return definition;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public void setLinkRole(String linkRole) {
		this.linkRole = linkRole;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public List<tax_form_header> getFormHeaderList() {
		return formHeaderList;
	}

	public void setFormHeaderList(List<tax_form_header> formHeaderList) {
		this.formHeaderList = formHeaderList;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}
	
	@JsonIgnore
    public List<tax_form_header> getChildren() {
        return formHeaderList;
    }

	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}    
	
}

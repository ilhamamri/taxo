package com.taxoDataModel.taxo.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.taxoDataModel.taxo.model.linkbase_form;
import com.taxoDataModel.taxo.model.tax_concept;
import com.taxoDataModel.taxo.model.tax_data_modal;
import com.taxoDataModel.taxo.model.tax_enum;
import com.taxoDataModel.taxo.model.tax_form;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class responsePojo {
	private String message;
	private boolean success;
	private tax_data_modal tdm;
	private List<tax_concept> taxConceptList;
	private List<tax_enum> taxEnumList;
	private linkbase_form form;
	private List<tax_form> tformList;
	
	public responsePojo() {
	}
	
	public responsePojo(String message, boolean success, List<tax_concept> taxConceptList, List<tax_enum> taxEnumList) {
		this.message = message;
		this.success = success;
		this.taxConceptList = taxConceptList;
		this.taxEnumList = taxEnumList;
	}

	public String getMessage() {
		return message;
	}

	public List<tax_concept> getTaxConceptList() {
		return taxConceptList;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setTaxConceptList(List<tax_concept> taxConceptList) {
		this.taxConceptList = taxConceptList;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public List<tax_enum> getTaxEnumList() {
		return taxEnumList;
	}

	public void setTaxEnumList(List<tax_enum> taxEnumList) {
		this.taxEnumList = taxEnumList;
	}

	public linkbase_form getForm() {
		return form;
	}

	public void setForm(linkbase_form form) {
		this.form = form;
	}

	public List<tax_form> getTformList() {
		return tformList;
	}

	public void setTformList(List<tax_form> tformList) {
		this.tformList = tformList;
	}

	public tax_data_modal getTdm() {
		return tdm;
	}

	public void setTdm(tax_data_modal tdm) {
		this.tdm = tdm;
	}
	

}

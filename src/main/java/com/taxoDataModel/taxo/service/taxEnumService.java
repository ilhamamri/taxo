package com.taxoDataModel.taxo.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import com.taxoDataModel.taxo.model.presentation_detail;
import com.taxoDataModel.taxo.model.tax_concept;
import com.taxoDataModel.taxo.model.tax_enum;
import com.taxoDataModel.taxo.repository.tax_enumRepository;

@Service
public class taxEnumService {
	
	@Autowired
	tax_enumRepository repository;
	
	@Autowired
	taxConceptService tcService;
	
	public static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd"); 
	public static final String SAMPLE_TAXO_FOLDER = "./src/main/output";
	
	public void save(List<tax_enum> tList) {
		try {
			repository.saveAll(tList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<tax_enum> getAll(){
		return repository.findAll();
	}
	
	public List<tax_enum> getAllParent(){
		return repository.findByParentIdIsNull();
	}
	
	public List<tax_enum> getAllByModalId(Long id){
		return repository.findByModalId(id);
	}
	
	public List<tax_enum> findByParentIdAndModalId(Long pid, Long mid){
		return repository.findByParentIdAndModalId(mid);
	}
	
	public tax_enum findByName(String name){
		tax_enum tList = new tax_enum();
		try {
			tList = repository.findTopByName(name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tList;
	}
	
	public tax_enum tableMapping(tax_enum enumObj, int index, String obj) {
		tax_enum tEnum = enumObj;
		
		switch (index) {
		case 0:
			tEnum.setPrefix(obj);
			break;
		case 1:
			tEnum.setName(obj);
			break;
		case 2:
			tEnum.setLabel_en(obj);
			break;
		case 3:
			tEnum.setLabel_ms(obj);
			break;
		case 4:
			tEnum.setUsable(obj);
			break;
		default:
		}
		
		return tEnum;
	}
	
	public boolean writeTaxEnum(String path, String dtString, Long mid) {
		boolean isCompleted = false;
		LocalDateTime now = LocalDateTime.now();
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/lhdnm-enum_" + dtString + ".xsd"))) {
			
			List<tax_enum> enumList = getAllByModalId(mid);
//			List<tax_concept> tcList1 = tcService.getAllByPrefix("lhdnm-enum");
			List<tax_concept> tcList1 = tcService.getAllByModalId(mid);
			
		    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		    writer.newLine();
		    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
		    writer.newLine();
		    writer.write("<xsd:schema targetNamespace=\"http://www.hasil.gov.my/xbrl/" +dtString+ "/enum\" attributeFormDefault=\"unqualified\" elementFormDefault=\"qualified\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" +dtString+ "/enum\" xmlns:enum=\"http://xbrl.org/2014/extensible-enumerations\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
		    writer.newLine();
		    writer.write("  <xsd:annotation>");
		    writer.newLine();
		    writer.write("    <xsd:appinfo>");
		    writer.newLine();
		    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"gla_lhdnm-enum-en_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    writer.newLine();
		    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"gla_lhdnm-enum-ms_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    writer.newLine();
		    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"def_lhdnm-enum_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/definitionLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    writer.newLine();
		    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-enum-en_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    writer.newLine();
		    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-enum-ms_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    
		    ArrayList<String> tempArray = new ArrayList<>();
		    for(tax_enum te : enumList) {
		    	
		    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(te.getEnum_id())).findAny().orElse(null) == null) {
		    		tempArray.add(te.getEnum_id());
		    		writer.newLine();
			    	writer.write("      <link:roleType roleURI=\"" + te.getLinkrole() + "\" id=\"" + te.getEnum_id() + "\">");
			    	writer.newLine();
			    	writer.write("        <link:definition>" + te.getDefinition_en() + "</link:definition>");
			    	writer.newLine();
			    	writer.write("        <link:usedOn>link:definitionLink</link:usedOn>");
			    	writer.newLine();
			    	writer.write("      </link:roleType>");
		    	}
		    	
		    }
		    
		    writer.write("    </xsd:appinfo>");
		    writer.newLine();
		    writer.write("  </xsd:annotation>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://www.xbrl.org/2003/instance\" schemaLocation=\"http://www.xbrl.org/2003/xbrl-instance-2003-12-31.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://xbrl.org/2014/extensible-enumerations\" schemaLocation=\"http://www.xbrl.org/2014/extensible-enumerations.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://www.xbrl.org/dtr/type/non-numeric\" schemaLocation=\"http://www.xbrl.org/dtr/type/nonNumeric-2009-12-16.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://xbrl.org/2005/xbrldt\" schemaLocation=\"http://www.xbrl.org/2005/xbrldt-2005.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/label\" schemaLocation=\"http://www.xbrl.org/2008/generic-label.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/reference\" schemaLocation=\"http://www.xbrl.org/2008/generic-reference.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/generic\" schemaLocation=\"http://www.xbrl.org/2008/generic-link.xsd\"/>");

		    List<tax_concept> tcList2 = tcList1.stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
		    List<tax_concept> tcList = tcList2.stream().filter(lform -> lform.getPrefix().equalsIgnoreCase("lhdnm-enum")).collect(Collectors.toList());
		    for(tax_concept tc : tcList) {
		    	String id = tc.getPrefix() + "_" + tc.getName();
		    	boolean headUsable = false;
//		    	if(tc.getEnum_headUsable() != null) {
//		    		headUsable = true;
//		    	}
		    	writer.newLine();
		    	writer.write("  <xsd:element name=\"" + tc.getName() + "\" id=\"" + id + "\" type=\"" + tc.getType() + "\" enum:headUsable=\"" + headUsable + "\" substitutionGroup=\"" + tc.getSubstitution_group() + "\" enum:linkrole=\"" + tc.getEnum_linkrole() + "\" enum:domain=\"" + tc.getEnum_domain() + "\" abstract=\"" + tc.isAbstract_col() + "\" nillable=\"" + tc.isNillable() + "\" xbrli:periodType=\"" + tc.getPeriod_type() + "\"/>");
		    }
		    
		    for(tax_enum te : enumList) {
		    	String id = te.getPrefix() + "_" + te.getName();
		    	writer.newLine();
		    	writer.write("  <xsd:element name=\"" + te.getName() + "\" id=\"" + id + "\" type=\"nonnum:domainItemType\" substitutionGroup=\"xbrli:item\" abstract=\"false\" nillable=\"true\" xbrli:periodType=\"duration\"/>");
		    	for(tax_enum child : te.getChildList()) {
		    		String id2 = child.getPrefix() + "_" + child.getName();
		    		writer.newLine();
			    	writer.write("  <xsd:element name=\"" + child.getName() + "\" id=\"" + id2 + "\" type=\"nonnum:domainItemType\" substitutionGroup=\"xbrli:item\" abstract=\"false\" nillable=\"true\" xbrli:periodType=\"duration\"/>");
		    	}
		    }
		    
		    writer.newLine();
		    writer.write("</xsd:schema>");
		    
		    isCompleted = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		return isCompleted;
	}

	public boolean writeTaxEnumDef(String path, String dtString, Long mid) {
		boolean isCompleted = false;
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/def_lhdnm-enum_" + dtString + ".xml"))) {
			
//			List<tax_enum> tempList = getAllByModalId(mid);
//			List<tax_enum> enumList = tempList.stream().filter(f -> f.getParentId() == null).collect(Collectors.toList());
			List<tax_enum> enumList = findByParentIdAndModalId(0L, mid);
			List<tax_concept> tcList = tcService.getAllByPrefix("lhdnm-enum");
			
		    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		    writer.newLine();
		    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
		    writer.newLine();
		    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\">");
		    
		    for(tax_enum te : enumList) {
		    	String href = "lhdnm-enum_" + dtString + ".xsd#" + te.getEnum_id();
		    	writer.newLine();
		    	writer.write("  <link:roleRef roleURI=\"" + te.getLinkrole() + "\" xlink:type=\"simple\" xlink:href=\"" + href + "\"/>");
		    }
		    
		    for(tax_enum te : enumList) {
		    	writer.newLine();
		    	String href = "lhdnm-enum_" + dtString + ".xsd#" + te.getPrefix() + "_" + te.getName();
		    	writer.newLine();
		    	writer.write("  <link:definitionLink xlink:type=\"extended\" xlink:role=\"" + te.getLinkrole() + "\">");
		    	writer.newLine();
		    	writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + href + "\" xlink:label=\"" + te.getName() + "\" xlink:title=\"" + te.getName() + "\"/>");
		    	
		    	int order = 0;
		    	for(tax_enum child : te.getChildList()) {
		    		order++;
		    		String href2 = "lhdnm-enum_" + dtString + ".xsd#" + child.getPrefix() + "_" + child.getName();
		    		writer.newLine();
			    	writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + href2 + "\" xlink:label=\"" + child.getName() + "\" xlink:title=\"" + child.getName() + "\"/>");
			    	writer.newLine();
			    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/int/dim/arcrole/domain-member\" xlink:from=\"" + te.getName() + "\" xlink:to=\"" + child.getName() + "\" xlink:title=\"definition: " + te.getName() + " to " + child.getName() + "\" priority=\"0\" order=\"" + order + ".0\"/>");
		    	}
		    	
		    	writer.newLine();
		    	writer.write("  </link:definitionLink>");
		    }
		    
		    writer.newLine();
	    	writer.write("</link:linkbase>");
		    
			isCompleted = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		return isCompleted;
	}
	
	public boolean writeTaxEnumLabEn(String path, String dtString, Long mid) {
		boolean isCompleted = false;
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/lab_lhdnm-enum-en_" + dtString + ".xml"))) {
			
			List<tax_enum> enumList = getAllByModalId(mid);
			List<tax_concept> tcList1 = tcService.getAllByModalId(mid);
			
		    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		    writer.newLine();
		    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
		    writer.newLine();
		    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:enum=\"http://xbrl.org/2014/extensible-enumerations\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:reference=\"http://xbrl.org/2008/reference\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
		    writer.newLine();
		    writer.write("  <link:labelLink xlink:type=\"extended\" xlink:role=\"http://www.xbrl.org/2003/role/link\">");
		    
		    List<tax_concept> tcList2 = tcList1.stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
		    List<tax_concept> tcList = tcList2.stream().filter(lform -> lform.getPrefix().equalsIgnoreCase("lhdnm-enum")).collect(Collectors.toList());
		    for(tax_concept tc : tcList) {
		    	String id = tc.getPrefix() + "_" + tc.getName();
		    	String href = "lhdnm-enum_" + dtString + ".xsd#" + id;
		    	String labelName = "label_" + tc.getName();

		    	writer.newLine();
	    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\""+ href +"\" xlink:label=\"" + tc.getName() + "\" xlink:title=\"" + tc.getName() + "\"/>");
	    		writer.newLine();
	    		writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"en\" id=\"" + labelName + "\">" + tc.getLabel_en() + "</link:label>");
	    		writer.newLine();
	    		writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + tc.getName() + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + tc.getName() + " to " + labelName + "\"/>");
		    }
		    
		    for(tax_enum te : enumList) {
		    	String id = te.getPrefix() + "_" + te.getName();
		    	String href = "lhdnm-enum_" + dtString + ".xsd#" + id;
		    	String labelName = "label_" + te.getName();

		    	writer.newLine();
	    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\""+ href +"\" xlink:label=\"" + te.getName() + "\" xlink:title=\"" + te.getName() + "\"/>");
	    		writer.newLine();
	    		writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"en\" id=\"" + labelName + "\">" + te.getLabel_en() + "</link:label>");
	    		writer.newLine();
	    		writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + te.getName() + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + te.getName() + " to " + labelName + "\"/>");
		    }
		    
		    writer.newLine();
		    writer.write("  </link:labelLink>");
		    writer.newLine();
		    writer.write("</link:linkbase>");
		    
			isCompleted = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		return isCompleted;
	}
	
	public boolean writeTaxEnumLabMs(String path, String dtString, Long mid) {
		boolean isCompleted = false;
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/lab_lhdnm-enum-ms_" + dtString + ".xml"))) {
			
			List<tax_enum> enumList = getAllByModalId(mid);
			List<tax_concept> tcList1 = tcService.getAllByModalId(mid);
			
		    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		    writer.newLine();
		    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
		    writer.newLine();
		    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:enum=\"http://xbrl.org/2014/extensible-enumerations\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:reference=\"http://xbrl.org/2008/reference\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
		    writer.newLine();
		    writer.write("  <link:labelLink xlink:type=\"extended\" xlink:role=\"http://www.xbrl.org/2003/role/link\">");
		    
		    List<tax_concept> tcList2 = tcList1.stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
		    List<tax_concept> tcList = tcList2.stream().filter(lform -> lform.getPrefix().equalsIgnoreCase("lhdnm-enum")).collect(Collectors.toList());
		    for(tax_concept tc : tcList) {
		    	String id = tc.getPrefix() + "_" + tc.getName();
		    	String href = "lhdnm-enum_" + dtString + ".xsd#" + id;
		    	String labelName = "label_" + tc.getName();

		    	writer.newLine();
	    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\""+ href +"\" xlink:label=\"" + tc.getName() + "\" xlink:title=\"" + tc.getName() + "\"/>");
	    		writer.newLine();
	    		writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"en\" id=\"" + labelName + "\">" + tc.getLabel_ms() + "</link:label>");
	    		writer.newLine();
	    		writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + tc.getName() + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + tc.getName() + " to " + labelName + "\"/>");
		    }
		    
		    for(tax_enum te : enumList) {
		    	String id = te.getPrefix() + "_" + te.getName();
		    	String href = "lhdnm-enum_" + dtString + ".xsd#" + id;
		    	String labelName = "label_" + te.getName();

		    	writer.newLine();
	    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\""+ href +"\" xlink:label=\"" + te.getName() + "\" xlink:title=\"" + te.getName() + "\"/>");
	    		writer.newLine();
	    		writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"en\" id=\"" + labelName + "\">" + te.getLabel_ms() + "</link:label>");
	    		writer.newLine();
	    		writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + te.getName() + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + te.getName() + " to " + labelName + "\"/>");
		    }
		    
		    writer.newLine();
		    writer.write("  </link:labelLink>");
		    writer.newLine();
		    writer.write("</link:linkbase>");
		    
			isCompleted = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		return isCompleted;
	}
	
	public boolean writeTaxEnumGlaEn(String path, String dtString, Long mid) {
		boolean isCompleted = false;
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/gla_lhdnm-enum-en_" + dtString + ".xml"))) {
			
			List<tax_enum> enumList = getAllByModalId(mid);
			List<tax_concept> tcList = tcService.getAllByPrefix("lhdnm-enum");
			
		    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		    writer.newLine();
		    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
		    writer.newLine();
		    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-" + dtString + ".xsd\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\">");
		    writer.newLine();
		    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#standard-label\"/>");
		    writer.newLine();
		    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/link\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-link.xsd#standard-link-role\"/>");
		    writer.newLine();
		    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/element-label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#element-label\"/>");
		    writer.newLine();
		    writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"http://www.xbrl.org/2008/role/link\">");
		    int count = 1;
		    ArrayList<String> tempArray = new ArrayList<>();
		    for(tax_enum te : enumList) {
		    	
		    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(te.getEnum_id())).findAny().orElse(null) == null) {
		    		tempArray.add(te.getEnum_id());
		    		writer.newLine();
			    	writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"lhdnm-enum_" + dtString + ".xsd#" + te.getEnum_id() + "\" xlink:label=\"roleType_" + count + "\" xlink:title=\"roleType\"/>");
			    	writer.newLine();
			    	writer.write("    <label:label xlink:type=\"resource\" xlink:label=\"label_" + count + "\" xlink:role=\"http://www.xbrl.org/2008/role/label\" xlink:title=\"label\" xml:lang=\"en\">" + te.getDefinition_en() + "</label:label>");
			    	writer.newLine();
			    	writer.write("    <gen:arc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/element-label\" xlink:from=\"roleType_" + count + "\" xlink:to=\"label_" + count + "\" xlink:title=\"user-defined: roleType to label\" order=\"1.0\"/>");
			    	count++;
		    	}
		    	
		    }
		    
		    writer.write("  </gen:link>");
		    writer.newLine();
		    writer.write("</link:linkbase>");
		    
		    isCompleted = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		return isCompleted;
	}
	
	public boolean writeTaxEnumGlaMs(String path, String dtString, Long mid) {
		boolean isCompleted = false;
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/gla_lhdnm-enum-ms_" + dtString + ".xml"))) {
			
			List<tax_enum> enumList = getAllByModalId(mid);
			List<tax_concept> tcList = tcService.getAllByPrefix("lhdnm-enum");
			
		    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		    writer.newLine();
		    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
		    writer.newLine();
		    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-" + dtString + ".xsd\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\">");
		    writer.newLine();
		    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#standard-label\"/>");
		    writer.newLine();
		    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/link\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-link.xsd#standard-link-role\"/>");
		    writer.newLine();
		    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/element-label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#element-label\"/>");
		    writer.newLine();
		    writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"http://www.xbrl.org/2008/role/link\">");
		    int count = 1;
		    ArrayList<String> tempArray = new ArrayList<>();
		    for(tax_enum te : enumList) {
		    	
		    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(te.getEnum_id())).findAny().orElse(null) == null) {
		    		tempArray.add(te.getEnum_id());
		    		writer.newLine();
			    	writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"lhdnm-enum_" + dtString + ".xsd#" + te.getEnum_id() + "\" xlink:label=\"roleType_" + count + "\" xlink:title=\"roleType\"/>");
			    	writer.newLine();
			    	writer.write("    <label:label xlink:type=\"resource\" xlink:label=\"label_" + count + "\" xlink:role=\"http://www.xbrl.org/2008/role/label\" xlink:title=\"label\" xml:lang=\"en\">" + te.getDefiniton_ms() + "</label:label>");
			    	writer.newLine();
			    	writer.write("    <gen:arc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/element-label\" xlink:from=\"roleType_" + count + "\" xlink:to=\"label_" + count + "\" xlink:title=\"user-defined: roleType to label\" order=\"1.0\"/>");
			    	count++;
		    	}
		    	
		    }
		    
		    writer.write("  </gen:link>");
		    writer.newLine();
		    writer.write("</link:linkbase>");
		    
		    isCompleted = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		return isCompleted;
	}
	
}

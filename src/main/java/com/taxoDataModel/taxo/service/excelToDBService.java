package com.taxoDataModel.taxo.service;

import java.io.*;
import java.nio.file.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.logging.log4j.util.Strings;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ser.impl.StringArraySerializer;
import com.taxoDataModel.taxo.model.linkbase_form;
import com.taxoDataModel.taxo.model.presentation_detail;
import com.taxoDataModel.taxo.model.presentation_formula;
import com.taxoDataModel.taxo.model.tax_concept;
import com.taxoDataModel.taxo.model.tax_data_modal;
import com.taxoDataModel.taxo.model.tax_enum;
import com.taxoDataModel.taxo.model.tax_form;
import com.taxoDataModel.taxo.model.tax_form_header;
import com.taxoDataModel.taxo.model.tax_form_linkbase;
import com.taxoDataModel.taxo.model.tax_sme_concept;
import com.taxoDataModel.taxo.model.tax_sme_enum;
import com.taxoDataModel.taxo.repository.tax_conceptRepository;

@Service
public class excelToDBService {

	public static final String SAMPLE_XLS_FILE_PATH = "./sample-xls-file.xls";
	public static final String SAMPLE_XLSX_FILE_PATH = "./LHDNMDataModel29072021.xlsx";
	public static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
	public static final String SAMPLE_MODEL_FOLDER = "./src/main/input";
	public static final String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	

	@Autowired
	private tax_conceptRepository repository;
	
	@Autowired
	taxDataModalService tdmService;
	
	@Autowired
	taxFormService fService;
	
	@Autowired
	taxFormHeaderService headerService;

	@Autowired
	taxConceptService tcService;
	
	@Autowired
	taxSmeConceptService tscService;

	@Autowired
	taxEnumService teService;
	
	@Autowired
	taxSmeEnumService tseService;

	@Autowired
	presentationDetailService pdService;

	@Autowired
	linkbaseFormService formService;
	
	@Autowired
	taxFormLinkbaseService flinkService;

	public boolean readExcel(Long id) {
		boolean isCompleted = false;
		List<tax_enum> enumList = new ArrayList<tax_enum>();
		linkbase_form form = new linkbase_form();
		try {
			tax_data_modal tdm = tdmService.getById(id);
			System.out.println("Return id: " + id);
			if(tdm != null) {
				// Creating a Workbook from an Excel file (.xls or .xlsx)
				
				File excelFile = new File(SAMPLE_MODEL_FOLDER + "/" + tdm.getFilename());
				Workbook workbook = WorkbookFactory.create(excelFile);

				// Retrieving the number of sheets in the Workbook
				System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");
				int totalSheet = workbook.getNumberOfSheets();

//		        System.out.println("Retrieving Sheets using for-each loop");
//		        for(Sheet sheet: workbook) {
//		            System.out.println("=> " + sheet.getSheetName());
//		            
//		        }
				
//				for(int i = 4; i < 109; i++) {
//					Sheet LinkbaseSheet = workbook.getSheetAt(i);
//					isCompleted = readLinkbaseForm(LinkbaseSheet);
//					if(!isCompleted) {
//						break;
//					}
//				}
				
				String fname = excelFile.getName();
				Sheet TaxOverviewSheet = workbook.getSheetAt(0);
				isCompleted = readTaxOverview(TaxOverviewSheet, id, "lhdnm");
				
				if(isCompleted) {
					Sheet TaxConceptSheet = workbook.getSheetAt(1);
					isCompleted = readTaxConcept(TaxConceptSheet, id);
				}

				if(isCompleted) {
					Sheet TaxEnumSheet = workbook.getSheetAt(2);
					isCompleted = readTaxEnum(TaxEnumSheet, id);
				}
								
				if(isCompleted) {
					Sheet TaxHeaderSheet = workbook.getSheetAt(3);
					isCompleted = readTaxHeader(TaxHeaderSheet, id, "lhdnm");
					
					if(isCompleted) {
						for(int i = 4; i < 114; i++) {
							Sheet LinkbaseSheet = workbook.getSheetAt(i);
							System.out.println("Sheet name ("+ i + ") : " + LinkbaseSheet.getSheetName());
							if(LinkbaseSheet.getSheetName().equalsIgnoreCase("HK-O") || LinkbaseSheet.getSheetName().equalsIgnoreCase("HK-J")) {
								//skip
							} else if(workbook.isSheetHidden(i) || workbook.isSheetVeryHidden(i)) {
								//skip
							} else {
								isCompleted = readLinkbaseForm(LinkbaseSheet, id, "lhdnm");
								if(!isCompleted) {
									break;
								}
							}
						}
					} else {
						return isCompleted;
					}
				} else {
					return isCompleted;
				}
				
				Sheet TaxOverviewSheet2 = workbook.getSheetAt(114);
				isCompleted = readTaxOverview(TaxOverviewSheet2, id, "lhdnm-sme");
				
				if(isCompleted) {
					Sheet TaxConceptSheet = workbook.getSheetAt(115);
					isCompleted = readTaxConceptSme(TaxConceptSheet, id);
				}

				if(isCompleted) {
					Sheet TaxEnumSheet = workbook.getSheetAt(116);
					isCompleted = readTaxEnumSme(TaxEnumSheet, id);
				}
				
				if(isCompleted) {
					Sheet TaxHeaderSheet = workbook.getSheetAt(117);
					isCompleted = readTaxHeader(TaxHeaderSheet, id, "lhdnm-sme");
					
					if(isCompleted) {
						for(int i = 118; i < totalSheet; i++) {
							Sheet LinkbaseSheet = workbook.getSheetAt(i);
							System.out.println("Sheet name ("+ i + ") : " + LinkbaseSheet.getSheetName());
							if(LinkbaseSheet.getSheetName().equalsIgnoreCase("HK-O") || LinkbaseSheet.getSheetName().equalsIgnoreCase("HK-J")) {
								//skip
							} else if(workbook.isSheetHidden(i) || workbook.isSheetVeryHidden(i)) {
								//skip
							} else {
								isCompleted = readLinkbaseForm(LinkbaseSheet, id, "lhdnm-sme");
								if(!isCompleted) {
									break;
								}
							}
						}
					} else {
						return isCompleted;
					}
				} else {
					return isCompleted;
				}
				
////				Sheet TaxEnumSheet = workbook.getSheetAt(4);
////				form = readLinkbaseForm(TaxEnumSheet);
			}
			
		} catch (Exception e) {
			isCompleted = false;
			throw new RuntimeException("fail to store excel data: " + e.getMessage());
		}
		return isCompleted;
	}

	// All proses for each sheet

	// for sheet no 1, tax overview
	public boolean readTaxOverview(Sheet TaxConceptSheet, Long id, String type) {
		boolean isCompleted = false;

		try {
			// Create a DataFormatter to format and get each cell's value as String
			LocalDateTime now = LocalDateTime.now(); 
			String dtString = dtf.format(now);
			Date currDate = new Date(System.currentTimeMillis());
			
			DataFormatter dataFormatter = new DataFormatter();
			List<tax_form> tList = new ArrayList<tax_form>();
			
//			tax_data_modal tdm = new tax_data_modal();
//			tdm.setFilename(fname);
//			tdm.setUploadDate(currDate);
//			tax_data_modal tdm = tdmService.getByFilename(fname);
			tax_data_modal tdm = tdmService.getById(id);
			
			if(tdm != null) {
				List<CellRangeAddress> cras = getCombineCell(TaxConceptSheet);
				int totalRow = TaxConceptSheet.getLastRowNum() + 1;
//				System.out.println(totalRow + " Total Row\t");
				// 2. Or you can use a for-each loop to iterate over the rows and columns
//				System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
				int countt = 0;
				int cntHeader = 0;
				int rowIndex = 0;
				int columnIndex = 0;
				
				for (int i = 0; i < totalRow; i++) {
					countt++;
					rowIndex = i;
					Row row = TaxConceptSheet.getRow(i);
//					System.out.println(i);
					linkbase_form link = new linkbase_form();
					
					if(row != null) {
						if(i == 0) {
							for (Cell cell : row) {
								tax_form form = new tax_form();
								form.setFormType(type);
								String cellValue = dataFormatter.formatCellValue(cell);
//								System.out.println("index: " + cell.getColumnIndex() + " | "  + cellValue + "\t");
								if(cellValue.equalsIgnoreCase("Template") || cellValue.equalsIgnoreCase("Working Sheet")) {
									
								}else if(cellValue.equalsIgnoreCase("Name")) {
									
								}else if(cellValue.equalsIgnoreCase("Name (en)")) {
									
								}else if(cellValue.equalsIgnoreCase("Name (ms)")) {
									
								}else {
									if(cellValue.contains("\n")) {
										String[] lines = cellValue.split("\n");
										if(lines.length > 0) {
											String str = lines[1];
											str = str.replaceAll(" & ", "/");
											form.setFormName(str);
										}
									} else {
										String rname = cellValue.replaceAll("entry", "");
										form.setFormName(rname);
									}
									form.setTemplateName(cellValue);
									form.setColumnIndex(cell.getColumnIndex());
//									fService.saveSingle(form);
									tList.add(form);
									
								}
								cntHeader++;
							}
							if(type.equalsIgnoreCase("lhdnm-sme")) {
								tdm.getChildren().addAll(tList);
							} else {
								tdm.getChildren().clear();
								tdm.getChildren().addAll(tList);
							}
							
							tdmService.saveSingle(tdm);
							
//							System.out.println(cntHeader + " header column\t");
						} else {
							int lastRow = 0;
							for (int x = 0; x < cntHeader; x++) {
								Cell cell = row.getCell(x);
								String val = "";
								if (cell != null) {
//									if(x == 0 || x == 1 || x == 2) {
									if(x == 0 || x == 1 ) {
										val = getCellValue(cell);
										link = formService.tableMapping(link, x, val);
										
										Hyperlink h = cell.getHyperlink();
										if(h != null) {
											String hl = h.getAddress();
//											System.out.println("link: " + hl);
											int count = 0;
											for(int c = 0; c < hl.length(); c++) {
											    if(hl.charAt(c) == '\'') {
											        count++;
											    }
											}
//											System.out.println("count: " + count);
											if(count == 0) {
												link.setNamespace(hl.substring(0, hl.indexOf("!")));
											} else {
												link.setNamespace(hl.substring(hl.indexOf("'") + 1, hl.indexOf("!") - 1));
											}
											
										} else if(x == 0) {
											link.setNamespace(val);
										}
										
									} else {
										val = getCellValue(cell);
										if(val.equalsIgnoreCase("Name (ms)")) {
											
										} else {
											if(link.getName() != null) {
												link.setModal_id(id);
												formService.saveSingle(link);
											}
//											System.out.println("id: " + link.getFrm_id());
											
											
											
											if(val.equalsIgnoreCase("x")) {
												tax_data_modal tdm2 = tdmService.getById(id);
												Long idx = (long) x;
												tax_form frm = tdm2.getTaxFormList().stream().filter(f -> f.getFormType().equalsIgnoreCase(type)).filter(f -> f.getColumnIndex() == idx).findAny().orElse(null);
//												tax_form frm = fService.getByColumnIndex((long) x);
												linkbase_form temp = formService.getByModalIdAndName(id, link.getName());
												
												tax_form_linkbase obj = new tax_form_linkbase();
												obj.setTax_form_id(frm.getId());
												obj.setLinkbase_form_id(temp.getFrm_id());
												flinkService.saveSingle(obj);
											}
										}
										
									}
								}
							}
						}
					}
				}
				isCompleted = true;
			} else {
				isCompleted = false;
			}
		} catch (Exception e) {
			isCompleted = false;
			e.printStackTrace();
			throw new RuntimeException("fail to store excel data: " + e.getMessage());
		}

		return isCompleted;
	}

	// for sheet no 2, tax concept
	public boolean readTaxConcept(Sheet TaxConceptSheet, Long id) {
		boolean isCompleted = false;

		try {
//			tax_data_modal tdm = tdmService.getByFilename(fname);
			// Create a DataFormatter to format and get each cell's value as String
			DataFormatter dataFormatter = new DataFormatter();
			List<tax_concept> tList = new ArrayList<tax_concept>();

			List<CellRangeAddress> cras = getCombineCell(TaxConceptSheet);
			int totalRow = TaxConceptSheet.getLastRowNum() + 1;
//			System.out.println(totalRow + " Total Row\t");
			// 2. Or you can use a for-each loop to iterate over the rows and columns
//			System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
			int countt = 0;
			int cntHeader = 0;
			int rowIndex = 0;
			int columnIndex = 0;
			for (int i = 0; i < totalRow; i++) {
				countt++;
				rowIndex = i;
				Row row = TaxConceptSheet.getRow(i);
//				System.out.println(i);
				tax_concept newTax = new tax_concept();

				if (countt == 1) {
					for (Cell cell : row) {
						String cellValue = dataFormatter.formatCellValue(cell);
//						System.out.print(cellValue + "\t");
						cntHeader++;
					}
//					System.out.println(cntHeader + " header column\t");
				} else {
					int lastRow = 0;
					for (int x = 0; x < cntHeader; x++) {
						Cell cell = row.getCell(x);
						String val = "";
						if (cell != null) {
							if (x == 17 || x == 18 || x == 19 || x == 20 || x == 21) {
								if (isMergedRegion(TaxConceptSheet, i, 0)) {
//                				System.out.println(x);
									lastRow = getRowNum(cras, TaxConceptSheet.getRow(i).getCell(0), TaxConceptSheet);
									int p = 0;
									for (p = i; p <= lastRow; p++) {
										Row row2 = TaxConceptSheet.getRow(p);
										cell = row2.getCell(x);
										val = "";
										if (cell != null) {
											val = getCellValue(cell);
											newTax = tcService.tableMapping(newTax, x, val);
										}
									}
									p--;
								} else {
									val = getCellValue(cell);
									newTax = tcService.tableMapping(newTax, x, val);
								}
							} else {
								val = getCellValue(cell);
								newTax = tcService.tableMapping(newTax, x, val);
							}
							newTax.setModalId(id);
							tList.add(newTax);
						} else {
//                		System.out.print("Empty cell \t");
						}
					}

					if (lastRow > i) {
						i = lastRow;
					}
				}

//            System.out.println(i);
//            if(countt == 1000) {
//            	break;
//            }
			}
			tcService.save(tList);
			isCompleted = true;

		} catch (Exception e) {
			isCompleted = false;
			e.printStackTrace();
			throw new RuntimeException("fail to store excel data: " + e.getMessage());
		}
		return isCompleted;
	}

	// for sheet no 3, tax enum
//	public boolean readTaxEnum(Sheet TaxEnumSheet) {
	public boolean readTaxEnum(Sheet TaxEnumSheet, Long id) {
		boolean isCompleted = false;
		List<tax_enum> enumList = new ArrayList<tax_enum>();

		try {
			DataFormatter dataFormatter = new DataFormatter();

			List<CellRangeAddress> cras = getCombineCell(TaxEnumSheet);
			int totalRow = TaxEnumSheet.getLastRowNum() + 1;
//			System.out.println(totalRow + " Total Row\t");
			// 2. Or you can use a for-each loop to iterate over the rows and columns
//			System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
			int countt = 0;
			int cntHeader = 5;
			int rowIndex = 0;
			int columnIndex = 0;
			tax_enum taxEnum = new tax_enum();

			String idVal = "";
			String linkroleVal = "";
			String definitionEnVal = "";
			String definitionMsVal = "";

			for (int i = 0; i < totalRow; i++) {
				countt++;
				rowIndex = i;
				Row row = TaxEnumSheet.getRow(i);
//				System.out.println("row: " + i);

				if (row != null) {
					for (int x = 0; x < cntHeader; x++) {
						Cell cell = row.getCell(x);
//						System.out.println("");
//						System.out.println("column: " + x);
						String val = "";

						if (cell != null) {

							val = getCellValue(cell);

							switch (val) {
							case "id":
								x++;
								cell = row.getCell(x);
								idVal = getCellValue(cell);
//								System.out.println("column: " + x + "value: " + idVal);
								x = cntHeader;
								break;
							case "LinkRole":
								x++;
								cell = row.getCell(x);
								linkroleVal = getCellValue(cell);
//								System.out.println("column: " + x + "value: " + linkroleVal);
								x = cntHeader;
								break;
							case "Definition":
								x++;
								cell = row.getCell(x);
								definitionEnVal = getCellValue(cell);
//								System.out.println("column: " + x + "value: " + definitionEnVal);
								x++;
								cell = row.getCell(x);
								definitionMsVal = getCellValue(cell);
								x = cntHeader;
								break;
							case "prefix":
								i++;
								tax_enum enum0 = new tax_enum();
								tax_enum enum1 = new tax_enum();
								tax_enum enum2 = new tax_enum();
								tax_enum enum3 = new tax_enum();
								tax_enum enum4 = new tax_enum();
								int prevInd = 0;
								for (; i < totalRow; i++) {
//									System.out.println("");
//									System.out.println("row inside prefix: " + i);
									int ind = 0;
									row = TaxEnumSheet.getRow(i);
//									cell = row.getCell(0);
									if (row != null) {
										cell = row.getCell(1);
										String cellValue = dataFormatter.formatCellValue(cell);
//										System.out.println(cellValue + "\t");
										cellValue = Strings.trimToNull(cellValue);
//										System.out.println(cellValue + "\t");
										if (cell != null && cellValue != null) {
											CellStyle style = cell.getCellStyle();
											ind = style.getIndention();
//											System.out.println(ind + "\t");
											val = "";
											if (ind == 0) {
												if (prevInd == 0) {
													if (enum0.getPrefix() != null) {
														enumList.add(enum0);
													}
												} else if (prevInd == 1) {
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													enumList.add(enum0);
													enum1 = new tax_enum();
												} else if (prevInd == 2) {
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													enumList.add(enum0);
												} else if (prevInd == 3) {
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													enumList.add(enum0);
												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													enumList.add(enum0);
												}

												enum0 = new tax_enum();
												enum0.setEnum_id(idVal);
												enum0.setLinkrole(linkroleVal);
												enum0.setDefinition_en(definitionEnVal);
												enum0.setDefiniton_ms(definitionMsVal);
												enum0.setModalId(id);

												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														enum0 = teService.tableMapping(enum0, x, val);
													}
												}

											} else if (ind == 1) {
												if (prevInd == 1) {
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 2) {
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 3) {
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												}

												enum1 = new tax_enum();
												enum1.setEnum_id(idVal);
												enum1.setLinkrole(linkroleVal);
												enum1.setDefinition_en(definitionEnVal);
												enum1.setDefiniton_ms(definitionMsVal);

												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														enum1 = teService.tableMapping(enum1, x, val);
													}
												}

											} else if (ind == 2) {
												if (prevInd == 1) {
													// enum1.addChild(enum2);
												} else if (prevInd == 2) {
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
												} else if (prevInd == 3) {
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												}

												enum2 = new tax_enum();
												enum2.setEnum_id(idVal);
												enum2.setLinkrole(linkroleVal);
												enum2.setDefinition_en(definitionEnVal);
												enum2.setDefiniton_ms(definitionMsVal);

												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														enum2 = teService.tableMapping(enum2, x, val);
													}
												}

											} else if (ind == 3) {
												if (prevInd == 2) {
													// enum1.addChild(enum2);
												} else if (prevInd == 3) {
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);

												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
												}

												enum3 = new tax_enum();
												enum3.setEnum_id(idVal);
												enum3.setLinkrole(linkroleVal);
												enum3.setDefinition_en(definitionEnVal);
												enum3.setDefiniton_ms(definitionMsVal);

												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														enum3 = teService.tableMapping(enum3, x, val);
													}
												}
											} else if (ind == 4) {
												if (prevInd == 2) {
													// enum1.addChild(enum2);
												} else if (prevInd == 3) {
//													enum3.setParent(enum2);
//													enum2.getChildren().add(enum3);

												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
//													enum3.setParent(enum2);
//													enum2.getChildren().add(enum3);
												}

												enum4 = new tax_enum();
												enum4.setEnum_id(idVal);
												enum4.setLinkrole(linkroleVal);
												enum4.setDefinition_en(definitionEnVal);
												enum4.setDefiniton_ms(definitionMsVal);

												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														enum4 = teService.tableMapping(enum4, x, val);
													}
												}
											} else {

											}
											prevInd = ind;
										} else {
											if (prevInd == 0) {
												if (enum0.getPrefix() != null) {
													enumList.add(enum0);
												}
											} else if (prevInd == 1) {
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												enumList.add(enum0);
												enum1 = new tax_enum();
											} else if (prevInd == 2) {
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												enumList.add(enum0);
											} else if (prevInd == 3) {
												enum3.setParent(enum2);
												enum2.getChildren().add(enum3);
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												enumList.add(enum0);
											} else if (prevInd == 4) {
												enum4.setParent(enum3);
												enum3.getChildren().add(enum4);
												enum3.setParent(enum2);
												enum2.getChildren().add(enum3);
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												enumList.add(enum0);
											}
											if (enum0.getPrefix() != null) {
												enumList.add(enum0);
											}
											idVal = "";
											linkroleVal = "";
											definitionEnVal = "";
											definitionMsVal = "";
											prevInd = ind;
											break;
										}

//										tax_enum enumObj = new tax_enum();
//										for (x = 0; x < cntHeader; x++) {
//											cell = row.getCell(x);
//											if (cell != null) {
//
//												val = getCellValue(cell);
//												if (ind == 0) {
//													enum0 = teService.tableMapping(enum0, x, val);
//												} else if (ind == 1) {
//													enum1 = teService.tableMapping(enum1, x, val);
//												} else if (ind == 2) {
//													enum2 = teService.tableMapping(enum2, x, val);
//												} else if (ind == 3) {
//													enum3 = teService.tableMapping(enum3, x, val);
//												} else {
//													enumObj = teService.tableMapping(enumObj, x, val);
//												}
//
//											}
//										}
//										System.out.println("id: " + idVal + "linkrole: " + linkroleVal + "definition: " + definitionEnVal);

									} else {
										if (prevInd == 0) {
											if (enum0.getPrefix() != null) {
												enumList.add(enum0);
											}
										} else if (prevInd == 1) {
											enum1.setParent(enum0);
											enum0.getChildren().add(enum1);
											enumList.add(enum0);
											enum1 = new tax_enum();
										} else if (prevInd == 2) {
											enum2.setParent(enum1);
											enum1.getChildren().add(enum2);
											enum1.setParent(enum0);
											enum0.getChildren().add(enum1);
											enumList.add(enum0);
										} else if (prevInd == 3) {
											enum3.setParent(enum2);
											enum2.getChildren().add(enum3);
											enum2.setParent(enum1);
											enum1.getChildren().add(enum2);
											enum1.setParent(enum0);
											enum0.getChildren().add(enum1);
											enumList.add(enum0);
										} else if (prevInd == 4) {
											enum4.setParent(enum3);
											enum3.getChildren().add(enum4);
											enum3.setParent(enum2);
											enum2.getChildren().add(enum3);
											enum2.setParent(enum1);
											enum1.getChildren().add(enum2);
											enum1.setParent(enum0);
											enum0.getChildren().add(enum1);
											enumList.add(enum0);
										}
										if (enum0.getPrefix() != null) {
											enumList.add(enum0);
										}
										idVal = "";
										linkroleVal = "";
										definitionEnVal = "";
										definitionMsVal = "";
										prevInd = ind;
										break;
									}
								}
								if (i == totalRow) {
									if (prevInd == 0) {
										if (enum0.getPrefix() != null) {
											enumList.add(enum0);
										}
									} else if (prevInd == 1) {
										enum1.setParent(enum0);
										enum0.getChildren().add(enum1);
										enumList.add(enum0);
										enum1 = new tax_enum();
									} else if (prevInd == 2) {
										enum2.setParent(enum1);
										enum1.getChildren().add(enum2);
										enum1.setParent(enum0);
										enum0.getChildren().add(enum1);
										enumList.add(enum0);
									} else if (prevInd == 3) {
										enum3.setParent(enum2);
										enum2.getChildren().add(enum3);
										enum2.setParent(enum1);
										enum1.getChildren().add(enum2);
										enum1.setParent(enum0);
										enum0.getChildren().add(enum1);
										enumList.add(enum0);
									} else if (prevInd == 4) {
										enum4.setParent(enum3);
										enum3.getChildren().add(enum4);
										enum3.setParent(enum2);
										enum2.getChildren().add(enum3);
										enum2.setParent(enum1);
										enum1.getChildren().add(enum2);
										enum1.setParent(enum0);
										enum0.getChildren().add(enum1);
										enumList.add(enum0);
									}
									if (enum0.getPrefix() != null) {
										enumList.add(enum0);
									}
								}
								break;
							}

						}

					}

				}
//				if (i > 1000) {
//					break;
//				}

			}
			teService.save(enumList);
//			for (tax_enum te : enumList) {
//				System.out.println(te.toString());
//			}
			isCompleted = true;
		} catch (Exception e) {
			isCompleted = false;
			e.printStackTrace();
			throw new RuntimeException("fail to store excel data: " + e.getMessage());
		}

		return isCompleted;
	}
	
	public boolean readTaxConceptSme(Sheet TaxConceptSheet, Long id) {
		boolean isCompleted = false;

		try {
//			tax_data_modal tdm = tdmService.getByFilename(fname);
			// Create a DataFormatter to format and get each cell's value as String
			DataFormatter dataFormatter = new DataFormatter();
			List<tax_sme_concept> tList = new ArrayList<tax_sme_concept>();

			List<CellRangeAddress> cras = getCombineCell(TaxConceptSheet);
			int totalRow = TaxConceptSheet.getLastRowNum() + 1;
//			System.out.println(totalRow + " Total Row\t");
			// 2. Or you can use a for-each loop to iterate over the rows and columns
//			System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
			int countt = 0;
			int cntHeader = 0;
			int rowIndex = 0;
			int columnIndex = 0;
			for (int i = 0; i < totalRow; i++) {
				countt++;
				rowIndex = i;
				Row row = TaxConceptSheet.getRow(i);
//				System.out.println(i);
				tax_sme_concept newTax = new tax_sme_concept();

				if (countt == 1) {
					for (Cell cell : row) {
						String cellValue = dataFormatter.formatCellValue(cell);
//						System.out.print(cellValue + "\t");
						cntHeader++;
					}
//					System.out.println(cntHeader + " header column\t");
				} else {
					int lastRow = 0;
					for (int x = 0; x < cntHeader; x++) {
						Cell cell = row.getCell(x);
						String val = "";
						if (cell != null) {
							if (x == 18 || x == 19 || x == 20 || x == 21 || x == 22) {
								if (isMergedRegion(TaxConceptSheet, i, 0)) {
//                				System.out.println(x);
									lastRow = getRowNum(cras, TaxConceptSheet.getRow(i).getCell(0), TaxConceptSheet);
									int p = 0;
									for (p = i; p <= lastRow; p++) {
										Row row2 = TaxConceptSheet.getRow(p);
										cell = row2.getCell(x);
										val = "";
										if (cell != null) {
											val = getCellValue(cell);
											newTax = tscService.tableMapping(newTax, x, val);
										}
									}
									p--;
								} else {
									val = getCellValue(cell);
									newTax = tscService.tableMapping(newTax, x, val);
								}
							} else {
								val = getCellValue(cell);
								newTax = tscService.tableMapping(newTax, x, val);
							}
							newTax.setModalId(id);
							tList.add(newTax);
						} else {
//                		System.out.print("Empty cell \t");
						}
					}

					if (lastRow > i) {
						i = lastRow;
					}
				}

//            System.out.println(i);
//            if(countt == 1000) {
//            	break;
//            }
			}
			tscService.save(tList);
			isCompleted = true;

		} catch (Exception e) {
			isCompleted = false;
			e.printStackTrace();
			throw new RuntimeException("fail to store excel data: " + e.getMessage());
		}
		return isCompleted;
	}

//	public boolean readTaxEnum(Sheet TaxEnumSheet) {
	public boolean readTaxEnumSme(Sheet TaxEnumSheet, Long id) {
		boolean isCompleted = false;
		List<tax_sme_enum> enumList = new ArrayList<tax_sme_enum>();

		try {
			DataFormatter dataFormatter = new DataFormatter();

			List<CellRangeAddress> cras = getCombineCell(TaxEnumSheet);
			int totalRow = TaxEnumSheet.getLastRowNum() + 1;
//			System.out.println(totalRow + " Total Row\t");
			// 2. Or you can use a for-each loop to iterate over the rows and columns
//			System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
			int countt = 0;
			int cntHeader = 5;
			int rowIndex = 0;
			int columnIndex = 0;
			tax_sme_enum taxEnum = new tax_sme_enum();

			String idVal = "";
			String linkroleVal = "";
			String definitionEnVal = "";
			String definitionMsVal = "";

			for (int i = 0; i < totalRow; i++) {
				countt++;
				rowIndex = i;
				Row row = TaxEnumSheet.getRow(i);
//				System.out.println("row: " + i);

				if (row != null) {
					for (int x = 0; x < cntHeader; x++) {
						Cell cell = row.getCell(x);
//						System.out.println("");
//						System.out.println("column: " + x);
						String val = "";

						if (cell != null) {

							val = getCellValue(cell);

							switch (val) {
							case "id":
								x++;
								cell = row.getCell(x);
								idVal = getCellValue(cell);
//								System.out.println("column: " + x + "value: " + idVal);
								x = cntHeader;
								break;
							case "LinkRole":
								x++;
								cell = row.getCell(x);
								linkroleVal = getCellValue(cell);
//								System.out.println("column: " + x + "value: " + linkroleVal);
								x = cntHeader;
								break;
							case "Definition":
								x++;
								cell = row.getCell(x);
								definitionEnVal = getCellValue(cell);
//								System.out.println("column: " + x + "value: " + definitionEnVal);
								x++;
								cell = row.getCell(x);
								definitionMsVal = getCellValue(cell);
								x = cntHeader;
								break;
							case "prefix":
								i++;
								tax_sme_enum enum0 = new tax_sme_enum();
								tax_sme_enum enum1 = new tax_sme_enum();
								tax_sme_enum enum2 = new tax_sme_enum();
								tax_sme_enum enum3 = new tax_sme_enum();
								tax_sme_enum enum4 = new tax_sme_enum();
								int prevInd = 0;
								for (; i < totalRow; i++) {
//									System.out.println("");
//									System.out.println("row inside prefix: " + i);
									int ind = 0;
									row = TaxEnumSheet.getRow(i);
//									cell = row.getCell(0);
									if (row != null) {
										cell = row.getCell(1);
										String cellValue = dataFormatter.formatCellValue(cell);
//										System.out.println(cellValue + "\t");
										cellValue = Strings.trimToNull(cellValue);
//										System.out.println(cellValue + "\t");
										if (cell != null && cellValue != null) {
											CellStyle style = cell.getCellStyle();
											ind = style.getIndention();
//											System.out.println(ind + "\t");
											val = "";
											if (ind == 0) {
												if (prevInd == 0) {
													if (enum0.getPrefix() != null) {
														enumList.add(enum0);
													}
												} else if (prevInd == 1) {
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													enumList.add(enum0);
													enum1 = new tax_sme_enum();
												} else if (prevInd == 2) {
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													enumList.add(enum0);
												} else if (prevInd == 3) {
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													enumList.add(enum0);
												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													enumList.add(enum0);
												}

												enum0 = new tax_sme_enum();
												enum0.setEnum_id(idVal);
												enum0.setLinkrole(linkroleVal);
												enum0.setDefinition_en(definitionEnVal);
												enum0.setDefiniton_ms(definitionMsVal);
												enum0.setModalId(id);

												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														enum0 = tseService.tableMapping(enum0, x, val);
													}
												}

											} else if (ind == 1) {
												if (prevInd == 1) {
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 2) {
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 3) {
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												}

												enum1 = new tax_sme_enum();
												enum1.setEnum_id(idVal);
												enum1.setLinkrole(linkroleVal);
												enum1.setDefinition_en(definitionEnVal);
												enum1.setDefiniton_ms(definitionMsVal);

												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														enum1 = tseService.tableMapping(enum1, x, val);
													}
												}

											} else if (ind == 2) {
												if (prevInd == 1) {
													// enum1.addChild(enum2);
												} else if (prevInd == 2) {
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
												} else if (prevInd == 3) {
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												}

												enum2 = new tax_sme_enum();
												enum2.setEnum_id(idVal);
												enum2.setLinkrole(linkroleVal);
												enum2.setDefinition_en(definitionEnVal);
												enum2.setDefiniton_ms(definitionMsVal);

												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														enum2 = tseService.tableMapping(enum2, x, val);
													}
												}

											} else if (ind == 3) {
												if (prevInd == 2) {
													// enum1.addChild(enum2);
												} else if (prevInd == 3) {
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);

												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
												}

												enum3 = new tax_sme_enum();
												enum3.setEnum_id(idVal);
												enum3.setLinkrole(linkroleVal);
												enum3.setDefinition_en(definitionEnVal);
												enum3.setDefiniton_ms(definitionMsVal);

												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														enum3 = tseService.tableMapping(enum3, x, val);
													}
												}
											} else if (ind == 4) {
												if (prevInd == 2) {
													// enum1.addChild(enum2);
												} else if (prevInd == 3) {
//													enum3.setParent(enum2);
//													enum2.getChildren().add(enum3);

												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
//													enum3.setParent(enum2);
//													enum2.getChildren().add(enum3);
												}

												enum4 = new tax_sme_enum();
												enum4.setEnum_id(idVal);
												enum4.setLinkrole(linkroleVal);
												enum4.setDefinition_en(definitionEnVal);
												enum4.setDefiniton_ms(definitionMsVal);

												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														enum4 = tseService.tableMapping(enum4, x, val);
													}
												}
											} else {

											}
											prevInd = ind;
										} else {
											if (prevInd == 0) {
												if (enum0.getPrefix() != null) {
													enumList.add(enum0);
												}
											} else if (prevInd == 1) {
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												enumList.add(enum0);
												enum1 = new tax_sme_enum();
											} else if (prevInd == 2) {
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												enumList.add(enum0);
											} else if (prevInd == 3) {
												enum3.setParent(enum2);
												enum2.getChildren().add(enum3);
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												enumList.add(enum0);
											} else if (prevInd == 4) {
												enum4.setParent(enum3);
												enum3.getChildren().add(enum4);
												enum3.setParent(enum2);
												enum2.getChildren().add(enum3);
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												enumList.add(enum0);
											}
											if (enum0.getPrefix() != null) {
												enumList.add(enum0);
											}
											idVal = "";
											linkroleVal = "";
											definitionEnVal = "";
											definitionMsVal = "";
											prevInd = ind;
											break;
										}

//										tax_enum enumObj = new tax_enum();
//										for (x = 0; x < cntHeader; x++) {
//											cell = row.getCell(x);
//											if (cell != null) {
//
//												val = getCellValue(cell);
//												if (ind == 0) {
//													enum0 = teService.tableMapping(enum0, x, val);
//												} else if (ind == 1) {
//													enum1 = teService.tableMapping(enum1, x, val);
//												} else if (ind == 2) {
//													enum2 = teService.tableMapping(enum2, x, val);
//												} else if (ind == 3) {
//													enum3 = teService.tableMapping(enum3, x, val);
//												} else {
//													enumObj = teService.tableMapping(enumObj, x, val);
//												}
//
//											}
//										}
//										System.out.println("id: " + idVal + "linkrole: " + linkroleVal + "definition: " + definitionEnVal);

									} else {
										if (prevInd == 0) {
											if (enum0.getPrefix() != null) {
												enumList.add(enum0);
											}
										} else if (prevInd == 1) {
											enum1.setParent(enum0);
											enum0.getChildren().add(enum1);
											enumList.add(enum0);
											enum1 = new tax_sme_enum();
										} else if (prevInd == 2) {
											enum2.setParent(enum1);
											enum1.getChildren().add(enum2);
											enum1.setParent(enum0);
											enum0.getChildren().add(enum1);
											enumList.add(enum0);
										} else if (prevInd == 3) {
											enum3.setParent(enum2);
											enum2.getChildren().add(enum3);
											enum2.setParent(enum1);
											enum1.getChildren().add(enum2);
											enum1.setParent(enum0);
											enum0.getChildren().add(enum1);
											enumList.add(enum0);
										} else if (prevInd == 4) {
											enum4.setParent(enum3);
											enum3.getChildren().add(enum4);
											enum3.setParent(enum2);
											enum2.getChildren().add(enum3);
											enum2.setParent(enum1);
											enum1.getChildren().add(enum2);
											enum1.setParent(enum0);
											enum0.getChildren().add(enum1);
											enumList.add(enum0);
										}
										if (enum0.getPrefix() != null) {
											enumList.add(enum0);
										}
										idVal = "";
										linkroleVal = "";
										definitionEnVal = "";
										definitionMsVal = "";
										prevInd = ind;
										break;
									}
								}
								if (i == totalRow) {
									if (prevInd == 0) {
										if (enum0.getPrefix() != null) {
											enumList.add(enum0);
										}
									} else if (prevInd == 1) {
										enum1.setParent(enum0);
										enum0.getChildren().add(enum1);
										enumList.add(enum0);
										enum1 = new tax_sme_enum();
									} else if (prevInd == 2) {
										enum2.setParent(enum1);
										enum1.getChildren().add(enum2);
										enum1.setParent(enum0);
										enum0.getChildren().add(enum1);
										enumList.add(enum0);
									} else if (prevInd == 3) {
										enum3.setParent(enum2);
										enum2.getChildren().add(enum3);
										enum2.setParent(enum1);
										enum1.getChildren().add(enum2);
										enum1.setParent(enum0);
										enum0.getChildren().add(enum1);
										enumList.add(enum0);
									} else if (prevInd == 4) {
										enum4.setParent(enum3);
										enum3.getChildren().add(enum4);
										enum3.setParent(enum2);
										enum2.getChildren().add(enum3);
										enum2.setParent(enum1);
										enum1.getChildren().add(enum2);
										enum1.setParent(enum0);
										enum0.getChildren().add(enum1);
										enumList.add(enum0);
									}
									if (enum0.getPrefix() != null) {
										enumList.add(enum0);
									}
								}
								break;
							}

						}

					}

				}
//				if (i > 1000) {
//					break;
//				}

			}
			tseService.save(enumList);
//			for (tax_enum te : enumList) {
//				System.out.println(te.toString());
//			}
			isCompleted = true;
		} catch (Exception e) {
			isCompleted = false;
			e.printStackTrace();
			throw new RuntimeException("fail to store excel data: " + e.getMessage());
		}

		return isCompleted;
	}
	
	// for sheet no 4, tax header
	public boolean readTaxHeader(Sheet TaxEnumSheet, Long id, String type) {
		boolean isCompleted = false;
		
		try {
			DataFormatter dataFormatter = new DataFormatter();
			tax_data_modal tdm = tdmService.getById(id);
			List<tax_form> taxFormList = new ArrayList<tax_form>();
			List<CellRangeAddress> cras = getCombineCell(TaxEnumSheet);
			int totalRow = TaxEnumSheet.getLastRowNum() + 1;
			System.out.println("Data Modal: " + tdm.getFilename());
			System.out.println(totalRow + " Total Row\t");
			// 2. Or you can use a for-each loop to iterate over the rows and columns
			System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
			int countt = 0;
			int cntHeader = 6;
			int rowIndex = 0;
			int columnIndex = 0;
			
			tax_form form = new tax_form();
			tax_form temp = new tax_form();

			String idVal = "";
			String linkroleVal = "";
			String definitionEnVal = "";
			
			for (int i = 0; i < totalRow; i++) {
				countt++;
				rowIndex = i;
				Row row = TaxEnumSheet.getRow(i);
//				System.out.println("row: " + i);
				
				if (row != null) {
					for (int x = 0; x < cntHeader; x++) {
						Cell cell = row.getCell(x);
//						System.out.println("");
//						System.out.println("column: " + x);
						String val = "";
						
						if (cell != null) {
							val = getCellValue(cell);
							
							switch (val) {
							case "id":
								x++;
								cell = row.getCell(x);
								idVal = getCellValue(cell);
//								System.out.println("column: " + x + "value: " + idVal);
								form.setName(idVal);
								x = cntHeader;
								break;
							case "LinkRole":
								x++;
								cell = row.getCell(x);
								linkroleVal = getCellValue(cell);
//								System.out.println("column: " + x + "value: " + linkroleVal);
								form.setLinkRole(linkroleVal);
								x = cntHeader;
								break;
							case "Definition":
								x++;
								cell = row.getCell(x);
								definitionEnVal = getCellValue(cell);
//								System.out.println("column: " + x + "value: " + definitionEnVal);
								form.setDefinition(definitionEnVal);
								String str = definitionEnVal.replaceAll("Filing information: ", "");
//								System.out.println("column: " + x + "value: " + str);
								form.setFormName(str);
								System.out.println("Form Name: " + form.getFormName());
								x = cntHeader;
								break;
							case "prefix":
								i++;
								List<tax_form_header> taxHeaderList = new ArrayList<tax_form_header>();
								int listCount = tdm.getTaxFormList().size();
								System.out.println("Temp Tax Form List: " + listCount);
								temp = tdm.getTaxFormList().stream().filter(f -> form.getFormName().equalsIgnoreCase(f.getFormName().trim())).findAny().orElse(null);

//								temp = fService.getByName(form.getFormName());
								if(temp != null) {
									System.out.println("Temp Form Name: " + temp.getFormName());
									temp.setName(form.getName());
									temp.setLinkRole(form.getLinkRole());
									temp.setDefinition(form.getDefinition());
									
									System.out.println("id: " + temp.getId());
									
									tax_form_header enum0 = new tax_form_header();
									tax_form_header enum1 = new tax_form_header();
									tax_form_header enum2 = new tax_form_header();
									tax_form_header enum3 = new tax_form_header();
									tax_form_header enum4 = new tax_form_header();
									int prevInd = 0;
									
									for (; i < totalRow; i++) {
										System.out.println("");
										System.out.println("row inside prefix: " + i);
										int ind = 0;
										row = TaxEnumSheet.getRow(i);
//										cell = row.getCell(0);
										if (row != null) {
											cell = row.getCell(1);
											String cellValue = dataFormatter.formatCellValue(cell);
											System.out.println(cellValue + "\t");
											cellValue = Strings.trimToNull(cellValue);
//											System.out.println(cellValue + "\t");
											if (cell != null && cellValue != null) {
												CellStyle style = cell.getCellStyle();
												ind = style.getIndention();
												System.out.println(ind + "\t");
												val = "";
												if (ind == 0) {
													if (prevInd == 0) {
														if (enum0.getPrefix() != null) {
															taxHeaderList.add(enum0);
														}
													} else if (prevInd == 1) {
														enum1.setParent(enum0);
														enum0.getChildren().add(enum1);
														taxHeaderList.add(enum0);
														enum1 = new tax_form_header();
													} else if (prevInd == 2) {
														enum2.setParent(enum1);
														enum1.getChildren().add(enum2);
														enum1.setParent(enum0);
														enum0.getChildren().add(enum1);
														taxHeaderList.add(enum0);
													} else if (prevInd == 3) {
														enum3.setParent(enum2);
														enum2.getChildren().add(enum3);
														enum2.setParent(enum1);
														enum1.getChildren().add(enum2);
														enum1.setParent(enum0);
														enum0.getChildren().add(enum1);
														taxHeaderList.add(enum0);
													} else if (prevInd == 4) {
														enum4.setParent(enum3);
														enum3.getChildren().add(enum4);
														enum3.setParent(enum2);
														enum2.getChildren().add(enum3);
														enum2.setParent(enum1);
														enum1.getChildren().add(enum2);
														enum1.setParent(enum0);
														enum0.getChildren().add(enum1);
														taxHeaderList.add(enum0);
													}

													enum0 = new tax_form_header();

													for (x = 0; x < cntHeader; x++) {
														cell = row.getCell(x);
														if (cell != null) {
															val = getCellValue(cell);
															enum0 = headerService.tableMapping(enum0, x, val);
														}
													}

												} else if (ind == 1) {
													if (prevInd == 1) {
														enum1.setParent(enum0);
														enum0.getChildren().add(enum1);
													} else if (prevInd == 2) {
														System.out.println("ind 1 test\t");
														enum2.setParent(enum1);
														enum1.getChildren().add(enum2);
														enum1.setParent(enum0);
														enum0.getChildren().add(enum1);
													} else if (prevInd == 3) {
														enum3.setParent(enum2);
														enum2.getChildren().add(enum3);
														enum2.setParent(enum1);
														enum1.getChildren().add(enum2);
														enum1.setParent(enum0);
														enum0.getChildren().add(enum1);
													} else if (prevInd == 4) {
														enum4.setParent(enum3);
														enum3.getChildren().add(enum4);
														enum3.setParent(enum2);
														enum2.getChildren().add(enum3);
														enum2.setParent(enum1);
														enum1.getChildren().add(enum2);
														enum1.setParent(enum0);
														enum0.getChildren().add(enum1);
													}

													enum1 = new tax_form_header();

													for (x = 0; x < cntHeader; x++) {
														cell = row.getCell(x);
														if (cell != null) {
															val = getCellValue(cell);
															enum1 = headerService.tableMapping(enum1, x, val);
														}
													}

												} else if (ind == 2) {
													if (prevInd == 1) {
														// enum1.addChild(enum2);
													} else if (prevInd == 2) {
														enum2.setParent(enum1);
														enum1.getChildren().add(enum2);
													} else if (prevInd == 3) {
														enum3.setParent(enum2);
														enum2.getChildren().add(enum3);
														enum2.setParent(enum1);
														enum1.getChildren().add(enum2);
														enum1.setParent(enum0);
														enum0.getChildren().add(enum1);
													} else if (prevInd == 4) {
														enum4.setParent(enum3);
														enum3.getChildren().add(enum4);
														enum3.setParent(enum2);
														enum2.getChildren().add(enum3);
														enum2.setParent(enum1);
														enum1.getChildren().add(enum2);
														enum1.setParent(enum0);
														enum0.getChildren().add(enum1);
													}

													enum2 = new tax_form_header();

													for (x = 0; x < cntHeader; x++) {
														cell = row.getCell(x);
														if (cell != null) {
															val = getCellValue(cell);
															enum2 = headerService.tableMapping(enum2, x, val);
														}
													}

												} else if (ind == 3) {
													if (prevInd == 2) {
														// enum1.addChild(enum2);
													} else if (prevInd == 3) {
														enum3.setParent(enum2);
														enum2.getChildren().add(enum3);

													} else if (prevInd == 4) {
														enum4.setParent(enum3);
														enum3.getChildren().add(enum4);
														enum3.setParent(enum2);
														enum2.getChildren().add(enum3);
													}

													enum3 = new tax_form_header();

													for (x = 0; x < cntHeader; x++) {
														cell = row.getCell(x);
														if (cell != null) {
															val = getCellValue(cell);
															enum3 = headerService.tableMapping(enum3, x, val);
														}
													}
												} else if (ind == 4) {
													if (prevInd == 2) {

													} else if (prevInd == 3) {

													} else if (prevInd == 4) {
														enum4.setParent(enum3);
														enum3.getChildren().add(enum4);
													}

													enum4 = new tax_form_header();

													for (x = 0; x < cntHeader; x++) {
														cell = row.getCell(x);
														if (cell != null) {
															val = getCellValue(cell);
															enum4 = headerService.tableMapping(enum4, x, val);
														}
													}
												} else {

												}
												prevInd = ind;
											} else {
												if (prevInd == 0) {
													if (enum0.getPrefix() != null) {
														taxHeaderList.add(enum0);
													}
												} else if (prevInd == 1) {
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													taxHeaderList.add(enum0);
													enum1 = new tax_form_header();
												} else if (prevInd == 2) {
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													taxHeaderList.add(enum0);
												} else if (prevInd == 3) {
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													taxHeaderList.add(enum0);
												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													taxHeaderList.add(enum0);
												}
												if (enum0.getPrefix() != null) {
													taxHeaderList.add(enum0);
												}

												prevInd = ind;
												break;
											}										
										} else {
											if (prevInd == 0) {
												if (enum0.getPrefix() != null) {
													taxHeaderList.add(enum0);
												}
											} else if (prevInd == 1) {
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												taxHeaderList.add(enum0);
												enum1 = new tax_form_header();
											} else if (prevInd == 2) {
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												taxHeaderList.add(enum0);
											} else if (prevInd == 3) {
												enum3.setParent(enum2);
												enum2.getChildren().add(enum3);
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												taxHeaderList.add(enum0);
											} else if (prevInd == 4) {
												enum4.setParent(enum3);
												enum3.getChildren().add(enum4);
												enum3.setParent(enum2);
												enum2.getChildren().add(enum3);
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												taxHeaderList.add(enum0);
											}
											if (enum0.getPrefix() != null) {
												taxHeaderList.add(enum0);
											}
											prevInd = ind;
											break;
										}
										if (i == (totalRow - 1)) {
											if (prevInd == 0) {
												if (enum0.getPrefix() != null) {
													taxHeaderList.add(enum0);
												}
											} else if (prevInd == 1) {
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												taxHeaderList.add(enum0);
												enum1 = new tax_form_header();
											} else if (prevInd == 2) {
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												taxHeaderList.add(enum0);
											} else if (prevInd == 3) {
												enum3.setParent(enum2);
												enum2.getChildren().add(enum3);
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												taxHeaderList.add(enum0);
											} else if (prevInd == 4) {
												enum4.setParent(enum3);
												enum3.getChildren().add(enum4);
												enum3.setParent(enum2);
												enum2.getChildren().add(enum3);
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												taxHeaderList.add(enum0);
											}
											if (enum0.getPrefix() != null) {
												taxHeaderList.add(enum0);
											}
										}
									}
									
									temp.getFormHeaderList().clear();
									temp.getFormHeaderList().addAll(taxHeaderList);
									taxFormList.add(temp);
									
								}
								break;
							}
						}
					}
				}
				
			}
			
			for(tax_form f: taxFormList) {
				System.out.println("id: " + f.getId());
				System.out.println("list: " + f.getFormHeaderList().size());
				fService.saveSingle(f);
			}
			
			isCompleted = true;
//			fService.save(taxFormList);
			
//			temp = fService.getByName(form.getName().toLowerCase());
			
//			if(temp.getName() != null) {
//				temp.setLinkrole(form.getLinkrole());
//				temp.getChildren().clear();
//				temp.getChildren().addAll(presentationList);
//				formService.saveSingle(temp);
//			} else {
//				form.getChildren().clear();
//				form.getChildren().addAll(presentationList);
//				formService.saveSingle(form);
//			}
			
		} catch (Exception e) {
			isCompleted = false;
			e.printStackTrace();
			throw new RuntimeException("fail to store excel data: " + e.getMessage());
		}
		
		return isCompleted;
	}
	
	// for sheet no 5++, form presentation
	public boolean readLinkbaseForm(Sheet TaxEnumSheet, Long id, String category) {
		List<presentation_detail> presentationList = new ArrayList<presentation_detail>();
		List<presentation_formula> formulaList = new ArrayList<presentation_formula>();
		linkbase_form form = new linkbase_form();
		linkbase_form temp = new linkbase_form();
		boolean isCompleted = false;

		try {
			DataFormatter dataFormatter = new DataFormatter();

			List<CellRangeAddress> cras = getCombineCell(TaxEnumSheet);
			int totalRow = TaxEnumSheet.getLastRowNum() + 1;
//			System.out.println( TaxEnumSheet.getSheetName() + " : " + totalRow + " Total Row\t");
			// 2. Or you can use a for-each loop to iterate over the rows and columns
//			System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
			int countt = 0;
			int cntHeader = 12;
			int rowIndex = 0;
			int columnIndex = 0;

			String idVal = "";
			String linkroleVal = "";
			String definitionEnVal = "";
			String definitionMsVal = "";

			for (int i = 0; i < totalRow; i++) {
				countt++;
				rowIndex = i;
				Row row = TaxEnumSheet.getRow(i);
//				System.out.println("row: " + i);

				if (row != null) {
					for (int x = 0; x < cntHeader; x++) {
						Cell cell = row.getCell(x);
//						System.out.println("");
//						System.out.println("column: " + x);
						String val = "";

						if (cell != null) {
							val = getCellValue(cell);

							switch (val) {
							case "id":
								x++;
								cell = row.getCell(x);
								idVal = getCellValue(cell);
//								System.out.println("column: " + x + "value: " + idVal);
								form.setName(idVal);
								form.setCategory(category);
								x = cntHeader;
								break;
							case "LinkRole":
								x++;
								cell = row.getCell(x);
								linkroleVal = getCellValue(cell);
//								System.out.println("column: " + x + "value: " + linkroleVal);
								form.setLinkrole(linkroleVal);
								x = cntHeader;
								break;
							case "Definition":
								x++;
								cell = row.getCell(x);
								definitionEnVal = getCellValue(cell);
//								System.out.println("column: " + x + "value: " + definitionEnVal);
								form.setDefinition(definitionEnVal);
								x = cntHeader;
								break;
							case "prefix":
								i++;
								presentation_detail enum0 = new presentation_detail();
								presentation_detail enum1 = new presentation_detail();
								presentation_detail enum2 = new presentation_detail();
								presentation_detail enum3 = new presentation_detail();
								presentation_detail enum4 = new presentation_detail();
								presentation_detail enum5 = new presentation_detail();
								presentation_detail enum6 = new presentation_detail();
								presentation_detail enum7 = new presentation_detail();
								int prevInd = 0;
								int formulaCnt = 0;
								for (; i < totalRow; i++) {
//									System.out.println("");
//									System.out.println("row inside prefix: " + i);
									int ind = 0;
									row = TaxEnumSheet.getRow(i);
//									cell = row.getCell(0);
									if (row != null) {
										cell = row.getCell(1);
										String cellValue = dataFormatter.formatCellValue(cell);
//										System.out.println(cellValue + "\t");
										cellValue = Strings.trimToNull(cellValue);
//										System.out.println(cellValue + "\t");
										if (cell != null && cellValue != null) {
											CellStyle style = cell.getCellStyle();
											ind = style.getIndention();
//											System.out.println(ind + "\t");
											val = "";
											if (ind == 0) {
												if (prevInd == 0) {
													if (enum0.getPrefix() != null) {
														presentationList.add(enum0);
													}
												} else if (prevInd == 1) {
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													presentationList.add(enum0);
													enum1 = new presentation_detail();
												} else if (prevInd == 2) {
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													presentationList.add(enum0);
												} else if (prevInd == 3) {
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													presentationList.add(enum0);
												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													presentationList.add(enum0);
												} else if (prevInd == 5) {
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													presentationList.add(enum0);
												} else if (prevInd == 6) {
													enum6.setParent(enum5);
													enum5.getChildren().add(enum6);
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													presentationList.add(enum0);
												} else if (prevInd == 7) {
													enum7.setParent(enum6);
													enum6.getChildren().add(enum7);
													enum6.setParent(enum5);
													enum5.getChildren().add(enum6);
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													presentationList.add(enum0);
												}

												enum0 = new presentation_detail();
												
												String rowDataItem = "";
												String rowFieldName = "";
												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														val = Strings.trimToNull(val);
														enum0 = pdService.tableMapping(enum0, x, val);
														if(x == 1) {
															rowFieldName = val;
														}
														if(x == 8) {
															rowDataItem = val;
														}
														if(x > 8) {
															presentation_formula formula = new presentation_formula();
															String cellval = dataFormatter.formatCellValue(cell);
//															System.out.println(cellval + "\t");
															if(cellval.contains("\n")) {
																String[] prelines = cellval.split("\n", -1);
																String[] lines = Arrays.stream(prelines).filter(value -> value  != null && value.length() > 0).toArray(size -> new String[size]);
																if(lines.length > 0) {
																	int cntStr = lines.length;
																	for (String str : lines) {
																		if(str == null || str.isEmpty() || str.trim().isEmpty()) {
//																			System.out.println("empty value" + cntStr + "\t");
																		} else {
																			formula = new presentation_formula();
//																			System.out.println(formulaCnt + "\t");
//																			System.out.println(str + "!\t");
																			if(formulaList.isEmpty() || x == 9) {
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else if(x == 9){
//																				cntStr = formulaList.size();
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else {
																				if(formulaCnt - cntStr < 0) {
																					formula = formulaList.get(0);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(0, formula);
																					cntStr--;
//																					formulaCnt++;
																				} else {
																					formula = formulaList.get(formulaCnt - cntStr);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(formulaCnt - cntStr, formula);
																					cntStr--;
//																					formulaCnt++;
																				}
																			}
																		}
																	}
//																	String str = lines[1];
//																	str = str.replaceAll(" & ", "/");
//																	form.setFormName(str);
																}
															} else {
																if(formulaList.isEmpty() && x == 9 && val != null) {
																	int cntStr = 0;
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(x == 9 && val != null){
																	int cntStr = formulaList.size();
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(val != null) {
																	int cntStr = formulaCnt - 1;
																	formula = formulaList.get(cntStr);
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.set(cntStr, formula);
//																	formulaCnt++;
																}
															}
														}
													}
												}

											} else if (ind == 1) {
												if (prevInd == 1) {
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 2) {
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 3) {
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 5) {
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													presentationList.add(enum0);
												} else if (prevInd == 6) {
													enum6.setParent(enum5);
													enum5.getChildren().add(enum6);
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													presentationList.add(enum0);
												} else if (prevInd == 7) {
													enum7.setParent(enum6);
													enum6.getChildren().add(enum7);
													enum6.setParent(enum5);
													enum5.getChildren().add(enum6);
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
													presentationList.add(enum0);
												}

												enum1 = new presentation_detail();

												String rowDataItem = "";
												String rowFieldName = "";
												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														val = Strings.trimToNull(val);
														enum1 = pdService.tableMapping(enum1, x, val);
														if(x == 1) {
															rowFieldName = val;
														}
														if(x == 8) {
															rowDataItem = val;
														}
														if(x > 8) {
															presentation_formula formula = new presentation_formula();
															String cellval = dataFormatter.formatCellValue(cell);
//															System.out.println(cellval + "\t");
															if(cellval.contains("\n")) {
																String[] prelines = cellval.split("\n", -1);
																String[] lines = Arrays.stream(prelines).filter(value -> value  != null && value.length() > 0).toArray(size -> new String[size]);
																if(lines.length > 0) {
																	int cntStr = lines.length;
																	for (String str : lines) {
																		if(str == null || str.isEmpty() || str.trim().isEmpty()) {
//																			System.out.println("empty value" + cntStr + "\t");
																		} else {
																			formula = new presentation_formula();
//																			System.out.println(formulaCnt + "\t");
//																			System.out.println(str + "!\t");
																			if(formulaList.isEmpty() || x == 9) {
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else if(x == 9){
//																				cntStr = formulaList.size();
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else {
																				if(formulaCnt - cntStr < 0) {
																					formula = formulaList.get(0);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(0, formula);
																					cntStr--;
//																					formulaCnt++;
																				} else {
																					formula = formulaList.get(formulaCnt - cntStr);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(formulaCnt - cntStr, formula);
																					cntStr--;
//																					formulaCnt++;
																				}
																			}
																		}
																	}
//																	String str = lines[1];
//																	str = str.replaceAll(" & ", "/");
//																	form.setFormName(str);
																}
															} else {
																if(formulaList.isEmpty() && x == 9 && val != null) {
																	int cntStr = 0;
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(x == 9 && val != null){
																	int cntStr = formulaList.size();
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(val != null) {
																	int cntStr = formulaCnt - 1;
																	formula = formulaList.get(cntStr);
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.set(cntStr, formula);
//																	formulaCnt++;
																}
															}
														}
													}
												}

											} else if (ind == 2) {
												if (prevInd == 1) {
													// enum1.addChild(enum2);
												} else if (prevInd == 2) {
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
												} else if (prevInd == 3) {
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 5) {
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 6) {
													enum6.setParent(enum5);
													enum5.getChildren().add(enum6);
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 7) {
													enum7.setParent(enum6);
													enum6.getChildren().add(enum7);
													enum6.setParent(enum5);
													enum5.getChildren().add(enum6);
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												}

												enum2 = new presentation_detail();

												String rowDataItem = "";
												String rowFieldName = "";
												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														val = Strings.trimToNull(val);
														enum2 = pdService.tableMapping(enum2, x, val);
														if(x == 1) {
															rowFieldName = val;
														}
														if(x == 8) {
															rowDataItem = val;
														}
														if(x > 8) {
															presentation_formula formula = new presentation_formula();
															String cellval = dataFormatter.formatCellValue(cell);
//															System.out.println(cellval + "\t");
															if(cellval.contains("\n")) {
																String[] prelines = cellval.split("\n", -1);
																String[] lines = Arrays.stream(prelines).filter(value -> value  != null && value.length() > 0).toArray(size -> new String[size]);
																if(lines.length > 0) {
																	int cntStr = lines.length;
																	for (String str : lines) {
																		if(str == null || str.isEmpty() || str.trim().isEmpty()) {
//																			System.out.println("empty value" + cntStr + "\t");
																		} else {
																			formula = new presentation_formula();
//																			System.out.println(formulaCnt + "\t");
//																			System.out.println(str + "!\t");
																			if(formulaList.isEmpty() || x == 9) {
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else if(x == 9){
//																				cntStr = formulaList.size();
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else {
																				if(formulaCnt - cntStr < 0) {
																					formula = formulaList.get(0);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(0, formula);
																					cntStr--;
//																					formulaCnt++;
																				} else {
																					formula = formulaList.get(formulaCnt - cntStr);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(formulaCnt - cntStr, formula);
																					cntStr--;
//																					formulaCnt++;
																				}
																			}
																		}
																	}
//																	String str = lines[1];
//																	str = str.replaceAll(" & ", "/");
//																	form.setFormName(str);
																}
															} else {
																if(formulaList.isEmpty() && x == 9 && val != null) {
																	int cntStr = 0;
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(x == 9 && val != null){
																	int cntStr = formulaList.size();
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(val != null) {
																	int cntStr = formulaCnt - 1;
																	formula = formulaList.get(cntStr);
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.set(cntStr, formula);
//																	formulaCnt++;
																}
															}
														}
													}
												}

											} else if (ind == 3) {
												if (prevInd == 2) {
													// enum1.addChild(enum2);
												} else if (prevInd == 3) {
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);

												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
												} else if (prevInd == 5) {
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 6) {
													enum6.setParent(enum5);
													enum5.getChildren().add(enum6);
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 7) {
													enum7.setParent(enum6);
													enum6.getChildren().add(enum7);
													enum6.setParent(enum5);
													enum5.getChildren().add(enum6);
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												}

												enum3 = new presentation_detail();

												String rowDataItem = "";
												String rowFieldName = "";
												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														val = Strings.trimToNull(val);
														enum3 = pdService.tableMapping(enum3, x, val);
														if(x == 1) {
															rowFieldName = val;
														}
														if(x == 8) {
															rowDataItem = val;
														}
														if(x > 8) {
															presentation_formula formula = new presentation_formula();
															String cellval = dataFormatter.formatCellValue(cell);
//															System.out.println(cellval + "\t");
															if(cellval.contains("\n")) {
																String[] prelines = cellval.split("\n", -1);
																String[] lines = Arrays.stream(prelines).filter(value -> value  != null && value.length() > 0).toArray(size -> new String[size]);
																if(lines.length > 0) {
																	int cntStr = lines.length;
																	for (String str : lines) {
																		if(str == null || str.isEmpty() || str.trim().isEmpty()) {
//																			System.out.println("empty value" + cntStr + "\t");
																		} else {
																			formula = new presentation_formula();
//																			System.out.println(formulaCnt + "\t");
//																			System.out.println(str + "!\t");
																			if(formulaList.isEmpty() || x == 9) {
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else if(x == 9){
//																				cntStr = formulaList.size();
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else {
																				if(formulaCnt - cntStr < 0) {
																					formula = formulaList.get(0);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(0, formula);
																					cntStr--;
//																					formulaCnt++;
																				} else {
																					formula = formulaList.get(formulaCnt - cntStr);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(formulaCnt - cntStr, formula);
																					cntStr--;
//																					formulaCnt++;
																				}
																			}
																		}
																	}
//																	String str = lines[1];
//																	str = str.replaceAll(" & ", "/");
//																	form.setFormName(str);
																}
															} else {
																if(formulaList.isEmpty() && x == 9 && val != null) {
																	int cntStr = 0;
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(x == 9 && val != null){
																	int cntStr = formulaList.size();
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(val != null) {
																	int cntStr = formulaCnt - 1;
																	formula = formulaList.get(cntStr);
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.set(cntStr, formula);
//																	formulaCnt++;
																}
															}
														}
													}
												}
											} else if (ind == 4) {
												if (prevInd == 2) {

												} else if (prevInd == 3) {

												} else if (prevInd == 4) {
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
												} else if (prevInd == 5) {
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 6) {
													enum6.setParent(enum5);
													enum5.getChildren().add(enum6);
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 7) {
													enum7.setParent(enum6);
													enum6.getChildren().add(enum7);
													enum6.setParent(enum5);
													enum5.getChildren().add(enum6);
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												}

												enum4 = new presentation_detail();

												String rowDataItem = "";
												String rowFieldName = "";
												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														val = Strings.trimToNull(val);
														enum4 = pdService.tableMapping(enum4, x, val);
														if(x == 1) {
															rowFieldName = val;
														}
														if(x == 8) {
															rowDataItem = val;
														}
														if(x > 8) {
															presentation_formula formula = new presentation_formula();
															String cellval = dataFormatter.formatCellValue(cell);
//															System.out.println(cellval + "\t");
															if(cellval.contains("\n")) {
																String[] prelines = cellval.split("\n", -1);
																String[] lines = Arrays.stream(prelines).filter(value -> value  != null && value.length() > 0).toArray(size -> new String[size]);
																if(lines.length > 0) {
																	int cntStr = lines.length;
																	for (String str : lines) {
																		if(str == null || str.isEmpty() || str.trim().isEmpty()) {
//																			System.out.println("empty value" + cntStr + "\t");
																		} else {
																			formula = new presentation_formula();
//																			System.out.println(formulaCnt + "\t");
//																			System.out.println(str + "!\t");
																			if(formulaList.isEmpty() || x == 9) {
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else if(x == 9){
//																				cntStr = formulaList.size();
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else {
																				if(formulaCnt - cntStr < 0) {
																					formula = formulaList.get(0);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(0, formula);
																					cntStr--;
//																					formulaCnt++;
																				} else {
																					formula = formulaList.get(formulaCnt - cntStr);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(formulaCnt - cntStr, formula);
																					cntStr--;
//																					formulaCnt++;
																				}
																			}
																		}
																	}
//																	String str = lines[1];
//																	str = str.replaceAll(" & ", "/");
//																	form.setFormName(str);
																}
															} else {
																if(formulaList.isEmpty() && x == 9 && val != null) {
																	int cntStr = 0;
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(x == 9 && val != null){
																	int cntStr = formulaList.size();
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(val != null) {
																	int cntStr = formulaCnt - 1;
																	formula = formulaList.get(cntStr);
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.set(cntStr, formula);
//																	formulaCnt++;
																}
															}
														}
													}
												}
											} else if (ind == 5) {
												if (prevInd == 2) {

												} else if (prevInd == 3) {

												} else if (prevInd == 4) {
													
												} else if (prevInd == 5) {
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													
												} else if (prevInd == 6) {
													enum6.setParent(enum5);
													enum5.getChildren().add(enum6);
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												} else if (prevInd == 7) {
													enum7.setParent(enum6);
													enum6.getChildren().add(enum7);
													enum6.setParent(enum5);
													enum5.getChildren().add(enum6);
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												}

												enum5 = new presentation_detail();

												String rowDataItem = "";
												String rowFieldName = "";
												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														val = Strings.trimToNull(val);
														enum5 = pdService.tableMapping(enum5, x, val);
														if(x == 1) {
															rowFieldName = val;
														}
														if(x == 8) {
															rowDataItem = val;
														}
														if(x > 8) {
															presentation_formula formula = new presentation_formula();
															String cellval = dataFormatter.formatCellValue(cell);
//															System.out.println(cellval + "\t");
															if(cellval.contains("\n")) {
																String[] prelines = cellval.split("\n", -1);
																String[] lines = Arrays.stream(prelines).filter(value -> value  != null && value.length() > 0).toArray(size -> new String[size]);
																if(lines.length > 0) {
																	int cntStr = lines.length;
																	for (String str : lines) {
																		if(str == null || str.isEmpty() || str.trim().isEmpty()) {
//																			System.out.println("empty value" + cntStr + "\t");
																		} else {
																			formula = new presentation_formula();
//																			System.out.println(formulaCnt + "\t");
//																			System.out.println(str + "!\t");
																			if(formulaList.isEmpty() || x == 9) {
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else if(x == 9){
//																				cntStr = formulaList.size();
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else {
																				if(formulaCnt - cntStr < 0) {
																					formula = formulaList.get(0);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(0, formula);
																					cntStr--;
//																					formulaCnt++;
																				} else {
																					formula = formulaList.get(formulaCnt - cntStr);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(formulaCnt - cntStr, formula);
																					cntStr--;
//																					formulaCnt++;
																				}
																			}
																		}
																	}
//																	String str = lines[1];
//																	str = str.replaceAll(" & ", "/");
//																	form.setFormName(str);
																}
															} else {
																if(formulaList.isEmpty() && x == 9 && val != null) {
																	int cntStr = 0;
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(x == 9 && val != null){
																	int cntStr = formulaList.size();
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(val != null) {
																	int cntStr = formulaCnt - 1;
																	formula = formulaList.get(cntStr);
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.set(cntStr, formula);
//																	formulaCnt++;
																}
															}
														}
													}
												}
											} else if (ind == 6) {
												if (prevInd == 5) {
													
												} else if (prevInd == 6) {
													enum6.setParent(enum5);
													enum5.getChildren().add(enum6);
													
												} else if (prevInd == 7) {
													enum7.setParent(enum6);
													enum6.getChildren().add(enum7);
													enum6.setParent(enum5);
													enum5.getChildren().add(enum6);
													enum5.setParent(enum4);
													enum4.getChildren().add(enum5);
													enum4.setParent(enum3);
													enum3.getChildren().add(enum4);
													enum3.setParent(enum2);
													enum2.getChildren().add(enum3);
													enum2.setParent(enum1);
													enum1.getChildren().add(enum2);
													enum1.setParent(enum0);
													enum0.getChildren().add(enum1);
												}

												enum6 = new presentation_detail();

												String rowDataItem = "";
												String rowFieldName = "";
												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														val = Strings.trimToNull(val);
														enum6 = pdService.tableMapping(enum6, x, val);
														if(x == 1) {
															rowFieldName = val;
														}
														if(x == 8) {
															rowDataItem = val;
														}
														if(x > 8) {
															presentation_formula formula = new presentation_formula();
															String cellval = dataFormatter.formatCellValue(cell);
//															System.out.println(cellval + "\t");
															if(cellval.contains("\n")) {
																String[] prelines = cellval.split("\n", -1);
																String[] lines = Arrays.stream(prelines).filter(value -> value  != null && value.length() > 0).toArray(size -> new String[size]);
																if(lines.length > 0) {
																	int cntStr = lines.length;
																	for (String str : lines) {
																		if(str == null || str.isEmpty() || str.trim().isEmpty()) {
//																			System.out.println("empty value" + cntStr + "\t");
																		} else {
																			formula = new presentation_formula();
//																			System.out.println(formulaCnt + "\t");
//																			System.out.println(str + "!\t");
																			if(formulaList.isEmpty() || x == 9) {
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else if(x == 9){
//																				cntStr = formulaList.size();
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else {
																				if(formulaCnt - cntStr < 0) {
																					formula = formulaList.get(0);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(0, formula);
																					cntStr--;
//																					formulaCnt++;
																				} else {
																					formula = formulaList.get(formulaCnt - cntStr);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(formulaCnt - cntStr, formula);
																					cntStr--;
//																					formulaCnt++;
																				}
																			}
																		}
																	}
//																	String str = lines[1];
//																	str = str.replaceAll(" & ", "/");
//																	form.setFormName(str);
																}
															} else {
																if(formulaList.isEmpty() && x == 9 && val != null) {
																	int cntStr = 0;
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(x == 9 && val != null){
																	int cntStr = formulaList.size();
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(val != null) {
																	int cntStr = formulaCnt - 1;
																	formula = formulaList.get(cntStr);
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.set(cntStr, formula);
//																	formulaCnt++;
																}
															}
														}
													}
												}
											} else if (ind == 7) {
												if (prevInd == 5) {
													
												} else if (prevInd == 6) {
													
												} else if (prevInd == 7) {
													enum7.setParent(enum6);
													enum6.getChildren().add(enum7);
												}

												enum7 = new presentation_detail();

												String rowDataItem = "";
												String rowFieldName = "";
												for (x = 0; x < cntHeader; x++) {
													cell = row.getCell(x);
													if (cell != null) {
														val = getCellValue(cell);
														val = Strings.trimToNull(val);
														enum7 = pdService.tableMapping(enum7, x, val);
														if(x == 1) {
															rowFieldName = val;
														}
														if(x == 8) {
															rowDataItem = val;
														}
														if(x > 8) {
															presentation_formula formula = new presentation_formula();
															String cellval = dataFormatter.formatCellValue(cell);
//															System.out.println(cellval + "\t");
															if(cellval.contains("\n")) {
																String[] prelines = cellval.split("\n", -1);
																String[] lines = Arrays.stream(prelines).filter(value -> value  != null && value.length() > 0).toArray(size -> new String[size]);
																if(lines.length > 0) {
																	int cntStr = lines.length;
																	for (String str : lines) {
																		if(str == null || str.isEmpty() || str.trim().isEmpty()) {
//																			System.out.println("empty value" + cntStr + "\t");
																		} else {
																			formula = new presentation_formula();
//																			System.out.println(formulaCnt + "\t");
//																			System.out.println(str + "!\t");
																			if(formulaList.isEmpty() || x == 9) {
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else if(x == 9){
//																				cntStr = formulaList.size();
																				formula = pdService.tableMappingFormula(formula, x, str);
																				formula.setValue_assertion(""+formulaCnt);
																				formula.setDataItem(rowDataItem);
																				formula.setFieldName(rowFieldName);
																				formulaList.add(formula);
																				cntStr--;
																				formulaCnt++;
																			} else {
																				if(formulaCnt - cntStr < 0) {
																					formula = formulaList.get(0);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(0, formula);
																					cntStr--;
//																					formulaCnt++;
																				} else {
																					formula = formulaList.get(formulaCnt - cntStr);
																					formula = pdService.tableMappingFormula(formula, x, str);
																					formula.setDataItem(rowDataItem);
																					formula.setFieldName(rowFieldName);
																					formulaList.set(formulaCnt - cntStr, formula);
																					cntStr--;
//																					formulaCnt++;
																				}
																			}
																		}
																	}
//																	String str = lines[1];
//																	str = str.replaceAll(" & ", "/");
//																	form.setFormName(str);
																}
															} else {
																if(formulaList.isEmpty() && x == 9 && val != null) {
																	int cntStr = 0;
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(x == 9 && val != null){
																	int cntStr = formulaList.size();
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setValue_assertion(""+formulaCnt);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.add(formula);
																	formulaCnt++;
																} else if(val != null) {
																	int cntStr = formulaCnt - 1;
																	formula = formulaList.get(cntStr);
																	formula = pdService.tableMappingFormula(formula, x, val);
																	formula.setDataItem(rowDataItem);
																	formula.setFieldName(rowFieldName);
																	formulaList.set(cntStr, formula);
//																	formulaCnt++;
																}
															}
														}
													}
												}
											} else {

											}
											prevInd = ind;
										} else {
											if (prevInd == 0) {
												if (enum0.getPrefix() != null) {
													presentationList.add(enum0);
												}
											} else if (prevInd == 1) {
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												presentationList.add(enum0);
												enum1 = new presentation_detail();
											} else if (prevInd == 2) {
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												presentationList.add(enum0);
											} else if (prevInd == 3) {
												enum3.setParent(enum2);
												enum2.getChildren().add(enum3);
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												presentationList.add(enum0);
											} else if (prevInd == 4) {
												enum4.setParent(enum3);
												enum3.getChildren().add(enum4);
												enum3.setParent(enum2);
												enum2.getChildren().add(enum3);
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												presentationList.add(enum0);
											} else if (prevInd == 5) {
												enum5.setParent(enum4);
												enum4.getChildren().add(enum5);
												enum4.setParent(enum3);
												enum3.getChildren().add(enum4);
												enum3.setParent(enum2);
												enum2.getChildren().add(enum3);
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												presentationList.add(enum0);
											} else if (prevInd == 6) {
												enum6.setParent(enum5);
												enum5.getChildren().add(enum6);
												enum5.setParent(enum4);
												enum4.getChildren().add(enum5);
												enum4.setParent(enum3);
												enum3.getChildren().add(enum4);
												enum3.setParent(enum2);
												enum2.getChildren().add(enum3);
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												presentationList.add(enum0);
											} else if (prevInd == 7) {
												enum7.setParent(enum6);
												enum6.getChildren().add(enum7);
												enum6.setParent(enum5);
												enum5.getChildren().add(enum6);
												enum5.setParent(enum4);
												enum4.getChildren().add(enum5);
												enum4.setParent(enum3);
												enum3.getChildren().add(enum4);
												enum3.setParent(enum2);
												enum2.getChildren().add(enum3);
												enum2.setParent(enum1);
												enum1.getChildren().add(enum2);
												enum1.setParent(enum0);
												enum0.getChildren().add(enum1);
												presentationList.add(enum0);
											}
											if (enum0.getPrefix() != null) {
												presentationList.add(enum0);
											}

											prevInd = ind;
											break;
										}

									} else {
										if (prevInd == 0) {
											if (enum0.getPrefix() != null) {
												presentationList.add(enum0);
											}
										} else if (prevInd == 1) {
											enum1.setParent(enum0);
											enum0.getChildren().add(enum1);
											presentationList.add(enum0);
											enum1 = new presentation_detail();
										} else if (prevInd == 2) {
											enum2.setParent(enum1);
											enum1.getChildren().add(enum2);
											enum1.setParent(enum0);
											enum0.getChildren().add(enum1);
											presentationList.add(enum0);
										} else if (prevInd == 3) {
											enum3.setParent(enum2);
											enum2.getChildren().add(enum3);
											enum2.setParent(enum1);
											enum1.getChildren().add(enum2);
											enum1.setParent(enum0);
											enum0.getChildren().add(enum1);
											presentationList.add(enum0);
										} else if (prevInd == 4) {
											enum4.setParent(enum3);
											enum3.getChildren().add(enum4);
											enum3.setParent(enum2);
											enum2.getChildren().add(enum3);
											enum2.setParent(enum1);
											enum1.getChildren().add(enum2);
											enum1.setParent(enum0);
											enum0.getChildren().add(enum1);
											presentationList.add(enum0);
										} else if (prevInd == 5) {
											enum5.setParent(enum4);
											enum4.getChildren().add(enum5);
											enum4.setParent(enum3);
											enum3.getChildren().add(enum4);
											enum3.setParent(enum2);
											enum2.getChildren().add(enum3);
											enum2.setParent(enum1);
											enum1.getChildren().add(enum2);
											enum1.setParent(enum0);
											enum0.getChildren().add(enum1);
											presentationList.add(enum0);
										} else if (prevInd == 6) {
											enum6.setParent(enum5);
											enum5.getChildren().add(enum6);
											enum5.setParent(enum4);
											enum4.getChildren().add(enum5);
											enum4.setParent(enum3);
											enum3.getChildren().add(enum4);
											enum3.setParent(enum2);
											enum2.getChildren().add(enum3);
											enum2.setParent(enum1);
											enum1.getChildren().add(enum2);
											enum1.setParent(enum0);
											enum0.getChildren().add(enum1);
											presentationList.add(enum0);
										} else if (prevInd == 7) {
											enum7.setParent(enum6);
											enum6.getChildren().add(enum7);
											enum6.setParent(enum5);
											enum5.getChildren().add(enum6);
											enum5.setParent(enum4);
											enum4.getChildren().add(enum5);
											enum4.setParent(enum3);
											enum3.getChildren().add(enum4);
											enum3.setParent(enum2);
											enum2.getChildren().add(enum3);
											enum2.setParent(enum1);
											enum1.getChildren().add(enum2);
											enum1.setParent(enum0);
											enum0.getChildren().add(enum1);
											presentationList.add(enum0);
										}
										if (enum0.getPrefix() != null) {
											presentationList.add(enum0);
										}
										prevInd = ind;
										break;
									}
								}
								if (i == totalRow) {
									if (prevInd == 0) {
										if (enum0.getPrefix() != null) {
											presentationList.add(enum0);
										}
									} else if (prevInd == 1) {
										enum1.setParent(enum0);
										enum0.getChildren().add(enum1);
										presentationList.add(enum0);
										enum1 = new presentation_detail();
									} else if (prevInd == 2) {
										enum2.setParent(enum1);
										enum1.getChildren().add(enum2);
										enum1.setParent(enum0);
										enum0.getChildren().add(enum1);
										presentationList.add(enum0);
									} else if (prevInd == 3) {
										enum3.setParent(enum2);
										enum2.getChildren().add(enum3);
										enum2.setParent(enum1);
										enum1.getChildren().add(enum2);
										enum1.setParent(enum0);
										enum0.getChildren().add(enum1);
										presentationList.add(enum0);
									} else if (prevInd == 4) {
										enum4.setParent(enum3);
										enum3.getChildren().add(enum4);
										enum3.setParent(enum2);
										enum2.getChildren().add(enum3);
										enum2.setParent(enum1);
										enum1.getChildren().add(enum2);
										enum1.setParent(enum0);
										enum0.getChildren().add(enum1);
										presentationList.add(enum0);
									} else if (prevInd == 5) {
										enum5.setParent(enum4);
										enum4.getChildren().add(enum5);
										enum4.setParent(enum3);
										enum3.getChildren().add(enum4);
										enum3.setParent(enum2);
										enum2.getChildren().add(enum3);
										enum2.setParent(enum1);
										enum1.getChildren().add(enum2);
										enum1.setParent(enum0);
										enum0.getChildren().add(enum1);
										presentationList.add(enum0);
									} else if (prevInd == 6) {
										enum6.setParent(enum5);
										enum5.getChildren().add(enum6);
										enum5.setParent(enum4);
										enum4.getChildren().add(enum5);
										enum4.setParent(enum3);
										enum3.getChildren().add(enum4);
										enum3.setParent(enum2);
										enum2.getChildren().add(enum3);
										enum2.setParent(enum1);
										enum1.getChildren().add(enum2);
										enum1.setParent(enum0);
										enum0.getChildren().add(enum1);
										presentationList.add(enum0);
									} else if (prevInd == 7) {
										enum7.setParent(enum6);
										enum6.getChildren().add(enum7);
										enum6.setParent(enum5);
										enum5.getChildren().add(enum6);
										enum5.setParent(enum4);
										enum4.getChildren().add(enum5);
										enum4.setParent(enum3);
										enum3.getChildren().add(enum4);
										enum3.setParent(enum2);
										enum2.getChildren().add(enum3);
										enum2.setParent(enum1);
										enum1.getChildren().add(enum2);
										enum1.setParent(enum0);
										enum0.getChildren().add(enum1);
										presentationList.add(enum0);
									}
									if (enum0.getPrefix() != null) {
										presentationList.add(enum0);
									}
								}
								break;
							}

						}
					}
				}
			}
//			form.setPresentationList(presentationList);
//			pdService.save(presentationList);
			
//			String str = form.getDefinition();
//			String str2 = str.substring(str.indexOf(":")+1);
//			str2.trim();
//			System.out.println("Substring : " + str2);
			
			String str2 = TaxEnumSheet.getSheetName();
			tax_data_modal tdm = tdmService.getById(id);
			
			List<linkbase_form> linkbaseList = formService.findByModalId2(id);
			temp = linkbaseList.stream().filter(f -> str2.trim().equalsIgnoreCase(f.getNamespace())).findAny().orElse(null);
//			temp = formService.getByNamespace(str2.trim());
//			System.out.println("form: " + str2);
//			System.out.println("formula count: " + formulaList.size());
			if(temp != null && temp.getName() != null) {
				temp.setLinkrole(form.getLinkrole());
				temp.setName(form.getName());
				temp.setCategory(form.getCategory());
				temp.getChildren().clear();
				temp.getChildren().addAll(presentationList);
				temp.getFormulaChildren().clear();
				temp.getFormulaChildren().addAll(formulaList);
				formService.saveSingle(temp);
			} else {
				form.getChildren().clear();
				form.getChildren().addAll(presentationList);
				form.getFormulaChildren().clear();
				form.getFormulaChildren().addAll(formulaList);
				formService.saveSingle(form);
			}
			isCompleted = true;

		} catch (Exception e) {
			isCompleted = false;
			e.printStackTrace();
			throw new RuntimeException("fail to store excel data: " + e.getMessage());
		}

		return isCompleted;
	}

	/***
	 * Merge cell processing, get merged rows
	 * 
	 * @param sheet
	 * @return List<CellRangeAddress>
	 */
	public List<CellRangeAddress> getCombineCell(Sheet sheet) {
		List<CellRangeAddress> list = new ArrayList<CellRangeAddress>();
		// Get the number of merged cells in a sheet
		int sheetmergerCount = sheet.getNumMergedRegions();
		// Traverse all merged cells
		for (int i = 0; i < sheetmergerCount; i++) {
			// Get the merged cell and save it in the list
			CellRangeAddress ca = sheet.getMergedRegion(i);
			list.add(ca);
		}
		return list;
	}

	private int getRowNum(List<CellRangeAddress> listCombineCell, Cell cell, Sheet sheet) {
		int xr = 0;
		int firstC = 0;
		int lastC = 0;
		int firstR = 0;
		int lastR = 0;
		for (CellRangeAddress ca : listCombineCell) {
			// Get the start row, end row, start column, end column of the merged cell
			firstC = ca.getFirstColumn();
			lastC = ca.getLastColumn();
			firstR = ca.getFirstRow();
			lastR = ca.getLastRow();
			if (cell.getRowIndex() >= firstR && cell.getRowIndex() <= lastR) {
				if (cell.getColumnIndex() >= firstC && cell.getColumnIndex() <= lastC) {
					xr = lastR;
				}
			}

		}
		return xr;

	}

	private boolean isMergedRegion(Sheet sheet, int row, int column) {
		int sheetMergeCount = sheet.getNumMergedRegions();
		for (int i = 0; i < sheetMergeCount; i++) {
			CellRangeAddress range = sheet.getMergedRegion(i);
			int firstColumn = range.getFirstColumn();
			int lastColumn = range.getLastColumn();
			int firstRow = range.getFirstRow();
			int lastRow = range.getLastRow();
			if (row >= firstRow && row <= lastRow) {
				if (column >= firstColumn && column <= lastColumn) {
					return true;
				}
			}
		}
		return false;
	}

	private String getCellValue(Cell cell) {
		String val = "";

		if (cell.getCellType() == CellType.FORMULA) {
			switch (cell.getCachedFormulaResultType()) {
			case BOOLEAN:
//	            System.out.print("boolean: " + cell.getBooleanCellValue() + "\t");
				val = "" + cell.getBooleanCellValue();
				break;
			case NUMERIC:
//	            System.out.print("numeric: " + cell.getNumericCellValue() + "\t");
				val = "" + cell.getNumericCellValue();
				break;
			case STRING:
//	            System.out.print("string: " + cell.getRichStringCellValue() + "\t");
				val = "" + cell.getRichStringCellValue();
				break;
			default:
				break;
			}
		} else {
			switch (cell.getCellType()) {
			case BOOLEAN:
//            System.out.print("boolean: " + cell.getBooleanCellValue() + "\t");
				val = "" + cell.getBooleanCellValue();
				break;
			case NUMERIC:
//            System.out.print("numeric: " + cell.getNumericCellValue() + "\t");
				val = "" + cell.getNumericCellValue();
				break;
			case STRING:
//            System.out.print("string: " + cell.getRichStringCellValue() + "\t");
				val = "" + cell.getRichStringCellValue();
				break;
			default:
				break;
			}
//		String cellValue = dataFormatter.formatCellValue(cell);
//        System.out.print(cellValue + "\t");
		}

		return val;
	}

	public boolean hasExcelFormat(MultipartFile file) {

	    if (!TYPE.equals(file.getContentType())) {
	      return false;
	    }

	    return true;
	  }

	public Long saveFile(String fileName, MultipartFile multipartFile) throws IOException {
        Long id = 0L;
		Path uploadPath = Paths.get(SAMPLE_MODEL_FOLDER);
        LocalDateTime now = LocalDateTime.now(); 
        String dtString = dtf.format(now);
        String Fname = dtString + "_" + fileName;
        Date currDate = new Date(System.currentTimeMillis());
        
        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
          
        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(Fname);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
            tax_data_modal tdm = new tax_data_modal();
            tdm.setSize((double) (multipartFile.getSize() / 1024));
			tdm.setFilename(Fname);
			tdm.setUploadDate(currDate);
			tdmService.saveSingle(tdm);
			id = tdm.getId();
			
        } catch (IOException ioe) {       
            throw new IOException("Could not save image file: " + fileName, ioe);
        }
        return id;
    }
	
}

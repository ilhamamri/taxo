package com.taxoDataModel.taxo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.taxoDataModel.taxo.model.tax_data_modal;
import com.taxoDataModel.taxo.repository.tax_data_modalRepository;

@Service
public class taxDataModalService {

	@Autowired
	tax_data_modalRepository repository;
	
	public void save(List<tax_data_modal> tList) {
		try {
			repository.saveAll(tList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveSingle(tax_data_modal data) {
		try {
			repository.save(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<tax_data_modal> getAll(){
		List<tax_data_modal> tList = new ArrayList<tax_data_modal>();
		try {
			tList = repository.findAll(Sort.by(Sort.Direction.ASC, "id"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tList;
	}
	
	public tax_data_modal getById(Long id) {
		return repository.findById(id).get();
	}
	
	public tax_data_modal getByFilename(String filename) {
		return repository.findByFilename(filename);
	}
}

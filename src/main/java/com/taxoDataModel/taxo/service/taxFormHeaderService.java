package com.taxoDataModel.taxo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.taxoDataModel.taxo.model.tax_form_header;
import com.taxoDataModel.taxo.repository.tax_form_headerRepository;

@Service
public class taxFormHeaderService {

	@Autowired
	tax_form_headerRepository repository;
	
	public void save(List<tax_form_header> tList) {
		try {
			repository.saveAll(tList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveSingle(tax_form_header header) {
		try {
			repository.save(header);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<tax_form_header> getAll(){
		return repository.findByParentIdIsNull();
	}
	
	public tax_form_header tableMapping(tax_form_header preObj, int index, String obj) {
		tax_form_header tEnum = preObj;
		
		switch (index) {
		case 0:
			tEnum.setPrefix(obj);
			break;
		case 1:
			tEnum.setName(obj);
			break;
		case 2:
			tEnum.setLabel_en(obj);
			break;
		case 3:
			tEnum.setLabel_ms(obj);
			break;
		case 4:
			tEnum.setLabel_en_form(obj);
			break;
		case 5:
			tEnum.setLabel_ms_form(obj);
			break;
		default:
		}
		
		return tEnum;
	}
}

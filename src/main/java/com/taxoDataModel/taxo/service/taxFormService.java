package com.taxoDataModel.taxo.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.taxoDataModel.taxo.model.linkbase_form;
import com.taxoDataModel.taxo.model.presentation_detail;
import com.taxoDataModel.taxo.model.presentation_formula;
import com.taxoDataModel.taxo.model.tax_concept;
import com.taxoDataModel.taxo.model.tax_data_modal;
import com.taxoDataModel.taxo.model.tax_enum;
import com.taxoDataModel.taxo.model.tax_form;
import com.taxoDataModel.taxo.model.tax_form_header;
import com.taxoDataModel.taxo.model.tax_form_linkbase;
import com.taxoDataModel.taxo.repository.tax_formRepository;

@Service
public class taxFormService {

	@Autowired
	tax_formRepository repository;

	@Autowired
	linkbaseFormService formService;
	
	@Autowired
	taxEnumService enumService;
	
	@Autowired
	taxConceptService conceptService;

	public void save(List<tax_form> tList) {
		try {
			repository.saveAll(tList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveSingle(tax_form form) {
		try {
			repository.save(form);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<tax_form> getAll() {
		List<tax_form> tList = new ArrayList<tax_form>();
		try {
			tList = repository.findAll(Sort.by(Sort.Direction.ASC, "id"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tList;
	}

	public tax_form getByColumnIndex(Long ind) {
		return repository.findByColumnIndex(ind);
	}

	public tax_form getByName(String name) {
		return repository.findByFormName(name);
	}

	public boolean writeTaxonomyPackage(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		List<tax_form> tfList = modal.getTaxFormList();
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/taxonomyPackage.xml"))) {
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			writer.newLine();
			writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			writer.newLine();
			writer.write("<tp:taxonomyPackage xml:lang=\"en\" xmlns:tp=\"http://xbrl.org/2016/taxonomy-package\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://xbrl.org/2016/taxonomy-package http://www.xbrl.org/2016/taxonomy-package.xsd\">");
			writer.newLine();
			writer.write("    <tp:identifier>http://www.hasil.gov.my/xbrl/lhdnm_" + dtString + ".zip</tp:identifier>");
			writer.newLine();
			writer.write("    <tp:name>LHDNM XBRL Taxonomy</tp:name>");
			writer.newLine();
			writer.write("    <tp:version>1.0</tp:version>");
			writer.newLine();
			writer.write("    <tp:publisher>Lembaga Hasil Dalam Negeri Malaysia</tp:publisher>");
			writer.newLine();
			writer.write("    <tp:publisherURL>http://www.hasil.gov.my/</tp:publisherURL>");
			writer.newLine();
			writer.write("    <tp:publicationDate>" + dtString + "</tp:publicationDate>");
			writer.newLine();
			writer.write("    <tp:entryPoints>");
			
			for (tax_form form : tfList) {
				String fname = "";
				if(form.getFormName() != null && !form.getFormName().isEmpty()) {
					if (form.getName() != null) {
						fname = form.getName();
						fname = fname.replaceAll("-fi", "");
						fname = fname.replaceAll("/", "-");
						fname = "form_" + fname;
					} else if (form.getFormName() != null) {
						fname = form.getFormName().toLowerCase();
						fname = fname.replaceAll(" ", "_");
						fname = fname.replaceAll("/", "-");
//						if(form.getFormName().contains("Form")) {
//							fname = "form_" + fname;
//						}
					} else {
						fname = form.getTemplateName().toLowerCase();
						fname = fname.replaceAll(" ", "_");
					}
					String formName = "/lhdnm-" + fname + "-full_entry_point_" + dtString + ".xsd";
					writer.newLine();
					writer.write("		<tp:entryPoint>");
					writer.newLine();
					writer.write("			<tp:name>" + fname.toUpperCase() + "</tp:name>");
					writer.newLine();
					writer.write("			<tp:description>" + form.getFormName() + "</tp:description>");
					writer.newLine();
					if(fname.contains("sme")) {
						writer.write("			<tp:entryPointDocument href=\"http://www.hasil.gov.my/xbrl/" + dtString + "/rep/lhdnm/tax_sme" + formName + "\"/>");
					} else {
						writer.write("			<tp:entryPointDocument href=\"http://www.hasil.gov.my/xbrl/" + dtString + "/rep/lhdnm/tax" + formName + "\"/>");
					}
					
					writer.newLine();
					writer.write("		</tp:entryPoint>");
				}
			}
			
			writer.newLine();
			writer.write("    </tp:entryPoints>");
			writer.newLine();
			writer.write("</tp:taxonomyPackage>");
			isCompleted = true;
			
		} catch (Exception e) {
			isCompleted = false;
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		
		return isCompleted;
	}
	
	public boolean writeTaxFormEntry(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
//		List<tax_form> tfList = getAll();
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());

		for (tax_form form : tfList) {
			String fname = "";
			String lname = "";
			if(form.getTemplateName() != null && !form.getTemplateName().isEmpty()) {
				if (form.getName() != null) {
					fname = form.getName();
					fname = fname.replaceAll("-fi", "");
					fname = fname.replaceAll("/", "-");
					lname = fname;
					fname = "form_" + fname;
				} else if (form.getFormName() != null) {
					fname = form.getFormName().toLowerCase();
					lname = fname.replaceAll("form", "");
					fname = fname.replaceAll(" ", "_");
					fname = fname.replaceAll("/", "-");
					lname = lname.replaceAll(" ", "");
//					fname = "form" + fname;
					
				} else {
					fname = form.getTemplateName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
				}
				String formName = "lhdnm-" + fname + "-full_entry_point";
				try (BufferedWriter writer = new BufferedWriter(
						new FileWriter(path + "/" +formName + "_" + dtString + ".xsd"))) {

					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
					writer.newLine();
					writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
					writer.newLine();
					writer.write("<xsd:schema targetNamespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/"
							+ lname
							+ "\" attributeFormDefault=\"unqualified\" elementFormDefault=\"qualified\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:"
							+ lname + "=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + lname
							+ "\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:lhdnm-fdn=\"http://www.hasil.gov.my/xbrl/"
							+ dtString + "/fdn\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
					writer.newLine();
					writer.write("  <xsd:annotation>");
					writer.newLine();
					writer.write("    <xsd:appinfo>");
					writer.newLine();
					writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"labels/gla_" + formName + "-en_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
					writer.newLine();
					writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"labels/gla_" + formName + "-ms_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
					writer.newLine();
					writer.write("    </xsd:appinfo>");
					writer.newLine();
					writer.write("  </xsd:annotation>");
					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/dimensions\" schemaLocation=\"dimensions/lhdnm-dim_" + dtString + ".xsd\"/>");
					writer.newLine();
					if(form.getName() != null) {
						writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + form.getName() + "\" schemaLocation=\"linkbases/lhdnm-" + form.getName() + "_" + dtString + ".xsd\"/>");
					} else {
						writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/c-fi\" schemaLocation=\"linkbases/lhdnm-c-fi_" + dtString + ".xsd\"/>");
					}					

					if (!form.getLinkbaseList().isEmpty()) {
						for (tax_form_linkbase lbase : form.getLinkbaseList()) {
							linkbase_form linkbaseF = formService.getSingle(lbase.getLinkbase_form_id());
							if (!Objects.isNull(linkbaseF) && linkbaseF.getCategory() != null) {
								writer.newLine();
								writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + linkbaseF.getName() + "\" schemaLocation=\"linkbases/lhdnm-" + linkbaseF.getName() + "_" + dtString + ".xsd\"/>");								
							}
						}
					}
					
					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/label\" schemaLocation=\"http://www.xbrl.org/2008/generic-label.xsd\"/>");
					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/reference\" schemaLocation=\"http://www.xbrl.org/2008/generic-reference.xsd\"/>");
					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/generic\" schemaLocation=\"http://www.xbrl.org/2008/generic-link.xsd\"/>");

					writer.newLine();
					writer.write("</xsd:schema>");

					isCompleted = true;
				} catch (Exception e) {
					isCompleted = false;
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}
		}

		return isCompleted;
	}

	public boolean writeTaxFormLabelEn(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
//		List<tax_form> tfList = getAll();
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		
		for(tax_form form : tfList) {
			if(form.getFormName() != null) {
				String eName = "";
				String fname = "";
				if(form.getName() != null) {
					fname = form.getName();
//					fname = fname.replaceAll("-fi", "");
					eName = form.getName();
					eName = eName.replaceAll("-fi", "");
//					fname = "form_" + fname;
					eName = "form_" + eName;
				} else if(form.getFormName() != null) {
					fname = form.getFormName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getFormName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
//					if(form.getFormName().contains("Form")) {
////						fname = "form_" + fname;
//						eName = "form_" + eName;
//					}
				} else {
					fname = form.getTemplateName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getTemplateName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				}
				String entryName = "lhdnm-" + eName + "_" + dtString + "_full_entry_point";
				String formName = "/gla_lhdnm-" + eName + "-full_entry_point-en";
				
				try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + formName + "_" + dtString + ".xml"))) {
					
					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				    writer.newLine();
				    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
				    writer.newLine();
				    writer.write("<link:linkbase xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\">");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#standard-label\" />");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/link\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-link.xsd#standard-link-role\" />");
				    writer.newLine();
				    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/element-label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#element-label\" />");
				    writer.newLine();
				    writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"http://www.xbrl.org/2008/role/link\">");
				    
				    writer.newLine();
				    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../linkbases/lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\" xlink:label=\"roleType\" xlink:title=\"roleType\" />");
				    writer.newLine();
				    writer.write("    <label:label xlink:type=\"resource\" xlink:label=\"label\" xlink:role=\"http://www.xbrl.org/2008/role/label\" xlink:title=\"label\" xml:lang=\"en\">" + form.getDefinition() + "</label:label>");
				    writer.newLine();
				    writer.write("    <gen:arc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/element-label\" xlink:from=\"roleType\" xlink:to=\"label\" xlink:title=\"user-defined: roleType to label\" order=\"1.0\" />");
				    
				    if (!form.getLinkbaseList().isEmpty()) {
				    	int countLink = 0;
						for (tax_form_linkbase lbase : form.getLinkbaseList()) {
							linkbase_form linkbaseF = formService.getSingle(lbase.getLinkbase_form_id());
							if (!Objects.isNull(linkbaseF) && linkbaseF.getCategory() != null) {
								countLink++;
								String linkName = "";
								if (linkbaseF.getName() != null) {
									linkName = linkbaseF.getName().toLowerCase();
//									fname = fname.replaceAll(" ", "-");
//									fname = fname.replaceAll(".", "");
//									System.out.println("fname: " + fname);
								}
								String labelText = StringEscapeUtils.escapeXml(linkbaseF.getDefinition());
								writer.newLine();
							    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../linkbases/lhdnm-" + linkName + "_" + dtString + ".xsd#" + linkName + "\" xlink:label=\"roleType" + countLink + "\" xlink:title=\"roleType\" />");
								writer.newLine();
								writer.write("    <label:label xlink:type=\"resource\" xlink:label=\"label" + countLink + "\" xlink:role=\"http://www.xbrl.org/2008/role/label\" xlink:title=\"label\" xml:lang=\"en\">" + labelText + "</label:label>");
								writer.newLine();
								writer.write("    <gen:arc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/element-label\" xlink:from=\"roleType" + countLink + "\" xlink:to=\"label" + countLink + "\" xlink:title=\"user-defined: roleType to label\" order=\"1.0\" />");
							}
						}
					}
					
					writer.newLine();
				    writer.write("  </gen:link>");
				    writer.newLine();
				    writer.write("</link:linkbase>");
				    
					isCompleted = true;
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}
		}
		return isCompleted;
	}
	
	public boolean writeTaxFormLabelMs(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
//		List<tax_form> tfList = getAll();
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		
		for(tax_form form : tfList) {
			if(form.getFormName() != null) {
				String eName = "";
				String fname = "";
				if(form.getName() != null) {
					fname = form.getName();
//					fname = fname.replaceAll("-fi", "");
					eName = form.getName();
					eName = eName.replaceAll("-fi", "");
//					fname = "form_" + fname;
					eName = "form_" + eName;
				} else if(form.getFormName() != null) {
					fname = form.getFormName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getFormName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
//					if(form.getFormName().contains("Form")) {
////						fname = "form_" + fname;
//						eName = "form_" + eName;
//					}
				} else {
					fname = form.getTemplateName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getTemplateName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				}
				String entryName = "lhdnm-" + eName + "_" + dtString + "_full_entry_point";
				String formName = "/gla_lhdnm-" + eName + "-full_entry_point-ms";
				
				try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + formName + "_" + dtString + ".xml"))) {
					
					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				    writer.newLine();
				    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
				    writer.newLine();
				    writer.write("<link:linkbase xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\">");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#standard-label\" />");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/link\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-link.xsd#standard-link-role\" />");
				    writer.newLine();
				    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/element-label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#element-label\" />");
				    writer.newLine();
				    writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"http://www.xbrl.org/2008/role/link\">");
				    
				    writer.newLine();
				    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../linkbases/lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\" xlink:label=\"roleType\" xlink:title=\"roleType\" />");
				    writer.newLine();
				    writer.write("    <label:label xlink:type=\"resource\" xlink:label=\"label\" xlink:role=\"http://www.xbrl.org/2008/role/label\" xlink:title=\"label\" xml:lang=\"en\">" + form.getDefinition() + "</label:label>");
				    writer.newLine();
				    writer.write("    <gen:arc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/element-label\" xlink:from=\"roleType\" xlink:to=\"label\" xlink:title=\"user-defined: roleType to label\" order=\"1.0\" />");
				    
				    if (!form.getLinkbaseList().isEmpty()) {
				    	int countLink = 0;
						for (tax_form_linkbase lbase : form.getLinkbaseList()) {
							linkbase_form linkbaseF = formService.getSingle(lbase.getLinkbase_form_id());
							if (!Objects.isNull(linkbaseF) && linkbaseF.getCategory() != null) {
								countLink++;
								String linkName = "";
								if (linkbaseF.getName() != null) {
									linkName = linkbaseF.getName().toLowerCase();
//									fname = fname.replaceAll(" ", "-");
//									fname = fname.replaceAll(".", "");
//									System.out.println("fname: " + fname);
								}
								String labelText = StringEscapeUtils.escapeXml(linkbaseF.getDefinition_ms());
								writer.newLine();
							    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../linkbases/lhdnm-" + linkName + "_" + dtString + ".xsd#" + linkName + "\" xlink:label=\"roleType" + countLink + "\" xlink:title=\"roleType\" />");
								writer.newLine();
								writer.write("    <label:label xlink:type=\"resource\" xlink:label=\"label" + countLink + "\" xlink:role=\"http://www.xbrl.org/2008/role/label\" xlink:title=\"label\" xml:lang=\"en\">" + labelText + "</label:label>");
								writer.newLine();
								writer.write("    <gen:arc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/element-label\" xlink:from=\"roleType" + countLink + "\" xlink:to=\"label" + countLink + "\" xlink:title=\"user-defined: roleType to label\" order=\"1.0\" />");
							}
						}
					}
					
					writer.newLine();
				    writer.write("  </gen:link>");
				    writer.newLine();
				    writer.write("</link:linkbase>");
				    
					isCompleted = true;
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}
		}
		return isCompleted;
	}
	
	public boolean writeTaxFormLinkbase(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());

		for (tax_form form : tfList) {
			if (form.getLinkRole() != null) {
				String fname = "";
				if (form.getName() != null) {
					fname = form.getName();
//					fname = fname.replaceAll("-fi", "");
				} else if (form.getFormName() != null) {
					fname = form.getFormName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
				} else {
					fname = form.getTemplateName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
				}
				String formName = "/lhdnm-" + fname;
				try (BufferedWriter writer = new BufferedWriter(
						new FileWriter(path + formName + "_" + dtString + ".xsd"))) {

					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
					writer.newLine();
					writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
					writer.newLine();
					writer.write("<xsd:schema targetNamespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + fname
							+ "\" attributeFormDefault=\"unqualified\" elementFormDefault=\"qualified\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:"
							+ fname + "=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + fname
							+ "\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\">");
					writer.newLine();
					writer.write("  <xsd:annotation>");
					writer.newLine();
					writer.write("    <xsd:appinfo>");
					writer.newLine();
					writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"frm_lhdnm-" + fname + "_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
					writer.newLine();
					writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"frm_lab_lhdnm-" + fname + "-en_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
					writer.newLine();
					writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"frm_lab_lhdnm-" + fname + "-ms_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
					writer.newLine();
					writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"pre_lhdnm-" + fname + "_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/presentationLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
					writer.newLine();
					writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"def_lhdnm-" + fname + "_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/definitionLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
					writer.newLine();
					writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-" + fname + "-en_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
					writer.newLine();
					writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-" + fname + "-ms_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
					writer.newLine();
					writer.write("      <link:roleType roleURI=\"" + form.getLinkRole() + "\" id=\"" + fname + "\">");
					writer.newLine();
					writer.write("        <link:definition>" + form.getDefinition() + "</link:definition>");
					writer.newLine();
					writer.write("        <link:usedOn>link:presentationLink</link:usedOn>");
					writer.newLine();
					writer.write("        <link:usedOn>link:definitionLink</link:usedOn>");
					writer.newLine();
					writer.write("        <link:usedOn>link:labelLink</link:usedOn>");
					writer.newLine();
					writer.write("        <link:usedOn>gen:link</link:usedOn>");
					writer.newLine();
					writer.write("      </link:roleType>");
					writer.newLine();
					writer.write("    </xsd:appinfo>");
					writer.newLine();
					writer.write("  </xsd:annotation>");

					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString
							+ "/enum\" schemaLocation=\"../enumerations/lhdnm-enum_" + dtString + ".xsd\"/>");
					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString
							+ "\" schemaLocation=\"../../../../def/ic/lhdnm-cor_" + dtString + ".xsd\"/>");
					writer.newLine();
					writer.write(
							"  <xsd:import namespace=\"http://xbrl.org/2008/label\" schemaLocation=\"http://www.xbrl.org/2008/generic-label.xsd\"/>");
					writer.newLine();
					writer.write(
							"  <xsd:import namespace=\"http://xbrl.org/2008/reference\" schemaLocation=\"http://www.xbrl.org/2008/generic-reference.xsd\"/>");
					writer.newLine();
					writer.write(
							"  <xsd:import namespace=\"http://xbrl.org/2008/generic\" schemaLocation=\"http://www.xbrl.org/2008/generic-link.xsd\"/>");

					writer.newLine();
					writer.write("</xsd:schema>");
					
					isCompleted = true;

				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}

		}

		return isCompleted;
	}

	public boolean writeTaxFormHeaderDef(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		
		for(tax_form form1 : tfList) {
			tax_form form = repository.getOne(form1.getId());
			if(form.getLinkRole() != null) {
				String fname = "";
				if(form.getName() != null) {
					fname = form.getName();
//					fname = fname.replaceAll("-fi", "");
				} else if(form.getFormName() != null) {
					fname = form.getFormName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
				} else {
					fname = form.getTemplateName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
				}
				String formName = "/def_lhdnm-" + fname;
				try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + formName + "_" + dtString + ".xml"))) {
					
					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				    writer.newLine();
				    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
				    writer.newLine();
				    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-" + dtString + ".xsd\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\">");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"" + form.getLinkRole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
				    writer.newLine();
				    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/int/dim/arcrole/domain-member\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2005/xbrldt-2005.xsd#domain-member\"/>");
				    writer.newLine();
				    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/int/dim/arcrole/all\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2005/xbrldt-2005.xsd#all\"/>");
				    writer.newLine();
				    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/int/dim/arcrole/hypercube-dimension\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2005/xbrldt-2005.xsd#hypercube-dimension\"/>");
				    writer.newLine();
				    writer.write("  <link:definitionLink xlink:type=\"extended\" xlink:role=\"" + form.getLinkRole() + "\">");
				    
				    for(tax_form_header header : form.getFormHeaderList()) {
				    	String prefixName = header.getPrefix() + "_" + header.getName();
				    	String pfx = "lhdnm-cor";
				    	tax_enum te;
				    	tax_concept tconcept = conceptService.findByName(header.getName(), modal.getId());
				    	if(Objects.isNull(tconcept)) {
				    		te = enumService.findByName(header.getName());
					    	if(Objects.isNull(tconcept)) {
					    		continue;
					    	} else {
					    		pfx = "../enumerations/" + te.getPrefix();
					    		prefixName = te.getPrefix() + "_" + header.getName();
					    	}
				    	} else {
				    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
				    			pfx = "../enumerations/" + tconcept.getPrefix();
				    		} else {
				    			pfx = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
				    		}				    		
				    		prefixName = tconcept.getPrefix() + "_" + header.getName();
				    	}
				    	writer.newLine();
					    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx + "_" + dtString + ".xsd#" + prefixName + "\" xlink:label=\"" + header.getName() + "\" xlink:title=\"" + header.getName() + "\"/>");
					    if(!header.getChildList().isEmpty()) {
					    	int count1 = 0;
					    	for(tax_form_header header1 : header.getChildList()) {
					    		count1++;
					    		String prefixName1 = header1.getPrefix() + "_" + header1.getName();
					    		String pfx1 = "lhdnm-cor";
					    		tax_enum te1;
						    	tax_concept tconcept1 = conceptService.findByName(header1.getName(), modal.getId());
						    	if(Objects.isNull(tconcept1)) {
						    		te1 = enumService.findByName(header1.getName());
							    	if(Objects.isNull(tconcept1)) {
							    		continue;
							    	} else {
							    		pfx1 = "../enumerations/" + te1.getPrefix();
							    		prefixName1 = te1.getPrefix() + "_" + header1.getName();
							    	}
						    	} else {
						    		if(tconcept1.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
						    			pfx1 = "../enumerations/" + tconcept1.getPrefix();
						    		} else {
						    			pfx1 = "../../../../def/ic/" + tconcept1.getPrefix() + "-cor";
						    		}				    		
						    		prefixName1 = tconcept1.getPrefix() + "_" + header1.getName();
						    	}
					    		writer.newLine();
							    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx1 + "_" + dtString + ".xsd#" + prefixName1 + "\" xlink:label=\"" + header1.getName() + "\" xlink:title=\"" + header1.getName() + "\"/>");
							    writer.newLine();
							    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/int/dim/arcrole/all\" xlink:from=\"" + header.getName() + "\" xlink:to=\"" + header1.getName() + "\" xlink:title=\"definition: " + header.getName() + " to " + header1.getName() + "\" order=\"" + count1 + ".0\" xbrldt:contextElement=\"scenario\" xbrldt:closed=\"true\"/>");
							    if(!header1.getChildList().isEmpty()) {
							    	int count2 = 0;
							    	for(tax_form_header header2 : header1.getChildList()) {
							    		count2++;
							    		String prefixName2 = header2.getPrefix() + "_" + header2.getName();
							    		String pfx2 = "lhdnm-cor";
							    		tax_enum te2;
								    	tax_concept tconcept2 = conceptService.findByName(header2.getName(), modal.getId());
								    	if(Objects.isNull(tconcept2)) {
								    		te2 = enumService.findByName(header2.getName());
									    	if(Objects.isNull(tconcept2)) {
									    		continue;
									    	} else {
									    		pfx2 = "../enumerations/" + te2.getPrefix();
									    		prefixName2 = te2.getPrefix() + "_" + header2.getName();
									    	}
								    	} else {
								    		if(tconcept2.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
								    			pfx2 = "../enumerations/" + tconcept2.getPrefix();
								    		} else {
								    			pfx2 = "../../../../def/ic/" + tconcept2.getPrefix() + "-cor";
								    		}				    		
								    		prefixName2 = tconcept2.getPrefix() + "_" + header2.getName();
								    	}
							    		writer.newLine();
									    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx2 + "_" + dtString + ".xsd#" + prefixName2 + "\" xlink:label=\"" + header2.getName() + "\" xlink:title=\"" + header2.getName() + "\"/>");
									    writer.newLine();
									    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/int/dim/arcrole/all\" xlink:from=\"" + header1.getName() + "\" xlink:to=\"" + header2.getName() + "\" xlink:title=\"definition: " + header1.getName() + " to " + header2.getName() + "\" order=\"" + count2 + ".0\" xbrldt:contextElement=\"scenario\" xbrldt:closed=\"true\"/>");
									    if(!header2.getChildList().isEmpty()) {
									    	int count3 = 0;
									    	for(tax_form_header header3 : header2.getChildList()) {
									    		count3++;	
									    		String prefixName3 = header3.getPrefix() + "_" + header3.getName();
									    		String pfx3 = "lhdnm-cor";
									    		tax_enum te3;
										    	tax_concept tconcept3 = conceptService.findByName(header3.getName(), modal.getId());
										    	if(Objects.isNull(tconcept3)) {
										    		te3 = enumService.findByName(header3.getName());
											    	if(Objects.isNull(tconcept3)) {
											    		continue;
											    	} else {
											    		pfx3 = "../enumerations/" + te3.getPrefix();
											    		prefixName3 = te3.getPrefix() + "_" + header3.getName();
											    	}
										    	} else {
										    		if(tconcept3.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
										    			pfx3 = "../enumerations/" + tconcept3.getPrefix();
										    		} else {
										    			pfx3 = "../../../../def/ic/" + tconcept3.getPrefix() + "-cor";
										    		}				    		
										    		prefixName3 = tconcept3.getPrefix() + "_" + header3.getName();
										    	}
									    		writer.newLine();
									    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx3 + "_" + dtString + ".xsd#" + prefixName3 + "\" xlink:label=\"" + header3.getName() + "\" xlink:title=\"" + header3.getName() + "\"/>");
									    		writer.newLine();
									    		writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/int/dim/arcrole/all\" xlink:from=\"" + header2.getName() + "\" xlink:to=\"" + header3.getName() + "\" xlink:title=\"definition: " + header2.getName() + " to " + header3.getName() + "\" order=\"" + count3 + ".0\" xbrldt:contextElement=\"scenario\" xbrldt:closed=\"true\"/>");
									    		if(!header3.getChildList().isEmpty()) {
									    			int count4 = 0;
											    	for(tax_form_header header4 : header3.getChildList()) {
											    		count4++;	
											    		String prefixName4 = header4.getPrefix() + "_" + header4.getName();
											    		String pfx4 = "lhdnm-cor";
											    		tax_enum te4;
												    	tax_concept tconcept4 = conceptService.findByName(header4.getName(), modal.getId());
												    	if(Objects.isNull(tconcept4)) {
												    		te4 = enumService.findByName(header4.getName());
													    	if(Objects.isNull(tconcept4)) {
													    		continue;
													    	} else {
													    		pfx4 = "../enumerations/" + te4.getPrefix();
													    		prefixName4 = te4.getPrefix() + "_" + header4.getName();
													    	}
												    	} else {
												    		if(tconcept4.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
												    			pfx4 = "../enumerations/" + tconcept4.getPrefix();
												    		} else {
												    			pfx4 = "../../../../def/ic/" + tconcept4.getPrefix() + "-cor";
												    		}				    		
												    		prefixName4 = tconcept4.getPrefix() + "_" + header4.getName();
												    	}
											    		writer.newLine();
											    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx4 + "_" + dtString + ".xsd#" + prefixName4 + "\" xlink:label=\"" + header4.getName() + "\" xlink:title=\"" + header4.getName() + "\"/>");
											    		writer.newLine();
											    		writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/int/dim/arcrole/all\" xlink:from=\"" + header3.getName() + "\" xlink:to=\"" + header4.getName() + "\" xlink:title=\"definition: " + header3.getName() + " to " + header4.getName() + "\" order=\"" + count4 + ".0\" xbrldt:contextElement=\"scenario\" xbrldt:closed=\"true\"/>");
											    	}
									    		}
									    	}
									    }
							    	}
							    }
					    	}
					    }
				    }
				    
				    writer.newLine();
				    writer.write("  </link:definitionLink>");
				    writer.newLine();
				    writer.write("</link:linkbase>");
				    
				    isCompleted = true;
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}
			
		}

	return isCompleted;
	}

	public boolean writeTaxFormHeaderPre(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		
		for(tax_form form1 : tfList) {
			tax_form form = repository.getOne(form1.getId());
			if(form.getLinkRole() != null) {
				String eName = "";
				String fname = "";
				if(form.getName() != null) {
					fname = form.getName();
//					fname = fname.replaceAll("-fi", "");
					eName = form.getName();
					eName = eName.replaceAll("-fi", "");
				} else if(form.getFormName() != null) {
					fname = form.getFormName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getFormName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				} else {
					fname = form.getTemplateName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getTemplateName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				}
				String entryName = "lhdnm-" + eName + "_" + dtString + "_full_entry_point";
				String formName = "/pre_lhdnm-" + fname;
				try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + formName + "_" + dtString + ".xml"))) {
					
					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				    writer.newLine();
				    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
				    writer.newLine();
				    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-" + dtString + ".xsd\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:" + entryName + "=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + eName + "\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:enum=\"http://xbrl.org/2014/extensible-enumerations\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"" + form.getLinkRole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
				    writer.newLine();
				    writer.write("  <link:presentationLink xlink:type=\"extended\" xlink:role=\"" + form.getLinkRole() + "\">");
					
				    for(tax_form_header header : form.getFormHeaderList()) {
				    	String prefixName = header.getPrefix() + "_" + header.getName();
				    	String pfx = "lhdnm-cor";
//				    	if(header.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
//				    		pfx = header.getPrefix();
//				    	}
				    	tax_enum te;
				    	tax_concept tconcept = conceptService.findByName(header.getName(), modal.getId());
				    	if(Objects.isNull(tconcept)) {
				    		te = enumService.findByName(header.getName());
					    	if(Objects.isNull(tconcept)) {
					    		continue;
					    	} else {
					    		pfx = "../enumerations/" + te.getPrefix();
					    		prefixName = te.getPrefix() + "_" + header.getName();
					    	}
				    	} else {
				    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
				    			pfx = "../enumerations/" + tconcept.getPrefix();
				    		} else {
				    			pfx = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
				    		}				    		
				    		prefixName = tconcept.getPrefix() + "_" + header.getName();
				    	}
				    	writer.newLine();
					    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx + "_" + dtString + ".xsd#" + prefixName + "\" xlink:label=\"" + header.getName() + "\" xlink:title=\"" + header.getName() + "\"/>");
					    if(!header.getChildList().isEmpty()) {
					    	int count1 = 0;
					    	for(tax_form_header header1 : header.getChildList()) {
					    		count1++;
					    		String prefixName1 = header1.getPrefix() + "_" + header1.getName();
					    		String pfx1 = "lhdnm-cor";
					    		tax_enum te1;
						    	tax_concept tconcept1 = conceptService.findByName(header1.getName(), modal.getId());
						    	if(Objects.isNull(tconcept1)) {
						    		te1 = enumService.findByName(header1.getName());
							    	if(Objects.isNull(tconcept1)) {
							    		continue;
							    	} else {
							    		pfx1 = "../enumerations/" + te1.getPrefix();
							    		prefixName1 = te1.getPrefix() + "_" + header1.getName();
							    	}
						    	} else {
						    		if(tconcept1.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
						    			pfx1 = "../enumerations/" + tconcept1.getPrefix();
						    		} else {
						    			pfx1 = "../../../../def/ic/" + tconcept1.getPrefix() + "-cor";
						    		}				    		
						    		prefixName1 = tconcept1.getPrefix() + "_" + header1.getName();
						    	}
					    		writer.newLine();
							    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx1 + "_" + dtString + ".xsd#" + prefixName1 + "\" xlink:label=\"" + header1.getName() + "\" xlink:title=\"" + header1.getName() + "\"/>");
							    writer.newLine();
							    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + header.getName() + "\" xlink:to=\"" + header1.getName() + "\" xlink:title=\"definition: " + header.getName() + " to " + header1.getName() + "\" order=\"" + count1 + ".0\" />");
							    if(!header1.getChildList().isEmpty()) {
							    	int count2 = 0;
							    	for(tax_form_header header2 : header1.getChildList()) {
							    		count2++;
							    		String prefixName2 = header2.getPrefix() + "_" + header2.getName();
							    		String pfx2 = "lhdnm-cor";
							    		tax_enum te2;
								    	tax_concept tconcept2 = conceptService.findByName(header2.getName(), modal.getId());
								    	if(Objects.isNull(tconcept2)) {
								    		te2 = enumService.findByName(header2.getName());
									    	if(Objects.isNull(tconcept2)) {
									    		continue;
									    	} else {
									    		pfx2 = "../enumerations/" + te2.getPrefix();
									    		prefixName2 = te2.getPrefix() + "_" + header2.getName();
									    	}
								    	} else {
								    		if(tconcept2.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
								    			pfx2 = "../enumerations/" + tconcept2.getPrefix();
								    		} else {
								    			pfx2 = "../../../../def/ic/" + tconcept2.getPrefix() + "-cor";
								    		}				    		
								    		prefixName2 = tconcept2.getPrefix() + "_" + header2.getName();
								    	}
							    		writer.newLine();
									    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx2 + "_" + dtString + ".xsd#" + prefixName2 + "\" xlink:label=\"" + header2.getName() + "\" xlink:title=\"" + header2.getName() + "\"/>");
									    writer.newLine();
									    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + header1.getName() + "\" xlink:to=\"" + header2.getName() + "\" xlink:title=\"definition: " + header1.getName() + " to " + header2.getName() + "\" order=\"" + count2 + ".0\" />");
									    if(!header2.getChildList().isEmpty()) {
									    	int count3 = 0;
									    	for(tax_form_header header3 : header2.getChildList()) {
									    		count3++;	
									    		String prefixName3 = header3.getPrefix() + "_" + header3.getName();
									    		String pfx3 = "lhdnm-cor";
									    		tax_enum te3;
										    	tax_concept tconcept3 = conceptService.findByName(header3.getName(), modal.getId());
										    	if(Objects.isNull(tconcept3)) {
										    		te3 = enumService.findByName(header3.getName());
											    	if(Objects.isNull(tconcept3)) {
											    		continue;
											    	} else {
											    		pfx3 = "../enumerations/" + te3.getPrefix();
											    		prefixName3 = te3.getPrefix() + "_" + header3.getName();
											    	}
										    	} else {
										    		if(tconcept3.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
										    			pfx3 = "../enumerations/" + tconcept3.getPrefix();
										    		} else {
										    			pfx3 = "../../../../def/ic/" + tconcept3.getPrefix() + "-cor";
										    		}				    		
										    		prefixName3 = tconcept3.getPrefix() + "_" + header3.getName();
										    	}
									    		writer.newLine();
									    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx3 + "_" + dtString + ".xsd#" + prefixName3 + "\" xlink:label=\"" + header3.getName() + "\" xlink:title=\"" + header3.getName() + "\"/>");
									    		writer.newLine();
									    		writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + header2.getName() + "\" xlink:to=\"" + header3.getName() + "\" xlink:title=\"definition: " + header2.getName() + " to " + header3.getName() + "\" order=\"" + count3 + ".0\" />");
									    		if(!header3.getChildList().isEmpty()) {
									    			int count4 = 0;
											    	for(tax_form_header header4 : header3.getChildList()) {
											    		count4++;	
											    		String prefixName4 = header4.getPrefix() + "_" + header4.getName();
											    		String pfx4 = "lhdnm-cor";
											    		tax_enum te4;
												    	tax_concept tconcept4 = conceptService.findByName(header4.getName(), modal.getId());
												    	if(Objects.isNull(tconcept4)) {
												    		te4 = enumService.findByName(header4.getName());
													    	if(Objects.isNull(tconcept4)) {
													    		continue;
													    	} else {
													    		pfx4 = "../enumerations/" + te4.getPrefix();
													    		prefixName4 = te4.getPrefix() + "_" + header4.getName();
													    	}
												    	} else {
												    		if(tconcept4.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
												    			pfx4 = "../enumerations/" + tconcept4.getPrefix();
												    		} else {
												    			pfx4 = "../../../../def/ic/" + tconcept4.getPrefix() + "-cor";
												    		}				    		
												    		prefixName4 = tconcept4.getPrefix() + "_" + header4.getName();
												    	}
											    		writer.newLine();
											    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx4 + "_" + dtString + ".xsd#" + prefixName4 + "\" xlink:label=\"" + header4.getName() + "\" xlink:title=\"" + header4.getName() + "\"/>");
											    		writer.newLine();
											    		writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + header3.getName() + "\" xlink:to=\"" + header4.getName() + "\" xlink:title=\"definition: " + header3.getName() + " to " + header4.getName() + "\" order=\"" + count4 + ".0\" />");
											    	}
									    		}
									    	}
									    }
							    	}
							    }
					    	}
					    }
				    }
				    
				    writer.newLine();
				    writer.write("  </link:presentationLink>");
				    writer.newLine();
				    writer.write("</link:linkbase>");
				    
					isCompleted = true;
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}
		}
		return isCompleted;
	}
	
	public boolean writeTaxFormHeaderLabelEn(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		
		for(tax_form form1 : tfList) {
			tax_form form = repository.getOne(form1.getId());
			if(form.getLinkRole() != null) {
				String eName = "";
				String fname = "";
				if(form.getName() != null) {
					fname = form.getName();
//					fname = fname.replaceAll("-fi", "");
					eName = form.getName();
					eName = eName.replaceAll("-fi", "");
				} else if(form.getFormName() != null) {
					fname = form.getFormName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getFormName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				} else {
					fname = form.getTemplateName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getTemplateName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				}
				String entryName = "lhdnm-" + eName + "_" + dtString + "_full_entry_point";
				String formName = "/lab_lhdnm-" + fname + "-en";
				
				try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + formName + "_" + dtString + ".xml"))) {
					
					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				    writer.newLine();
				    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
				    writer.newLine();
				    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-" + dtString + ".xsd\" xmlns:" + fname + "=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + fname + "\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:" + entryName + "=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + eName + "\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:enum=\"http://xbrl.org/2014/extensible-enumerations\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:reference=\"http://xbrl.org/2008/reference\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"" + form.getLinkRole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
				    writer.newLine();
				    writer.write("  <link:labelLink xlink:type=\"extended\" xlink:role=\"" + form.getLinkRole() + "\">");
					
				    for(tax_form_header header : form.getFormHeaderList()) {
				    	
				    	if(!header.getLabel_en_form().isEmpty()) {
				    		String prefixName = header.getPrefix() + "_" + header.getName();
				    		String labelName = "label_" + header.getName();
					    	String pfx = "lhdnm-cor";
					    	tax_enum te;
					    	tax_concept tconcept = conceptService.findByName(header.getName(), modal.getId());
					    	if(Objects.isNull(tconcept)) {
					    		te = enumService.findByName(header.getName());
						    	if(Objects.isNull(tconcept)) {
						    		continue;
						    	} else {
						    		pfx = "../enumerations/" + te.getPrefix();
						    		prefixName = te.getPrefix() + "_" + header.getName();
						    	}
					    	} else {
					    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
					    			pfx = "../enumerations/" + tconcept.getPrefix();
					    		} else {
					    			pfx = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
					    		}				    		
					    		prefixName = tconcept.getPrefix() + "_" + header.getName();
					    	}
					    	String labelText = StringEscapeUtils.escapeXml(header.getLabel_en_form());
				    		writer.newLine();
						    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx + "_" + dtString + ".xsd#" + prefixName + "\" xlink:label=\"" + header.getName() + "\" xlink:title=\"" + header.getName() + "\"/>");
						    writer.newLine();
						    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"en\" id=\"" + labelName + "\">" + labelText + "</link:label>");
						    writer.newLine();
						    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + header.getName() + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + header.getName() + " to " + labelName + "\"/>");
				    	}
				    	
					    if(!header.getChildList().isEmpty()) {
					    	for(tax_form_header header1 : header.getChildList()) {
					    		
					    		if(!header1.getLabel_en_form().isEmpty()) {
					    			String prefixName1 = header1.getPrefix() + "_" + header1.getName();
					    			String labelName1 = "label_" + header1.getName();
						    		String pfx1 = "lhdnm-cor";
						    		tax_enum te1;
							    	tax_concept tconcept1 = conceptService.findByName(header1.getName(), modal.getId());
							    	if(Objects.isNull(tconcept1)) {
							    		te1 = enumService.findByName(header1.getName());
								    	if(Objects.isNull(tconcept1)) {
								    		continue;
								    	} else {
								    		pfx1 = "../enumerations/" + te1.getPrefix();
								    		prefixName1 = te1.getPrefix() + "_" + header1.getName();
								    	}
							    	} else {
							    		if(tconcept1.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
							    			pfx1 = "../enumerations/" + tconcept1.getPrefix();
							    		} else {
							    			pfx1 = "../../../../def/ic/" + tconcept1.getPrefix() + "-cor";
							    		}				    		
							    		prefixName1 = tconcept1.getPrefix() + "_" + header1.getName();
							    	}
							    	String labelText1 = StringEscapeUtils.escapeXml(header1.getLabel_en_form());
						    		writer.newLine();
								    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx1 + "_" + dtString + ".xsd#" + prefixName1 + "\" xlink:label=\"" + header1.getName() + "\" xlink:title=\"" + header1.getName() + "\"/>");
								    writer.newLine();
								    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName1 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName1 + "\" xml:lang=\"en\" id=\"" + labelName1 + "\">" + labelText1 + "</link:label>");
								    writer.newLine();
								    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + header1.getName() + "\" xlink:to=\"" + labelName1 + "\" xlink:title=\"label: " + header1.getName() + " to " + labelName1 + "\"/>");
					    		}
					    		
							    if(!header1.getChildList().isEmpty()) {
							    	for(tax_form_header header2 : header1.getChildList()) {
							    		
							    		if(!header2.getLabel_en_form().isEmpty()) {
							    			String prefixName2 = header2.getPrefix() + "_" + header2.getName();
							    			String labelName2 = "label_" + header2.getName();
								    		String pfx2 = "lhdnm-cor";
								    		tax_enum te2;
									    	tax_concept tconcept2 = conceptService.findByName(header2.getName(), modal.getId());
									    	if(Objects.isNull(tconcept2)) {
									    		te2 = enumService.findByName(header2.getName());
										    	if(Objects.isNull(tconcept2)) {
										    		continue;
										    	} else {
										    		pfx2 = "../enumerations/" + te2.getPrefix();
										    		prefixName2 = te2.getPrefix() + "_" + header2.getName();
										    	}
									    	} else {
									    		if(tconcept2.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
									    			pfx2 = "../enumerations/" + tconcept2.getPrefix();
									    		} else {
									    			pfx2 = "../../../../def/ic/" + tconcept2.getPrefix() + "-cor";
									    		}				    		
									    		prefixName2 = tconcept2.getPrefix() + "_" + header2.getName();
									    	}
									    	String labelText2 = StringEscapeUtils.escapeXml(header2.getLabel_en_form());
								    		writer.newLine();
										    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx2 + "_" + dtString + ".xsd#" + prefixName2 + "\" xlink:label=\"" + header2.getName() + "\" xlink:title=\"" + header2.getName() + "\"/>");
										    writer.newLine();
										    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName2 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName2 + "\" xml:lang=\"en\" id=\"" + labelName2 + "\">" + labelText2 + "</link:label>");
										    writer.newLine();
										    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + header2.getName() + "\" xlink:to=\"" + labelName2 + "\" xlink:title=\"label: " + header2.getName() + " to " + labelName2 + "\"/>");
							    		}
							    		
							    		if(!header2.getChildList().isEmpty()) {
									    	for(tax_form_header header3 : header2.getChildList()) {
									    		
									    		if(!header3.getLabel_en_form().isEmpty()) {
									    			String prefixName3 = header3.getPrefix() + "_" + header3.getName();
									    			String labelName3 = "label_" + header3.getName();
										    		String pfx3 = "lhdnm-cor";
										    		tax_enum te3;
											    	tax_concept tconcept3 = conceptService.findByName(header3.getName(), modal.getId());
											    	if(Objects.isNull(tconcept3)) {
											    		te3 = enumService.findByName(header3.getName());
												    	if(Objects.isNull(tconcept3)) {
												    		continue;
												    	} else {
												    		pfx3 = "../enumerations/" + te3.getPrefix();
												    		prefixName3 = te3.getPrefix() + "_" + header3.getName();
												    	}
											    	} else {
											    		if(tconcept3.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
											    			pfx3 = "../enumerations/" + tconcept3.getPrefix();
											    		} else {
											    			pfx3 = "../../../../def/ic/" + tconcept3.getPrefix() + "-cor";
											    		}				    		
											    		prefixName3 = tconcept3.getPrefix() + "_" + header3.getName();
											    	}
											    	String labelText3 = StringEscapeUtils.escapeXml(header3.getLabel_en_form());
										    		writer.newLine();
										    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx3 + "_" + dtString + ".xsd#" + prefixName3 + "\" xlink:label=\"" + header3.getName() + "\" xlink:title=\"" + header3.getName() + "\"/>");
										    		writer.newLine();
												    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName3 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName3 + "\" xml:lang=\"en\" id=\"" + labelName3 + "\">" + labelText3 + "</link:label>");
												    writer.newLine();
												    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + header3.getName() + "\" xlink:to=\"" + labelName3 + "\" xlink:title=\"label: " + header3.getName() + " to " + labelName3 + "\"/>");
									    		}
									    		
									    		if(!header3.getChildList().isEmpty()) {
											    	for(tax_form_header header4 : header3.getChildList()) {
											    		
											    		if(!header4.getLabel_en_form().isEmpty()) {
											    			String prefixName4 = header4.getPrefix() + "_" + header4.getName();
											    			String labelName4 = "label_" + header4.getName();
												    		String pfx4 = "lhdnm-cor";
												    		tax_enum te4;
													    	tax_concept tconcept4 = conceptService.findByName(header4.getName(), modal.getId());
													    	if(Objects.isNull(tconcept4)) {
													    		te4 = enumService.findByName(header4.getName());
														    	if(Objects.isNull(tconcept4)) {
														    		continue;
														    	} else {
														    		pfx4 = "../enumerations/" + te4.getPrefix();
														    		prefixName4 = te4.getPrefix() + "_" + header4.getName();
														    	}
													    	} else {
													    		if(tconcept4.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
													    			pfx4 = "../enumerations/" + tconcept4.getPrefix();
													    		} else {
													    			pfx4= "../../../../def/ic/" + tconcept4.getPrefix() + "-cor";
													    		}				    		
													    		prefixName4 = tconcept4.getPrefix() + "_" + header4.getName();
													    	}
													    	String labelText4 = StringEscapeUtils.escapeXml(header4.getLabel_en_form());
												    		writer.newLine();
												    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx4 + "_" + dtString + ".xsd#" + prefixName4 + "\" xlink:label=\"" + header4.getName() + "\" xlink:title=\"" + header4.getName() + "\"/>");
												    		writer.newLine();
														    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName4 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName4 + "\" xml:lang=\"en\" id=\"" + labelName4 + "\">" + labelText4 + "</link:label>");
														    writer.newLine();
														    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + header4.getName() + "\" xlink:to=\"" + labelName4 + "\" xlink:title=\"label: " + header4.getName() + " to " + labelName4 + "\"/>");
											    		}
											    		
											    	}
									    		}
									    	}
									    }
							    	}
							    }
					    	}
					    }
				    }
				    
				    writer.newLine();
				    writer.write("  </link:labelLink>");
				    writer.newLine();
				    writer.write("</link:linkbase>");
				    
					isCompleted = true;
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}
		}
		return isCompleted;
	}
	
	public boolean writeTaxFormHeaderLabelMs(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		
		for(tax_form form1 : tfList) {
			tax_form form = repository.getOne(form1.getId());
			if(form.getLinkRole() != null) {
				String eName = "";
				String fname = "";
				if(form.getName() != null) {
					fname = form.getName();
//					fname = fname.replaceAll("-fi", "");
					eName = form.getName();
					eName = eName.replaceAll("-fi", "");
				} else if(form.getFormName() != null) {
					fname = form.getFormName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getFormName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				} else {
					fname = form.getTemplateName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getTemplateName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				}
				String entryName = "lhdnm-" + eName + "_" + dtString + "_full_entry_point";
				String formName = "/lab_lhdnm-" + fname + "-ms";
				
				try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + formName + "_" + dtString + ".xml"))) {
					
					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				    writer.newLine();
				    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
				    writer.newLine();
				    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-" + dtString + ".xsd\" xmlns:" + fname + "=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + fname + "\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:" + entryName + "=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + eName + "\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:enum=\"http://xbrl.org/2014/extensible-enumerations\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:reference=\"http://xbrl.org/2008/reference\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"" + form.getLinkRole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
				    writer.newLine();
				    writer.write("  <link:labelLink xlink:type=\"extended\" xlink:role=\"" + form.getLinkRole() + "\">");
					
				    for(tax_form_header header : form.getFormHeaderList()) {
				    	
				    	if(!header.getLabel_ms_form().isEmpty()) {
				    		String prefixName = header.getPrefix() + "_" + header.getName();
				    		String labelName = "label_" + header.getName();
					    	String pfx = "lhdnm-cor";
					    	tax_enum te;
					    	tax_concept tconcept = conceptService.findByName(header.getName(), modal.getId());
					    	if(Objects.isNull(tconcept)) {
					    		te = enumService.findByName(header.getName());
						    	if(Objects.isNull(tconcept)) {
						    		continue;
						    	} else {
						    		pfx = "../enumerations/" + te.getPrefix();
						    		prefixName = te.getPrefix() + "_" + header.getName();
						    	}
					    	} else {
					    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
					    			pfx = "../enumerations/" + tconcept.getPrefix();
					    		} else {
					    			pfx = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
					    		}				    		
					    		prefixName = tconcept.getPrefix() + "_" + header.getName();
					    	}
					    	String labelText = StringEscapeUtils.escapeXml(header.getLabel_en_form());
				    		writer.newLine();
						    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx + "_" + dtString + ".xsd#" + prefixName + "\" xlink:label=\"" + header.getName() + "\" xlink:title=\"" + header.getName() + "\"/>");
						    writer.newLine();
						    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"en\" id=\"" + labelName + "\">" + labelText + "</link:label>");
						    writer.newLine();
						    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + header.getName() + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + header.getName() + " to " + labelName + "\"/>");
				    	}
				    	
					    if(!header.getChildList().isEmpty()) {
					    	for(tax_form_header header1 : header.getChildList()) {
					    		
					    		if(!header1.getLabel_ms_form().isEmpty()) {
					    			String prefixName1 = header1.getPrefix() + "_" + header1.getName();
					    			String labelName1 = "label_" + header1.getName();
						    		String pfx1 = "lhdnm-cor";
						    		tax_enum te1;
							    	tax_concept tconcept1 = conceptService.findByName(header1.getName(), modal.getId());
							    	if(Objects.isNull(tconcept1)) {
							    		te1 = enumService.findByName(header1.getName());
								    	if(Objects.isNull(tconcept1)) {
								    		continue;
								    	} else {
								    		pfx1 = "../enumerations/" + te1.getPrefix();
								    		prefixName1 = te1.getPrefix() + "_" + header1.getName();
								    	}
							    	} else {
							    		if(tconcept1.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
							    			pfx1 = "../enumerations/" + tconcept1.getPrefix();
							    		} else {
							    			pfx1 = "../../../../def/ic/" + tconcept1.getPrefix() + "-cor";
							    		}				    		
							    		prefixName1 = tconcept1.getPrefix() + "_" + header1.getName();
							    	}
							    	String labelText1 = StringEscapeUtils.escapeXml(header1.getLabel_en_form());
						    		writer.newLine();
								    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx1 + "_" + dtString + ".xsd#" + prefixName1 + "\" xlink:label=\"" + header1.getName() + "\" xlink:title=\"" + header1.getName() + "\"/>");
								    writer.newLine();
								    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName1 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName1 + "\" xml:lang=\"en\" id=\"" + labelName1 + "\">" + labelText1 + "</link:label>");
								    writer.newLine();
								    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + header1.getName() + "\" xlink:to=\"" + labelName1 + "\" xlink:title=\"label: " + header1.getName() + " to " + labelName1 + "\"/>");
					    		}
					    		
							    if(!header1.getChildList().isEmpty()) {
							    	for(tax_form_header header2 : header1.getChildList()) {
							    		
							    		if(!header2.getLabel_ms_form().isEmpty()) {
							    			String prefixName2 = header2.getPrefix() + "_" + header2.getName();
							    			String labelName2 = "label_" + header2.getName();
								    		String pfx2 = "lhdnm-cor";
								    		tax_enum te2;
									    	tax_concept tconcept2 = conceptService.findByName(header2.getName(), modal.getId());
									    	if(Objects.isNull(tconcept2)) {
									    		te2 = enumService.findByName(header2.getName());
										    	if(Objects.isNull(tconcept2)) {
										    		continue;
										    	} else {
										    		pfx2 = "../enumerations/" + te2.getPrefix();
										    		prefixName2 = te2.getPrefix() + "_" + header2.getName();
										    	}
									    	} else {
									    		if(tconcept2.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
									    			pfx2 = "../enumerations/" + tconcept2.getPrefix();
									    		} else {
									    			pfx2 = "../../../../def/ic/" + tconcept2.getPrefix() + "-cor";
									    		}				    		
									    		prefixName2 = tconcept2.getPrefix() + "_" + header2.getName();
									    	}
									    	String labelText2 = StringEscapeUtils.escapeXml(header2.getLabel_en_form());
								    		writer.newLine();
										    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx2 + "_" + dtString + ".xsd#" + prefixName2 + "\" xlink:label=\"" + header2.getName() + "\" xlink:title=\"" + header2.getName() + "\"/>");
										    writer.newLine();
										    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName2 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName2 + "\" xml:lang=\"en\" id=\"" + labelName2 + "\">" + labelText2 + "</link:label>");
										    writer.newLine();
										    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + header2.getName() + "\" xlink:to=\"" + labelName2 + "\" xlink:title=\"label: " + header2.getName() + " to " + labelName2 + "\"/>");
							    		}
							    		
							    		if(!header2.getChildList().isEmpty()) {
									    	for(tax_form_header header3 : header2.getChildList()) {
									    		
									    		if(!header3.getLabel_ms_form().isEmpty()) {
									    			String prefixName3 = header3.getPrefix() + "_" + header3.getName();
									    			String labelName3 = "label_" + header3.getName();
										    		String pfx3 = "lhdnm-cor";
										    		tax_enum te3;
											    	tax_concept tconcept3 = conceptService.findByName(header3.getName(), modal.getId());
											    	if(Objects.isNull(tconcept3)) {
											    		te3 = enumService.findByName(header3.getName());
												    	if(Objects.isNull(tconcept3)) {
												    		continue;
												    	} else {
												    		pfx3 = "../enumerations/" + te3.getPrefix();
												    		prefixName3 = te3.getPrefix() + "_" + header3.getName();
												    	}
											    	} else {
											    		if(tconcept3.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
											    			pfx3 = "../enumerations/" + tconcept3.getPrefix();
											    		} else {
											    			pfx3 = "../../../../def/ic/" + tconcept3.getPrefix() + "-cor";
											    		}				    		
											    		prefixName3 = tconcept3.getPrefix() + "_" + header3.getName();
											    	}
											    	String labelText3 = StringEscapeUtils.escapeXml(header3.getLabel_en_form());
										    		writer.newLine();
										    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx3 + "_" + dtString + ".xsd#" + prefixName3 + "\" xlink:label=\"" + header3.getName() + "\" xlink:title=\"" + header3.getName() + "\"/>");
										    		writer.newLine();
												    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName3 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName3 + "\" xml:lang=\"en\" id=\"" + labelName3 + "\">" + labelText3 + "</link:label>");
												    writer.newLine();
												    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + header3.getName() + "\" xlink:to=\"" + labelName3 + "\" xlink:title=\"label: " + header3.getName() + " to " + labelName3 + "\"/>");
									    		}
									    		
									    		if(!header3.getChildList().isEmpty()) {
											    	for(tax_form_header header4 : header3.getChildList()) {
											    		
											    		if(!header4.getLabel_ms_form().isEmpty()) {
											    			String prefixName4 = header4.getPrefix() + "_" + header4.getName();
											    			String labelName4 = "label_" + header4.getName();
												    		String pfx4 = "lhdnm-cor";
												    		tax_enum te4;
													    	tax_concept tconcept4 = conceptService.findByName(header4.getName(), modal.getId());
													    	if(Objects.isNull(tconcept4)) {
													    		te4 = enumService.findByName(header4.getName());
														    	if(Objects.isNull(tconcept4)) {
														    		continue;
														    	} else {
														    		pfx4 = "../enumerations/" + te4.getPrefix();
														    		prefixName4 = te4.getPrefix() + "_" + header4.getName();
														    	}
													    	} else {
													    		if(tconcept4.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
													    			pfx4 = "../enumerations/" + tconcept4.getPrefix();
													    		} else {
													    			pfx4 = "../../../../def/ic/" + tconcept4.getPrefix() + "-cor";
													    		}				    		
													    		prefixName4 = tconcept4.getPrefix() + "_" + header4.getName();
													    	}
													    	String labelText4 = StringEscapeUtils.escapeXml(header4.getLabel_en_form());
												    		writer.newLine();
												    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx4 + "_" + dtString + ".xsd#" + prefixName4 + "\" xlink:label=\"" + header4.getName() + "\" xlink:title=\"" + header4.getName() + "\"/>");
												    		writer.newLine();
														    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName4 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName4 + "\" xml:lang=\"en\" id=\"" + labelName4 + "\">" + labelText4 + "</link:label>");
														    writer.newLine();
														    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + header4.getName() + "\" xlink:to=\"" + labelName4 + "\" xlink:title=\"label: " + header4.getName() + " to " + labelName4 + "\"/>");
											    		}
											    		
											    	}
									    		}
									    	}
									    }
							    	}
							    }
					    	}
					    }
				    }
				    
				    writer.newLine();
				    writer.write("  </link:labelLink>");
				    writer.newLine();
				    writer.write("</link:linkbase>");
				    
					isCompleted = true;
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}
		}
		return isCompleted;
	}
	
	public boolean writeLhdnmFdn(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		List<tax_form> tfList = modal.getTaxFormList();
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/lhdnm-fdn_" + dtString + ".xsd"))) {
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			writer.newLine();
			writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			writer.newLine();
			writer.write("<xsd:schema targetNamespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/fdn\" elementFormDefault=\"qualified\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:lhdnm-fdn=\"http://www.hasil.gov.my/xbrl/" + dtString + "/fdn\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\">");
			writer.newLine();
			writer.write("  <xsd:annotation>");
			writer.newLine();
			writer.write("    <xsd:appinfo>");
			
			for (int count = 1; count <= 10; count++) {
				writer.newLine();
				writer.write("      <link:roleType roleURI=\"http://www.hasil.gov.my/role/occurance"+count+"Label\" id=\"occurance"+count+"Label\">");
				writer.newLine();
				writer.write("        <link:definition>Occurance "+count+" label</link:definition>");
				writer.newLine();
				writer.write("        <link:usedOn>link:label</link:usedOn>");
				writer.newLine();
				writer.write("      </link:roleType>");
			}
			for (int count = 1; count <= 10; count++) {
				writer.newLine();
				writer.write("      <link:roleType roleURI=\"http://www.hasil.gov.my/role/occurance"+count+"Documentation\" id=\"occurance"+count+"Documentation\">");
				writer.newLine();
				writer.write("        <link:definition>Occurance "+count+" documentation</link:definition>");
				writer.newLine();
				writer.write("        <link:usedOn>link:label</link:usedOn>");
				writer.newLine();
				writer.write("      </link:roleType>");
			}
			
			writer.newLine();
			writer.write("    </xsd:appinfo>");
			writer.newLine();
			writer.write("  </xsd:annotation>");
			
			writer.newLine();
			writer.write("  <xsd:import namespace=\"http://www.xbrl.org/2003/linkbase\" schemaLocation=\"http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\"/>");
			writer.newLine();
			writer.write("  <xsd:attribute name=\"orientation\">");
			writer.newLine();
			writer.write("    <xsd:simpleType>");
			writer.newLine();
			writer.write("      <xsd:restriction base=\"xsd:token\">");
			writer.newLine();
			writer.write("        <xsd:enumeration value=\"rows\"/>");
			writer.newLine();
			writer.write("        <xsd:enumeration value=\"columns\"/>");
			writer.newLine();
			writer.write("        <xsd:enumeration value=\"header\"/>");
			writer.newLine();
			writer.write("        <xsd:enumeration value=\"hidden\"/>");
			writer.newLine();
			writer.write("        <xsd:enumeration value=\"attachment\"/>");
			writer.newLine();
			writer.write("      </xsd:restriction>");
			writer.newLine();
			writer.write("    </xsd:simpleType>");
			writer.newLine();
			writer.write("  </xsd:attribute>");
			writer.newLine();
			writer.write("</xsd:schema>");
			
			isCompleted = true;
			
		} catch (Exception e) {
			isCompleted = false;
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		
		return isCompleted;
	}
	
	public boolean writeFormulaDef(String path, String dtString, tax_data_modal modal, String type) {
		boolean isCompleted = false;
		String regex = "^\\s+";
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());

		for(tax_form form : tfList) {
			String fname = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
				fname = fname.replaceAll(" ", "-");
//				fname = fname.replaceAll(".", "");
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/frm_lhdnm-" + fname + "_" + dtString + ".xml"))) {
				
				writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd http://xbrl.org/2008/generic http://www.xbrl.org/2008/generic-link.xsd http://xbrl.org/2008/formula http://www.xbrl.org/2008/formula.xsd http://xbrl.org/2008/label http://www.xbrl.org/2008/generic-label.xsd http://xbrl.org/2008/reference http://www.xbrl.org/2008/generic-reference.xsd http://xbrl.org/2008/assertion/consistency http://www.xbrl.org/2008/consistency-assertion.xsd http://xbrl.org/2008/assertion/existence http://www.xbrl.org/2008/existence-assertion.xsd http://xbrl.org/2008/assertion/value http://www.xbrl.org/2008/value-assertion.xsd http://xbrl.org/2008/filter/concept http://www.xbrl.org/2008/concept-filter.xsd http://xbrl.org/2010/message http://www.xbrl.org/2010/generic-message.xsd http://xbrl.org/2010/message/validation http://www.xbrl.org/2010/validation-message.xsd http://xbrl.org/2008/filter/dimension http://www.xbrl.org/2008/dimension-filter.xsd\" xmlns:ca=\"http://xbrl.org/2008/assertion/consistency\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:fn=\"http://www.w3.org/2005/xpath-functions\" xmlns:variable=\"http://xbrl.org/2008/variable\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:xfi=\"http://www.xbrl.org/2008/function/instance\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:cf=\"http://xbrl.org/2008/filter/concept\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:valm=\"http://xbrl.org/2010/message/validation\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:ea=\"http://xbrl.org/2008/assertion/existence\" xmlns:df=\"http://xbrl.org/2008/filter/dimension\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:validation=\"http://xbrl.org/2008/validation\" xmlns:formula=\"http://xbrl.org/2008/formula\" xmlns:msg=\"http://xbrl.org/2010/message\" xmlns:va=\"http://xbrl.org/2008/assertion/value\" xmlns:label=\"http://xbrl.org/2008/label\">");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"" + form.getLinkRole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/link\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-link.xsd#standard-link-role\"/>");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2010/role/message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/generic-message.xsd#standard-message\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/variable-filter\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/variable.xsd#variable-filter\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/variable-set\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/variable.xsd#variable-set\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/variable-set-filter\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/variable.xsd#variable-set-filter\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/boolean-filter\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/boolean-filter.xsd#boolean-filter\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2010/assertion-unsatisfied-message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/validation-message.xsd#assertion-unsatisfied-message\"/>");
			    writer.newLine();
			    writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"" + form.getLinkRole() + "\">");
			    
			    writer.newLine();
			    writer.newLine();
				
			    writer.newLine();
			    writer.write("  </gen:link>");
				writer.newLine();
			    writer.write("</link:linkbase>");
			    
			    isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
		
		return isCompleted;
	}
		
	public boolean writeFormulaLabelEn(String path, String dtString, tax_data_modal modal, String type) {
		boolean isCompleted = false;
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		
		for(tax_form form : tfList) {
			String fname = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
				fname = fname.replaceAll(" ", "-");
//				fname = fname.replaceAll(".", "");
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/frm_lab_lhdnm-" + fname + "-en_" + dtString + ".xml"))) {
				
				writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    writer.write("<link:linkbase xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:bf=\"http://xbrl.org/2008/filter/boolean\" xmlns:ca=\"http://xbrl.org/2008/assertion/consistency\" xmlns:cf=\"http://xbrl.org/2008/filter/concept\" xmlns:df=\"http://xbrl.org/2008/filter/dimension\" xmlns:ea=\"http://xbrl.org/2008/assertion/existence\" xmlns:fn=\"http://www.w3.org/2005/xpath-functions\" xmlns:formula=\"http://xbrl.org/2008/formula\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:msg=\"http://xbrl.org/2010/message\" xmlns:va=\"http://xbrl.org/2008/assertion/value\" xmlns:validation=\"http://xbrl.org/2008/validation\" xmlns:valm=\"http://xbrl.org/2010/message/validation\" xmlns:variable=\"http://xbrl.org/2008/variable\" xmlns:xfi=\"http://www.xbrl.org/2008/function/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd http://xbrl.org/2008/generic http://www.xbrl.org/2008/generic-link.xsd http://xbrl.org/2008/formula http://www.xbrl.org/2008/formula.xsd http://xbrl.org/2008/label http://www.xbrl.org/2008/generic-label.xsd http://xbrl.org/2008/reference http://www.xbrl.org/2008/generic-reference.xsd http://xbrl.org/2008/assertion/consistency http://www.xbrl.org/2008/consistency-assertion.xsd http://xbrl.org/2008/assertion/existence http://www.xbrl.org/2008/existence-assertion.xsd http://xbrl.org/2008/assertion/value http://www.xbrl.org/2008/value-assertion.xsd http://xbrl.org/2008/filter/concept http://www.xbrl.org/2008/concept-filter.xsd http://xbrl.org/2008/filter/dimension http://www.xbrl.org/2008/dimension-filter.xsd http://xbrl.org/2010/message http://www.xbrl.org/2010/generic-message.xsd http://xbrl.org/2010/message/validation http://www.xbrl.org/2010/validation-message.xsd http://xbrl.org/2008/filter/boolean http://www.xbrl.org/2008/boolean-filter.xsd\">");
			   			    
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"" + form.getLinkRole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\" />");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2010/role/message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/generic-message.xsd#standard-message\" />");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2010/assertion-unsatisfied-message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/validation-message.xsd#assertion-unsatisfied-message\" />");
			    writer.newLine();
			    writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"" + form.getLinkRole() + "\">");
			    			   				
				writer.newLine();
			    writer.write("  </gen:link>");
			    writer.newLine();
			    writer.write("</link:linkbase>");
			    
			    isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
		
		return isCompleted;
	}
			
	public boolean writeFormulaLabelMs(String path, String dtString, tax_data_modal modal, String type) {
		boolean isCompleted = false;
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		
		for(tax_form form : tfList) {
			String fname = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
				fname = fname.replaceAll(" ", "-");
//				fname = fname.replaceAll(".", "");
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/frm_lab_lhdnm-" + fname + "-ms_" + dtString + ".xml"))) {
				
				writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    writer.write("<link:linkbase xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:bf=\"http://xbrl.org/2008/filter/boolean\" xmlns:ca=\"http://xbrl.org/2008/assertion/consistency\" xmlns:cf=\"http://xbrl.org/2008/filter/concept\" xmlns:df=\"http://xbrl.org/2008/filter/dimension\" xmlns:ea=\"http://xbrl.org/2008/assertion/existence\" xmlns:fn=\"http://www.w3.org/2005/xpath-functions\" xmlns:formula=\"http://xbrl.org/2008/formula\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:msg=\"http://xbrl.org/2010/message\" xmlns:va=\"http://xbrl.org/2008/assertion/value\" xmlns:validation=\"http://xbrl.org/2008/validation\" xmlns:valm=\"http://xbrl.org/2010/message/validation\" xmlns:variable=\"http://xbrl.org/2008/variable\" xmlns:xfi=\"http://www.xbrl.org/2008/function/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd http://xbrl.org/2008/generic http://www.xbrl.org/2008/generic-link.xsd http://xbrl.org/2008/formula http://www.xbrl.org/2008/formula.xsd http://xbrl.org/2008/label http://www.xbrl.org/2008/generic-label.xsd http://xbrl.org/2008/reference http://www.xbrl.org/2008/generic-reference.xsd http://xbrl.org/2008/assertion/consistency http://www.xbrl.org/2008/consistency-assertion.xsd http://xbrl.org/2008/assertion/existence http://www.xbrl.org/2008/existence-assertion.xsd http://xbrl.org/2008/assertion/value http://www.xbrl.org/2008/value-assertion.xsd http://xbrl.org/2008/filter/concept http://www.xbrl.org/2008/concept-filter.xsd http://xbrl.org/2008/filter/dimension http://www.xbrl.org/2008/dimension-filter.xsd http://xbrl.org/2010/message http://www.xbrl.org/2010/generic-message.xsd http://xbrl.org/2010/message/validation http://www.xbrl.org/2010/validation-message.xsd http://xbrl.org/2008/filter/boolean http://www.xbrl.org/2008/boolean-filter.xsd\">");

			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"" + form.getLinkRole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\" />");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2010/role/message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/generic-message.xsd#standard-message\" />");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2010/assertion-unsatisfied-message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/validation-message.xsd#assertion-unsatisfied-message\" />");
			    writer.newLine();
			    writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"" + form.getLinkRole() + "\">");
			    
				writer.newLine();
			    writer.write("  </gen:link>");
			    writer.newLine();
			    writer.write("</link:linkbase>");
			    
			    isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
		
		return isCompleted;
	}
	
}

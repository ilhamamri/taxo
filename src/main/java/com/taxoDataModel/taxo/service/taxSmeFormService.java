package com.taxoDataModel.taxo.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.taxoDataModel.taxo.model.linkbase_form;
import com.taxoDataModel.taxo.model.tax_data_modal;
import com.taxoDataModel.taxo.model.tax_form;
import com.taxoDataModel.taxo.model.tax_form_header;
import com.taxoDataModel.taxo.model.tax_form_linkbase;
import com.taxoDataModel.taxo.model.tax_sme_concept;
import com.taxoDataModel.taxo.model.tax_sme_enum;
import com.taxoDataModel.taxo.repository.tax_formRepository;

@Service
public class taxSmeFormService {
	
	@Autowired
	tax_formRepository repository;

	@Autowired
	linkbaseFormService formService;
	
	@Autowired
	taxSmeConceptService tscService;
	
	@Autowired
	taxSmeEnumService tseService;

	public void save(List<tax_form> tList) {
		try {
			repository.saveAll(tList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveSingle(tax_form form) {
		try {
			repository.save(form);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<tax_form> getAll() {
		List<tax_form> tList = new ArrayList<tax_form>();
		try {
			tList = repository.findAll(Sort.by(Sort.Direction.ASC, "id"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tList;
	}

	public tax_form getByColumnIndex(Long ind) {
		return repository.findByColumnIndex(ind);
	}

	public tax_form getByName(String name) {
		return repository.findByFormName(name);
	}

	public boolean writeTaxonomyPackage(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		List<tax_form> tfList = modal.getTaxFormList();
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/taxonomyPackage.xml"))) {
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			writer.newLine();
			writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			writer.newLine();
			writer.write("<tp:taxonomyPackage xml:lang=\"en\" xmlns:tp=\"http://xbrl.org/2016/taxonomy-package\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://xbrl.org/2016/taxonomy-package http://www.xbrl.org/2016/taxonomy-package.xsd\">");
			writer.newLine();
			writer.write("    <tp:identifier>http://www.hasil.gov.my/xbrl/lhdnm_" + dtString + ".zip</tp:identifier>");
			writer.newLine();
			writer.write("    <tp:name>LHDNM XBRL Taxonomy</tp:name>");
			writer.newLine();
			writer.write("    <tp:version>1.0</tp:version>");
			writer.newLine();
			writer.write("    <tp:publisher>Lembaga Hasil Dalam Negeri Malaysia</tp:publisher>");
			writer.newLine();
			writer.write("    <tp:publisherURL>http://www.hasil.gov.my/</tp:publisherURL>");
			writer.newLine();
			writer.write("    <tp:publicationDate>" + dtString + "</tp:publicationDate>");
			writer.newLine();
			writer.write("    <tp:entryPoints>");
			
			for (tax_form form : tfList) {
				String fname = "";
				if(form.getFormName() != null) {
					if (form.getName() != null) {
						fname = form.getName();
						fname = fname.replaceAll("-fi", "");
						fname = fname.replaceAll("/", "-");
					} else if (form.getFormName() != null) {
						fname = form.getFormName().toLowerCase();
						fname = fname.replaceAll(" ", "_");
						fname = fname.replaceAll("/", "-");
					} else {
						fname = form.getTemplateName().toLowerCase();
						fname = fname.replaceAll(" ", "_");
					}
					String formName = "/lhdnm-" + fname + "-full_entry_point_" + dtString + ".xsd";
					writer.newLine();
					writer.write("		<tp:entryPoint>");
					writer.newLine();
					writer.write("			<tp:name>" + fname.toUpperCase() + "</tp:name>");
					writer.newLine();
					writer.write("			<tp:description>" + form.getFormName() + "</tp:description>");
					writer.newLine();
					writer.write("			<tp:entryPointDocument href=\"http://www.hasil.gov.my/xbrl/" + dtString + "/rep/lhdnm/tax/" + formName + "\"/>");
					writer.newLine();
					writer.write("		</tp:entryPoint>");
				}
			}
			
			writer.newLine();
			writer.write("    </tp:entryPoints>");
			writer.newLine();
			writer.write("</tp:taxonomyPackage>");
			isCompleted = true;
			
		} catch (Exception e) {
			isCompleted = false;
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		
		
		
		return isCompleted;
	}
	
	public boolean writeTaxFormEntry(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
//		List<tax_form> tfList = getAll();
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());

		for (tax_form form : tfList) {
			String fname = "";
			if(form.getLinkRole() != null) {
				if (form.getName() != null) {
					fname = form.getName().trim();
					fname = fname.replaceAll("-fi", "");
					fname = fname.replaceAll("/", "-");
				} else if (form.getFormName() != null) {
					fname = form.getFormName().trim().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					fname = fname.replaceAll("/", "-");
				} else {
					fname = form.getTemplateName().trim().toLowerCase();
					fname = fname.replaceAll(" ", "_");
				}
				String formName = "/lhdnm-form_" + fname + "-full_entry_point";
				String glaName = "form_" + fname + "-full_entry_point";
				try (BufferedWriter writer = new BufferedWriter(
						new FileWriter(path + formName + "_" + dtString + ".xsd"))) {

					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
					writer.newLine();
					writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
					writer.newLine();
					writer.write("<xsd:schema targetNamespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/"
							+ fname
							+ "\" attributeFormDefault=\"unqualified\" elementFormDefault=\"qualified\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:lhdnm-form-"
							+ fname + "=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + fname
							+ "\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:lhdnm-fdn=\"http://www.hasil.gov.my/xbrl/"
							+ dtString + "/fdn\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
					writer.newLine();
					writer.write("  <xsd:annotation>");
					writer.newLine();
					writer.write("    <xsd:appinfo>");
					writer.newLine();
					writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"labels/gla_lhdnm-" + glaName + "-en_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
					writer.newLine();
					writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"labels/gla_lhdnm-" + glaName + "-ms_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
					writer.newLine();
					writer.write("    </xsd:appinfo>");
					writer.newLine();
					writer.write("  </xsd:annotation>");
					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + form.getName().trim() + "\" schemaLocation=\"linkbases/lhdnm-" + form.getName().trim() + "_" + dtString + ".xsd\"/>");

					if (!form.getLinkbaseList().isEmpty()) {
						for (tax_form_linkbase lbase : form.getLinkbaseList()) {
							linkbase_form linkbaseF = formService.getSingle(lbase.getLinkbase_form_id());
							if (linkbaseF != null) {
								writer.newLine();
								writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + linkbaseF.getName() + "\" schemaLocation=\"linkbases/lhdnm-" + linkbaseF.getName() + "_" + dtString + ".xsd\"/>");
							}
						}
					}
					
					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/label\" schemaLocation=\"http://www.xbrl.org/2008/generic-label.xsd\"/>");
					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/reference\" schemaLocation=\"http://www.xbrl.org/2008/generic-reference.xsd\"/>");
					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/generic\" schemaLocation=\"http://www.xbrl.org/2008/generic-link.xsd\"/>");
					writer.newLine();
					writer.write("</xsd:schema>");

					isCompleted = true;
				} catch (Exception e) {
					isCompleted = false;
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}
		}

		return isCompleted;
	}

	public boolean writeTaxFormLabelEn(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
//		List<tax_form> tfList = getAll();
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		
		for(tax_form form : tfList) {
			if(form.getFormName() != null) {
				String eName = "";
				String fname = "";
				if(form.getName() != null) {
					fname = form.getName().trim();
					eName = form.getName();
					eName = eName.replaceAll("-fi", "");
				} else if(form.getFormName() != null) {
					fname = form.getFormName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getFormName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				} else {
					fname = form.getTemplateName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getTemplateName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				}
				String entryName = "lhdnm-form_" + eName + "_" + dtString + "_full_entry_point";
				String formName = "/gla_lhdnm-form_" + eName + "-full_entry_point-en";
				
				try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + formName + "_" + dtString + ".xml"))) {
					
					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				    writer.newLine();
				    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
				    writer.newLine();
				    writer.write("<link:linkbase xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\">");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#standard-label\" />");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/link\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-link.xsd#standard-link-role\" />");
				    writer.newLine();
				    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/element-label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#element-label\" />");
				    writer.newLine();
				    writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"http://www.xbrl.org/2008/role/link\">");
				    
				    writer.newLine();
				    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../linkbases/lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\" xlink:label=\"roleType\" xlink:title=\"roleType\" />");
				    writer.newLine();
				    writer.write("    <label:label xlink:type=\"resource\" xlink:label=\"label\" xlink:role=\"http://www.xbrl.org/2008/role/label\" xlink:title=\"label\" xml:lang=\"en\">" + form.getDefinition() + "</label:label>");
				    writer.newLine();
				    writer.write("    <gen:arc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/element-label\" xlink:from=\"roleType\" xlink:to=\"label\" xlink:title=\"user-defined: roleType to label\" order=\"1.0\" />");
				    
				    if (!form.getLinkbaseList().isEmpty()) {
				    	int countLink = 0;
						for (tax_form_linkbase lbase : form.getLinkbaseList()) {
							linkbase_form linkbaseF = formService.getSingle(lbase.getLinkbase_form_id());
							if (linkbaseF != null) {
								countLink++;
								writer.newLine();
							    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../linkbases/lhdnm-" + linkbaseF.getName() + "_" + dtString + ".xsd#" + linkbaseF.getName() + "\" xlink:label=\"roleType" + countLink + "\" xlink:title=\"roleType\" />");
								writer.newLine();
								writer.write("    <label:label xlink:type=\"resource\" xlink:label=\"label" + countLink + "\" xlink:role=\"http://www.xbrl.org/2008/role/label\" xlink:title=\"label\" xml:lang=\"en\">" + linkbaseF.getDefinition() + "</label:label>");
								writer.newLine();
								writer.write("    <gen:arc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/element-label\" xlink:from=\"roleType" + countLink + "\" xlink:to=\"label" + countLink + "\" xlink:title=\"user-defined: roleType to label\" order=\"1.0\" />");
							}
						}
					}
					
					writer.newLine();
				    writer.write("  </gen:link>");
				    writer.newLine();
				    writer.write("</link:linkbase>");
				    
					isCompleted = true;
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}
		}
		return isCompleted;
	}
	
	public boolean writeTaxFormLabelMs(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
//		List<tax_form> tfList = getAll();
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		
		for(tax_form form : tfList) {
			if(form.getFormName() != null) {
				String eName = "";
				String fname = "";
				if(form.getName() != null) {
					fname = form.getName();
					eName = form.getName();
					eName = eName.replaceAll("-fi", "");
				} else if(form.getFormName() != null) {
					fname = form.getFormName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getFormName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				} else {
					fname = form.getTemplateName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getTemplateName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				}
				String entryName = "lhdnm-" + eName + "_" + dtString + "_full_entry_point";
				String formName = "/gla_lhdnm-form_" + eName + "-full_entry_point-ms";
				
				try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + formName + "_" + dtString + ".xml"))) {
					
					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				    writer.newLine();
				    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
				    writer.newLine();
				    writer.write("<link:linkbase xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\">");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#standard-label\" />");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/link\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-link.xsd#standard-link-role\" />");
				    writer.newLine();
				    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/element-label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#element-label\" />");
				    writer.newLine();
				    writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"http://www.xbrl.org/2008/role/link\">");
				    
				    writer.newLine();
				    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../linkbases/lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\" xlink:label=\"roleType\" xlink:title=\"roleType\" />");
				    writer.newLine();
				    writer.write("    <label:label xlink:type=\"resource\" xlink:label=\"label\" xlink:role=\"http://www.xbrl.org/2008/role/label\" xlink:title=\"label\" xml:lang=\"en\">" + form.getDefinition() + "</label:label>");
				    writer.newLine();
				    writer.write("    <gen:arc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/element-label\" xlink:from=\"roleType\" xlink:to=\"label\" xlink:title=\"user-defined: roleType to label\" order=\"1.0\" />");
				    
				    if (!form.getLinkbaseList().isEmpty()) {
				    	int countLink = 0;
						for (tax_form_linkbase lbase : form.getLinkbaseList()) {
							linkbase_form linkbaseF = formService.getSingle(lbase.getLinkbase_form_id());
							if (linkbaseF != null) {
								countLink++;
								writer.newLine();
							    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../linkbases/lhdnm-" + linkbaseF.getName() + "_" + dtString + ".xsd#" + linkbaseF.getName() + "\" xlink:label=\"roleType" + countLink + "\" xlink:title=\"roleType\" />");
								writer.newLine();
								writer.write("    <label:label xlink:type=\"resource\" xlink:label=\"label" + countLink + "\" xlink:role=\"http://www.xbrl.org/2008/role/label\" xlink:title=\"label\" xml:lang=\"en\">" + linkbaseF.getDefinition_ms() + "</label:label>");
								writer.newLine();
								writer.write("    <gen:arc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/element-label\" xlink:from=\"roleType" + countLink + "\" xlink:to=\"label" + countLink + "\" xlink:title=\"user-defined: roleType to label\" order=\"1.0\" />");
							}
						}
					}
					
					writer.newLine();
				    writer.write("  </gen:link>");
				    writer.newLine();
				    writer.write("</link:linkbase>");
				    
					isCompleted = true;
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}
		}
		return isCompleted;
	}
	
	public boolean writeTaxFormLinkbase(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());

		for (tax_form form : tfList) {
			if (form.getLinkRole() != null) {
				String fname = "";
				if (form.getName() != null) {
					fname = form.getName();
				} else if (form.getFormName() != null) {
					fname = form.getFormName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
				} else {
					fname = form.getTemplateName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
				}
				String formName = "/lhdnm-" + fname;
				try (BufferedWriter writer = new BufferedWriter(new FileWriter(path + formName + "_" + dtString + ".xsd"))) {

					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
					writer.newLine();
					writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
					writer.newLine();
					writer.write("<xsd:schema targetNamespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + fname
							+ "\" attributeFormDefault=\"unqualified\" elementFormDefault=\"qualified\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:"
							+ fname + "=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + fname
							+ "\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\">");
					writer.newLine();
					writer.write("  <xsd:annotation>");
					writer.newLine();
					writer.write("    <xsd:appinfo>");
					writer.newLine();
					writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"pre_lhdnm-" + fname + "_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/presentationLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
					writer.newLine();
					writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"def_lhdnm-" + fname + "_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/definitionLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
					writer.newLine();
					writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-" + fname + "-en_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
					writer.newLine();
					writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-" + fname + "-ms_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
					writer.newLine();
					writer.write("      <link:roleType roleURI=\"" + form.getLinkRole() + "\" id=\"" + fname + "\">");
					writer.newLine();
					writer.write("        <link:definition>" + form.getDefinition() + "</link:definition>");
					writer.newLine();
					writer.write("        <link:usedOn>link:presentationLink</link:usedOn>");
					writer.newLine();
					writer.write("        <link:usedOn>link:definitionLink</link:usedOn>");
					writer.newLine();
					writer.write("        <link:usedOn>link:labelLink</link:usedOn>");
					writer.newLine();
					writer.write("        <link:usedOn>gen:link</link:usedOn>");
					writer.newLine();
					writer.write("      </link:roleType>");
					writer.newLine();
					writer.write("    </xsd:appinfo>");
					writer.newLine();
					writer.write("  </xsd:annotation>");

					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/sme-enum\" schemaLocation=\"../enumerations/lhdnm-sme-enum_" + dtString + ".xsd\"/>");
					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/lhdnm-sme\" schemaLocation=\"../../../../def/ic/lhdnm-sme-cor_" + dtString + ".xsd\"/>");
					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/fdn\" schemaLocation=\"../../../../def/fdn/lhdnm-fdn_" + dtString + ".xsd\"/>");
					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/label\" schemaLocation=\"http://www.xbrl.org/2008/generic-label.xsd\"/>");
					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/reference\" schemaLocation=\"http://www.xbrl.org/2008/generic-reference.xsd\"/>");
					writer.newLine();
					writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/generic\" schemaLocation=\"http://www.xbrl.org/2008/generic-link.xsd\"/>");

					writer.newLine();
					writer.write("</xsd:schema>");
					
					isCompleted = true;

				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}

		}

		return isCompleted;
	}

	public boolean writeTaxFormHeaderDef(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		List<tax_sme_concept> tsconceptList = tscService.getAllByModalId(modal.getId());
		List<tax_sme_enum> tseList = tseService.getAllByModalId(modal.getId());
		
		for(tax_form form1 : tfList) {
			tax_form form = repository.getOne(form1.getId());
			if(form.getLinkRole() != null) {
				String fname = "";
				if(form.getName() != null) {
					fname = form.getName();
				} else if(form.getFormName() != null) {
					fname = form.getFormName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
				} else {
					fname = form.getTemplateName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
				}
				String formName = "/def_lhdnm-" + fname;
				try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + formName + "_" + dtString + ".xml"))) {
					
					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				    writer.newLine();
				    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
				    writer.newLine();
				    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-" + dtString + ".xsd\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" >");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"" + form.getLinkRole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
				    writer.newLine();
				    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/int/dim/arcrole/domain-member\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2005/xbrldt-2005.xsd#domain-member\"/>");
				    writer.newLine();
				    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/int/dim/arcrole/all\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2005/xbrldt-2005.xsd#all\"/>");
				    writer.newLine();
				    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/int/dim/arcrole/hypercube-dimension\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2005/xbrldt-2005.xsd#hypercube-dimension\"/>");
				    writer.newLine();
				    writer.write("  <link:definitionLink xlink:type=\"extended\" xlink:role=\"" + form.getLinkRole() + "\">");
				    
				    for(tax_form_header header : form.getFormHeaderList()) {
				    	String prefixName = header.getPrefix() + "_" + header.getName();
				    	String pfx = "../../../../def/ic/lhdnm-sme-cor";
				    	String name = header.getName();
				    	tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header.getName())).findFirst().orElse(null);
				    	if(Objects.isNull(tsconcept)) {
				    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header.getName())).findFirst().orElse(null);
					    	if(Objects.isNull(tse)) {
					    		continue;
					    	} else {
					    		name = tse.getName();
					    		pfx = "../enumerations/" + tse.getPrefix();
					    		prefixName = tse.getPrefix() + "_" + name;
					    	}
				    	} else {
				    		name = tsconcept.getName();
				    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
				    			pfx = "../enumerations/" + tsconcept.getPrefix();
				    		} else {
				    			pfx = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
				    		}				    		
				    		prefixName = tsconcept.getPrefix() + "_" + name;
				    	}
				    	writer.newLine();
					    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx + "_" + dtString + ".xsd#" + prefixName + "\" xlink:label=\"" + name + "\" xlink:title=\"" + name + "\"/>");
					    if(!header.getChildList().isEmpty()) {
					    	int count1 = 0;
					    	for(tax_form_header header1 : header.getChildList()) {
					    		count1++;
					    		String prefixName1 = header1.getPrefix() + "_" + header1.getName();
					    		String pfx1 = "../../../../def/ic/lhdnm-sme-cor";
					    		String name1 = header1.getName();
					    		tax_sme_concept tsconcept1 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header1.getName())).findFirst().orElse(null);
						    	if(Objects.isNull(tsconcept1)) {
						    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header1.getName())).findFirst().orElse(null);
							    	if(Objects.isNull(tse)) {
							    		continue;
							    	} else {
							    		name1 = tse.getName();
							    		pfx1 = "../enumerations/" + tse.getPrefix();
							    		prefixName1 = tse.getPrefix() + "_" + name1;
							    	}
						    	} else {
						    		name1 = tsconcept1.getName();
						    		if(tsconcept1.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
						    			pfx1 = "../enumerations/" + tsconcept1.getPrefix();
						    		} else {
						    			pfx1 = "../../../../def/ic/" + tsconcept1.getPrefix() + "-cor";
						    		}				    		
						    		prefixName1 = tsconcept1.getPrefix() + "_" + name1;
						    	}
					    		writer.newLine();
							    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx1 + "_" + dtString + ".xsd#" + prefixName1 + "\" xlink:label=\"" + name1 + "\" xlink:title=\"" + name1 + "\"/>");
							    writer.newLine();
							    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/int/dim/arcrole/all\" xlink:from=\"" + name + "\" xlink:to=\"" + name1 + "\" xlink:title=\"definition: " + name + " to " + name1 + "\" order=\"" + count1 + ".0\" />");
							    if(!header1.getChildList().isEmpty()) {
							    	int count2 = 0;
							    	for(tax_form_header header2 : header1.getChildList()) {
							    		count2++;
							    		String prefixName2 = header2.getPrefix() + "_" + header2.getName();
							    		String pfx2 = "../../../../def/ic/lhdnm-sme-cor";
							    		String name2 = header2.getName();
							    		tax_sme_concept tsconcept2 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header2.getName())).findFirst().orElse(null);
								    	if(Objects.isNull(tsconcept2)) {
								    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header2.getName())).findFirst().orElse(null);
									    	if(Objects.isNull(tse)) {
									    		continue;
									    	} else {
									    		name2 = tse.getName();
									    		pfx2 = "../enumerations/" + tse.getPrefix();
									    		prefixName2 = tse.getPrefix() + "_" + name2;
									    	}
								    	} else {
								    		name2 = tsconcept2.getName();
								    		if(tsconcept2.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
								    			pfx2 = "../enumerations/" + tsconcept2.getPrefix();
								    		} else {
								    			pfx2 = "../../../../def/ic/" + tsconcept2.getPrefix() + "-cor";
								    		}				    		
								    		prefixName2 = tsconcept2.getPrefix() + "_" + name2;
								    	}
							    		writer.newLine();
									    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx2 + "_" + dtString + ".xsd#" + prefixName2 + "\" xlink:label=\"" + name2 + "\" xlink:title=\"" + name2 + "\"/>");
									    writer.newLine();
									    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/int/dim/arcrole/all\" xlink:from=\"" + name1 + "\" xlink:to=\"" + name2 + "\" xlink:title=\"definition: " + name1 + " to " + name2 + "\" order=\"" + count2 + ".0\" />");
									    if(!header2.getChildList().isEmpty()) {
									    	int count3 = 0;
									    	for(tax_form_header header3 : header2.getChildList()) {
									    		count3++;	
									    		String prefixName3 = header3.getPrefix() + "_" + header3.getName();
									    		String pfx3 = "../../../../def/ic/lhdnm-sme-cor";
									    		String name3 = header3.getName();
									    		tax_sme_concept tsconcept3 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header3.getName())).findFirst().orElse(null);
										    	if(Objects.isNull(tsconcept3)) {
										    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header3.getName())).findFirst().orElse(null);
											    	if(Objects.isNull(tse)) {
											    		continue;
											    	} else {
											    		name3 = tse.getName();
											    		pfx3 = "../enumerations/" + tse.getPrefix();
											    		prefixName3 = tse.getPrefix() + "_" + name3;
											    	}
										    	} else {
										    		name3 = tsconcept3.getName();
										    		if(tsconcept3.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
										    			pfx3 = "../enumerations/" + tsconcept3.getPrefix();
										    		} else {
										    			pfx3 = "../../../../def/ic/" + tsconcept3.getPrefix() + "-cor";
										    		}				    		
										    		prefixName3 = tsconcept3.getPrefix() + "_" + name3;
										    	}
									    		writer.newLine();
									    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx3 + "_" + dtString + ".xsd#" + prefixName3 + "\" xlink:label=\"" + name3 + "\" xlink:title=\"" + name3 + "\"/>");
									    		writer.newLine();
									    		writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/int/dim/arcrole/all\" xlink:from=\"" + name2 + "\" xlink:to=\"" + name3 + "\" xlink:title=\"definition: " + name2 + " to " + name3 + "\" order=\"" + count3 + ".0\" />");
									    		if(!header3.getChildList().isEmpty()) {
									    			int count4 = 0;
											    	for(tax_form_header header4 : header3.getChildList()) {
											    		count4++;	
											    		String prefixName4 = header4.getPrefix() + "_" + header4.getName();
											    		String pfx4 = "../../../../def/ic/lhdnm-sme-cor";
											    		String name4 = header4.getName();
											    		tax_sme_concept tsconcept4 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header4.getName())).findFirst().orElse(null);
												    	if(Objects.isNull(tsconcept4)) {
												    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header4.getName())).findFirst().orElse(null);
													    	if(Objects.isNull(tse)) {
													    		continue;
													    	} else {
													    		name4 = tse.getName();
													    		pfx4 = "../enumerations/" + tse.getPrefix();
													    		prefixName4 = tse.getPrefix() + "_" + name4;
													    	}
												    	} else {
												    		name4 = tsconcept4.getName();
												    		if(tsconcept4.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
												    			pfx4 = "../enumerations/" + tsconcept4.getPrefix();
												    		} else {
												    			pfx4 = "../../../../def/ic/" + tsconcept4.getPrefix() + "-cor";
												    		}				    		
												    		prefixName4 = tsconcept4.getPrefix() + "_" + name4;
												    	}
											    		writer.newLine();
											    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx4 + "_" + dtString + ".xsd#" + prefixName4 + "\" xlink:label=\"" + name4 + "\" xlink:title=\"" + name4 + "\"/>");
											    		writer.newLine();
											    		writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/int/dim/arcrole/all\" xlink:from=\"" + name3 + "\" xlink:to=\"" + name4 + "\" xlink:title=\"definition: " + name3 + " to " + name4 + "\" order=\"" + count4 + ".0\" />");
											    	}
									    		}
									    	}
									    }
							    	}
							    }
					    	}
					    }
				    }
				    
				    writer.newLine();
				    writer.write("  </link:definitionLink>");
				    writer.newLine();
				    writer.write("</link:linkbase>");
				    
				    isCompleted = true;
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}
			
		}

	return isCompleted;
	}

	public boolean writeTaxFormHeaderPre(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		List<tax_sme_concept> tsconceptList = tscService.getAllByModalId(modal.getId());
		List<tax_sme_enum> tseList = tseService.getAllByModalId(modal.getId());
		
		for(tax_form form1 : tfList) {
			tax_form form = repository.getOne(form1.getId());
			
			if(form.getLinkRole() != null) {
				String eName = "";
				String fname = "";
				if(form.getName() != null) {
					fname = form.getName();
					eName = form.getName();
					eName = eName.replaceAll("-fi", "");
				} else if(form.getFormName() != null) {
					fname = form.getFormName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getFormName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				} else {
					fname = form.getTemplateName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getTemplateName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				}
				String entryName = "lhdnm-" + eName + "_" + dtString + "_full_entry_point";
				String formName = "/pre_lhdnm-" + fname;
				try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + formName + "_" + dtString + ".xml"))) {
					
					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				    writer.newLine();
				    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
				    writer.newLine();
				    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-" + dtString + ".xsd\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"" + form.getLinkRole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
				    writer.newLine();
				    writer.write("  <link:presentationLink xlink:type=\"extended\" xlink:role=\"" + form.getLinkRole() + "\">");
					
				    for(tax_form_header header : form.getFormHeaderList()) {
				    	String prefixName = header.getPrefix() + "_" + header.getName();
				    	String pfx = "../../../../def/ic/lhdnm-sme-cor";
				    	String name = header.getName();
				    	tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header.getName())).findFirst().orElse(null);
				    	if(Objects.isNull(tsconcept)) {
				    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header.getName())).findFirst().orElse(null);
					    	if(Objects.isNull(tse)) {
					    		continue;
					    	} else {
					    		name = tse.getName();
					    		pfx = "../enumerations/" + tse.getPrefix();
					    		prefixName = tse.getPrefix() + "_" + name;
					    	}
				    	} else {
				    		name = tsconcept.getName();
				    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
				    			pfx = "../enumerations/" + tsconcept.getPrefix();
				    		} else {
				    			pfx = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
				    		}				    		
				    		prefixName = tsconcept.getPrefix() + "_" + name;
				    	}
				    	writer.newLine();
					    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx + "_" + dtString + ".xsd#" + prefixName + "\" xlink:label=\"" + name + "\" xlink:title=\"" + name + "\"/>");
					    if(!header.getChildList().isEmpty()) {
					    	int count1 = 0;
					    	for(tax_form_header header1 : header.getChildList()) {
					    		count1++;
					    		String prefixName1 = header1.getPrefix() + "_" + header1.getName();
					    		String pfx1 = "../../../../def/ic/lhdnm-sme-cor";
					    		String name1 = header1.getName();
					    		tax_sme_concept tsconcept1 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header1.getName())).findFirst().orElse(null);
						    	if(Objects.isNull(tsconcept1)) {
						    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header1.getName())).findFirst().orElse(null);
							    	if(Objects.isNull(tse)) {
							    		continue;
							    	} else {
							    		name1 = tse.getName();
							    		pfx1 = "../enumerations/" + tse.getPrefix();
							    		prefixName1 = tse.getPrefix() + "_" + name1;
							    	}
						    	} else {
						    		name1 = tsconcept1.getName();
						    		if(tsconcept1.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
						    			pfx1 = "../enumerations/" + tsconcept1.getPrefix();
						    		} else {
						    			pfx1 = "../../../../def/ic/" + tsconcept1.getPrefix() + "-cor";
						    		}				    		
						    		prefixName1 = tsconcept1.getPrefix() + "_" + name1;
						    	}
					    		writer.newLine();
							    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx1 + "_" + dtString + ".xsd#" + prefixName1 + "\" xlink:label=\"" + name1 + "\" xlink:title=\"" + name1 + "\"/>");
							    writer.newLine();
							    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name + "\" xlink:to=\"" + name1 + "\" xlink:title=\"definition: " + name + " to " + name1 + "\" order=\"" + count1 + ".0\" />");
							    if(!header1.getChildList().isEmpty()) {
							    	int count2 = 0;
							    	for(tax_form_header header2 : header1.getChildList()) {
							    		count2++;
							    		String prefixName2 = header2.getPrefix() + "_" + header2.getName();
							    		String pfx2 = "../../../../def/ic/lhdnm-sme-cor";
							    		String name2 = header2.getName();
							    		tax_sme_concept tsconcept2 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header2.getName())).findFirst().orElse(null);
								    	if(Objects.isNull(tsconcept2)) {
								    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header2.getName())).findFirst().orElse(null);
									    	if(Objects.isNull(tse)) {
									    		continue;
									    	} else {
									    		name2 = tse.getName();
									    		pfx2 = "../enumerations/" + tse.getPrefix();
									    		prefixName2 = tse.getPrefix() + "_" + name2;
									    	}
								    	} else {
								    		name2 = tsconcept2.getName();
								    		if(tsconcept2.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
								    			pfx2 = "../enumerations/" + tsconcept2.getPrefix();
								    		} else {
								    			pfx2 = "../../../../def/ic/" + tsconcept2.getPrefix() + "-cor";
								    		}				    		
								    		prefixName2 = tsconcept2.getPrefix() + "_" + name2;
								    	}
							    		writer.newLine();
									    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx2 + "_" + dtString + ".xsd#" + prefixName2 + "\" xlink:label=\"" + name2 + "\" xlink:title=\"" + name2 + "\"/>");
									    writer.newLine();
									    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name1 + "\" xlink:to=\"" + name2 + "\" xlink:title=\"definition: " + name1 + " to " + name2 + "\" order=\"" + count2 + ".0\" />");
									    if(!header2.getChildList().isEmpty()) {
									    	int count3 = 0;
									    	for(tax_form_header header3 : header2.getChildList()) {
									    		count3++;	
									    		String prefixName3 = header3.getPrefix() + "_" + header3.getName();
									    		String pfx3 = "../../../../def/ic/lhdnm-sme-cor";
									    		String name3 = header3.getName();
									    		tax_sme_concept tsconcept3 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header3.getName())).findFirst().orElse(null);
										    	if(Objects.isNull(tsconcept3)) {
										    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header3.getName())).findFirst().orElse(null);
											    	if(Objects.isNull(tse)) {
											    		continue;
											    	} else {
											    		name3 = tse.getName();
											    		pfx3 = "../enumerations/" + tse.getPrefix();
											    		prefixName3 = tse.getPrefix() + "_" + name3;
											    	}
										    	} else {
										    		name3 = tsconcept3.getName();
										    		if(tsconcept3.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
										    			pfx3 = "../enumerations/" + tsconcept3.getPrefix();
										    		} else {
										    			pfx3 = "../../../../def/ic/" + tsconcept3.getPrefix() + "-cor";
										    		}				    		
										    		prefixName3 = tsconcept3.getPrefix() + "_" + name3;
										    	}
									    		writer.newLine();
									    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx3 + "_" + dtString + ".xsd#" + prefixName3 + "\" xlink:label=\"" + name3 + "\" xlink:title=\"" + name3 + "\"/>");
									    		writer.newLine();
									    		writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name2 + "\" xlink:to=\"" + name3 + "\" xlink:title=\"definition: " + name2 + " to " + name3 + "\" order=\"" + count3 + ".0\" />");
									    		if(!header3.getChildList().isEmpty()) {
									    			int count4 = 0;
											    	for(tax_form_header header4 : header3.getChildList()) {
											    		count4++;	
											    		String prefixName4 = header4.getPrefix() + "_" + header4.getName();
											    		String pfx4 = "../../../../def/ic/lhdnm-sme-cor";
											    		String name4 = header4.getName();
											    		tax_sme_concept tsconcept4 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header4.getName())).findFirst().orElse(null);
												    	if(Objects.isNull(tsconcept4)) {
												    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header4.getName())).findFirst().orElse(null);
													    	if(Objects.isNull(tse)) {
													    		continue;
													    	} else {
													    		name4 = tse.getName();
													    		pfx4 = "../enumerations/" + tse.getPrefix();
													    		prefixName4 = tse.getPrefix() + "_" + name4;
													    	}
												    	} else {
												    		name4 = tsconcept4.getName();
												    		if(tsconcept4.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
												    			pfx4 = "../enumerations/" + tsconcept4.getPrefix();
												    		} else {
												    			pfx4 = "../../../../def/ic/" + tsconcept4.getPrefix() + "-cor";
												    		}				    		
												    		prefixName4 = tsconcept4.getPrefix() + "_" + name4;
												    	}
											    		writer.newLine();
											    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx4 + "_" + dtString + ".xsd#" + prefixName4 + "\" xlink:label=\"" + name4 + "\" xlink:title=\"" + name4 + "\"/>");
											    		writer.newLine();
											    		writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name3 + "\" xlink:to=\"" + name4 + "\" xlink:title=\"definition: " + name3 + " to " + name4 + "\" order=\"" + count4 + ".0\" />");
											    	}
									    		}
									    	}
									    }
							    	}
							    }
					    	}
					    }
				    }
				    
				    writer.newLine();
				    writer.write("  </link:presentationLink>");
				    writer.newLine();
				    writer.write("</link:linkbase>");
				    
					isCompleted = true;
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}
		}
		return isCompleted;
	}
	
	public boolean writeTaxFormHeaderLabelEn(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		List<tax_sme_concept> tsconceptList = tscService.getAllByModalId(modal.getId());
		List<tax_sme_enum> tseList = tseService.getAllByModalId(modal.getId());
		
		for(tax_form form1 : tfList) {
			tax_form form = repository.getOne(form1.getId());
			if(form.getLinkRole() != null) {
				String eName = "";
				String fname = "";
				if(form.getName() != null) {
					fname = form.getName();
					eName = form.getName();
					eName = eName.replaceAll("-fi", "");
				} else if(form.getFormName() != null) {
					fname = form.getFormName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getFormName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				} else {
					fname = form.getTemplateName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getTemplateName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				}
				String entryName = "lhdnm-" + eName + "_" + dtString + "_full_entry_point";
				String formName = "/lab_lhdnm-" + fname + "-en";
				
				try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + formName + "_" + dtString + ".xml"))) {
					
					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				    writer.newLine();
				    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
				    writer.newLine();
				    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:lhdnm-sme-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/sme-enum\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:enum=\"http://xbrl.org/2014/extensible-enumerations\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:reference=\"http://xbrl.org/2008/reference\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:lhdnm-sme=\"http://www.hasil.gov.my/xbrl/" + dtString + "/lhdnm-sme\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"" + form.getLinkRole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
				    writer.newLine();
				    writer.write("  <link:labelLink xlink:type=\"extended\" xlink:role=\"" + form.getLinkRole() + "\">");
					
				    for(tax_form_header header : form.getFormHeaderList()) {
				    	
				    	if(!header.getLabel_en_form().isEmpty()) {
				    		String prefixName = header.getPrefix() + "_" + header.getName();
				    		String labelName = "label_" + header.getName();
					    	String pfx = "lhdnm-sme-cor";
					    	String name = header.getName();
					    	tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header.getName())).findFirst().orElse(null);
					    	if(Objects.isNull(tsconcept)) {
					    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header.getName())).findFirst().orElse(null);
						    	if(Objects.isNull(tse)) {
						    		continue;
						    	} else {
						    		name = tse.getName();
						    		pfx = "../enumerations/" + tse.getPrefix();
						    		prefixName = tse.getPrefix() + "_" + name;
						    	}
					    	} else {
					    		name = tsconcept.getName();
					    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
					    			pfx = "../enumerations/" + tsconcept.getPrefix();
					    		} else {
					    			pfx = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
					    		}				    		
					    		prefixName = tsconcept.getPrefix() + "_" + name;
					    	}
					    	String labelText = StringEscapeUtils.escapeXml(header.getLabel_en_form());
				    		writer.newLine();
						    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../../../../def/ic/" + pfx + "_" + dtString + ".xsd#" + prefixName + "\" xlink:label=\"" + name + "\" xlink:title=\"" + name + "\"/>");
						    writer.newLine();
						    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"en\" id=\"" + labelName + "\">" + labelText + "</link:label>");
						    writer.newLine();
						    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + name + " to " + labelName + "\"/>");
				    	}
				    	
					    if(!header.getChildList().isEmpty()) {
					    	for(tax_form_header header1 : header.getChildList()) {
					    		
					    		if(!header1.getLabel_en_form().isEmpty()) {
					    			String prefixName1 = header1.getPrefix() + "_" + header1.getName();
					    			String labelName1 = "label_" + header1.getName();
						    		String pfx1 = "lhdnm-sme-cor";
						    		String name1 = header1.getName();
						    		tax_sme_concept tsconcept1 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header1.getName())).findFirst().orElse(null);
							    	if(Objects.isNull(tsconcept1)) {
							    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header1.getName())).findFirst().orElse(null);
								    	if(Objects.isNull(tse)) {
								    		continue;
								    	} else {
								    		name1 = tse.getName();
								    		pfx1 = "../enumerations/" + tse.getPrefix();
								    		prefixName1 = tse.getPrefix() + "_" + name1;
								    	}
							    	} else {
							    		name1 = tsconcept1.getName();
							    		if(tsconcept1.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
							    			pfx1 = "../enumerations/" + tsconcept1.getPrefix();
							    		} else {
							    			pfx1 = "../../../../def/ic/" + tsconcept1.getPrefix() + "-cor";
							    		}				    		
							    		prefixName1 = tsconcept1.getPrefix() + "_" + name1;
							    	}
							    	String labelText = StringEscapeUtils.escapeXml(header1.getLabel_en_form());
						    		writer.newLine();
								    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../../../../def/ic/" + pfx1 + "_" + dtString + ".xsd#" + prefixName1 + "\" xlink:label=\"" + name1 + "\" xlink:title=\"" + name1 + "\"/>");
								    writer.newLine();
								    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName1 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName1 + "\" xml:lang=\"en\" id=\"" + labelName1 + "\">" + labelText + "</link:label>");
								    writer.newLine();
								    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name1 + "\" xlink:to=\"" + labelName1 + "\" xlink:title=\"label: " + name1 + " to " + labelName1 + "\"/>");
					    		}
					    		
							    if(!header1.getChildList().isEmpty()) {
							    	for(tax_form_header header2 : header1.getChildList()) {
							    		
							    		if(!header2.getLabel_en_form().isEmpty()) {
							    			String prefixName2 = header2.getPrefix() + "_" + header2.getName();
							    			String labelName2 = "label_" + header2.getName();
								    		String pfx2 = "lhdnm-sme-cor";
								    		String name2 = header2.getName();
								    		tax_sme_concept tsconcept2 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header2.getName())).findFirst().orElse(null);
									    	if(Objects.isNull(tsconcept2)) {
									    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header2.getName())).findFirst().orElse(null);
										    	if(Objects.isNull(tse)) {
										    		continue;
										    	} else {
										    		name2 = tse.getName();
										    		pfx2 = "../enumerations/" + tse.getPrefix();
										    		prefixName2 = tse.getPrefix() + "_" + name2;
										    	}
									    	} else {
									    		name2 = tsconcept2.getName();
									    		if(tsconcept2.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
									    			pfx2 = "../enumerations/" + tsconcept2.getPrefix();
									    		} else {
									    			pfx2 = "../../../../def/ic/" + tsconcept2.getPrefix() + "-cor";
									    		}				    		
									    		prefixName2 = tsconcept2.getPrefix() + "_" + name2;
									    	}
									    	String labelText = StringEscapeUtils.escapeXml(header2.getLabel_en_form());
								    		writer.newLine();
										    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../../../../def/ic/" + pfx2 + "_" + dtString + ".xsd#" + prefixName2 + "\" xlink:label=\"" + name2 + "\" xlink:title=\"" + name2 + "\"/>");
										    writer.newLine();
										    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName2 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName2 + "\" xml:lang=\"en\" id=\"" + labelName2 + "\">" + labelText + "</link:label>");
										    writer.newLine();
										    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name2 + "\" xlink:to=\"" + labelName2 + "\" xlink:title=\"label: " + name2 + " to " + labelName2 + "\"/>");
							    		}
							    		
							    		if(!header2.getChildList().isEmpty()) {
									    	for(tax_form_header header3 : header2.getChildList()) {
									    		
									    		if(!header3.getLabel_en_form().isEmpty()) {
									    			String prefixName3 = header3.getPrefix() + "_" + header3.getName();
									    			String labelName3 = "label_" + header3.getName();
										    		String pfx3 = "lhdnm-sme-cor";
										    		String name3 = header3.getName();
										    		tax_sme_concept tsconcept3 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header3.getName())).findFirst().orElse(null);
											    	if(Objects.isNull(tsconcept3)) {
											    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header3.getName())).findFirst().orElse(null);
												    	if(Objects.isNull(tse)) {
												    		continue;
												    	} else {
												    		name3 = tse.getName();
												    		pfx3 = "../enumerations/" + tse.getPrefix();
												    		prefixName3 = tse.getPrefix() + "_" + name3;
												    	}
											    	} else {
											    		name3 = tsconcept3.getName();
											    		if(tsconcept3.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
											    			pfx3 = "../enumerations/" + tsconcept3.getPrefix();
											    		} else {
											    			pfx3 = "../../../../def/ic/" + tsconcept3.getPrefix() + "-cor";
											    		}				    		
											    		prefixName3 = tsconcept3.getPrefix() + "_" + name3;
											    	}
											    	String labelText = StringEscapeUtils.escapeXml(header3.getLabel_en_form());
										    		writer.newLine();
										    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../../../../def/ic/" + pfx3 + "_" + dtString + ".xsd#" + prefixName3 + "\" xlink:label=\"" + name3 + "\" xlink:title=\"" + name3 + "\"/>");
										    		writer.newLine();
												    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName3 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName3 + "\" xml:lang=\"en\" id=\"" + labelName3 + "\">" + labelText + "</link:label>");
												    writer.newLine();
												    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name3 + "\" xlink:to=\"" + labelName3 + "\" xlink:title=\"label: " + name3 + " to " + labelName3 + "\"/>");
									    		}
									    		
									    		if(!header3.getChildList().isEmpty()) {
											    	for(tax_form_header header4 : header3.getChildList()) {
											    		
											    		if(!header4.getLabel_en_form().isEmpty()) {
											    			String prefixName4 = header4.getPrefix() + "_" + header4.getName();
											    			String labelName4 = "label_" + header4.getName();
												    		String pfx4 = "lhdnm-sme-cor";
												    		String name4 = header4.getName();
												    		tax_sme_concept tsconcept4 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header4.getName())).findFirst().orElse(null);
													    	if(Objects.isNull(tsconcept4)) {
													    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header4.getName())).findFirst().orElse(null);
														    	if(Objects.isNull(tse)) {
														    		continue;
														    	} else {
														    		name4 = tse.getName();
														    		pfx4 = "../enumerations/" + tse.getPrefix();
														    		prefixName4 = tse.getPrefix() + "_" + name4;
														    	}
													    	} else {
													    		name4 = tsconcept4.getName();
													    		if(tsconcept4.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
													    			pfx4 = "../enumerations/" + tsconcept4.getPrefix();
													    		} else {
													    			pfx4 = "../../../../def/ic/" + tsconcept4.getPrefix() + "-cor";
													    		}				    		
													    		prefixName4 = tsconcept4.getPrefix() + "_" + name4;
													    	}
													    	String labelText = StringEscapeUtils.escapeXml(header4.getLabel_en_form());
												    		writer.newLine();
												    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../../../../def/ic/" + pfx4 + "_" + dtString + ".xsd#" + prefixName4 + "\" xlink:label=\"" + name4 + "\" xlink:title=\"" + name4 + "\"/>");
												    		writer.newLine();
														    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName4 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName4 + "\" xml:lang=\"en\" id=\"" + labelName4 + "\">" + labelText + "</link:label>");
														    writer.newLine();
														    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name4 + "\" xlink:to=\"" + labelName4 + "\" xlink:title=\"label: " + name4 + " to " + labelName4 + "\"/>");
											    		}
											    		
											    	}
									    		}
									    	}
									    }
							    	}
							    }
					    	}
					    }
				    }
				    
				    writer.newLine();
				    writer.write("  </link:labelLink>");
				    writer.newLine();
				    writer.write("</link:linkbase>");
				    
					isCompleted = true;
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}
		}
		return isCompleted;
	}
	
	public boolean writeTaxFormHeaderLabelMs(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		List<tax_sme_concept> tsconceptList = tscService.getAllByModalId(modal.getId());
		List<tax_sme_enum> tseList = tseService.getAllByModalId(modal.getId());
		
		for(tax_form form1 : tfList) {
			tax_form form = repository.getOne(form1.getId());
			if(form.getLinkRole() != null) {
				String eName = "";
				String fname = "";
				if(form.getName() != null) {
					fname = form.getName();
					eName = form.getName();
					eName = eName.replaceAll("-fi", "");
				} else if(form.getFormName() != null) {
					fname = form.getFormName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getFormName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				} else {
					fname = form.getTemplateName().toLowerCase();
					fname = fname.replaceAll(" ", "_");
					eName = form.getTemplateName().toLowerCase();
					eName = eName.replaceAll(" ", "_");
				}
				String entryName = "lhdnm-" + eName + "_" + dtString + "_full_entry_point";
				String formName = "/lab_lhdnm-" + fname + "-ms";
				
				try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + formName + "_" + dtString + ".xml"))) {
					
					writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				    writer.newLine();
				    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
				    writer.newLine();
				    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:lhdnm-sme-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/sme-enum\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:enum=\"http://xbrl.org/2014/extensible-enumerations\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:reference=\"http://xbrl.org/2008/reference\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:lhdnm-sme=\"http://www.hasil.gov.my/xbrl/" + dtString + "/lhdnm-sme\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
				    writer.newLine();
				    writer.write("  <link:roleRef roleURI=\"" + form.getLinkRole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
				    writer.newLine();
				    writer.write("  <link:labelLink xlink:type=\"extended\" xlink:role=\"" + form.getLinkRole() + "\">");
					
				    for(tax_form_header header : form.getFormHeaderList()) {
				    	
				    	if(!header.getLabel_en_form().isEmpty()) {
				    		String prefixName = header.getPrefix() + "_" + header.getName();
				    		String labelName = "label_" + header.getName();
					    	String pfx = "lhdnm-sme-cor";
					    	String name = header.getName();
					    	tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header.getName())).findFirst().orElse(null);
					    	if(Objects.isNull(tsconcept)) {
					    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header.getName())).findFirst().orElse(null);
						    	if(Objects.isNull(tse)) {
						    		continue;
						    	} else {
						    		name = tse.getName();
						    		pfx = "../enumerations/" + tse.getPrefix();
						    		prefixName = tse.getPrefix() + "_" + name;
						    	}
					    	} else {
					    		name = tsconcept.getName();
					    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
					    			pfx = "../enumerations/" + tsconcept.getPrefix();
					    		} else {
					    			pfx = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
					    		}				    		
					    		prefixName = tsconcept.getPrefix() + "_" + name;
					    	}
					    	String labelText = StringEscapeUtils.escapeXml(header.getLabel_ms_form());
				    		writer.newLine();
						    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../../../../def/ic/" + pfx + "_" + dtString + ".xsd#" + prefixName + "\" xlink:label=\"" + name + "\" xlink:title=\"" + name + "\"/>");
						    writer.newLine();
						    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"en\" id=\"" + labelName + "\">" + labelText + "</link:label>");
						    writer.newLine();
						    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + name + " to " + labelName + "\"/>");
				    	}
				    	
					    if(!header.getChildList().isEmpty()) {
					    	for(tax_form_header header1 : header.getChildList()) {
					    		
					    		if(!header1.getLabel_en_form().isEmpty()) {
					    			String prefixName1 = header1.getPrefix() + "_" + header1.getName();
					    			String labelName1 = "label_" + header1.getName();
						    		String pfx1 = "lhdnm-sme-cor";
						    		String name1 = header1.getName();
						    		tax_sme_concept tsconcept1 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header1.getName())).findFirst().orElse(null);
							    	if(Objects.isNull(tsconcept1)) {
							    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header1.getName())).findFirst().orElse(null);
								    	if(Objects.isNull(tse)) {
								    		continue;
								    	} else {
								    		name1 = tse.getName();
								    		pfx1 = "../enumerations/" + tse.getPrefix();
								    		prefixName1 = tse.getPrefix() + "_" + name1;
								    	}
							    	} else {
							    		name1 = tsconcept1.getName();
							    		if(tsconcept1.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
							    			pfx1 = "../enumerations/" + tsconcept1.getPrefix();
							    		} else {
							    			pfx1 = "../../../../def/ic/" + tsconcept1.getPrefix() + "-cor";
							    		}				    		
							    		prefixName1 = tsconcept1.getPrefix() + "_" + name1;
							    	}
							    	String labelText = StringEscapeUtils.escapeXml(header1.getLabel_ms_form());
						    		writer.newLine();
								    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../../../../def/ic/" + pfx1 + "_" + dtString + ".xsd#" + prefixName1 + "\" xlink:label=\"" + name1 + "\" xlink:title=\"" + name1 + "\"/>");
								    writer.newLine();
								    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName1 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName1 + "\" xml:lang=\"en\" id=\"" + labelName1 + "\">" + labelText + "</link:label>");
								    writer.newLine();
								    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name1 + "\" xlink:to=\"" + labelName1 + "\" xlink:title=\"label: " + name1 + " to " + labelName1 + "\"/>");
					    		}
					    		
							    if(!header1.getChildList().isEmpty()) {
							    	for(tax_form_header header2 : header1.getChildList()) {
							    		
							    		if(!header2.getLabel_en_form().isEmpty()) {
							    			String prefixName2 = header2.getPrefix() + "_" + header2.getName();
							    			String labelName2 = "label_" + header2.getName();
								    		String pfx2 = "lhdnm-sme-cor";
								    		String name2 = header2.getName();
								    		tax_sme_concept tsconcept2 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header2.getName())).findFirst().orElse(null);
									    	if(Objects.isNull(tsconcept2)) {
									    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header2.getName())).findFirst().orElse(null);
										    	if(Objects.isNull(tse)) {
										    		continue;
										    	} else {
										    		name2 = tse.getName();
										    		pfx2 = "../enumerations/" + tse.getPrefix();
										    		prefixName2 = tse.getPrefix() + "_" + name2;
										    	}
									    	} else {
									    		name2 = tsconcept2.getName();
									    		if(tsconcept2.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
									    			pfx2 = "../enumerations/" + tsconcept2.getPrefix();
									    		} else {
									    			pfx2 = "../../../../def/ic/" + tsconcept2.getPrefix() + "-cor";
									    		}				    		
									    		prefixName2 = tsconcept2.getPrefix() + "_" + name2;
									    	}
									    	String labelText = StringEscapeUtils.escapeXml(header2.getLabel_ms_form());
								    		writer.newLine();
										    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../../../../def/ic/" + pfx2 + "_" + dtString + ".xsd#" + prefixName2 + "\" xlink:label=\"" + name2 + "\" xlink:title=\"" + name2 + "\"/>");
										    writer.newLine();
										    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName2 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName2 + "\" xml:lang=\"en\" id=\"" + labelName2 + "\">" + labelText + "</link:label>");
										    writer.newLine();
										    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name2 + "\" xlink:to=\"" + labelName2 + "\" xlink:title=\"label: " + name2 + " to " + labelName2 + "\"/>");
							    		}
							    		
							    		if(!header2.getChildList().isEmpty()) {
									    	for(tax_form_header header3 : header2.getChildList()) {
									    		
									    		if(!header3.getLabel_en_form().isEmpty()) {
									    			String prefixName3 = header3.getPrefix() + "_" + header3.getName();
									    			String labelName3 = "label_" + header3.getName();
										    		String pfx3 = "lhdnm-sme-cor";
										    		String name3 = header3.getName();
										    		tax_sme_concept tsconcept3 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header3.getName())).findFirst().orElse(null);
											    	if(Objects.isNull(tsconcept3)) {
											    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header3.getName())).findFirst().orElse(null);
												    	if(Objects.isNull(tse)) {
												    		continue;
												    	} else {
												    		name3 = tse.getName();
												    		pfx3 = "../enumerations/" + tse.getPrefix();
												    		prefixName3 = tse.getPrefix() + "_" + name3;
												    	}
											    	} else {
											    		name3 = tsconcept3.getName();
											    		if(tsconcept3.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
											    			pfx3 = "../enumerations/" + tsconcept3.getPrefix();
											    		} else {
											    			pfx3 = "../../../../def/ic/" + tsconcept3.getPrefix() + "-cor";
											    		}				    		
											    		prefixName3 = tsconcept3.getPrefix() + "_" + name3;
											    	}
											    	String labelText = StringEscapeUtils.escapeXml(header3.getLabel_ms_form());
										    		writer.newLine();
										    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../../../../def/ic/" + pfx3 + "_" + dtString + ".xsd#" + prefixName3 + "\" xlink:label=\"" + name3 + "\" xlink:title=\"" + name3 + "\"/>");
										    		writer.newLine();
												    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName3 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName3 + "\" xml:lang=\"en\" id=\"" + labelName3 + "\">" + labelText + "</link:label>");
												    writer.newLine();
												    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name3 + "\" xlink:to=\"" + labelName3 + "\" xlink:title=\"label: " + name3 + " to " + labelName3 + "\"/>");
									    		}
									    		
									    		if(!header3.getChildList().isEmpty()) {
											    	for(tax_form_header header4 : header3.getChildList()) {
											    		
											    		if(!header4.getLabel_en_form().isEmpty()) {
											    			String prefixName4 = header4.getPrefix() + "_" + header4.getName();
											    			String labelName4 = "label_" + header4.getName();
												    		String pfx4 = "lhdnm-sme-cor";
												    		String name4 = header4.getName();
												    		tax_sme_concept tsconcept4 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(header4.getName())).findFirst().orElse(null);
													    	if(Objects.isNull(tsconcept4)) {
													    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(header4.getName())).findFirst().orElse(null);
														    	if(Objects.isNull(tse)) {
														    		continue;
														    	} else {
														    		name4 = tse.getName();
														    		pfx4 = "../enumerations/" + tse.getPrefix();
														    		prefixName4 = tse.getPrefix() + "_" + name4;
														    	}
													    	} else {
													    		name4 = tsconcept4.getName();
													    		if(tsconcept4.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
													    			pfx4 = "../enumerations/" + tsconcept4.getPrefix();
													    		} else {
													    			pfx4 = "../../../../def/ic/" + tsconcept4.getPrefix() + "-cor";
													    		}				    		
													    		prefixName4 = tsconcept4.getPrefix() + "_" + name4;
													    	}
													    	String labelText = StringEscapeUtils.escapeXml(header4.getLabel_ms_form());
												    		writer.newLine();
												    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../../../../def/ic/" + pfx4 + "_" + dtString + ".xsd#" + prefixName4 + "\" xlink:label=\"" + name4 + "\" xlink:title=\"" + name4 + "\"/>");
												    		writer.newLine();
														    writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName4 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName4 + "\" xml:lang=\"en\" id=\"" + labelName4 + "\">" + labelText + "</link:label>");
														    writer.newLine();
														    writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name4 + "\" xlink:to=\"" + labelName4 + "\" xlink:title=\"label: " + name4 + " to " + labelName4 + "\"/>");
											    		}
											    		
											    	}
									    		}
									    	}
									    }
							    	}
							    }
					    	}
					    }
				    }
				    
				    writer.newLine();
				    writer.write("  </link:labelLink>");
				    writer.newLine();
				    writer.write("</link:linkbase>");
				    
					isCompleted = true;
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("fail to write file: " + e.getMessage());
				}
			}
		}
		return isCompleted;
	}
	
	public boolean writeFormulaDef(String path, String dtString, tax_data_modal modal, String type) {
		boolean isCompleted = false;
		String regex = "^\\s+";
		List<tax_form> preList = modal.getTaxFormList();
		List<tax_form> tfList = preList.stream().filter(lform -> lform.getFormType().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());

		for(tax_form form : tfList) {
			String fname = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
				fname = fname.replaceAll(" ", "-");
//				fname = fname.replaceAll(".", "");
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/frm_lhdnm-sme-" + fname + "_" + dtString + ".xml"))) {
				
				writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd http://xbrl.org/2008/generic http://www.xbrl.org/2008/generic-link.xsd http://xbrl.org/2008/formula http://www.xbrl.org/2008/formula.xsd http://xbrl.org/2008/label http://www.xbrl.org/2008/generic-label.xsd http://xbrl.org/2008/reference http://www.xbrl.org/2008/generic-reference.xsd http://xbrl.org/2008/assertion/consistency http://www.xbrl.org/2008/consistency-assertion.xsd http://xbrl.org/2008/assertion/existence http://www.xbrl.org/2008/existence-assertion.xsd http://xbrl.org/2008/assertion/value http://www.xbrl.org/2008/value-assertion.xsd http://xbrl.org/2008/filter/concept http://www.xbrl.org/2008/concept-filter.xsd http://xbrl.org/2010/message http://www.xbrl.org/2010/generic-message.xsd http://xbrl.org/2010/message/validation http://www.xbrl.org/2010/validation-message.xsd http://xbrl.org/2008/filter/dimension http://www.xbrl.org/2008/dimension-filter.xsd\" xmlns:ca=\"http://xbrl.org/2008/assertion/consistency\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:fn=\"http://www.w3.org/2005/xpath-functions\" xmlns:variable=\"http://xbrl.org/2008/variable\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:xfi=\"http://www.xbrl.org/2008/function/instance\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:cf=\"http://xbrl.org/2008/filter/concept\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:valm=\"http://xbrl.org/2010/message/validation\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:ea=\"http://xbrl.org/2008/assertion/existence\" xmlns:df=\"http://xbrl.org/2008/filter/dimension\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:validation=\"http://xbrl.org/2008/validation\" xmlns:formula=\"http://xbrl.org/2008/formula\" xmlns:msg=\"http://xbrl.org/2010/message\" xmlns:va=\"http://xbrl.org/2008/assertion/value\" xmlns:label=\"http://xbrl.org/2008/label\">");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"" + form.getLinkRole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/link\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-link.xsd#standard-link-role\"/>");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2010/role/message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/generic-message.xsd#standard-message\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/variable-filter\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/variable.xsd#variable-filter\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/variable-set\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/variable.xsd#variable-set\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/variable-set-filter\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/variable.xsd#variable-set-filter\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/boolean-filter\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/boolean-filter.xsd#boolean-filter\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2010/assertion-unsatisfied-message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/validation-message.xsd#assertion-unsatisfied-message\"/>");
			    writer.newLine();
			    writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"" + form.getLinkRole() + "\">");
			    
			    writer.newLine();
			    writer.newLine();
				
			    writer.newLine();
			    writer.write("  </gen:link>");
				writer.newLine();
			    writer.write("</link:linkbase>");
			    
			    isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
		
		return isCompleted;
	}
	
}

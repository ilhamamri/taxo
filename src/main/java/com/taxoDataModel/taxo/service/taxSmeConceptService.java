package com.taxoDataModel.taxo.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.taxoDataModel.taxo.model.tax_concept;
import com.taxoDataModel.taxo.model.tax_sme_concept;
import com.taxoDataModel.taxo.repository.tax_sme_conceptRepository;

@Service
public class taxSmeConceptService {

	@Autowired
	tax_sme_conceptRepository repository;
	
	public static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd"); 
	public static final String SAMPLE_TAXO_FOLDER = "./src/main/output";
	
	public void save(List<tax_sme_concept> tList) {
		try {
			repository.saveAll(tList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<tax_sme_concept> getAll(){
		List<tax_sme_concept> tList = new ArrayList<tax_sme_concept>();
		try {
			tList = repository.findAll(Sort.by(Sort.Direction.ASC, "rno"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tList;
	}
	
	public List<tax_sme_concept> getAllByPrefix(String prefix){
		List<tax_sme_concept> tList = new ArrayList<tax_sme_concept>();
		try {
			tList = repository.findByPrefix(prefix);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tList;
	}
	
	public List<tax_sme_concept> getAllByModalId(Long id){
		List<tax_sme_concept> tList = new ArrayList<tax_sme_concept>();
		try {
			tList = repository.findByModalId(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tList;
	}
	
	public List<tax_sme_concept> getAllByWorkingSheet(String workingSheet){
		List<tax_sme_concept> tList = new ArrayList<tax_sme_concept>();
		try {
			tList = repository.findByWorkingSheet(workingSheet);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tList;
	}
	
	public tax_sme_concept findByName(String name, Long mid){
		tax_sme_concept tList = new tax_sme_concept();
		try {
			tList = repository.findTopByName(name, mid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tList;
	}

	public tax_sme_concept tableMapping(tax_sme_concept tax, int index, String obj) {
		tax_sme_concept newTax = tax;

		switch (index) {
		case 0:
			newTax.setWorkingSheet(obj);
			break;
		case 1:
			newTax.setPrefix(obj);
			break;
		case 2:
			newTax.setName(obj);
			break;
		case 3:
			newTax.setOccurence(obj);
			break;
		case 4:
			newTax.setConcept_id(obj);
			break;
		case 5:
			newTax.setType(obj);
			break;
		case 6:
			newTax.setSubstitution_group(obj);
			break;
		case 7:
			newTax.setEnum_headUsable(obj);
			break;
		case 8:
			newTax.setEnum_domain(obj);
			break;
		case 9:
			newTax.setEnum_linkrole(obj);
			break;
		case 10:
			newTax.setTypedDomainRef(obj);
			break;
		case 11:
			newTax.setBalance(obj);
			break;
		case 12:
			newTax.setPeriod_type(obj);
			break;
		case 13:
			newTax.setAbstract_col(Boolean.parseBoolean(obj));
			break;
		case 14:
			newTax.setNillable(Boolean.parseBoolean(obj));
			break;
		case 15:
			newTax.setLabel_en(obj);
			break;
		case 16:
			newTax.setLabel_ms(obj);
			break;
		case 17:
			if (newTax.getReference_name() == null) {
				newTax.setReference_name(obj);
			} else {
				newTax.setReference_name2(obj);
			}
			break;
		case 18:
			if (newTax.getReference_section() == null) {
				newTax.setReference_section(obj);
			} else {
				newTax.setReference_section2(obj);
			}
			break;
		case 19:
			if (newTax.getReference_subsection() == null) {
				newTax.setReference_subsection(obj);
			} else {
				newTax.setReference_subsection2(obj);
			}
			break;
		case 20:
			if (newTax.getReference_paragraph() == null) {
				newTax.setReference_paragraph(obj);
			} else {
				newTax.setReference_paragraph2(obj);
			}
			break;
		case 21:
			if (newTax.getReference_subparagraph() == null) {
				newTax.setReference_subparagraph(obj);
			} else {
				newTax.setReference_subparagraph2(obj);
			}
			break;
		case 22:
			newTax.setRemarks(obj);
			break;
		default:
		}
		return newTax;
	}
	
	public boolean writeTaxConcept(String path, String dtString, Long id) {
		boolean isCompleted = false;
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/lhdnm-sme-cor_" + dtString + ".xsd"))) {
		    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		    writer.newLine();
		    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
		    writer.newLine();
		    writer.write("<xsd:schema targetNamespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" attributeFormDefault=\"unqualified\" elementFormDefault=\"qualified\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\">");
		    writer.newLine();
		    writer.write("  <xsd:annotation>");
		    writer.newLine();
		    writer.write("    <xsd:appinfo>");
		    writer.newLine();
		    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-sme-en_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    writer.newLine();
		    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-sme-ms_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    writer.newLine();
		    writer.write("    </xsd:appinfo>");
		    writer.newLine();
		    writer.write("  </xsd:annotation>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://www.xbrl.org/2003/instance\" schemaLocation=\"http://www.xbrl.org/2003/xbrl-instance-2003-12-31.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://www.xbrl.org/dtr/type/non-numeric\" schemaLocation=\"http://www.xbrl.org/dtr/type/nonNumeric-2009-12-16.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://www.xbrl.org/dtr/type/numeric\" schemaLocation=\"http://www.xbrl.org/dtr/type/numeric-2009-12-16.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://xbrl.org/2005/xbrldt\" schemaLocation=\"http://www.xbrl.org/2005/xbrldt-2005.xsd\"/>");
		    
		    
		    List<tax_sme_concept> tList = getAllByModalId(id);
		    
		    for(tax_sme_concept tc : tList) {
		    	if(tc.getName() != null) {
		    		if(!Character.isDigit(tc.getName().charAt(0))) {
		    			writer.newLine();
				    	writer.write("  <xsd:element name=\"" + tc.getName() + "\" id=\"" + tc.getConcept_id() + "\" type=\"" + tc.getType() + "\" substitutionGroup=\"" + tc.getSubstitution_group() + "\" abstract=\"" + tc.isAbstract_col() + "\" nillable=\"" + tc.isNillable() + "\" xbrli:periodType=\"" + tc.getPeriod_type() + "\"/>");
				    
		    		}
		    	}
		    }
		    
		    writer.newLine();
		    writer.write("</xsd:schema>");
		    
		    isCompleted = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		return isCompleted;
	}
	
	public boolean writeTaxConceptLabelEn(String path, String dtString, Long id) {
		boolean isCompleted = false;
		
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/lab_lhdnm-sme-en_" + dtString + ".xml"))) {
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		    writer.newLine();
		    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
		    writer.newLine();
		    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
		    writer.newLine();
		    writer.write("  <link:labelLink xlink:type=\"extended\" xlink:role=\"http://www.xbrl.org/2003/role/link\">");
		    
		    List<tax_sme_concept> tList = getAllByModalId(id);
		    String corName = "lhdnm-sme-cor_" + dtString + ".xsd#";
		    for(tax_sme_concept tc : tList) {
		    	if(tc.getPrefix() != null && tc.getPrefix().equalsIgnoreCase("lhdnm-sme")) {
		    		String labelName = "label_" + tc.getName();
		    		String labelText = StringEscapeUtils.escapeXml(tc.getLabel_en());
		    		writer.newLine();
		    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\""+ corName + tc.getConcept_id() +"\" xlink:label=\"" + tc.getName() + "\" xlink:title=\"" + tc.getName() + "\"/>");
		    		writer.newLine();
		    		writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"en\" id=\"" + labelName + "\">" + labelText + "</link:label>");
		    		writer.newLine();
		    		writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + tc.getName() + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + tc.getName() + " to " + labelName + "\"/>");
		    	}		    	
		    }

		    writer.newLine();
		    writer.write("  </link:labelLink>");
		    writer.newLine();
		    writer.write("</link:linkbase>");
		    
		    isCompleted = true;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		
		return isCompleted;
	}
	
	public boolean writeTaxConceptLabelMs(String path, String dtString, Long id) {
		boolean isCompleted = false;
		
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/lab_lhdnm-sme-ms_" + dtString + ".xml"))) {
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		    writer.newLine();
		    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
		    writer.newLine();
		    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
		    writer.newLine();
		    writer.write("  <link:labelLink xlink:type=\"extended\" xlink:role=\"http://www.xbrl.org/2003/role/link\">");
		    
		    List<tax_sme_concept> tList = getAllByModalId(id);
		    String corName = "lhdnm-sme-cor_" + dtString + ".xsd#";
		    for(tax_sme_concept tc : tList) {
		    	if(tc.getPrefix() != null && tc.getPrefix().equalsIgnoreCase("lhdnm-sme")) {
		    		String labelName = "label_" + tc.getName();
		    		String labelText = StringEscapeUtils.escapeXml(tc.getLabel_ms());
		    		writer.newLine();
		    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\""+ corName + tc.getConcept_id() +"\" xlink:label=\"" + tc.getName() + "\" xlink:title=\"" + tc.getName() + "\"/>");
		    		writer.newLine();
		    		writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"en\" id=\"" + labelName + "\">" + labelText + "</link:label>");
		    		writer.newLine();
		    		writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + tc.getName() + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + tc.getName() + " to " + labelName + "\"/>");
		    	}		    	
		    }

		    writer.newLine();
		    writer.write("  </link:labelLink>");
		    writer.newLine();
		    writer.write("</link:linkbase>");
		    
		    isCompleted = true;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		
		return isCompleted;
	}
	
}

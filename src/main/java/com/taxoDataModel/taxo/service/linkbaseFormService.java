package com.taxoDataModel.taxo.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringEscapeUtils;

import com.fasterxml.jackson.databind.ser.std.ObjectArraySerializer;
import com.taxoDataModel.taxo.model.linkbase_form;
import com.taxoDataModel.taxo.model.presentation_detail;
import com.taxoDataModel.taxo.model.presentation_formula;
import com.taxoDataModel.taxo.model.tax_concept;
import com.taxoDataModel.taxo.model.tax_enum;
import com.taxoDataModel.taxo.model.tax_form;
import com.taxoDataModel.taxo.model.tax_sme_concept;
import com.taxoDataModel.taxo.model.tax_sme_enum;
import com.taxoDataModel.taxo.pojo.tempData;
import com.taxoDataModel.taxo.repository.linkbase_formRepository;

@Service
public class linkbaseFormService {
	
	private Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

	@Autowired
	linkbase_formRepository repository;
	
	@Autowired
	presentationFormulaService pfService;
	
	@Autowired
	taxConceptService tcService;
	
	@Autowired
	taxEnumService enumService;
	
	@Autowired
	taxSmeConceptService tscService;
	
	@Autowired
	taxSmeEnumService tseService;
	
	public void save(List<linkbase_form> formList) {
		try {
			repository.saveAll(formList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveSingle(linkbase_form form) {
		try {
			repository.save(form);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<linkbase_form> getAll(){
		return repository.findAll();
	}
	
	public List<linkbase_form> findByModalId(Long id){
		return repository.findByCategoryAndModalId(id);
	}
	
	public List<linkbase_form> findByModalId2(Long id){
		return repository.findByModalId(id);
	}
	
	public linkbase_form getSingle(Long id){
		return repository.findById(id).get();
	}
	
	public linkbase_form getByName(String name){
		return repository.findByName(name);
	}
	
	public linkbase_form getByModalIdAndName(Long id, String name){
		return repository.findByModalIdAndName(id, name);
	}
	
	public linkbase_form getByDefinition(String definition){
		return repository.findByDefinition(definition);
	}
	
	public linkbase_form getByNamespace(String namespace, Long modalId){
		return repository.findByNamespaceAndModalId(modalId, namespace);
	}
	
	public linkbase_form tableMapping(linkbase_form tax, int index, String obj) {
		linkbase_form form = tax;
		
		switch (index) {
		case 0:
			form.setName(obj.toLowerCase());
			break;
		case 1:
//			String val = form.getName();
			form.setDefinition(obj);
			break;
		case 2:
//			String val = form.getName();
			form.setDefinition_ms(obj);
			break;
		default:
		}
		
		return form;
	}
	
	public boolean writeLinkbaseForm(String path, String dtString, Long id, String type) {
		boolean isCompleted = false;
		List<linkbase_form> linkbaseList1 = findByModalId(id);
		List<linkbase_form> linkbaseList;
		if(type.equalsIgnoreCase("lhdnm-sme")) {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		} else {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		}
		System.out.println("list count: " + linkbaseList1.size());
		for(linkbase_form form : linkbaseList) {
			System.out.println("Form Name: " + form.getName());
			String fname = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
//				fname = fname.replaceAll(" ", "-");
//				fname = fname.replaceAll(".", "");
				System.out.println("fname: " + fname);
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/lhdnm-" + fname + "_" + dtString + ".xsd"))) {
				System.out.println("Path: " + path + "/lhdnm-" + fname + "_" + dtString + ".xsd");
			    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    writer.write("<xsd:schema targetNamespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + fname + "\" attributeFormDefault=\"unqualified\" elementFormDefault=\"qualified\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:" + form.getName() + "=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + form.getName() + "\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:lhdnm-fdn=\"http://www.hasil.gov.my/xbrl/" + dtString + "/fdn\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
			    writer.newLine();
			    writer.write("  <xsd:annotation>");
			    writer.newLine();
			    writer.write("    <xsd:appinfo>");
			    writer.newLine();
			    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"frm_lhdnm-" + fname + "_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			    writer.newLine();
			    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"frm_lab_lhdnm-" + fname + "-en_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			    writer.newLine();
			    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"frm_lab_lhdnm-" + fname + "-ms_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			    writer.newLine();
			    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"pre_lhdnm-" + fname + "_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/presentationLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			    writer.newLine();
			    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"def_lhdnm-" + fname + "_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/definitionLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			    writer.newLine();
			    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-" + fname + "-en_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			    writer.newLine();
			    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-" + fname + "-ms_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			    writer.newLine();
			    writer.write("      <link:roleType roleURI=\"" + form.getLinkrole() + "\" id=\"" + fname + "\">");
			    writer.newLine();
			    writer.write("        <link:definition>" + form.getNamespace() + ": " + form.getDefinition() + "</link:definition>");
			    writer.newLine();
			    writer.write("        <link:usedOn>link:presentationLink</link:usedOn>");
			    writer.newLine();
			    writer.write("        <link:usedOn>link:definitionLink</link:usedOn>");
			    writer.newLine();
			    writer.write("        <link:usedOn>link:labelLink</link:usedOn>");
			    writer.newLine();
			    writer.write("        <link:usedOn>gen:link</link:usedOn>");
			    writer.newLine();
			    writer.write("      </link:roleType>");
			    writer.newLine();
			    writer.write("    </xsd:appinfo>");
			    writer.newLine();
			    writer.write("  </xsd:annotation>");
			    
			    writer.newLine();
			    writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" schemaLocation=\"../enumerations/lhdnm-enum_" + dtString + ".xsd\"/>");
			    writer.newLine();
			    writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" schemaLocation=\"../../../../def/ic/lhdnm-cor_" + dtString + ".xsd\"/>");
			    writer.newLine();
			    writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/label\" schemaLocation=\"http://www.xbrl.org/2008/generic-label.xsd\"/>");
			    writer.newLine();
			    writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/reference\" schemaLocation=\"http://www.xbrl.org/2008/generic-reference.xsd\"/>");
			    writer.newLine();
			    writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/generic\" schemaLocation=\"http://www.xbrl.org/2008/generic-link.xsd\"/>");
			    writer.newLine();
			    writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/fdn\" schemaLocation=\"../../../../def/fdn/lhdnm-fdn_" + dtString + ".xsd\"/>");
			    
			    writer.newLine();
			    writer.write("</xsd:schema>");
			    
			    isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
		return isCompleted;
	}
	
	public boolean writeLinkbaseSmeForm(String path, String dtString, Long id, String type) {
		boolean isCompleted = false;
		List<linkbase_form> linkbaseList1 = findByModalId(id);
		List<linkbase_form> linkbaseList;
		if(type.equalsIgnoreCase("lhdnm-sme")) {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		} else {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		}
				
		for(linkbase_form form : linkbaseList) {
			System.out.println("Form Name: " + form.getName());
			String fname = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
//				fname = fname.replaceAll(" ", "-");
//				fname = fname.replaceAll(".", "");
				System.out.println("fname: " + fname);
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/lhdnm-" + fname + "_" + dtString + ".xsd"))) {
				System.out.println("Path: " + path + "/lhdnm-" + fname + "_" + dtString + ".xsd");
			    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    writer.write("<xsd:schema targetNamespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + fname + "\" attributeFormDefault=\"unqualified\" elementFormDefault=\"qualified\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:" + form.getName() + "=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + form.getName() + "\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:lhdnm-fdn=\"http://www.hasil.gov.my/xbrl/" + dtString + "/fdn\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
			    writer.newLine();
			    writer.write("  <xsd:annotation>");
			    writer.newLine();
			    writer.write("    <xsd:appinfo>");
			    writer.newLine();
			    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"frm_lhdnm-" + fname + "_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			    writer.newLine();
			    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"frm_lab_lhdnm-" + fname + "-en_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			    writer.newLine();
			    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"frm_lab_lhdnm-" + fname + "-ms_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			    writer.newLine();
			    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"pre_lhdnm-" + fname + "_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/presentationLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			    writer.newLine();
			    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"def_lhdnm-" + fname + "_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/definitionLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			    writer.newLine();
			    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-" + fname + "-en_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			    writer.newLine();
			    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-" + fname + "-ms_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			    writer.newLine();
			    writer.write("      <link:roleType roleURI=\"" + form.getLinkrole() + "\" id=\"" + fname + "\">");
			    writer.newLine();
			    writer.write("        <link:definition>" + form.getNamespace() + ": " + form.getDefinition() + "</link:definition>");
			    writer.newLine();
			    writer.write("        <link:usedOn>link:presentationLink</link:usedOn>");
			    writer.newLine();
			    writer.write("        <link:usedOn>link:definitionLink</link:usedOn>");
			    writer.newLine();
			    writer.write("        <link:usedOn>link:labelLink</link:usedOn>");
			    writer.newLine();
			    writer.write("        <link:usedOn>gen:link</link:usedOn>");
			    writer.newLine();
			    writer.write("      </link:roleType>");
			    writer.newLine();
			    writer.write("    </xsd:appinfo>");
			    writer.newLine();
			    writer.write("  </xsd:annotation>");
			    
			    writer.newLine();
			    writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/sme-enum\" schemaLocation=\"../enumerations/lhdnm-sme-enum_" + dtString + ".xsd\"/>");
			    writer.newLine();
			    writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/lhdnm-sme\" schemaLocation=\"../../../../def/ic/lhdnm-sme-cor_" + dtString + ".xsd\"/>");
			    writer.newLine();
			    writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/label\" schemaLocation=\"http://www.xbrl.org/2008/generic-label.xsd\"/>");
			    writer.newLine();
			    writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/reference\" schemaLocation=\"http://www.xbrl.org/2008/generic-reference.xsd\"/>");
			    writer.newLine();
			    writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/generic\" schemaLocation=\"http://www.xbrl.org/2008/generic-link.xsd\"/>");
			    writer.newLine();
			    writer.write("  <xsd:import namespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/fdn\" schemaLocation=\"../../../../def/fdn/lhdnm-fdn_" + dtString + ".xsd\"/>");
			    
			    writer.newLine();
			    writer.write("</xsd:schema>");
			    
			    isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
		return isCompleted;
	}
	
	public boolean writeLinkbaseDef(String path, String dtString, Long id, String type) {
		boolean isCompleted = false;
		List<linkbase_form> linkbaseList1 = findByModalId(id);
		List<linkbase_form> linkbaseList = null;
		List<tax_concept> taxConceptList = tcService.getAllByModalId(id);
	    List<tax_enum> taxEnumList = enumService.getAllByModalId(id);
		if(type.equalsIgnoreCase("lhdnm-sme")) {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		} else {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		}
		
		for(linkbase_form form : linkbaseList) {
			String fname = "";
//			String name2 = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
//				fname = fname.replaceAll(" ", "-");
//				name2 = form.getName().toUpperCase();
//				name2 = name2.replaceAll("-", "");
//				fname = fname.replaceAll(".", "");
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/def_lhdnm-" + fname + "_" + dtString + ".xml"))) {
				
			    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-" + dtString + ".xsd\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\">");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"" + form.getLinkrole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/int/dim/arcrole/all\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2005/xbrldt-2005.xsd#all\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/int/dim/arcrole/dimension-domain\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2005/xbrldt-2005.xsd#dimension-domain\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/int/dim/arcrole/domain-member\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2005/xbrldt-2005.xsd#domain-member\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/int/dim/arcrole/hypercube-dimension\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2005/xbrldt-2005.xsd#hypercube-dimension\"/>");
			    writer.newLine();
			    writer.write("  <link:definitionLink xlink:type=\"extended\" xlink:role=\"" + form.getLinkrole() + "\">");
			    
			    List<presentation_detail> preList = form.getPresentationList().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
			    
			    ArrayList<String> tempArray = new ArrayList<>();
			    
			    for(presentation_detail pre : preList) {
			    				    	
			    	String prefixName = pre.getPrefix() + "_" + pre.getName();
			    	String pfx = "../../../../def/ic/lhdnm-cor";
			    	String name = pre.getName();
			    	tax_enum te;
			    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
			    	if(Objects.isNull(tconcept)) {
			    		te = taxEnumList.stream().filter(tcl -> pre.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
				    	if(Objects.isNull(te)) {
				    		continue;
				    	} else {
				    		name = te.getName();
				    		pfx = "../enumerations/" + te.getPrefix();
				    		prefixName = te.getPrefix() + "_" + name;
				    	}
			    	} else {
			    		name = tconcept.getName();
			    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
			    			pfx = "../enumerations/" + tconcept.getPrefix();
			    		} else {
			    			pfx = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
			    		}				    		
			    		prefixName = tconcept.getPrefix() + "_" + name;
			    	}
			    	
			    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre.getName())).findAny().orElse(null) == null) {
			    		tempArray.add(pre.getName());
			    		writer.newLine();
					    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx + "_" + dtString + ".xsd#" + prefixName + "\" xlink:label=\"" + name + "\" xlink:title=\"" + name + "\"/>");
			    	} else {
			    		
			    	}
				    
				    int count1 = 0;
				    List<presentation_detail> preList1 = pre.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
				    
				    for(presentation_detail pre1 : preList1) {
				    	count1++;
				    	String prefixName1 = pre1.getPrefix() + "_" + pre1.getName();
				    	String pfx1 = "../../../../def/ic/lhdnm-cor";
				    	String name1 = pre1.getName();
				    	tax_enum te1;
				    	tax_concept tconcept1 = taxConceptList.stream().filter(tcl -> pre1.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
				    	if(Objects.isNull(tconcept1)) {
				    		te1 = taxEnumList.stream().filter(tcl -> pre1.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
					    	if(Objects.isNull(te1)) {
					    		continue;
					    	} else {
					    		name1 = te1.getName();
					    		pfx1 = "../enumerations/" + te1.getPrefix();
					    		prefixName1 = te1.getPrefix() + "_" + name1;
					    	}
				    	} else {
				    		name1 = tconcept1.getName();
				    		if(tconcept1.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
				    			pfx1 = "../enumerations/" + tconcept1.getPrefix();
				    		} else {
				    			pfx1 = "../../../../def/ic/" + tconcept1.getPrefix() + "-cor";
				    		}				    		
				    		prefixName1 = tconcept1.getPrefix() + "_" + name1;
				    	}
				    	
				    	String arcrole1 = "";
				    	tax_concept tc1 = taxConceptList.stream().filter(tcl -> pre.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
					    if(tc1 != null) {
					    	
					    } else {
					    	tc1 = tcService.findByName(pre.getName(), id);
					    }
					    
					    if (tc1 != null) {
					    	System.out.println("tc1 "+tc1.getSubstitution_group());
						    if(tc1.getSubstitution_group().equalsIgnoreCase("xbrldt:hypercubeItem")) {
						    	arcrole1 = "http://xbrl.org/int/dim/arcrole/hypercube-dimension";
						    } else if(tc1.getSubstitution_group().equalsIgnoreCase("xbrldt:dimensionItem")) {
						    	arcrole1 = "http://xbrl.org/int/dim/arcrole/dimension-domain";
						    } else if(tc1.getSubstitution_group().equalsIgnoreCase("xbrli:item")) {
						    	arcrole1 = "http://xbrl.org/int/dim/arcrole/domain-member";
						    } else {
						    	
						    }
						    
					    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre1.getName())).findAny().orElse(null) == null) {
					    		tempArray.add(pre1.getName());
							    
							    writer.newLine();
							    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx1 + "_" + dtString + ".xsd#" + prefixName1 + "\" xlink:label=\"" + name1 + "\" xlink:title=\"" + name1 + "\"/>");
							    writer.newLine();
							    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole1 + "\" xlink:from=\"" + name + "\" xlink:to=\"" + name1 + "\" xlink:title=\"definition: " + name + " to " + name1 + "\" order=\"" + count1 + ".0\"/>");
							    
					    	} else {
					    		writer.newLine();
							    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole1 + "\" xlink:from=\"" + name + "\" xlink:to=\"" +name1 + "\" xlink:title=\"definition: " + name + " to " + name1 + "\" order=\"" + count1 + ".0\"/>");
							    
					    	}
						    
						    if(!pre1.getChildren().isEmpty()) {
						    	int count2 = 0;
						    	List<presentation_detail> preList2 = pre1.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
							    for(presentation_detail pre2 : preList2) {
							    	count2++;
							    	String prefixName2 = pre2.getPrefix() + "_" + pre2.getName();
							    	String pfx2 = "../../../../def/ic/lhdnm-cor";
							    	String name2 = pre2.getName();
							    	tax_enum te2;
							    	tax_concept tconcept2 = taxConceptList.stream().filter(tcl -> pre2.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
							    	if(Objects.isNull(tconcept2)) {
							    		te2 = taxEnumList.stream().filter(tcl -> pre2.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
								    	if(Objects.isNull(te2)) {
								    		continue;
								    	} else {
								    		name2 = te2.getName();
								    		pfx2 = "../enumerations/" + te2.getPrefix();
								    		prefixName2 = te2.getPrefix() + "_" + name2;
								    	}
							    	} else {
							    		name2 = tconcept2.getName();
							    		if(tconcept2.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
							    			pfx2 = "../enumerations/" + tconcept2.getPrefix();
							    		} else {
							    			pfx2 = "../../../../def/ic/" + tconcept2.getPrefix() + "-cor";
							    		}				    		
							    		prefixName2 = tconcept2.getPrefix() + "_" + name2;
							    	}
							    	
							    	String arcrole2 = "";
							    	tax_concept tc2 = taxConceptList.stream().filter(tcl -> pre1.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
								    if(tc2 != null) {
								    	
								    } else {
								    	tc2 = tcService.findByName(pre1.getName(), id);
								    }
								    
								    if (tc2 != null) {
								    	System.out.println("tc2 "+tc2.getSubstitution_group());
								    	if(pre1.getName().contains("Abstract") && pre2.getName().contains("Table")) {
									    	arcrole2 = "http://xbrl.org/int/dim/arcrole/all";
									    } else if(pre2.getName().contains("Table")) {
									    	arcrole2 = "http://xbrl.org/int/dim/arcrole/all";
									    } else if(tc2.getSubstitution_group().equalsIgnoreCase("xbrldt:hypercubeItem")) {
									    	arcrole2 = "http://xbrl.org/int/dim/arcrole/hypercube-dimension";
									    } else if(tc2.getSubstitution_group().equalsIgnoreCase("xbrldt:dimensionItem")) {
									    	arcrole2 = "http://xbrl.org/int/dim/arcrole/dimension-domain";
									    } else if(tc2.getSubstitution_group().equalsIgnoreCase("xbrli:item")) {
									    	arcrole2 = "http://xbrl.org/int/dim/arcrole/domain-member";
									    } else {
									    	
									    }
								    	
									    
									    if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre2.getName())).findAny().orElse(null) == null) {
								    		tempArray.add(pre2.getName());
										    
								    		writer.newLine();
										    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx2 + "_" + dtString + ".xsd#" + prefixName2 + "\" xlink:label=\"" + name2 + "\" xlink:title=\"" + name2 + "\"/>");
										    writer.newLine();
										    if(pre1.getName().contains("Abstract") && pre2.getName().contains("Table")) {
										    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole2 + "\" xlink:from=\"" + name1 + "\" xlink:to=\"" + name2 + "\" xlink:title=\"definition: " + name1 + " to " + name2 + "\" order=\"" + count2 + ".0\" xbrldt:contextElement=\"scenario\" xbrldt:closed=\"true\" />");
										    } else {
										    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole2 + "\" xlink:from=\"" + name1 + "\" xlink:to=\"" + name2 + "\" xlink:title=\"definition: " + name1 + " to " + name2 + "\" order=\"" + count2 + ".0\"/>");
										    }
										    										    
								    	} else {
								    		writer.newLine();
										    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole2 + "\" xlink:from=\"" + name1 + "\" xlink:to=\"" + name2 + "\" xlink:title=\"definition: " + name1 + " to " + name2 + "\" order=\"" + count2 + ".0\"/>");
										    
								    	}
								    	
									    if(!pre2.getChildren().isEmpty()) {
									    	int count3 = 0;
									    	List<presentation_detail> preList3 = pre2.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
										    for(presentation_detail pre3 : preList3) {
										    	count3++;
										    	String prefixName3 = pre3.getPrefix() + "_" + pre3.getName();
										    	String pfx3 = "../../../../def/ic/lhdnm-cor";
										    	String name3 = pre3.getName();
										    	tax_enum te3;
										    	tax_concept tconcept3 = taxConceptList.stream().filter(tcl -> pre3.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
										    	if(Objects.isNull(tconcept3)) {
										    		te3 = taxEnumList.stream().filter(tcl -> pre3.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
											    	if(Objects.isNull(te3)) {
											    		continue;
											    	} else {
											    		name3 = te3.getName();
											    		pfx3 = "../enumerations/" + te3.getPrefix();
											    		prefixName3 = te3.getPrefix() + "_" + name3;
											    	}
										    	} else {
										    		name3 = tconcept3.getName();
										    		if(tconcept3.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
										    			pfx3 = "../enumerations/" + tconcept3.getPrefix();
										    		} else {
										    			pfx3 = "../../../../def/ic/" + tconcept3.getPrefix() + "-cor";
										    		}				    		
										    		prefixName3 = tconcept3.getPrefix() + "_" + name3;
										    	}
										    	
										    	System.out.println("pre2 "+ pre2.getName());
										    	String arcrole3 = "";
										    	tax_concept tc3 = taxConceptList.stream().filter(tcl -> pre2.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
											    if(tc3 != null) {
											    	
											    } else {
											    	tc3 = tcService.findByName(pre2.getName(), id);
											    }
											    
											    if (tc3 != null) {
											    	System.out.println("tc3 "+tc3.getSubstitution_group());
											    	if(pre2.getName().contains("Abstract") && pre3.getName().contains("Table")) {
												    	arcrole3 = "http://xbrl.org/int/dim/arcrole/all";
												    } else if(tc3.getSubstitution_group().equalsIgnoreCase("xbrldt:hypercubeItem")) {
												    	arcrole3 = "http://xbrl.org/int/dim/arcrole/hypercube-dimension";
												    } else if(tc3.getSubstitution_group().equalsIgnoreCase("xbrldt:dimensionItem")) {
												    	arcrole3 = "http://xbrl.org/int/dim/arcrole/dimension-domain";
												    } else if(tc3.getSubstitution_group().equalsIgnoreCase("xbrli:item")) {
												    	arcrole3 = "http://xbrl.org/int/dim/arcrole/domain-member";
												    } else {
												    	
												    }
												    
												    if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre3.getName())).findAny().orElse(null) == null) {
											    		tempArray.add(pre3.getName());
													    
											    		writer.newLine();
													    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx3 + "_" + dtString + ".xsd#" + prefixName3 + "\" xlink:label=\"" + name3 + "\" xlink:title=\"" + name3 + "\"/>");
													    writer.newLine();
													    if(pre2.getName().contains("Abstract") && pre3.getName().contains("Table")) {
													    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole3 + "\" xlink:from=\"" + name2 + "\" xlink:to=\"" + name3 + "\" xlink:title=\"definition: " + name2 + " to " + name3 + "\" order=\"" + count3 + ".0\" xbrldt:contextElement=\"scenario\" xbrldt:closed=\"true\" />");
													    } else {
													    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole3 + "\" xlink:from=\"" + name2 + "\" xlink:to=\"" + name3 + "\" xlink:title=\"definition: " + name2 + " to " + name3 + "\" order=\"" + count3 + ".0\"/>");
													    }
													    													    
											    	} else {
											    		writer.newLine();
													    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole3 + "\" xlink:from=\"" + name2 + "\" xlink:to=\"" + name3 + "\" xlink:title=\"definition: " + name2 + " to " + name3 + "\" order=\"" + count3 + ".0\"/>");
													    
											    	}
											    	
												    if(!pre3.getChildren().isEmpty()) {
												    	int count4 = 0;
												    	List<presentation_detail> preList4 = pre3.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
													    for(presentation_detail pre4 : preList4) {
													    	count4++;
													    	String prefixName4 = pre4.getPrefix() + "_" + pre4.getName();
													    	String pfx4 = "../../../../def/ic/lhdnm-cor";
													    	String name4 = pre4.getName();
													    	tax_enum te4;
													    	System.out.println("name "+pre4.getName());
													    	tax_concept tconcept4 = taxConceptList.stream().filter(tcl -> pre4.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
													    	if(Objects.isNull(tconcept4)) {
													    		te4 = taxEnumList.stream().filter(tcl -> pre4.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
														    	if(Objects.isNull(te4)) {
														    		continue;
														    	} else {
														    		name4 = te4.getName();
														    		pfx4 = "../enumerations/" + te4.getPrefix();
														    		prefixName4 = te4.getPrefix() + "_" + name4;
														    	}
													    	} else {
													    		name4 = tconcept4.getName();
													    		if(tconcept4.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
													    			pfx4 = "../enumerations/" + tconcept4.getPrefix();
													    		} else {
													    			pfx4 = "../../../../def/ic/" + tconcept4.getPrefix() + "-cor";
													    		}				    		
													    		prefixName4 = tconcept4.getPrefix() + "_" + name4;
													    	}
													    	
													    	String arcrole4 = "";
													    	tax_concept tc4 = taxConceptList.stream().filter(tcl -> pre3.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
														    if(tc4 != null) {
														    	
														    } else {
														    	tc4 = tcService.findByName(pre3.getName(), id);
														    }
														    
														    if (tc4 != null) {
														    	System.out.println("tc4 "+tc4.getSubstitution_group());
														    	if(pre3.getName().contains("Abstract") && pre4.getName().contains("Table")) {
															    	arcrole4 = "http://xbrl.org/int/dim/arcrole/all";
															    } else if(tc4.getSubstitution_group().equalsIgnoreCase("xbrldt:hypercubeItem")) {
															    	arcrole4 = "http://xbrl.org/int/dim/arcrole/hypercube-dimension";
															    } else if(tc4.getSubstitution_group().equalsIgnoreCase("xbrldt:dimensionItem")) {
															    	arcrole4 = "http://xbrl.org/int/dim/arcrole/dimension-domain";
															    } else if(tc4.getSubstitution_group().equalsIgnoreCase("xbrli:item")) {
															    	arcrole4 = "http://xbrl.org/int/dim/arcrole/domain-member";
															    } else {
															    	
															    }
															    
															    if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre4.getName())).findAny().orElse(null) == null) {
														    		tempArray.add(pre4.getName());
																    
														    		writer.newLine();
																    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx4 + "_" + dtString + ".xsd#" + prefixName4 + "\" xlink:label=\"" + name4 + "\" xlink:title=\"" + name4 + "\"/>");
																    writer.newLine();
																    if(pre3.getName().contains("Abstract") && pre4.getName().contains("Table")) {
																    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole4 + "\" xlink:from=\"" + name3 + "\" xlink:to=\"" + name4 + "\" xlink:title=\"definition: " + name3 + " to " + name4 + "\" order=\"" + count4 + ".0\" xbrldt:contextElement=\"scenario\" xbrldt:closed=\"true\" />");
																    } else {
																    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole4 + "\" xlink:from=\"" + name3 + "\" xlink:to=\"" + name4 + "\" xlink:title=\"definition: " + name3 + " to " + name4 + "\" order=\"" + count4 + ".0\"/>");
																    }
																    
																    
														    	} else {
														    		writer.newLine();
																    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole4 + "\" xlink:from=\"" + name3 + "\" xlink:to=\"" + name4 + "\" xlink:title=\"definition: " + name3 + " to " + name4 + "\" order=\"" + count4 + ".0\"/>");
																    
														    	}
															    if(!pre4.getChildren().isEmpty()) {
															    	int count5 = 0;
															    	List<presentation_detail> preList5 = pre4.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
																    for(presentation_detail pre5 : preList5) {
																    	count5++;
																    	String prefixName5 = pre5.getPrefix() + "_" + pre5.getName();
																    	String pfx5 = "../../../../def/ic/lhdnm-cor";
																    	String name5 = pre5.getName();
																    	tax_enum te5;
																    	tax_concept tconcept5 = taxConceptList.stream().filter(tcl -> pre5.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																    	if(Objects.isNull(tconcept5)) {
																    		te5 = taxEnumList.stream().filter(tcl -> pre5.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																	    	if(Objects.isNull(te5)) {
																	    		continue;
																	    	} else {
																	    		name5 = te5.getName();
																	    		pfx5 = "../enumerations/" + te5.getPrefix();
																	    		prefixName5 = te5.getPrefix() + "_" + name5;
																	    	}
																    	} else {
																    		name5 = tconcept5.getName();
																    		if(tconcept5.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
																    			pfx5 = "../enumerations/" + tconcept5.getPrefix();
																    		} else {
																    			pfx5 = "../../../../def/ic/" + tconcept5.getPrefix() + "-cor";
																    		}				    		
																    		prefixName5 = tconcept5.getPrefix() + "_" + name5;
																    	}
																    	
																    	String arcrole5 = "";
																    	tax_concept tc5 = taxConceptList.stream().filter(tcl -> pre4.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																	    if(tc5 != null) {
																	    	
																	    } else {
																	    	tc5 = tcService.findByName(pre4.getName(), id);
																	    }
																	    
																	    if (tc5 != null) {
																	    	System.out.println("tc5 "+tc5.getSubstitution_group());
																	    	if(pre4.getName().contains("Abstract") && pre5.getName().contains("Table")) {
																		    	arcrole5 = "http://xbrl.org/int/dim/arcrole/all";
																		    } else if(tc5.getSubstitution_group().equalsIgnoreCase("xbrldt:hypercubeItem")) {
																		    	arcrole5 = "http://xbrl.org/int/dim/arcrole/hypercube-dimension";
																		    } else if(tc5.getSubstitution_group().equalsIgnoreCase("xbrldt:dimensionItem")) {
																		    	arcrole5 = "http://xbrl.org/int/dim/arcrole/dimension-domain";
																		    } else if(tc5.getSubstitution_group().equalsIgnoreCase("xbrli:item")) {
																		    	arcrole5 = "http://xbrl.org/int/dim/arcrole/domain-member";
																		    } else {
																		    	
																		    }
																		    
																		    if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre5.getName())).findAny().orElse(null) == null) {
																	    		tempArray.add(pre5.getName());
																			    
																	    		writer.newLine();
																			    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx5 + "_" + dtString + ".xsd#" + prefixName5 + "\" xlink:label=\"" + name5 + "\" xlink:title=\"" + name5 + "\"/>");
																			    writer.newLine();
																			    if(pre4.getName().contains("Abstract") && pre5.getName().contains("Table")) {
																			    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole5 + "\" xlink:from=\"" + name4 + "\" xlink:to=\"" + name5 + "\" xlink:title=\"definition: " + name4 + " to " + name5 + "\" order=\"" + count5 + ".0\" xbrldt:contextElement=\"scenario\" xbrldt:closed=\"true\" />");
																			    } else {
																			    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole5 + "\" xlink:from=\"" + name4 + "\" xlink:to=\"" + name5 + "\" xlink:title=\"definition: " + name4 + " to " + name5 + "\" order=\"" + count5 + ".0\"/>");
																			    }
																			    
																			    
																	    	} else {
																	    		writer.newLine();
																			    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole5 + "\" xlink:from=\"" + name4 + "\" xlink:to=\"" + name5 + "\" xlink:title=\"definition: " + name4 + " to " + name5 + "\" order=\"" + count5 + ".0\"/>");
																			    
																	    	}
																		    
																		    if(!pre5.getChildren().isEmpty()) {
																		    	int count6 = 0;
																		    	List<presentation_detail> preList6 = pre5.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
																			    for(presentation_detail pre6 : preList6) {
																			    	count6++;
																			    	String prefixName6 = pre6.getPrefix() + "_" + pre6.getName();
																			    	String pfx6 = "../../../../def/ic/lhdnm-cor";
																			    	String name6 = pre6.getName();
																			    	tax_enum te6;
																			    	tax_concept tconcept6 = taxConceptList.stream().filter(tcl -> pre6.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																			    	if(Objects.isNull(tconcept6)) {
																			    		te6 = taxEnumList.stream().filter(tcl -> pre6.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																				    	if(Objects.isNull(te6)) {
																				    		continue;
																				    	} else {
																				    		name6 = te6.getName();
																				    		pfx6 = "../enumerations/" + te6.getPrefix();
																				    		prefixName6 = te6.getPrefix() + "_" + name6;
																				    	}
																			    	} else {
																			    		name6 = tconcept6.getName();
																			    		if(tconcept6.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
																			    			pfx6 = "../enumerations/" + tconcept6.getPrefix();
																			    		} else {
																			    			pfx6 = "../../../../def/ic/" + tconcept6.getPrefix() + "-cor";
																			    		}				    		
																			    		prefixName6 = tconcept6.getPrefix() + "_" + name6;
																			    	}
																			    	
																			    	String arcrole6 = "";
																			    	tax_concept tc6 = taxConceptList.stream().filter(tcl -> pre5.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																				    if(tc6 != null) {
																				    	
																				    } else {
																				    	tc6 = tcService.findByName(pre5.getName(), id);
																				    }
																				    
																				    if (tc6 != null) {
																				    	System.out.println("tc6 "+tc6.getSubstitution_group());
																				    	if(pre5.getName().contains("Abstract") && pre6.getName().contains("Table")) {
																					    	arcrole6 = "http://xbrl.org/int/dim/arcrole/all";
																					    } else if(tc6.getSubstitution_group().equalsIgnoreCase("xbrldt:hypercubeItem")) {
																					    	arcrole6 = "http://xbrl.org/int/dim/arcrole/hypercube-dimension";
																					    } else if(tc6.getSubstitution_group().equalsIgnoreCase("xbrldt:dimensionItem")) {
																					    	arcrole6 = "http://xbrl.org/int/dim/arcrole/dimension-domain";
																					    } else if(tc6.getSubstitution_group().equalsIgnoreCase("xbrli:item")) {
																					    	arcrole6 = "http://xbrl.org/int/dim/arcrole/domain-member";
																					    } else {
																					    	
																					    }
																					    
																					    if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre6.getName())).findAny().orElse(null) == null) {
																				    		tempArray.add(pre6.getName());
																						    
																				    		writer.newLine();
																						    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx6 + "_" + dtString + ".xsd#" + prefixName6 + "\" xlink:label=\"" + name6 + "\" xlink:title=\"" + name6 + "\"/>");
																						    writer.newLine();
																						    if(pre5.getName().contains("Abstract") && pre6.getName().contains("Table")) {
																						    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole6 + "\" xlink:from=\"" + name5 + "\" xlink:to=\"" + name6 + "\" xlink:title=\"definition: " + name5 + " to " + name6 + "\" order=\"" + count6 + ".0\" xbrldt:contextElement=\"scenario\" xbrldt:closed=\"true\" />");
																						    } else {
																						    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole6 + "\" xlink:from=\"" + name5 + "\" xlink:to=\"" + name6 + "\" xlink:title=\"definition: " + name5 + " to " + name6 + "\" order=\"" + count6 + ".0\"/>");
																						    }
																						    
																						    
																				    	} else {
																				    		writer.newLine();
																						    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole6 + "\" xlink:from=\"" + name5 + "\" xlink:to=\"" + name6 + "\" xlink:title=\"definition: " + name5 + " to " + name6 + "\" order=\"" + count6 + ".0\"/>");
																						    
																				    	}
																					    
																					    if(!pre6.getChildren().isEmpty()) {
																					    	int count7 = 0;
																					    	List<presentation_detail> preList7 = pre6.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
																						    for(presentation_detail pre7 : preList7) {
																						    	count7++;
																						    	String prefixName7 = pre7.getPrefix() + "_" + pre7.getName();
																						    	String pfx7 = "../../../../def/ic/lhdnm-cor";
																						    	String name7 = pre7.getName();
																						    	tax_enum te7;
																						    	tax_concept tconcept7 = taxConceptList.stream().filter(tcl -> pre7.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																						    	if(Objects.isNull(tconcept7)) {
																						    		te7 = taxEnumList.stream().filter(tcl -> pre7.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																							    	if(Objects.isNull(te7)) {
																							    		continue;
																							    	} else {
																							    		name7 = te7.getName();
																							    		pfx7 = "../enumerations/" + te7.getPrefix();
																							    		prefixName7 = te7.getPrefix() + "_" + name7;
																							    	}
																						    	} else {
																						    		name7 = tconcept7.getName();
																						    		if(tconcept7.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
																						    			pfx7 = "../enumerations/" + tconcept7.getPrefix();
																						    		} else {
																						    			pfx7 = "../../../../def/ic/" + tconcept7.getPrefix() + "-cor";
																						    		}				    		
																						    		prefixName7 = tconcept7.getPrefix() + "_" + name7;
																						    	}
																						    	
																						    	String arcrole7 = "";
																						    	tax_concept tc7 = taxConceptList.stream().filter(tcl -> pre6.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																							    if(tc7 != null) {
																							    	
																							    } else {
																							    	tc7 = tcService.findByName(pre6.getName(), id);
																							    }
																							    
																							    if (tc7 != null) {
																							    	System.out.println("tc7 "+tc7.getSubstitution_group());
																							    	if(pre6.getName().contains("Abstract") && pre7.getName().contains("Table")) {
																								    	arcrole7 = "http://xbrl.org/int/dim/arcrole/all";
																								    } else if(tc7.getSubstitution_group().equalsIgnoreCase("xbrldt:hypercubeItem")) {
																								    	arcrole7 = "http://xbrl.org/int/dim/arcrole/hypercube-dimension";
																								    } else if(tc7.getSubstitution_group().equalsIgnoreCase("xbrldt:dimensionItem")) {
																								    	arcrole7 = "http://xbrl.org/int/dim/arcrole/dimension-domain";
																								    } else if(tc7.getSubstitution_group().equalsIgnoreCase("xbrli:item")) {
																								    	arcrole7 = "http://xbrl.org/int/dim/arcrole/domain-member";
																								    } else {
																								    	
																								    }
																								    
																								    if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre7.getName())).findAny().orElse(null) == null) {
																							    		tempArray.add(pre7.getName());
																									    
																							    		writer.newLine();
																									    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx7 + "_" + dtString + ".xsd#" + prefixName7 + "\" xlink:label=\"" + name7 + "\" xlink:title=\"" + name7 + "\"/>");
																									    writer.newLine();
																									    if(pre6.getName().contains("Abstract") && pre7.getName().contains("Table")) {
																									    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole7 + "\" xlink:from=\"" + name6 + "\" xlink:to=\"" + name7 + "\" xlink:title=\"definition: " + name6 + " to " + name7 + "\" order=\"" + count7 + ".0\" xbrldt:contextElement=\"scenario\" xbrldt:closed=\"true\" />");
																									    } else {
																									    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole7 + "\" xlink:from=\"" + name6 + "\" xlink:to=\"" + name7 + "\" xlink:title=\"definition: " + name6 + " to " + name7 + "\" order=\"" + count7 + ".0\"/>");
																									    }
																									    
																									    
																							    	} else {
																							    		writer.newLine();
																									    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole7 + "\" xlink:from=\"" + name6 + "\" xlink:to=\"" + name7 + "\" xlink:title=\"definition: " + name6 + " to " + name7 + "\" order=\"" + count7 + ".0\"/>");
																									    
																							    	}
																							    }
																							    
																						    }
																					    }
																					    
																				    }
																				    
																			    }
																		    }																		    
																	    }																	    
																    }
															    }
														    }														    
													    }
												    }
											    }
										    }
									    }
								    }
								    
							    }
						    }
					    }
					    
				    }
			    	
			    }
			    
			    writer.newLine();
			    writer.write("  </link:definitionLink>");
			    writer.newLine();
			    writer.write("</link:linkbase>");
			    
			    isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
		
		return isCompleted;
	}
	
	public boolean writeLinkbaseSmeDef(String path, String dtString, Long id, String type) {
		boolean isCompleted = false;
		List<linkbase_form> linkbaseList1 = findByModalId(id);
		List<linkbase_form> linkbaseList = null;
		List<tax_sme_concept> tsconceptList = tscService.getAllByModalId(id);
		List<tax_sme_enum> tseList = tseService.getAllByModalId(id);
		if(type.equalsIgnoreCase("lhdnm-sme")) {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		} else {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		}
		
		for(linkbase_form form : linkbaseList) {
			String fname = "";
			String fname2 = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
				fname = fname.replaceAll(" ", "-");
				fname2 = form.getName().toUpperCase();
				fname2 = fname2.replaceAll("-", "");
//				fname = fname.replaceAll(".", "");
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/def_lhdnm-" + fname + "_" + dtString + ".xml"))) {
				
			    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-" + dtString + ".xsd\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\">");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"" + form.getLinkrole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/int/dim/arcrole/all\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2005/xbrldt-2005.xsd#all\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/int/dim/arcrole/dimension-domain\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2005/xbrldt-2005.xsd#dimension-domain\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/int/dim/arcrole/domain-member\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2005/xbrldt-2005.xsd#domain-member\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/int/dim/arcrole/hypercube-dimension\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2005/xbrldt-2005.xsd#hypercube-dimension\"/>");
			    writer.newLine();
			    writer.write("  <link:definitionLink xlink:type=\"extended\" xlink:role=\"" + form.getLinkrole() + "\">");
			    
			    List<presentation_detail> preList = form.getPresentationList().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
			    List<tax_sme_concept> taxConceptList = tscService.getAllByWorkingSheet(fname2);
			    ArrayList<String> tempArray = new ArrayList<>();
			    
			    for(presentation_detail pre : preList) {
			    	
			    	String prefixName = pre.getPrefix() + "_" + pre.getName();
			    	String pfx = "../../../../def/ic/lhdnm-cor";
			    	String name = pre.getName();
			    	tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre.getName())).findFirst().orElse(null);
			    	if(Objects.isNull(tsconcept)) {
			    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre.getName())).findFirst().orElse(null);
				    	if(Objects.isNull(tse)) {
				    		continue;
				    	} else {
				    		name = tse.getName();
				    		pfx = "../enumerations/" + tse.getPrefix();
				    		prefixName = tse.getPrefix() + "_" + name;
				    	}
			    	} else {
			    		name = tsconcept.getName();
			    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
			    			pfx = "../enumerations/" + tsconcept.getPrefix();
			    		} else {
			    			pfx = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
			    		}				    		
			    		prefixName = tsconcept.getPrefix() + "_" + name;
			    	}
			    	
			    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre.getName())).findAny().orElse(null) == null) {
			    		tempArray.add(pre.getName());
			    		writer.newLine();
					    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx + "_" + dtString + ".xsd#" + prefixName + "\" xlink:label=\"" + name + "\" xlink:title=\"" + name + "\"/>");
			    	} else {
			    		
			    	}
				    
				    int count1 = 0;
				    List<presentation_detail> preList1 = pre.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
				    
				    for(presentation_detail pre1 : preList1) {
				    	count1++;
				    	String prefixName1 = pre1.getPrefix() + "_" + pre1.getName();
				    	String pfx1 = "../../../../def/ic/lhdnm-cor";
				    	String name1 = pre1.getName();
				    	tax_sme_concept tsconcept1 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre1.getName())).findFirst().orElse(null);
				    	if(Objects.isNull(tsconcept1)) {
				    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre1.getName())).findFirst().orElse(null);
					    	if(Objects.isNull(tse)) {
					    		continue;
					    	} else {
					    		name1 = tse.getName();
					    		pfx1 = "../enumerations/" + tse.getPrefix();
					    		prefixName1 = tse.getPrefix() + "_" + pre1.getName();
					    	}
				    	} else {
				    		name1 = tsconcept1.getName();
				    		if(tsconcept1.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
				    			pfx1 = "../enumerations/" + tsconcept1.getPrefix();
				    		} else {
				    			pfx1 = "../../../../def/ic/" + tsconcept1.getPrefix() + "-cor";
				    		}				    		
				    		prefixName1 = tsconcept1.getPrefix() + "_" + pre1.getName();
				    	}
				    	
				    	String arcrole1 = "";
				    	tax_sme_concept tc1 = taxConceptList.stream().filter(tcl -> pre.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
					    if(tc1 != null) {
					    	
					    } else {
					    	tc1 = tscService.findByName(pre.getName(), id);
					    }
					    
					    if (tc1 != null) {
					    	System.out.println("tc1 "+tc1.getSubstitution_group());
					    	
						    if(tc1.getSubstitution_group().equalsIgnoreCase("xbrldt:hypercubeItem")) {
						    	arcrole1 = "http://xbrl.org/int/dim/arcrole/hypercube-dimension";
						    } else if(tc1.getSubstitution_group().equalsIgnoreCase("xbrldt:dimensionItem")) {
						    	arcrole1 = "http://xbrl.org/int/dim/arcrole/dimension-domain";
						    } else if(tc1.getSubstitution_group().equalsIgnoreCase("xbrli:item")) {
						    	arcrole1 = "http://xbrl.org/int/dim/arcrole/domain-member";
						    } else {
						    	
						    }
						    
					    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre1.getName())).findAny().orElse(null) == null) {
					    		tempArray.add(pre1.getName());
							    
							    writer.newLine();
							    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx1 + "_" + dtString + ".xsd#" + prefixName1 + "\" xlink:label=\"" + name1 + "\" xlink:title=\"" + name1 + "\"/>");
							    writer.newLine();
							    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole1 + "\" xlink:from=\"" + name + "\" xlink:to=\"" + name1 + "\" xlink:title=\"definition: " + name + " to " + name1 + "\" order=\"" + count1 + ".0\"/>");
							    
					    	} else {
					    		writer.newLine();
							    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole1 + "\" xlink:from=\"" + name + "\" xlink:to=\"" + name1 + "\" xlink:title=\"definition: " + name + " to " + name1 + "\" order=\"" + count1 + ".0\"/>");
							    
					    	}
						    
						    if(!pre1.getChildren().isEmpty()) {
						    	int count2 = 0;
						    	List<presentation_detail> preList2 = pre1.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
							    for(presentation_detail pre2 : preList2) {
							    	count2++;
							    	String prefixName2 = pre2.getPrefix() + "_" + pre2.getName();
							    	String pfx2 = "../../../../def/ic/lhdnm-cor";
							    	String name2 = pre2.getName();
							    	tax_sme_concept tsconcept2 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre2.getName())).findFirst().orElse(null);
							    	if(Objects.isNull(tsconcept2)) {
							    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre2.getName())).findFirst().orElse(null);
								    	if(Objects.isNull(tse)) {
								    		continue;
								    	} else {
								    		name2 = tse.getName();
								    		pfx2 = "../enumerations/" + tse.getPrefix();
								    		prefixName2 = tse.getPrefix() + "_" + name2;
								    	}
							    	} else {
							    		name2 = tsconcept2.getName();
							    		if(tsconcept2.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
							    			pfx2 = "../enumerations/" + tsconcept2.getPrefix();
							    		} else {
							    			pfx2 = "../../../../def/ic/" + tsconcept2.getPrefix() + "-cor";
							    		}				    		
							    		prefixName2 = tsconcept2.getPrefix() + "_" + name2;
							    	}
							    	
							    	String arcrole2 = "";
							    	tax_sme_concept tc2 = taxConceptList.stream().filter(tcl -> pre1.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
								    if(tc2 != null) {
								    	
								    } else {
								    	tc2 = tscService.findByName(pre1.getName(), id);
								    }
								    
								    if (tc2 != null) {
								    	System.out.println("tc2 "+tc2.getSubstitution_group());
									    if(pre1.getName().contains("Abstract") && pre2.getName().contains("Table")) {
									    	arcrole2 = "http://xbrl.org/int/dim/arcrole/all";
									    } else if(pre2.getName().contains("Table")) {
									    	arcrole2 = "http://xbrl.org/int/dim/arcrole/all";
									    } else if(tc2.getSubstitution_group().equalsIgnoreCase("xbrldt:hypercubeItem")) {
									    	arcrole2 = "http://xbrl.org/int/dim/arcrole/hypercube-dimension";
									    } else if(tc2.getSubstitution_group().equalsIgnoreCase("xbrldt:dimensionItem")) {
									    	arcrole2 = "http://xbrl.org/int/dim/arcrole/dimension-domain";
									    } else if(tc2.getSubstitution_group().equalsIgnoreCase("xbrli:item")) {
									    	arcrole2 = "http://xbrl.org/int/dim/arcrole/domain-member";
									    } else {
									    	
									    }
									    
									    if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre2.getName())).findAny().orElse(null) == null) {
								    		tempArray.add(pre2.getName());
										    
								    		writer.newLine();
										    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx2 + "_" + dtString + ".xsd#" + prefixName2 + "\" xlink:label=\"" + name2 + "\" xlink:title=\"" + name2 + "\"/>");
										    writer.newLine();
										    if(pre1.getName().contains("Abstract") && pre2.getName().contains("Table")) {
										    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole2 + "\" xlink:from=\"" + name1 + "\" xlink:to=\"" + name2 + "\" xlink:title=\"definition: " + name1 + " to " + name2 + "\" order=\"" + count2 + ".0\" xbrldt:contextElement=\"scenario\" xbrldt:closed=\"true\" />");
										    } else {
										    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole2 + "\" xlink:from=\"" + name1 + "\" xlink:to=\"" + name2 + "\" xlink:title=\"definition: " + name1 + " to " + name2 + "\" order=\"" + count2 + ".0\"/>");
										    }
										    
								    	} else {
								    		writer.newLine();
										    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole2 + "\" xlink:from=\"" + name1 + "\" xlink:to=\"" + name2 + "\" xlink:title=\"definition: " + name1 + " to " + name2 + "\" order=\"" + count2 + ".0\"/>");
										    
								    	}
								    	
									    if(!pre2.getChildren().isEmpty()) {
									    	int count3 = 0;
									    	List<presentation_detail> preList3 = pre2.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
										    for(presentation_detail pre3 : preList3) {
										    	count3++;
										    	String prefixName3 = pre3.getPrefix() + "_" + pre3.getName();
										    	String pfx3 = "../../../../def/ic/lhdnm-cor";
										    	String name3 = pre3.getName();
										    	tax_sme_concept tsconcept3 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre3.getName())).findFirst().orElse(null);
										    	if(Objects.isNull(tsconcept3)) {
										    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre3.getName())).findFirst().orElse(null);
											    	if(Objects.isNull(tse)) {
											    		continue;
											    	} else {
											    		name3 = tse.getName();
											    		pfx3 = "../enumerations/" + tse.getPrefix();
											    		prefixName3 = tse.getPrefix() + "_" + name3;
											    	}
										    	} else {
										    		name3 = tsconcept3.getName();
										    		if(tsconcept3.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
										    			pfx3 = "../enumerations/" + tsconcept3.getPrefix();
										    		} else {
										    			pfx3 = "../../../../def/ic/" + tsconcept3.getPrefix() + "-cor";
										    		}				    		
										    		prefixName3 = tsconcept3.getPrefix() + "_" + name3;
										    	}
										    	
										    	System.out.println("pre2 "+ pre2.getName());
										    	String arcrole3 = "";
										    	tax_sme_concept tc3 = taxConceptList.stream().filter(tcl -> pre2.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
											    if(tc3 != null) {
											    	
											    } else {
											    	tc3 = tscService.findByName(pre2.getName(), id);
											    }
											    
											    if (tc3 != null) {
											    	System.out.println("tc3 "+tc3.getSubstitution_group());
											    	if(pre2.getName().contains("Abstract") && pre3.getName().contains("Table")) {
												    	arcrole3 = "http://xbrl.org/int/dim/arcrole/all";
												    } else if(pre3.getName().contains("Table")) {
												    	arcrole3 = "http://xbrl.org/int/dim/arcrole/all";
												    } else if(tc3.getSubstitution_group().equalsIgnoreCase("xbrldt:hypercubeItem")) {
												    	arcrole3 = "http://xbrl.org/int/dim/arcrole/hypercube-dimension";
												    } else if(tc3.getSubstitution_group().equalsIgnoreCase("xbrldt:dimensionItem")) {
												    	arcrole3 = "http://xbrl.org/int/dim/arcrole/dimension-domain";
												    } else if(tc3.getSubstitution_group().equalsIgnoreCase("xbrli:item")) {
												    	arcrole3 = "http://xbrl.org/int/dim/arcrole/domain-member";
												    } else {
												    	
												    }
												    
												    if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre3.getName())).findAny().orElse(null) == null) {
											    		tempArray.add(pre3.getName());
													    
											    		writer.newLine();
													    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx3 + "_" + dtString + ".xsd#" + prefixName3 + "\" xlink:label=\"" + name3 + "\" xlink:title=\"" + name3 + "\"/>");
													    writer.newLine();
													    if(pre2.getName().contains("Abstract") && pre3.getName().contains("Table")) {
													    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole3 + "\" xlink:from=\"" + name2 + "\" xlink:to=\"" + name3 + "\" xlink:title=\"definition: " + name2 + " to " + name3 + "\" order=\"" + count3 + ".0\" xbrldt:contextElement=\"scenario\" xbrldt:closed=\"true\" />");
													    } else {
													    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole3 + "\" xlink:from=\"" + name2 + "\" xlink:to=\"" + name3 + "\" xlink:title=\"definition: " + name2 + " to " + name3 + "\" order=\"" + count3 + ".0\"/>");
													    }
													    
											    	} else {
											    		writer.newLine();
													    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole3 + "\" xlink:from=\"" + name2 + "\" xlink:to=\"" + name3 + "\" xlink:title=\"definition: " + name2 + " to " + name3 + "\" order=\"" + count3 + ".0\"/>");
													    
											    	}
											    	
												    if(!pre3.getChildren().isEmpty()) {
												    	int count4 = 0;
												    	List<presentation_detail> preList4 = pre3.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
													    for(presentation_detail pre4 : preList4) {
													    	count4++;
													    	String prefixName4 = pre4.getPrefix() + "_" + pre4.getName();
													    	String pfx4 = "../../../../def/ic/lhdnm-cor";
													    	String name4 = pre4.getName();
													    	tax_sme_concept tsconcept4 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre4.getName())).findFirst().orElse(null);
													    	if(Objects.isNull(tsconcept4)) {
													    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre4.getName())).findFirst().orElse(null);
														    	if(Objects.isNull(tse)) {
														    		continue;
														    	} else {
														    		name4 = tse.getName();
														    		pfx4 = "../enumerations/" + tse.getPrefix();
														    		prefixName4 = tse.getPrefix() + "_" + name4;
														    	}
													    	} else {
													    		name4 = tsconcept4.getName();
													    		if(tsconcept4.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
													    			pfx4 = "../enumerations/" + tsconcept4.getPrefix();
													    		} else {
													    			pfx4 = "../../../../def/ic/" + tsconcept4.getPrefix() + "-cor";
													    		}				    		
													    		prefixName4 = tsconcept4.getPrefix() + "_" + name4;
													    	}
													    	
													    	String arcrole4 = "";
													    	tax_sme_concept tc4 = taxConceptList.stream().filter(tcl -> pre3.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
														    if(tc4 != null) {
														    	
														    } else {
														    	tc4 = tscService.findByName(pre3.getName(), id);
														    }
														    
														    if (tc4 != null) {
														    	System.out.println("tc4 "+tc4.getSubstitution_group());
														    	if(pre3.getName().contains("Abstract") && pre4.getName().contains("Table")) {
															    	arcrole4 = "http://xbrl.org/int/dim/arcrole/all";
															    } else if(pre4.getName().contains("Table")) {
															    	arcrole4 = "http://xbrl.org/int/dim/arcrole/all";
															    } else if(tc4.getSubstitution_group().equalsIgnoreCase("xbrldt:hypercubeItem")) {
															    	arcrole4 = "http://xbrl.org/int/dim/arcrole/hypercube-dimension";
															    } else if(tc4.getSubstitution_group().equalsIgnoreCase("xbrldt:dimensionItem")) {
															    	arcrole4 = "http://xbrl.org/int/dim/arcrole/dimension-domain";
															    } else if(tc4.getSubstitution_group().equalsIgnoreCase("xbrli:item")) {
															    	arcrole4 = "http://xbrl.org/int/dim/arcrole/domain-member";
															    } else {
															    	
															    }
															    
															    if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre4.getName())).findAny().orElse(null) == null) {
														    		tempArray.add(pre4.getName());
																    
														    		writer.newLine();
																    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx4 + "_" + dtString + ".xsd#" + prefixName4 + "\" xlink:label=\"" + name4 + "\" xlink:title=\"" + name4 + "\"/>");
																    writer.newLine();
																    if(pre3.getName().contains("Abstract") && pre4.getName().contains("Table")) {
																    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole4 + "\" xlink:from=\"" + name3 + "\" xlink:to=\"" + name4 + "\" xlink:title=\"definition: " + name3 + " to " + name4 + "\" order=\"" + count4 + ".0\" xbrldt:contextElement=\"scenario\" xbrldt:closed=\"true\" />");
																    } else {
																    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole4 + "\" xlink:from=\"" + name3 + "\" xlink:to=\"" + name4 + "\" xlink:title=\"definition: " + name3 + " to " + name4 + "\" order=\"" + count4 + ".0\"/>");
																    }
																    
														    	} else {
														    		writer.newLine();
																    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole4 + "\" xlink:from=\"" + name3 + "\" xlink:to=\"" + name4 + "\" xlink:title=\"definition: " + name3 + " to " + name4 + "\" order=\"" + count4 + ".0\"/>");
																    
														    	}
															    
															    if(!pre4.getChildren().isEmpty()) {
															    	int count5 = 0;
															    	List<presentation_detail> preList5 = pre4.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
															    	for(presentation_detail pre5 : preList5) {
															    		count5++;
																    	String prefixName5 = pre5.getPrefix() + "_" + pre5.getName();
																    	String pfx5 = "../../../../def/ic/lhdnm-cor";
																    	String name5 = pre5.getName();
																    	tax_sme_concept tsconcept5 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre5.getName())).findFirst().orElse(null);
																    	if(Objects.isNull(tsconcept4)) {
																    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre5.getName())).findFirst().orElse(null);
																	    	if(Objects.isNull(tse)) {
																	    		continue;
																	    	} else {
																	    		name5 = tse.getName();
																	    		pfx5 = "../enumerations/" + tse.getPrefix();
																	    		prefixName5 = tse.getPrefix() + "_" + name5;
																	    	}
																    	} else {
																    		name5 = tsconcept5.getName();
																    		if(tsconcept5.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
																    			pfx5 = "../enumerations/" + tsconcept5.getPrefix();
																    		} else {
																    			pfx5 = "../../../../def/ic/" + tsconcept5.getPrefix() + "-cor";
																    		}				    		
																    		prefixName5 = tsconcept5.getPrefix() + "_" + name5;
																    	}
																    	
																    	String arcrole5 = "";
																    	tax_sme_concept tc5 = taxConceptList.stream().filter(tcl -> pre4.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																	    if(tc5 != null) {
																	    	
																	    } else {
																	    	tc5 = tscService.findByName(pre4.getName(), id);
																	    }
																	    
																	    if (tc5 != null) {
																	    	System.out.println("tc5 "+tc5.getSubstitution_group());
																	    	if(pre4.getName().contains("Abstract") && pre5.getName().contains("Table")) {
																		    	arcrole5 = "http://xbrl.org/int/dim/arcrole/all";
																		    } else if(pre5.getName().contains("Table")) {
																		    	arcrole5 = "http://xbrl.org/int/dim/arcrole/all";
																		    } else if(tc5.getSubstitution_group().equalsIgnoreCase("xbrldt:hypercubeItem")) {
																		    	arcrole5 = "http://xbrl.org/int/dim/arcrole/hypercube-dimension";
																		    } else if(tc5.getSubstitution_group().equalsIgnoreCase("xbrldt:dimensionItem")) {
																		    	arcrole5 = "http://xbrl.org/int/dim/arcrole/dimension-domain";
																		    } else if(tc5.getSubstitution_group().equalsIgnoreCase("xbrli:item")) {
																		    	arcrole5 = "http://xbrl.org/int/dim/arcrole/domain-member";
																		    } else {
																		    	
																		    }
																		    
																		    if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre5.getName())).findAny().orElse(null) == null) {
																	    		tempArray.add(pre5.getName());
																			    
																	    		writer.newLine();
																			    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx5 + "_" + dtString + ".xsd#" + prefixName5 + "\" xlink:label=\"" + name5 + "\" xlink:title=\"" + name5 + "\"/>");
																			    writer.newLine();
																			    if(pre4.getName().contains("Abstract") && pre5.getName().contains("Table")) {
																			    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole5 + "\" xlink:from=\"" + name4 + "\" xlink:to=\"" + name5 + "\" xlink:title=\"definition: " + name4 + " to " + name5+ "\" order=\"" + count5 + ".0\" xbrldt:contextElement=\"scenario\" xbrldt:closed=\"true\" />");
																			    } else {
																			    	writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole5 + "\" xlink:from=\"" + name4 + "\" xlink:to=\"" + name5 + "\" xlink:title=\"definition: " + name4 + " to " + name5 + "\" order=\"" + count5 + ".0\"/>");
																			    }
																			    
																	    	} else {
																	    		writer.newLine();
																			    writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"" + arcrole5 + "\" xlink:from=\"" + name4 + "\" xlink:to=\"" + name5 + "\" xlink:title=\"definition: " + name4 + " to " + name5 + "\" order=\"" + count5 + ".0\"/>");
																			    
																	    	}
																	    }
															    	}
															    }
														    }
														    
													    }
												    }
											    }
										    }
									    }
								    }
								    
							    }
						    }
					    }
					    
				    }
			    	
			    }
			    
			    writer.newLine();
			    writer.write("  </link:definitionLink>");
			    writer.newLine();
			    writer.write("</link:linkbase>");
			    
			    isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
		
		return isCompleted;
	}
	
	public boolean writeLinkbasePre(String path, String dtString, Long id, String type) {
		boolean isCompleted = false;
		List<linkbase_form> linkbaseList1 = findByModalId(id);
		List<linkbase_form> linkbaseList = null;
		List<tax_concept> taxConceptList = tcService.getAllByModalId(id);
	    List<tax_enum> taxEnumList = enumService.getAllByModalId(id);
		if(type.equalsIgnoreCase("lhdnm-sme")) {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		} else {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		}
		
		for(linkbase_form form : linkbaseList) {
			String fname = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
				fname = fname.replaceAll(" ", "-");
//				fname = fname.replaceAll(".", "");
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/pre_lhdnm-" + fname + "_" + dtString + ".xml"))) {
				
			    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-" + dtString + ".xsd\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:lhdnm-c_" + dtString + "_full_entry_point=\"http://www.hasil.gov.my/xbrl/" + dtString + "/c\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:enum=\"http://xbrl.org/2014/extensible-enumerations\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:lhdnm-fdn=\"http://www.hasil.gov.my/xbrl/" + dtString + "/fdn\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"" + form.getLinkrole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.hasil.gov.my/role/occurance1Label\" xlink:type=\"simple\" xlink:href=\"../../../../def/fdn/lhdnm-fdn_" + dtString + ".xsd#occurance1Label\"/>");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.hasil.gov.my/role/occurance2Label\" xlink:type=\"simple\" xlink:href=\"../../../../def/fdn/lhdnm-fdn_" + dtString + ".xsd#occurance2Label\"/>");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.hasil.gov.my/role/occurance3Label\" xlink:type=\"simple\" xlink:href=\"../../../../def/fdn/lhdnm-fdn_" + dtString + ".xsd#occurance3Label\"/>");
			    writer.newLine();
			    writer.write("  <link:presentationLink xlink:type=\"extended\" xlink:role=\"" + form.getLinkrole() + "\">");
			    
			    List<presentation_detail> preList = form.getPresentationList().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
			    ArrayList<String> tempArray = new ArrayList<>();
			    
			    for(presentation_detail pre : preList) {
			    	
			    	String prefixName = pre.getPrefix() + "_" + pre.getName();
			    	String pfx = "../../../../def/ic/lhdnm-cor";
			    	String name = pre.getName();
			    	tax_enum te;
			    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
			    	if(Objects.isNull(tconcept)) {
			    		te = taxEnumList.stream().filter(tcl -> pre.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
				    	if(Objects.isNull(te)) {
				    		continue;
				    	} else {
				    		name = te.getName();
				    		pfx = "../enumerations/" + te.getPrefix();
				    		prefixName = te.getPrefix() + "_" + name;
				    	}
			    	} else {
			    		name = tconcept.getName();
			    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
			    			pfx = "../enumerations/" + tconcept.getPrefix();
			    		} else {
			    			pfx = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
			    		}				    		
			    		prefixName = tconcept.getPrefix() + "_" + name;
			    	}
			    	
			    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre.getName())).findAny().orElse(null) == null) {
			    		tempArray.add(pre.getName());
			    		writer.newLine();
					    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx + "_" + dtString + ".xsd#" + prefixName + "\" xlink:label=\"" + name + "\" xlink:title=\"" + name + "\"/>");
					} else {
			    		
			    	}
			    	
				    int count1 = 0;
				    List<presentation_detail> preList1 = pre.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
				    for(presentation_detail pre1 : preList1) {
				    	count1++;
				    	String prefixName1 = pre1.getPrefix() + "_" + pre1.getName();
				    	String pfx1 = "../../../../def/ic/lhdnm-cor";
				    	String name1 = pre1.getName();
				    	tax_enum te1;
				    	tax_concept tconcept1 = taxConceptList.stream().filter(tcl -> pre1.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
				    	if(Objects.isNull(tconcept1)) {
				    		te1 = taxEnumList.stream().filter(tcl -> pre1.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
					    	if(Objects.isNull(te1)) {
					    		continue;
					    	} else {
					    		name1 = te1.getName();
					    		pfx1 = "../enumerations/" + te1.getPrefix();
					    		prefixName1 = te1.getPrefix() + "_" + name1;
					    	}
				    	} else {
				    		name1 = tconcept1.getName();
				    		if(tconcept1.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
				    			pfx1 = "../enumerations/" + tconcept1.getPrefix();
				    		} else {
				    			pfx1 = "../../../../def/ic/" + tconcept1.getPrefix() + "-cor";
				    		}				    		
				    		prefixName1 = tconcept1.getPrefix() + "_" + name1;
				    	}
				    	
				    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre1.getName())).findAny().orElse(null) == null) {
				    		tempArray.add(pre1.getName());
				    		writer.newLine();
						    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx1 + "_" + dtString + ".xsd#" + prefixName1 + "\" xlink:label=\"" + name1 + "\" xlink:title=\"" + name1 + "\"/>");
						    writer.newLine();
						    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name + "\" xlink:to=\"" + name1 + "\" xlink:title=\"presentation: " + name + " to " + name1 + "\" priority=\"0\" order=\"" + count1 + ".0\"/>");
						    
						} else {
							writer.newLine();
						    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name + "\" xlink:to=\"" + name1 + "\" xlink:title=\"presentation: " + name + " to " + name1 + "\" priority=\"0\" order=\"" + count1 + ".0\"/>");
						    
				    	}
				    	
					    if(!pre1.getChildren().isEmpty()) {
					    	int count2 = 0;
					    	List<presentation_detail> preList2 = pre1.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
						    for(presentation_detail pre2 : preList2) {
						    	count2++;
						    	String prefixName2 = pre2.getPrefix() + "_" + pre2.getName();
						    	String pfx2 = "../../../../def/ic/lhdnm-cor";
						    	String name2 = pre2.getName();
						    	tax_enum te2;
						    	tax_concept tconcept2 = taxConceptList.stream().filter(tcl -> pre2.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
						    	if(Objects.isNull(tconcept2)) {
						    		te2 = taxEnumList.stream().filter(tcl -> pre2.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
							    	if(Objects.isNull(te2)) {
							    		continue;
							    	} else {
							    		name2 = te2.getName();
							    		pfx2 = "../enumerations/" + te2.getPrefix();
							    		prefixName2 = te2.getPrefix() + "_" + name2;
							    	}
						    	} else {
						    		name2 = tconcept2.getName();
						    		if(tconcept2.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
						    			pfx2 = "../enumerations/" + tconcept2.getPrefix();
						    		} else {
						    			pfx2 = "../../../../def/ic/" + tconcept2.getPrefix() + "-cor";
						    		}				    		
						    		prefixName2 = tconcept2.getPrefix() + "_" + name2;
						    	}
						    	
						    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre2.getName())).findAny().orElse(null) == null) {
						    		tempArray.add(pre2.getName());
						    		writer.newLine();
								    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx2 + "_" + dtString + ".xsd#" + prefixName2 + "\" xlink:label=\"" + name2 + "\" xlink:title=\"" + name2 + "\"/>");
								    writer.newLine();
								    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name1 + "\" xlink:to=\"" + name2 + "\" xlink:title=\"presentation: " + name1 + " to " + name2 + "\" priority=\"0\" order=\"" + count2 + ".0\"/>");
								    
								} else {
									writer.newLine();
								    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name1 + "\" xlink:to=\"" + name2 + "\" xlink:title=\"presentation: " + name1 + " to " + name2 + "\" priority=\"0\" order=\"" + count2 + ".0\"/>");
								    
						    	}
						    	
							    if(!pre2.getChildren().isEmpty()) {
							    	int count3 = 0;
							    	List<presentation_detail> preList3 = pre2.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
								    for(presentation_detail pre3 : preList3) {
								    	count3++;
								    	String prefixName3 = pre3.getPrefix() + "_" + pre3.getName();
								    	String pfx3 = "../../../../def/ic/lhdnm-cor";
								    	String name3 = pre3.getName();
								    	tax_enum te3;
								    	tax_concept tconcept3 = taxConceptList.stream().filter(tcl -> pre3.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
								    	if(Objects.isNull(tconcept3)) {
								    		te3 = taxEnumList.stream().filter(tcl -> pre3.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
									    	if(Objects.isNull(te3)) {
									    		continue;
									    	} else {
									    		name3 = te3.getName();
									    		pfx3 = "../enumerations/" + te3.getPrefix();
									    		prefixName3 = te3.getPrefix() + "_" + name3;
									    	}
								    	} else {
								    		name3 = tconcept3.getName();
								    		if(tconcept3.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
								    			pfx3 = "../enumerations/" + tconcept3.getPrefix();
								    		} else {
								    			pfx3 = "../../../../def/ic/" + tconcept3.getPrefix() + "-cor";
								    		}				    		
								    		prefixName3 = tconcept3.getPrefix() + "_" + name3;
								    	}
								    	
								    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre3.getName())).findAny().orElse(null) == null) {
								    		tempArray.add(pre3.getName());
								    		writer.newLine();
										    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx3 + "_" + dtString + ".xsd#" + prefixName3 + "\" xlink:label=\"" + name3 + "\" xlink:title=\"" + name3 + "\"/>");
										    writer.newLine();
										    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name2 + "\" xlink:to=\"" + name3 + "\" xlink:title=\"presentation: " + name2 + " to " + name3 + "\" priority=\"0\" order=\"" + count3 + ".0\"/>");
										    
										} else {
											writer.newLine();
										    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name2 + "\" xlink:to=\"" + name3 + "\" xlink:title=\"presentation: " + name2 + " to " + name3 + "\" priority=\"0\" order=\"" + count3 + ".0\"/>");
										    
								    	}
								    									    	
									    if(!pre3.getChildren().isEmpty()) {
									    	int count4 = 0;
									    	List<presentation_detail> preList4 = pre3.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
										    for(presentation_detail pre4 : preList4) {
										    	count4++;
										    	String prefixName4 = pre4.getPrefix() + "_" + pre4.getName();
										    	String pfx4 = "../../../../def/ic/lhdnm-cor";
										    	String name4 = pre4.getName();
										    	tax_enum te4;
										    	tax_concept tconcept4 = taxConceptList.stream().filter(tcl -> pre4.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
										    	if(Objects.isNull(tconcept4)) {
										    		te4 = taxEnumList.stream().filter(tcl -> pre4.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
											    	if(Objects.isNull(te4)) {
											    		continue;
											    	} else {
											    		name4 = te4.getName();
											    		pfx4 = "../enumerations/" + te4.getPrefix();
											    		prefixName4 = te4.getPrefix() + "_" + name4;
											    	}
										    	} else {
										    		name4 = tconcept4.getName();
										    		if(tconcept4.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
										    			pfx4 = "../enumerations/" + tconcept4.getPrefix();
										    		} else {
										    			pfx4 = "../../../../def/ic/" + tconcept4.getPrefix() + "-cor";
										    		}				    		
										    		prefixName4 = tconcept4.getPrefix() + "_" + name4;
										    	}
										    	
										    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre4.getName())).findAny().orElse(null) == null) {
										    		tempArray.add(pre4.getName());
										    		writer.newLine();
												    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx4 + "_" + dtString + ".xsd#" + prefixName4 + "\" xlink:label=\"" + name4 + "\" xlink:title=\"" + name4 + "\"/>");
												    writer.newLine();
												    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name3 + "\" xlink:to=\"" + name4 + "\" xlink:title=\"presentation: " + name3 + " to " + name4 + "\" priority=\"0\" order=\"" + count4 + ".0\"/>");
												    
												} else {
													writer.newLine();
												    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name3 + "\" xlink:to=\"" + name4 + "\" xlink:title=\"presentation: " + name3 + " to " + name4 + "\" priority=\"0\" order=\"" + count4 + ".0\"/>");
												    
										    	}
										    	
										    	if(!pre4.getChildren().isEmpty()) {
											    	int count5 = 0;
											    	List<presentation_detail> preList5 = pre4.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
												    for(presentation_detail pre5 : preList5) {
												    	count5++;
												    	String prefixName5 = pre5.getPrefix() + "_" + pre5.getName();
												    	String pfx5 = "../../../../def/ic/lhdnm-cor";
												    	String name5 = pre5.getName();
												    	tax_enum te5;
												    	tax_concept tconcept5 = taxConceptList.stream().filter(tcl -> pre5.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
												    	if(Objects.isNull(tconcept5)) {
												    		te5 = taxEnumList.stream().filter(tcl -> pre5.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
													    	if(Objects.isNull(te5)) {
													    		continue;
													    	} else {
													    		name5 = te5.getName();
													    		pfx5 = "../enumerations/" + te5.getPrefix();
													    		prefixName5 = te5.getPrefix() + "_" + name5;
													    	}
												    	} else {
												    		name5 = tconcept5.getName();
												    		if(tconcept5.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
												    			pfx5 = "../enumerations/" + tconcept5.getPrefix();
												    		} else {
												    			pfx5 = "../../../../def/ic/" + tconcept5.getPrefix() + "-cor";
												    		}				    		
												    		prefixName5 = tconcept5.getPrefix() + "_" + name5;
												    	}
												    	
												    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre5.getName())).findAny().orElse(null) == null) {
												    		tempArray.add(pre5.getName());
												    		writer.newLine();
														    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx5 + "_" + dtString + ".xsd#" + prefixName5 + "\" xlink:label=\"" + name5 + "\" xlink:title=\"" + name5 + "\"/>");
														    writer.newLine();
														    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name4 + "\" xlink:to=\"" + name5 + "\" xlink:title=\"presentation: " + name4 + " to " + name5 + "\" priority=\"0\" order=\"" + count5 + ".0\"/>");
														    
														} else {
															writer.newLine();
														    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name4 + "\" xlink:to=\"" + name5 + "\" xlink:title=\"presentation: " + name4 + " to " + name5 + "\" priority=\"0\" order=\"" + count5 + ".0\"/>");
														    
												    	}
												    	
												    	if(!pre5.getChildren().isEmpty()) {
													    	int count6 = 0;
													    	List<presentation_detail> preList6 = pre5.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
														    for(presentation_detail pre6 : preList6) {
														    	count6++;
														    	String prefixName6 = pre6.getPrefix() + "_" + pre6.getName();
														    	String pfx6 = "../../../../def/ic/lhdnm-cor";
														    	String name6 = pre6.getName();
														    	tax_enum te6;
														    	tax_concept tconcept6 = taxConceptList.stream().filter(tcl -> pre6.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
														    	if(Objects.isNull(tconcept6)) {
														    		te6 = taxEnumList.stream().filter(tcl -> pre6.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
															    	if(Objects.isNull(te6)) {
															    		continue;
															    	} else {
															    		name6 = te6.getName();
															    		pfx6 = "../enumerations/" + te6.getPrefix();
															    		prefixName6 = te6.getPrefix() + "_" + name6;
															    	}
														    	} else {
														    		name6 = tconcept6.getName();
														    		if(tconcept6.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
														    			pfx6 = "../enumerations/" + tconcept6.getPrefix();
														    		} else {
														    			pfx6 = "../../../../def/ic/" + tconcept6.getPrefix() + "-cor";
														    		}				    		
														    		prefixName6 = tconcept6.getPrefix() + "_" + name6;
														    	}
														    	
														    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre6.getName())).findAny().orElse(null) == null) {
														    		tempArray.add(pre6.getName());
														    		writer.newLine();
																    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx6 + "_" + dtString + ".xsd#" + prefixName6 + "\" xlink:label=\"" + name6 + "\" xlink:title=\"" + name6 + "\"/>");
																    writer.newLine();
																    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name5 + "\" xlink:to=\"" + name6 + "\" xlink:title=\"presentation: " + name5 + " to " + name6 + "\" priority=\"0\" order=\"" + count6 + ".0\"/>");
																    
																} else {
																	writer.newLine();
																    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name5 + "\" xlink:to=\"" + name6 + "\" xlink:title=\"presentation: " + name5 + " to " + name6 + "\" priority=\"0\" order=\"" + count6 + ".0\"/>");
																    
														    	}
														    	
														    	if(!pre6.getChildren().isEmpty()) {
															    	int count7 = 0;
															    	List<presentation_detail> preList7 = pre6.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
																    for(presentation_detail pre7 : preList7) {
																    	count7++;
																    	String prefixName7 = pre7.getPrefix() + "_" + pre7.getName();
																    	String pfx7 = "../../../../def/ic/lhdnm-cor";
																    	String name7 = pre7.getName();
																    	tax_enum te7;
																    	tax_concept tconcept7 = taxConceptList.stream().filter(tcl -> pre7.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																    	if(Objects.isNull(tconcept7)) {
																    		te7 = taxEnumList.stream().filter(tcl -> pre7.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																	    	if(Objects.isNull(te7)) {
																	    		continue;
																	    	} else {
																	    		name7 = te7.getName();
																	    		pfx7 = "../enumerations/" + te7.getPrefix();
																	    		prefixName7 = te7.getPrefix() + "_" + name7;
																	    	}
																    	} else {
																    		name7 = tconcept7.getName();
																    		if(tconcept7.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
																    			pfx7 = "../enumerations/" + tconcept7.getPrefix();
																    		} else {
																    			pfx7 = "../../../../def/ic/" + tconcept7.getPrefix() + "-cor";
																    		}				    		
																    		prefixName7 = tconcept7.getPrefix() + "_" + name7;
																    	}
																    	
																    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre7.getName())).findAny().orElse(null) == null) {
																    		tempArray.add(pre7.getName());
																    		writer.newLine();
																		    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx7 + "_" + dtString + ".xsd#" + prefixName7 + "\" xlink:label=\"" + name7 + "\" xlink:title=\"" + name7 + "\"/>");
																		    writer.newLine();
																		    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name6 + "\" xlink:to=\"" + name7 + "\" xlink:title=\"presentation: " + name6 + " to " + name7 + "\" priority=\"0\" order=\"" + count7 + ".0\"/>");
																		    
																		} else {
																			writer.newLine();
																		    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name6 + "\" xlink:to=\"" + name7 + "\" xlink:title=\"presentation: " + name6 + " to " + name7 + "\" priority=\"0\" order=\"" + count7 + ".0\"/>");
																		    
																    	}
																    	
																    }
															    }
														    	
														    }
													    }
												    }
											    }
										    	
										    }
									    }
								    }
							    }
							    
						    }
					    }
				    }
			    	
			    }
			    
			    writer.newLine();
			    writer.write("  </link:presentationLink>");
			    writer.newLine();
			    writer.write("</link:linkbase>");
			    
			    isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
				
		return isCompleted;
	}
	
	public boolean writeLinkbaseSmePre(String path, String dtString, Long id, String type) {
		boolean isCompleted = false;
		List<linkbase_form> linkbaseList1 = findByModalId(id);
		List<linkbase_form> linkbaseList = null;
		List<tax_sme_concept> tsconceptList = tscService.getAllByModalId(id);
		List<tax_sme_enum> tseList = tseService.getAllByModalId(id);
		if(type.equalsIgnoreCase("lhdnm-sme")) {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		} else {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		}
		
		for(linkbase_form form : linkbaseList) {
			String fname = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
				fname = fname.replaceAll(" ", "-");
//				fname = fname.replaceAll(".", "");
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/pre_lhdnm-" + fname + "_" + dtString + ".xml"))) {
				
			    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"" + form.getLinkrole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
			    writer.newLine();
			    writer.write("  <link:presentationLink xlink:type=\"extended\" xlink:role=\"" + form.getLinkrole() + "\">");
			    
			    List<presentation_detail> preList = form.getPresentationList().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
			    ArrayList<String> tempArray = new ArrayList<>();
			    
			    for(presentation_detail pre : preList) {
			    	
			    	String prefixName = pre.getPrefix() + "_" + pre.getName();
			    	String pfx = "../../../../def/ic/lhdnm-sme-cor";
			    	String name = pre.getName();
			    	tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre.getName())).findFirst().orElse(null);
			    	if(Objects.isNull(tsconcept)) {
			    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre.getName())).findFirst().orElse(null);
				    	if(Objects.isNull(tse)) {
				    		continue;
				    	} else {
				    		name = tse.getName();
				    		pfx = "../enumerations/" + tse.getPrefix();
				    		prefixName = tse.getPrefix() + "_" + name;
				    	}
			    	} else {
			    		name = tsconcept.getName();
			    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
			    			pfx = "../enumerations/" + tsconcept.getPrefix();
			    		} else {
			    			pfx = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
			    		}				    		
			    		prefixName = tsconcept.getPrefix() + "_" + name;
			    	}
			    	
			    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre.getName())).findAny().orElse(null) == null) {
			    		tempArray.add(pre.getName());
			    		writer.newLine();
					    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx + "_" + dtString + ".xsd#" + prefixName + "\" xlink:label=\"" + name + "\" xlink:title=\"" + name + "\"/>");
					} else {
			    		
			    	}
			    	
				    int count1 = 0;
				    List<presentation_detail> preList1 = pre.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
				    for(presentation_detail pre1 : preList1) {
				    	count1++;
				    	String prefixName1 = pre1.getPrefix() + "_" + pre1.getName();
				    	String pfx1 = "../../../../def/ic/lhdnm-sme-cor";
				    	String name1 = pre1.getName();
				    	tax_sme_concept tsconcept1 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre1.getName())).findFirst().orElse(null);
				    	if(Objects.isNull(tsconcept1)) {
				    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre1.getName())).findFirst().orElse(null);
					    	if(Objects.isNull(tse)) {
					    		continue;
					    	} else {
					    		name1 = tse.getName();
					    		pfx1 = "../enumerations/" + tse.getPrefix();
					    		prefixName1 = tse.getPrefix() + "_" + name1;
					    	}
				    	} else {
				    		name1 = tsconcept1.getName();
				    		if(tsconcept1.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
				    			pfx1 = "../enumerations/" + tsconcept1.getPrefix();
				    		} else {
				    			pfx1 = "../../../../def/ic/" + tsconcept1.getPrefix() + "-cor";
				    		}				    		
				    		prefixName1 = tsconcept1.getPrefix() + "_" + name1;
				    	}
				    	
				    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre1.getName())).findAny().orElse(null) == null) {
				    		tempArray.add(pre1.getName());
				    		writer.newLine();
						    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx1 + "_" + dtString + ".xsd#" + prefixName1 + "\" xlink:label=\"" + name1 + "\" xlink:title=\"" + name1 + "\"/>");
						    writer.newLine();
						    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name + "\" xlink:to=\"" + name1 + "\" xlink:title=\"presentation: " + name + " to " + name1 + "\" priority=\"0\" order=\"" + count1 + ".0\"/>");
						    
						} else {
							writer.newLine();
						    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name + "\" xlink:to=\"" + name1 + "\" xlink:title=\"presentation: " + name + " to " + name1 + "\" priority=\"0\" order=\"" + count1 + ".0\"/>");
						    
				    	}
				    	
					    if(!pre1.getChildren().isEmpty()) {
					    	int count2 = 0;
					    	List<presentation_detail> preList2 = pre1.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
						    for(presentation_detail pre2 : preList2) {
						    	count2++;
						    	String prefixName2 = pre2.getPrefix() + "_" + pre2.getName();
						    	String pfx2 = "../../../../def/ic/lhdnm-sme-cor";
						    	String name2 = pre2.getName();
						    	tax_sme_concept tsconcept2 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre2.getName())).findFirst().orElse(null);
						    	if(Objects.isNull(tsconcept2)) {
						    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre2.getName())).findFirst().orElse(null);
							    	if(Objects.isNull(tse)) {
							    		continue;
							    	} else {
							    		name2 = tse.getName();
							    		pfx2 = "../enumerations/" + tse.getPrefix();
							    		prefixName2 = tse.getPrefix() + "_" + name2;
							    	}
						    	} else {
						    		name2 = tsconcept2.getName();
						    		if(tsconcept2.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
						    			pfx2 = "../enumerations/" + tsconcept2.getPrefix();
						    		} else {
						    			pfx2 = "../../../../def/ic/" + tsconcept2.getPrefix() + "-cor";
						    		}				    		
						    		prefixName2 = tsconcept2.getPrefix() + "_" + name2;
						    	}
						    	
						    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre2.getName())).findAny().orElse(null) == null) {
						    		tempArray.add(pre2.getName());
						    		writer.newLine();
								    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx2 + "_" + dtString + ".xsd#" + prefixName2 + "\" xlink:label=\"" + name2 + "\" xlink:title=\"" + name2 + "\"/>");
								    writer.newLine();
								    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name1 + "\" xlink:to=\"" + name2 + "\" xlink:title=\"presentation: " + name1 + " to " + name2 + "\" priority=\"0\" order=\"" + count2 + ".0\"/>");
								    
								} else {
									writer.newLine();
								    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name1 + "\" xlink:to=\"" + name2 + "\" xlink:title=\"presentation: " + name1 + " to " + name2 + "\" priority=\"0\" order=\"" + count2 + ".0\"/>");
								    
						    	}
						    	
							    if(!pre2.getChildren().isEmpty()) {
							    	int count3 = 0;
							    	List<presentation_detail> preList3 = pre2.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
								    for(presentation_detail pre3 : preList3) {
								    	count3++;
								    	String prefixName3 = pre3.getPrefix() + "_" + pre3.getName();
								    	String pfx3 = "../../../../def/ic/lhdnm-sme-cor";
								    	String name3 = pre3.getName();
								    	tax_sme_concept tsconcept3 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre3.getName())).findFirst().orElse(null);
								    	if(Objects.isNull(tsconcept3)) {
								    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre3.getName())).findFirst().orElse(null);
									    	if(Objects.isNull(tse)) {
									    		continue;
									    	} else {
									    		name3 = tse.getName();
									    		pfx3 = "../enumerations/" + tse.getPrefix();
									    		prefixName3 = tse.getPrefix() + "_" + name3;
									    	}
								    	} else {
								    		name3 = tsconcept3.getName();
								    		if(tsconcept3.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
								    			pfx3 = "../enumerations/" + tsconcept3.getPrefix();
								    		} else {
								    			pfx3 = "../../../../def/ic/" + tsconcept3.getPrefix() + "-cor";
								    		}				    		
								    		prefixName3 = tsconcept3.getPrefix() + "_" + name3;
								    	}
								    	
								    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre3.getName())).findAny().orElse(null) == null) {
								    		tempArray.add(pre3.getName());
								    		writer.newLine();
										    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx3 + "_" + dtString + ".xsd#" + prefixName3 + "\" xlink:label=\"" + name3 + "\" xlink:title=\"" + name3 + "\"/>");
										    writer.newLine();
										    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name2 + "\" xlink:to=\"" + name3 + "\" xlink:title=\"presentation: " + name2 + " to " + name3 + "\" priority=\"0\" order=\"" + count3 + ".0\"/>");
										    
										} else {
											writer.newLine();
										    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name2 + "\" xlink:to=\"" + name3 + "\" xlink:title=\"presentation: " + name2 + " to " + name3 + "\" priority=\"0\" order=\"" + count3 + ".0\"/>");
										    
								    	}
								    									    	
									    if(!pre3.getChildren().isEmpty()) {
									    	int count4 = 0;
									    	List<presentation_detail> preList4 = pre3.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
										    for(presentation_detail pre4 : preList4) {
										    	count4++;
										    	String prefixName4 = pre4.getPrefix() + "_" + pre4.getName();
										    	String pfx4 = "../../../../def/ic/lhdnm-sme-cor";
										    	String name4 = pre4.getName();
										    	tax_sme_concept tsconcept4 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre4.getName())).findFirst().orElse(null);
										    	if(Objects.isNull(tsconcept4)) {
										    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre4.getName())).findFirst().orElse(null);
											    	if(Objects.isNull(tse)) {
											    		continue;
											    	} else {
											    		name4 = tse.getName();
											    		pfx4 = "../enumerations/" + tse.getPrefix();
											    		prefixName4 = tse.getPrefix() + "_" + name4;
											    	}
										    	} else {
										    		name4 = tsconcept4.getName();
										    		if(tsconcept4.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
										    			pfx4 = "../enumerations/" + tsconcept4.getPrefix();
										    		} else {
										    			pfx4 = "../../../../def/ic/" + tsconcept4.getPrefix() + "-cor";
										    		}				    		
										    		prefixName4 = tsconcept4.getPrefix() + "_" + name4;
										    	}
										    	
										    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre4.getName())).findAny().orElse(null) == null) {
										    		tempArray.add(pre4.getName());
										    		writer.newLine();
												    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx4 + "_" + dtString + ".xsd#" + prefixName4 + "\" xlink:label=\"" + name4 + "\" xlink:title=\"" + name4 + "\"/>");
												    writer.newLine();
												    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name3 + "\" xlink:to=\"" + name4 + "\" xlink:title=\"presentation: " + name3 + " to " + name4 + "\" priority=\"0\" order=\"" + count4 + ".0\"/>");
												    
												} else {
													writer.newLine();
												    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name3 + "\" xlink:to=\"" + name4 + "\" xlink:title=\"presentation: " + name3 + " to " + name4 + "\" priority=\"0\" order=\"" + count4 + ".0\"/>");
												    
										    	}
										    	
										    	if(!pre4.getChildren().isEmpty()) {
										    		int count5 = 0;
											    	List<presentation_detail> preList5 = pre4.getChildren().stream().filter(lform -> lform.getPrefix() != null).collect(Collectors.toList());
											    	for(presentation_detail pre5 : preList5) {
											    		count5++;
												    	String prefixName5 = pre5.getPrefix() + "_" + pre5.getName();
												    	String pfx5 = "../../../../def/ic/lhdnm-sme-cor";
												    	String name5 = pre5.getName();
												    	tax_sme_concept tsconcept5 = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre5.getName())).findFirst().orElse(null);
												    	if(Objects.isNull(tsconcept5)) {
												    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre5.getName())).findFirst().orElse(null);
													    	if(Objects.isNull(tse)) {
													    		continue;
													    	} else {
													    		name5 = tse.getName();
													    		pfx5 = "../enumerations/" + tse.getPrefix();
													    		prefixName5 = tse.getPrefix() + "_" + name5;
													    	}
												    	} else {
												    		name5 = tsconcept5.getName();
												    		if(tsconcept5.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
												    			pfx5 = "../enumerations/" + tsconcept5.getPrefix();
												    		} else {
												    			pfx5 = "../../../../def/ic/" + tsconcept5.getPrefix() + "-cor";
												    		}				    		
												    		prefixName5 = tsconcept5.getPrefix() + "_" + name5;
												    	}
												    	
												    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre5.getName())).findAny().orElse(null) == null) {
												    		tempArray.add(pre5.getName());
												    		writer.newLine();
														    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"" + pfx5 + "_" + dtString + ".xsd#" + prefixName5 + "\" xlink:label=\"" + name5 + "\" xlink:title=\"" + name5 + "\"/>");
														    writer.newLine();
														    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name4 + "\" xlink:to=\"" + name5 + "\" xlink:title=\"presentation: " + name4 + " to " + name5 + "\" priority=\"0\" order=\"" + count5 + ".0\"/>");
														    
														} else {
															writer.newLine();
														    writer.write("    <link:presentationArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/parent-child\" xlink:from=\"" + name4 + "\" xlink:to=\"" + name5 + "\" xlink:title=\"presentation: " + name4 + " to " + name5 + "\" priority=\"0\" order=\"" + count5 + ".0\"/>");
														    
												    	}
											    	}
										    	}
										    	
										    }
									    }
								    }
							    }
							    
						    }
					    }
				    }
			    	
			    }
			    
			    writer.newLine();
			    writer.write("  </link:presentationLink>");
			    writer.newLine();
			    writer.write("</link:linkbase>");
			    
			    isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
				
		return isCompleted;
	}
	
	public boolean writeLinkbaseLabEn(String path, String dtString, Long id, String type) {
		boolean isCompleted = false;
		List<linkbase_form> linkbaseList1 = findByModalId(id);
		List<linkbase_form> linkbaseList = null;
		List<tax_concept> taxConceptList = tcService.getAllByModalId(id);
	    List<tax_enum> taxEnumList = enumService.getAllByModalId(id);
	    List<tax_sme_concept> tsconceptList = tscService.getAllByModalId(id);
		List<tax_sme_enum> tseList = tseService.getAllByModalId(id);
		if(type.equalsIgnoreCase("lhdnm-sme")) {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		} else {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		}
		
		for(linkbase_form form : linkbaseList) {
			String fname = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
				fname = fname.replaceAll(" ", "-");
//				fname = fname.replaceAll(".", "");
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/lab_lhdnm-" + fname + "-en_" + dtString + ".xml"))) {
				String href = "lhdnm-" + fname + "_" + dtString + ".xsd#" + fname;
				
				writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    if(type.equalsIgnoreCase("lhdnm-sme")) {
			    	writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\" xmlns:sme-is-bs=\"http://www.hasil.gov.my/xbrl/" + dtString + "/sme-bs\" xmlns:c-fi=\"http://www.hasil.gov.my/xbrl/" + dtString + "/c-fi\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:enum=\"http://xbrl.org/2014/extensible-enumerations\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:lhdnm-sme-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/sme-enum\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:lhdnm-sme=\"http://www.hasil.gov.my/xbrl/" + dtString + "/lhdnm-sme\" xmlns:reference=\"http://xbrl.org/2008/reference\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\">");
			    } else {
			    	writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-" + dtString + ".xsd\" xmlns:" + fname + "=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + fname + "\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:lhdnm-c_" + dtString + "_full_entry_point=\"http://www.hasil.gov.my/xbrl/" + dtString + "/c\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:enum=\"http://xbrl.org/2014/extensible-enumerations\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
			    }
			    
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"" + form.getLinkrole() + "\" xlink:type=\"simple\" xlink:href=\"" + href + "\" />");
			    writer.newLine();
			    writer.write("  <link:labelLink xlink:type=\"extended\" xlink:role=\"" + form.getLinkrole() + "\">");
			    
//			    List<presentation_detail> preList = form.getPresentationList().stream().filter(lform -> lform.getLabel_en_form() != null).collect(Collectors.toList());
			    List<presentation_detail> preList = form.getPresentationList();
			    ArrayList<String> tempArray = new ArrayList<>();
			    ArrayList<String> tempArray2 = new ArrayList<>();
			    int count = 0;
			    
			    for(presentation_detail pre : preList) {

			    	if(Strings.isNotBlank(pre.getLabel_en_form())) {
			    		
			    		String pfx = "lhdnm-cor";
			    		String prefixName = pre.getPrefix() + "_" + pre.getName();
				    	String name = pre.getName();
				    	if(pre.getPrefix().contains("lhdnm-sme")) {
				    		tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre.getName())).findFirst().orElse(null);
					    	if(Objects.isNull(tsconcept)) {
					    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre.getName())).findFirst().orElse(null);
						    	if(Objects.isNull(tse)) {
						    		continue;
						    	} else {
						    		name = tse.getName();
						    		pfx = "../enumerations/" + tse.getPrefix();
						    		prefixName = tse.getPrefix() + "_" + name;
						    	}
					    	} else {
					    		name = tsconcept.getName();
					    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
					    			pfx = "../enumerations/" + tsconcept.getPrefix();
					    		} else {
					    			pfx = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
					    		}				    		
					    		prefixName = tsconcept.getPrefix() + "_" + name;
					    	}
				    	} else {
				    		tax_enum te;
					    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
					    	if(Objects.isNull(tconcept)) {
					    		te = taxEnumList.stream().filter(tcl -> pre.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
						    	if(Objects.isNull(te)) {
						    		continue;
						    	} else {
						    		name = te.getName();
						    		pfx = "../enumerations/" + te.getPrefix();
						    		prefixName = te.getPrefix() + "_" + name;
						    	}
					    	} else {
					    		name = tconcept.getName();
					    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
					    			pfx = "../enumerations/" + tconcept.getPrefix();
					    		} else {
					    			pfx = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
					    		}				    		
					    		prefixName = tconcept.getPrefix() + "_" + name;
					    	}
				    	}
				    	
				    	String labelName = "label_" + name;
				    	String labelText = StringEscapeUtils.escapeXml(pre.getLabel_en_form());
				    	
				    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre.getName())).findAny().orElse(null) == null) {
				    		tempArray.add(pre.getName());
				    		writer.newLine();
						    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx + "_" + dtString + ".xsd#" + prefixName + "\" xlink:label=\"" + name + "\" xlink:title=\"" + name + "\"/>");
						    writer.newLine();
						    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"en\" id=\"" + labelName + "\">" + labelText + "</link:label>");
						    writer.newLine();
						    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + name + " to " + labelName + "\"/>");
						    
				    	} else {
				    		writer.newLine();
						    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"en\" id=\"" + labelName + "\">" + labelText + "</link:label>");
						    writer.newLine();
						    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + name + " to " + labelName + "\"/>");
						    
				    	}
				    	
					    if(Strings.isNotBlank(pre.getLabel_en_document())) {
					    	if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre.getName())).findAny().orElse(null) == null) {
					    		tempArray2.add(pre.getName());
					    		count++;
						    	String labelDoc = "label_" + pre.getName() + "_" + count;
						    	String docText = StringEscapeUtils.escapeXml(pre.getLabel_en_document());
						    	writer.newLine();
							    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc + "\" xml:lang=\"en\" id=\"" + labelDoc + "\">" + docText + "</link:label>");
							    writer.newLine();
							    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name + "\" xlink:to=\"" + labelDoc + "\" xlink:title=\"label: " + name + " to " + labelName + "\"/>");
					    	}
						    
					    }
			    	}
			    	
			    	List<presentation_detail> preList1 = pre.getChildren();
			    	int count1 = 0;
				    for(presentation_detail pre1 : preList1) {
				    	
				    	if(Strings.isNotBlank(pre1.getLabel_en_form())) {
				    		String prefixName1 = pre1.getPrefix() + "_" + pre1.getName();
				    		String pfx1 = "lhdnm-cor";
				    		String name1 = pre1.getName();
				    		if(pre1.getPrefix().contains("lhdnm-sme")) {
				    			tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre1.getName())).findFirst().orElse(null);
						    	if(Objects.isNull(tsconcept)) {
						    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre1.getName())).findFirst().orElse(null);
							    	if(Objects.isNull(tse)) {
							    		continue;
							    	} else {
							    		name1 = tse.getName();
							    		pfx1 = "../enumerations/" + tse.getPrefix();
							    		prefixName1 = tse.getPrefix() + "_" + name1;
							    	}
						    	} else {
						    		name1 = tsconcept.getName();
						    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
						    			pfx1 = "../enumerations/" + tsconcept.getPrefix();
						    		} else {
						    			pfx1 = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
						    		}				    		
						    		prefixName1 = tsconcept.getPrefix() + "_" + name1;
						    	}
					    	} else {
					    		tax_enum te;
						    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre1.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
						    	if(Objects.isNull(tconcept)) {
						    		te =taxEnumList.stream().filter(tcl -> pre1.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
							    	if(Objects.isNull(te)) {
							    		continue;
							    	} else {
							    		name1 = te.getName();
							    		pfx1 = "../enumerations/" + te.getPrefix();
							    		prefixName1 = te.getPrefix() + "_" + name1;
							    	}
						    	} else {
						    		name1 = tconcept.getName();
						    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
						    			pfx1 = "../enumerations/" + tconcept.getPrefix();
						    		} else {
						    			pfx1 = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
						    		}				    		
						    		prefixName1 = tconcept.getPrefix() + "_" + name1;
						    	}
					    	}
				    		
					    	String labelName1 = "label_" + name1;
					    	String labelText1 = StringEscapeUtils.escapeXml(pre1.getLabel_en_form());
					    	
					    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre1.getName())).findAny().orElse(null) == null) {
					    		tempArray.add(pre1.getName());
					    		writer.newLine();
							    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx1 + "_" + dtString + ".xsd#" + prefixName1 + "\" xlink:label=\"" + name1 + "\" xlink:title=\"" + name1 + "\"/>");
							    writer.newLine();
							    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName1 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName1 + "\" xml:lang=\"en\" id=\"" + labelName1 + "\">" + labelText1 + "</link:label>");
							    writer.newLine();
							    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name1 + "\" xlink:to=\"" + labelName1 + "\" xlink:title=\"label: " + name1 + " to " + labelName1 + "\"/>");
							    
					    	} else {
					    		writer.newLine();
							    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName1 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName1 + "\" xml:lang=\"en\" id=\"" + labelName1 + "\">" + labelText1 + "</link:label>");
							    writer.newLine();
							    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name1 + "\" xlink:to=\"" + labelName1 + "\" xlink:title=\"label: " + name1 + " to " + labelName1 + "\"/>");
							    
					    	}
					    	
					    	if(Strings.isNotBlank(pre1.getLabel_en_document())) {
					    		if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre1.getName())).findAny().orElse(null) == null) {
						    		tempArray2.add(pre1.getName());
						    		count1++;
							    	String labelDoc1 = "label_" + pre1.getName() + "_" + count1;
							    	String docText1 = StringEscapeUtils.escapeXml(pre1.getLabel_en_document());
							    	writer.newLine();
								    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc1 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc1 + "\" xml:lang=\"en\" id=\"" + labelDoc1 + "\">" + docText1 + "</link:label>");
								    writer.newLine();
								    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name1 + "\" xlink:to=\"" + labelDoc1 + "\" xlink:title=\"label: " + name1 + " to " + labelDoc1 + "\"/>");
								    
						    	}
						    	
						    }
				    		
				    	}
				    	
					    if(!pre1.getChildren().isEmpty()) {
					    	List<presentation_detail> preList2 = pre1.getChildren();
					    	int count2 = 0;
						    for(presentation_detail pre2 : preList2) {
						    	
						    	if(Strings.isNotBlank(pre2.getLabel_en_form())) {
						    		String prefixName2 = pre2.getPrefix() + "_" + pre2.getName();
						    		String pfx2 = "lhdnm-cor";
						    		String name2 = pre2.getName();
						    		if(pre2.getPrefix().contains("lhdnm-sme")) {
						    			tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre2.getName())).findFirst().orElse(null);
								    	if(Objects.isNull(tsconcept)) {
								    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre2.getName())).findFirst().orElse(null);
									    	if(Objects.isNull(tse)) {
									    		continue;
									    	} else {
									    		name2 = tse.getName();
									    		pfx2 = "../enumerations/" + tse.getPrefix();
									    		prefixName2 = tse.getPrefix() + "_" + name2;
									    	}
								    	} else {
								    		name2 = tsconcept.getName();
								    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
								    			pfx2 = "../enumerations/" + tsconcept.getPrefix();
								    		} else {
								    			pfx2 = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
								    		}				    		
								    		prefixName2 = tsconcept.getPrefix() + "_" + name2;
								    	}
							    	} else {
							    		tax_enum te;
								    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre2.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
								    	if(Objects.isNull(tconcept)) {
								    		te = taxEnumList.stream().filter(tcl -> pre2.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
									    	if(Objects.isNull(te)) {
									    		continue;
									    	} else {
									    		name2 = te.getName();
									    		pfx2 = "../enumerations/" + te.getPrefix();
									    		prefixName2 = te.getPrefix() + "_" + name2;
									    	}
								    	} else {
								    		name2 = tconcept.getName();
								    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
								    			pfx2 = "../enumerations/" + tconcept.getPrefix();
								    		} else {
								    			pfx2 = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
								    		}				    		
								    		prefixName2 = tconcept.getPrefix() + "_" + name2;
								    	}
							    	}
							    	
							    	String labelName2 = "label_" + name2;
							    	String labelText2 = StringEscapeUtils.escapeXml(pre2.getLabel_en_form());
							    	
							    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre2.getName())).findAny().orElse(null) == null) {
							    		tempArray.add(pre2.getName());
							    		writer.newLine();
									    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx2 + "_" + dtString + ".xsd#" + prefixName2 + "\" xlink:label=\"" + name2 + "\" xlink:title=\"" + name2 + "\"/>");
									    writer.newLine();
									    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName2 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName2 + "\" xml:lang=\"en\" id=\"" + labelName2 + "\">" + labelText2 + "</link:label>");
									    writer.newLine();
									    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name2 + "\" xlink:to=\"" + labelName2 + "\" xlink:title=\"label: " + name2 + " to " + labelName2 + "\"/>");
									    
							    	} else {
							    		writer.newLine();
									    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName2 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName2 + "\" xml:lang=\"en\" id=\"" + labelName2 + "\">" + labelText2 + "</link:label>");
									    writer.newLine();
									    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name2 + "\" xlink:to=\"" + labelName2 + "\" xlink:title=\"label: " + name2 + " to " + labelName2 + "\"/>");
									    
							    	}
							    	
							    	if(Strings.isNotBlank(pre2.getLabel_en_document())) {
							    		if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre2.getName())).findAny().orElse(null) == null) {
								    		tempArray2.add(pre2.getName());
								    		count2++;
									    	String labelDoc2 = "label_" + pre2.getName() + "_" + count2;
									    	String docText2 = StringEscapeUtils.escapeXml(pre2.getLabel_en_document());
									    	writer.newLine();
										    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc2 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc2 + "\" xml:lang=\"en\" id=\"" + labelDoc2 + "\">" + docText2 + "</link:label>");
										    writer.newLine();
										    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name2 + "\" xlink:to=\"" + labelDoc2 + "\" xlink:title=\"label: " + name2 + " to " + labelDoc2 + "\"/>");
										    
								    	}
							    		
								    }
						    		
						    	}
						    	
							    if(!pre2.getChildren().isEmpty()) {
							    	List<presentation_detail> preList3 = pre2.getChildren();
							    	int count3 = 0;
								    for(presentation_detail pre3 : preList3) {
								    	
								    	if(Strings.isNotBlank(pre3.getLabel_en_form())) {
								    		String prefixName3 = pre3.getPrefix() + "_" + pre3.getName();
								    		String pfx3 = "lhdnm-cor";
								    		String name3 = pre3.getName();
								    		if(pre3.getPrefix().contains("lhdnm-sme")) {
								    			tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre3.getName())).findFirst().orElse(null);
										    	if(Objects.isNull(tsconcept)) {
										    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre3.getName())).findFirst().orElse(null);
											    	if(Objects.isNull(tse)) {
											    		continue;
											    	} else {
											    		name3 = tse.getName();
											    		pfx3 = "../enumerations/" + tse.getPrefix();
											    		prefixName3 = tse.getPrefix() + "_" + name3;
											    	}
										    	} else {
										    		name3 = tsconcept.getName();
										    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
										    			pfx3 = "../enumerations/" + tsconcept.getPrefix();
										    		} else {
										    			pfx3 = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
										    		}				    		
										    		prefixName3 = tsconcept.getPrefix() + "_" + name3;
										    	}
									    	} else {
									    		tax_enum te;
										    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre3.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
										    	if(Objects.isNull(tconcept)) {
										    		te = taxEnumList.stream().filter(tcl -> pre3.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
											    	if(Objects.isNull(te)) {
											    		continue;
											    	} else {
											    		name3 = te.getName();
											    		pfx3 = "../enumerations/" + te.getPrefix();
											    		prefixName3 = te.getPrefix() + "_" + name3;
											    	}
										    	} else {
										    		name3 = tconcept.getName();
										    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
										    			pfx3 = "../enumerations/" + tconcept.getPrefix();
										    		} else {
										    			pfx3 = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
										    		}				    		
										    		prefixName3 = tconcept.getPrefix() + "_" + name3;
										    	}
									    	}
									    	
									    	String labelName3 = "label_" + name3;
									    	String labelText3 = StringEscapeUtils.escapeXml(pre3.getLabel_en_form());
									    	
									    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre3.getName())).findAny().orElse(null) == null) {
									    		tempArray.add(pre3.getName());
									    		writer.newLine();
											    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx3 + "_" + dtString + ".xsd#" + prefixName3 + "\" xlink:label=\"" + name3 + "\" xlink:title=\"" + name3 + "\"/>");
											    writer.newLine();
											    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName3 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName3 + "\" xml:lang=\"en\" id=\"" + labelName3 + "\">" + labelText3 + "</link:label>");
											    writer.newLine();
											    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name3 + "\" xlink:to=\"" + labelName3 + "\" xlink:title=\"label: " + name3 + " to " + labelName3 + "\"/>");
											    
									    	} else {
									    		writer.newLine();
											    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName3 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName3 + "\" xml:lang=\"en\" id=\"" + labelName3 + "\">" + labelText3 + "</link:label>");
											    writer.newLine();
											    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name3 + "\" xlink:to=\"" + labelName3 + "\" xlink:title=\"label: " + name3 + " to " + labelName3 + "\"/>");
											    
									    	}
									    	
									    	if(Strings.isNotBlank(pre3.getLabel_en_document())) {
									    		if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre3.getName())).findAny().orElse(null) == null) {
										    		tempArray2.add(pre3.getName());
										    		count3++;
											    	String labelDoc3 = "label_" + pre3.getName() + "_" + count3;
											    	String docText3 = StringEscapeUtils.escapeXml(pre3.getLabel_en_document());
											    	writer.newLine();
												    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc3 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc3 + "\" xml:lang=\"en\" id=\"" + labelDoc3 + "\">" + docText3 + "</link:label>");
												    writer.newLine();
												    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name3 + "\" xlink:to=\"" + labelDoc3 + "\" xlink:title=\"label: " + name3 + " to " + labelDoc3 + "\"/>");
												    
										    	}
									    		
										    }
								    		
								    	}
									    if(!pre3.getChildren().isEmpty()) {
									    	List<presentation_detail> preList4 = pre3.getChildren();
									    	int count4 = 0;
										    for(presentation_detail pre4 : preList4) {
										    	
										    	if(Strings.isNotBlank(pre4.getLabel_en_form())) {
										    		String prefixName4 = pre4.getPrefix() + "_" + pre4.getName();
										    		String pfx4 = "lhdnm-cor";
										    		String name4 = pre4.getName();
										    		if(pre4.getPrefix().contains("lhdnm-sme")) {
										    			tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre4.getName())).findFirst().orElse(null);
												    	if(Objects.isNull(tsconcept)) {
												    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre4.getName())).findFirst().orElse(null);
													    	if(Objects.isNull(tse)) {
													    		continue;
													    	} else {
													    		name4 = tse.getName();
													    		pfx4 = "../enumerations/" + tse.getPrefix();
													    		prefixName4 = tse.getPrefix() + "_" + name4;
													    	}
												    	} else {
												    		name4 = tsconcept.getName();
												    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
												    			pfx4 = "../enumerations/" + tsconcept.getPrefix();
												    		} else {
												    			pfx4 = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
												    		}				    		
												    		prefixName4 = tsconcept.getPrefix() + "_" + name4;
												    	}
											    	} else {
											    		tax_enum te;
												    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre4.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
												    	if(Objects.isNull(tconcept)) {
												    		te = taxEnumList.stream().filter(tcl -> pre4.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
													    	if(Objects.isNull(te)) {
													    		continue;
													    	} else {
													    		name4 = te.getName();
													    		pfx4 = "../enumerations/" + te.getPrefix();
													    		prefixName4 = te.getPrefix() + "_" + name4;
													    	}
												    	} else {
												    		name4 = tconcept.getName();
												    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
												    			pfx4 = "../enumerations/" + tconcept.getPrefix();
												    		} else {
												    			pfx4 = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
												    		}				    		
												    		prefixName4 = tconcept.getPrefix() + "_" + name4;
												    	}
											    	}
											    	
											    	String labelName4 = "label_" + name4;
											    	String labelText4 = StringEscapeUtils.escapeXml(pre4.getLabel_en_form());
											    	
											    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre4.getName())).findAny().orElse(null) == null) {
											    		tempArray.add(pre4.getName());
											    		writer.newLine();
													    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx4 + "_" + dtString + ".xsd#" + prefixName4 + "\" xlink:label=\"" + name4 + "\" xlink:title=\"" + name4 + "\"/>");
													    writer.newLine();
													    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName4 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName4 + "\" xml:lang=\"en\" id=\"" + labelName4 + "\">" + labelText4 + "</link:label>");
													    writer.newLine();
													    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name4 + "\" xlink:to=\"" + labelName4 + "\" xlink:title=\"label: " + name4 + " to " + labelName4 + "\"/>");
													    
											    	} else {
											    		writer.newLine();
													    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName4 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName4 + "\" xml:lang=\"en\" id=\"" + labelName4 + "\">" + labelText4 + "</link:label>");
													    writer.newLine();
													    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name4 + "\" xlink:to=\"" + labelName4 + "\" xlink:title=\"label: " + name4 + " to " + labelName4 + "\"/>");
													    
											    	}
											    	
											    	if(Strings.isNotBlank(pre4.getLabel_en_document())) {
											    		if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre4.getName())).findAny().orElse(null) == null) {
												    		tempArray2.add(pre4.getName());
												    		count4++;
													    	String labelDoc4 = "label_" + pre4.getName() + "_" + count4;
													    	String docText4 = StringEscapeUtils.escapeXml(pre4.getLabel_en_document());
													    	writer.newLine();
														    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc4 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc4 + "\" xml:lang=\"en\" id=\"" + labelDoc4 + "\">" + docText4 + "</link:label>");
														    writer.newLine();
														    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name4 + "\" xlink:to=\"" + labelDoc4 + "\" xlink:title=\"label: " + name4 + " to " + labelDoc4 + "\"/>");
														    
												    	}
											    		
												    }
										    		
										    	}
										    	
										    	if(!pre4.getChildren().isEmpty()) {
											    	List<presentation_detail> preList5 = pre4.getChildren();
											    	int count5 = 0;
												    for(presentation_detail pre5 : preList5) {
												    	
												    	if(Strings.isNotBlank(pre5.getLabel_en_form())) {
												    		String prefixName5 = pre5.getPrefix() + "_" + pre5.getName();
												    		String pfx5 = "lhdnm-cor";
												    		String name5 = pre5.getName();
												    		if(pre5.getPrefix().contains("lhdnm-sme")) {
												    			tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre5.getName())).findFirst().orElse(null);
														    	if(Objects.isNull(tsconcept)) {
														    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre5.getName())).findFirst().orElse(null);
															    	if(Objects.isNull(tse)) {
															    		continue;
															    	} else {
															    		name5 = tse.getName();
															    		pfx5 = "../enumerations/" + tse.getPrefix();
															    		prefixName5 = tse.getPrefix() + "_" + name5;
															    	}
														    	} else {
														    		name5 = tsconcept.getName();
														    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
														    			pfx5 = "../enumerations/" + tsconcept.getPrefix();
														    		} else {
														    			pfx5 = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
														    		}				    		
														    		prefixName5 = tsconcept.getPrefix() + "_" + name5;
														    	}
													    	} else {
													    		tax_enum te;
														    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre5.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
														    	if(Objects.isNull(tconcept)) {
														    		te = taxEnumList.stream().filter(tcl -> pre5.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
															    	if(Objects.isNull(te)) {
															    		continue;
															    	} else {
															    		name5 = te.getName();
															    		pfx5 = "../enumerations/" + te.getPrefix();
															    		prefixName5 = te.getPrefix() + "_" + name5;
															    	}
														    	} else {
														    		name5 = tconcept.getName();
														    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
														    			pfx5 = "../enumerations/" + tconcept.getPrefix();
														    		} else {
														    			pfx5 = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
														    		}				    		
														    		prefixName5 = tconcept.getPrefix() + "_" + name5;
														    	}
													    	}
													    	
													    	String labelName5 = "label_" + name5;
													    	String labelText5 = StringEscapeUtils.escapeXml(pre5.getLabel_en_form());
													    	
													    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre5.getName())).findAny().orElse(null) == null) {
													    		tempArray.add(pre5.getName());
													    		writer.newLine();
															    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx5 + "_" + dtString + ".xsd#" + prefixName5 + "\" xlink:label=\"" + name5 + "\" xlink:title=\"" + name5 + "\"/>");
															    writer.newLine();
															    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName5 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName5 + "\" xml:lang=\"en\" id=\"" + labelName5 + "\">" + labelText5 + "</link:label>");
															    writer.newLine();
															    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name5 + "\" xlink:to=\"" + labelName5 + "\" xlink:title=\"label: " + name5 + " to " + labelName5 + "\"/>");
															    
													    	} else {
													    		writer.newLine();
															    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName5 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName5 + "\" xml:lang=\"en\" id=\"" + labelName5 + "\">" + labelText5 + "</link:label>");
															    writer.newLine();
															    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name5 + "\" xlink:to=\"" + labelName5 + "\" xlink:title=\"label: " + name5 + " to " + labelName5 + "\"/>");
															    
													    	}
													    	
													    	if(Strings.isNotBlank(pre5.getLabel_en_document())) {
													    		if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre5.getName())).findAny().orElse(null) == null) {
														    		tempArray2.add(pre5.getName());
														    		count5++;
															    	String labelDoc5 = "label_" + pre5.getName() + "_" + count5;
															    	String docText5 = StringEscapeUtils.escapeXml(pre5.getLabel_en_document());
															    	writer.newLine();
																    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc5 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc5 + "\" xml:lang=\"en\" id=\"" + labelDoc5 + "\">" + docText5 + "</link:label>");
																    writer.newLine();
																    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name5 + "\" xlink:to=\"" + labelDoc5 + "\" xlink:title=\"label: " + name5 + " to " + labelDoc5 + "\"/>");
																    
														    	}
													    		
														    }
												    		
												    	}
												    	
												    	if(!pre5.getChildren().isEmpty()) {
													    	List<presentation_detail> preList6 = pre5.getChildren();
													    	int count6 = 0;
														    for(presentation_detail pre6 : preList6) {
														    	
														    	if(Strings.isNotBlank(pre6.getLabel_en_form())) {
														    		String prefixName6 = pre6.getPrefix() + "_" + pre6.getName();
														    		String pfx6 = "lhdnm-cor";
														    		String name6 = pre6.getName();
														    		if(pre6.getPrefix().contains("lhdnm-sme")) {
														    			tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre6.getName())).findFirst().orElse(null);
																    	if(Objects.isNull(tsconcept)) {
																    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre6.getName())).findFirst().orElse(null);
																	    	if(Objects.isNull(tse)) {
																	    		continue;
																	    	} else {
																	    		name6 = tse.getName();
																	    		pfx6 = "../enumerations/" + tse.getPrefix();
																	    		prefixName6 = tse.getPrefix() + "_" + name6;
																	    	}
																    	} else {
																    		name6 = tsconcept.getName();
																    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
																    			pfx6 = "../enumerations/" + tsconcept.getPrefix();
																    		} else {
																    			pfx6 = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
																    		}				    		
																    		prefixName6 = tsconcept.getPrefix() + "_" + name6;
																    	}
															    	} else {
															    		tax_enum te;
																    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre6.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																    	if(Objects.isNull(tconcept)) {
																    		te = taxEnumList.stream().filter(tcl -> pre6.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																	    	if(Objects.isNull(te)) {
																	    		continue;
																	    	} else {
																	    		name6 = te.getName();
																	    		pfx6 = "../enumerations/" + te.getPrefix();
																	    		prefixName6 = te.getPrefix() + "_" + name6;
																	    	}
																    	} else {
																    		name6 = tconcept.getName();
																    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
																    			pfx6 = "../enumerations/" + tconcept.getPrefix();
																    		} else {
																    			pfx6 = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
																    		}				    		
																    		prefixName6 = tconcept.getPrefix() + "_" + name6;
																    	}
															    	}
															    	
															    	String labelName6 = "label_" + name6;
															    	String labelText6 = StringEscapeUtils.escapeXml(pre6.getLabel_en_form());
															    	
															    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre6.getName())).findAny().orElse(null) == null) {
															    		tempArray.add(pre6.getName());
															    		writer.newLine();
																	    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx6 + "_" + dtString + ".xsd#" + prefixName6 + "\" xlink:label=\"" + name6 + "\" xlink:title=\"" + name6 + "\"/>");
																	    writer.newLine();
																	    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName6 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName6 + "\" xml:lang=\"en\" id=\"" + labelName6 + "\">" + labelText6 + "</link:label>");
																	    writer.newLine();
																	    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name6 + "\" xlink:to=\"" + labelName6 + "\" xlink:title=\"label: " + name6 + " to " + labelName6 + "\"/>");
																	    
															    	} else {
															    		writer.newLine();
																	    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName6 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName6 + "\" xml:lang=\"en\" id=\"" + labelName6 + "\">" + labelText6 + "</link:label>");
																	    writer.newLine();
																	    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name6 + "\" xlink:to=\"" + labelName6 + "\" xlink:title=\"label: " + name6 + " to " + labelName6 + "\"/>");
																	    
															    	}
															    	
															    	if(Strings.isNotBlank(pre6.getLabel_en_document())) {
															    		if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre6.getName())).findAny().orElse(null) == null) {
																    		tempArray2.add(pre6.getName());
																    		count6++;
																	    	String labelDoc6 = "label_" + pre6.getName() + "_" + count6;
																	    	String docText6 = StringEscapeUtils.escapeXml(pre6.getLabel_en_document());
																	    	writer.newLine();
																		    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc6 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc6 + "\" xml:lang=\"en\" id=\"" + labelDoc6 + "\">" + docText6 + "</link:label>");
																		    writer.newLine();
																		    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name6 + "\" xlink:to=\"" + labelDoc6 + "\" xlink:title=\"label: " + name6 + " to " + labelDoc6 + "\"/>");
																		    
																    	}
															    		
																    }
														    		
														    	}
														    	
														    	if(!pre6.getChildren().isEmpty()) {
															    	List<presentation_detail> preList7 = pre6.getChildren();
															    	int count7 = 0;
																    for(presentation_detail pre7 : preList7) {
																    	
																    	if(Strings.isNotBlank(pre7.getLabel_en_form())) {
																    		String prefixName7 = pre7.getPrefix() + "_" + pre7.getName();
																    		String pfx7 = "lhdnm-cor";
																    		String name7 = pre7.getName();
																    		if(pre7.getPrefix().contains("lhdnm-sme")) {
																    			tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre7.getName())).findFirst().orElse(null);
																		    	if(Objects.isNull(tsconcept)) {
																		    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre7.getName())).findFirst().orElse(null);
																			    	if(Objects.isNull(tse)) {
																			    		continue;
																			    	} else {
																			    		name7 = tse.getName();
																			    		pfx7 = "../enumerations/" + tse.getPrefix();
																			    		prefixName7 = tse.getPrefix() + "_" + name7;
																			    	}
																		    	} else {
																		    		name7 = tsconcept.getName();
																		    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
																		    			pfx7 = "../enumerations/" + tsconcept.getPrefix();
																		    		} else {
																		    			pfx7 = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
																		    		}				    		
																		    		prefixName7 = tsconcept.getPrefix() + "_" + name7;
																		    	}
																	    	} else {
																	    		tax_enum te;
																		    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre7.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																		    	if(Objects.isNull(tconcept)) {
																		    		te = taxEnumList.stream().filter(tcl -> pre7.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																			    	if(Objects.isNull(te)) {
																			    		continue;
																			    	} else {
																			    		name7 = te.getName();
																			    		pfx7 = "../enumerations/" + te.getPrefix();
																			    		prefixName7 = te.getPrefix() + "_" + name7;
																			    	}
																		    	} else {
																		    		name7 = tconcept.getName();
																		    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
																		    			pfx7 = "../enumerations/" + tconcept.getPrefix();
																		    		} else {
																		    			pfx7 = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
																		    		}				    		
																		    		prefixName7 = tconcept.getPrefix() + "_" + name7;
																		    	}
																	    	}
																	    	
																	    	String labelName7 = "label_" + name7;
																	    	String labelText7 = StringEscapeUtils.escapeXml(pre7.getLabel_en_form());
																	    	
																	    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre7.getName())).findAny().orElse(null) == null) {
																	    		tempArray.add(pre7.getName());
																	    		writer.newLine();
																			    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx7 + "_" + dtString + ".xsd#" + prefixName7 + "\" xlink:label=\"" + name7 + "\" xlink:title=\"" + name7 + "\"/>");
																			    writer.newLine();
																			    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName7 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName7 + "\" xml:lang=\"en\" id=\"" + labelName7 + "\">" + labelText7 + "</link:label>");
																			    writer.newLine();
																			    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name7 + "\" xlink:to=\"" + labelName7 + "\" xlink:title=\"label: " + name7 + " to " + labelName7 + "\"/>");
																			    
																	    	} else {
																	    		writer.newLine();
																			    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName7 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName7 + "\" xml:lang=\"en\" id=\"" + labelName7 + "\">" + labelText7 + "</link:label>");
																			    writer.newLine();
																			    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name7 + "\" xlink:to=\"" + labelName7 + "\" xlink:title=\"label: " + name7 + " to " + labelName7 + "\"/>");
																			    
																	    	}
																	    	
																	    	if(Strings.isNotBlank(pre7.getLabel_en_document())) {
																	    		if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre7.getName())).findAny().orElse(null) == null) {
																		    		tempArray2.add(pre7.getName());
																		    		count7++;
																			    	String labelDoc7 = "label_" + pre7.getName() + "_" + count7;
																			    	String docText7 = StringEscapeUtils.escapeXml(pre7.getLabel_en_document());
																			    	writer.newLine();
																				    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc7 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc7 + "\" xml:lang=\"en\" id=\"" + labelDoc7 + "\">" + docText7 + "</link:label>");
																				    writer.newLine();
																				    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name7 + "\" xlink:to=\"" + labelDoc7 + "\" xlink:title=\"label: " + name7 + " to " + labelDoc7 + "\"/>");
																				    
																		    	}
																	    		
																		    }
																    		
																    	}
																    	
																    }
															    }
														    	
														    }
													    }
												    	
												    }
											    }
										    	
										    }
									    }
								    }
							    }
							    
						    }
					    }
				    }
			    	
			    }
			    
			    
			    writer.newLine();
			    writer.write("  </link:labelLink>");
			    writer.newLine();
			    writer.write("</link:linkbase>");
				
				isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
		return isCompleted;
	}
	
	public boolean writeLinkbaseLabMs(String path, String dtString, Long id, String type) {
		boolean isCompleted = false;
		List<linkbase_form> linkbaseList1 = findByModalId(id);
		List<linkbase_form> linkbaseList = null;
		List<tax_concept> taxConceptList = tcService.getAllByModalId(id);
	    List<tax_enum> taxEnumList = enumService.getAllByModalId(id);
	    List<tax_sme_concept> tsconceptList = tscService.getAllByModalId(id);
		List<tax_sme_enum> tseList = tseService.getAllByModalId(id);
		if(type.equalsIgnoreCase("lhdnm-sme")) {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		} else {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		}
		
		for(linkbase_form form : linkbaseList) {
			String fname = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
				fname = fname.replaceAll(" ", "-");
//				fname = fname.replaceAll(".", "");
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/lab_lhdnm-" + fname + "-ms_" + dtString + ".xml"))) {
				String href = "lhdnm-" + fname + "_" + dtString + ".xsd#" + fname;
				
				writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    if(type.equalsIgnoreCase("lhdnm-sme")) {
			    	writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\" xmlns:sme-is-bs=\"http://www.hasil.gov.my/xbrl/" + dtString + "/sme-bs\" xmlns:c-fi=\"http://www.hasil.gov.my/xbrl/" + dtString + "/c-fi\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:enum=\"http://xbrl.org/2014/extensible-enumerations\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:lhdnm-sme-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/sme-enum\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:lhdnm-sme=\"http://www.hasil.gov.my/xbrl/" + dtString + "/lhdnm-sme\" xmlns:reference=\"http://xbrl.org/2008/reference\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\">");
			    } else {
			    	writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-" + dtString + ".xsd\" xmlns:" + fname + "=\"http://www.hasil.gov.my/xbrl/" + dtString + "/" + fname + "\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:lhdnm-c_" + dtString + "_full_entry_point=\"http://www.hasil.gov.my/xbrl/" + dtString + "/c\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:enum=\"http://xbrl.org/2014/extensible-enumerations\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
			    }
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"" + form.getLinkrole() + "\" xlink:type=\"simple\" xlink:href=\"" + href + "\" />");
			    writer.newLine();
			    writer.write("  <link:labelLink xlink:type=\"extended\" xlink:role=\"" + form.getLinkrole() + "\">");
			    
//			    List<presentation_detail> preList = form.getPresentationList().stream().filter(lform -> lform.getLabel_en_form() != null).collect(Collectors.toList());
			    List<presentation_detail> preList = form.getPresentationList();
			    ArrayList<String> tempArray = new ArrayList<>();
			    ArrayList<String> tempArray2 = new ArrayList<>();
			    int count = 0;
			    
			    for(presentation_detail pre : preList) {

			    	if(Strings.isNotBlank(pre.getLabel_ms_form())) {
			    		
			    		String pfx = "lhdnm-cor";
			    		String prefixName = pre.getPrefix() + "_" + pre.getName();
				    	String name = pre.getName();
				    	if(pre.getPrefix().contains("lhdnm-sme")) {
				    		tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre.getName())).findFirst().orElse(null);
					    	if(Objects.isNull(tsconcept)) {
					    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre.getName())).findFirst().orElse(null);
						    	if(Objects.isNull(tse)) {
						    		continue;
						    	} else {
						    		name = tse.getName();
						    		pfx = "../enumerations/" + tse.getPrefix();
						    		prefixName = tse.getPrefix() + "_" + name;
						    	}
					    	} else {
					    		name = tsconcept.getName();
					    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
					    			pfx = "../enumerations/" + tsconcept.getPrefix();
					    		} else {
					    			pfx = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
					    		}				    		
					    		prefixName = tsconcept.getPrefix() + "_" + name;
					    	}
				    	} else {
				    		tax_enum te;
					    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
					    	if(Objects.isNull(tconcept)) {
					    		te = taxEnumList.stream().filter(tcl -> pre.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
						    	if(Objects.isNull(te)) {
						    		continue;
						    	} else {
						    		name = te.getName();
						    		pfx = "../enumerations/" + te.getPrefix();
						    		prefixName = te.getPrefix() + "_" + name;
						    	}
					    	} else {
					    		name = tconcept.getName();
					    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
					    			pfx = "../enumerations/" + tconcept.getPrefix();
					    		} else {
					    			pfx = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
					    		}				    		
					    		prefixName = tconcept.getPrefix() + "_" + name;
					    	}
				    	}
				    	
				    	String labelName = "label_" + name;
				    	String labelText = StringEscapeUtils.escapeXml(pre.getLabel_ms_form());
				    	
				    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre.getName())).findAny().orElse(null) == null) {
				    		tempArray.add(pre.getName());
				    		writer.newLine();
						    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx + "_" + dtString + ".xsd#" + prefixName + "\" xlink:label=\"" + name + "\" xlink:title=\"" + name + "\"/>");
						    writer.newLine();
						    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"en\" id=\"" + labelName + "\">" + labelText + "</link:label>");
						    writer.newLine();
						    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + name + " to " + labelName + "\"/>");
						    
				    	} else {
				    		writer.newLine();
						    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"en\" id=\"" + labelName + "\">" + labelText + "</link:label>");
						    writer.newLine();
						    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + name + " to " + labelName + "\"/>");
						    
				    	}
				    	
					    if(Strings.isNotBlank(pre.getLabel_ms_document())) {
					    	if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre.getName())).findAny().orElse(null) == null) {
					    		tempArray2.add(pre.getName());
					    		count++;
						    	String labelDoc = "label_" + pre.getName() + "_" + count;
						    	String docText = StringEscapeUtils.escapeXml(pre.getLabel_ms_document());
						    	writer.newLine();
							    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc + "\" xml:lang=\"en\" id=\"" + labelDoc + "\">" + docText + "</link:label>");
							    writer.newLine();
							    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name + "\" xlink:to=\"" + labelDoc + "\" xlink:title=\"label: " + name + " to " + labelName + "\"/>");
					    	}
						    
					    }
			    	}
			    	
			    	List<presentation_detail> preList1 = pre.getChildren();
			    	int count1 = 0;
				    for(presentation_detail pre1 : preList1) {
				    	
				    	if(Strings.isNotBlank(pre1.getLabel_ms_form())) {
				    		String prefixName1 = pre1.getPrefix() + "_" + pre1.getName();
				    		String pfx1 = "lhdnm-cor";
				    		String name1 = pre1.getName();
				    		if(pre1.getPrefix().contains("lhdnm-sme")) {
				    			tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre1.getName())).findFirst().orElse(null);
						    	if(Objects.isNull(tsconcept)) {
						    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre1.getName())).findFirst().orElse(null);
							    	if(Objects.isNull(tse)) {
							    		continue;
							    	} else {
							    		name1 = tse.getName();
							    		pfx1 = "../enumerations/" + tse.getPrefix();
							    		prefixName1 = tse.getPrefix() + "_" + name1;
							    	}
						    	} else {
						    		name1 = tsconcept.getName();
						    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
						    			pfx1 = "../enumerations/" + tsconcept.getPrefix();
						    		} else {
						    			pfx1 = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
						    		}				    		
						    		prefixName1 = tsconcept.getPrefix() + "_" + name1;
						    	}
					    	} else {
					    		tax_enum te;
						    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre1.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
						    	if(Objects.isNull(tconcept)) {
						    		te =taxEnumList.stream().filter(tcl -> pre1.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
							    	if(Objects.isNull(te)) {
							    		continue;
							    	} else {
							    		name1 = te.getName();
							    		pfx1 = "../enumerations/" + te.getPrefix();
							    		prefixName1 = te.getPrefix() + "_" + name1;
							    	}
						    	} else {
						    		name1 = tconcept.getName();
						    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
						    			pfx1 = "../enumerations/" + tconcept.getPrefix();
						    		} else {
						    			pfx1 = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
						    		}				    		
						    		prefixName1 = tconcept.getPrefix() + "_" + name1;
						    	}
					    	}
				    		
					    	String labelName1 = "label_" + name1;
					    	String labelText1 = StringEscapeUtils.escapeXml(pre1.getLabel_ms_form());
					    	
					    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre1.getName())).findAny().orElse(null) == null) {
					    		tempArray.add(pre1.getName());
					    		writer.newLine();
							    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx1 + "_" + dtString + ".xsd#" + prefixName1 + "\" xlink:label=\"" + name1 + "\" xlink:title=\"" + name1 + "\"/>");
							    writer.newLine();
							    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName1 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName1 + "\" xml:lang=\"en\" id=\"" + labelName1 + "\">" + labelText1 + "</link:label>");
							    writer.newLine();
							    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name1 + "\" xlink:to=\"" + labelName1 + "\" xlink:title=\"label: " + name1 + " to " + labelName1 + "\"/>");
							    
					    	} else {
					    		writer.newLine();
							    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName1 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName1 + "\" xml:lang=\"en\" id=\"" + labelName1 + "\">" + labelText1 + "</link:label>");
							    writer.newLine();
							    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name1 + "\" xlink:to=\"" + labelName1 + "\" xlink:title=\"label: " + name1 + " to " + labelName1 + "\"/>");
							    
					    	}
					    	
					    	if(Strings.isNotBlank(pre1.getLabel_ms_document())) {
					    		if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre1.getName())).findAny().orElse(null) == null) {
						    		tempArray2.add(pre1.getName());
						    		count1++;
							    	String labelDoc1 = "label_" + pre1.getName() + "_" + count1;
							    	String docText1 = StringEscapeUtils.escapeXml(pre1.getLabel_ms_document());
							    	writer.newLine();
								    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc1 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc1 + "\" xml:lang=\"en\" id=\"" + labelDoc1 + "\">" + docText1 + "</link:label>");
								    writer.newLine();
								    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name1 + "\" xlink:to=\"" + labelDoc1 + "\" xlink:title=\"label: " + name1 + " to " + labelDoc1 + "\"/>");
								    
						    	}
						    	
						    }
				    		
				    	}
				    	
					    if(!pre1.getChildren().isEmpty()) {
					    	List<presentation_detail> preList2 = pre1.getChildren();
					    	int count2 = 0;
						    for(presentation_detail pre2 : preList2) {
						    	
						    	if(Strings.isNotBlank(pre2.getLabel_ms_form())) {
						    		String prefixName2 = pre2.getPrefix() + "_" + pre2.getName();
						    		String pfx2 = "lhdnm-cor";
						    		String name2 = pre2.getName();
						    		if(pre2.getPrefix().contains("lhdnm-sme")) {
						    			tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre2.getName())).findFirst().orElse(null);
								    	if(Objects.isNull(tsconcept)) {
								    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre2.getName())).findFirst().orElse(null);
									    	if(Objects.isNull(tse)) {
									    		continue;
									    	} else {
									    		name2 = tse.getName();
									    		pfx2 = "../enumerations/" + tse.getPrefix();
									    		prefixName2 = tse.getPrefix() + "_" + name2;
									    	}
								    	} else {
								    		name2 = tsconcept.getName();
								    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
								    			pfx2 = "../enumerations/" + tsconcept.getPrefix();
								    		} else {
								    			pfx2 = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
								    		}				    		
								    		prefixName2 = tsconcept.getPrefix() + "_" + name2;
								    	}
							    	} else {
							    		tax_enum te;
								    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre2.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
								    	if(Objects.isNull(tconcept)) {
								    		te = taxEnumList.stream().filter(tcl -> pre2.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
									    	if(Objects.isNull(te)) {
									    		continue;
									    	} else {
									    		name2 = te.getName();
									    		pfx2 = "../enumerations/" + te.getPrefix();
									    		prefixName2 = te.getPrefix() + "_" + name2;
									    	}
								    	} else {
								    		name2 = tconcept.getName();
								    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
								    			pfx2 = "../enumerations/" + tconcept.getPrefix();
								    		} else {
								    			pfx2 = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
								    		}				    		
								    		prefixName2 = tconcept.getPrefix() + "_" + name2;
								    	}
							    	}
							    	
							    	String labelName2 = "label_" + name2;
							    	String labelText2 = StringEscapeUtils.escapeXml(pre2.getLabel_ms_form());
							    	
							    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre2.getName())).findAny().orElse(null) == null) {
							    		tempArray.add(pre2.getName());
							    		writer.newLine();
									    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx2 + "_" + dtString + ".xsd#" + prefixName2 + "\" xlink:label=\"" + name2 + "\" xlink:title=\"" + name2 + "\"/>");
									    writer.newLine();
									    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName2 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName2 + "\" xml:lang=\"en\" id=\"" + labelName2 + "\">" + labelText2 + "</link:label>");
									    writer.newLine();
									    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name2 + "\" xlink:to=\"" + labelName2 + "\" xlink:title=\"label: " + name2 + " to " + labelName2 + "\"/>");
									    
							    	} else {
							    		writer.newLine();
									    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName2 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName2 + "\" xml:lang=\"en\" id=\"" + labelName2 + "\">" + labelText2 + "</link:label>");
									    writer.newLine();
									    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name2 + "\" xlink:to=\"" + labelName2 + "\" xlink:title=\"label: " + name2 + " to " + labelName2 + "\"/>");
									    
							    	}
							    	
							    	if(Strings.isNotBlank(pre2.getLabel_ms_document())) {
							    		if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre2.getName())).findAny().orElse(null) == null) {
								    		tempArray2.add(pre2.getName());
								    		count2++;
									    	String labelDoc2 = "label_" + pre2.getName() + "_" + count2;
									    	String docText2 = StringEscapeUtils.escapeXml(pre2.getLabel_ms_document());
									    	writer.newLine();
										    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc2 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc2 + "\" xml:lang=\"en\" id=\"" + labelDoc2 + "\">" + docText2 + "</link:label>");
										    writer.newLine();
										    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name2 + "\" xlink:to=\"" + labelDoc2 + "\" xlink:title=\"label: " + name2 + " to " + labelDoc2 + "\"/>");
										    
								    	}
							    		
								    }
						    		
						    	}
						    	
							    if(!pre2.getChildren().isEmpty()) {
							    	List<presentation_detail> preList3 = pre2.getChildren();
							    	int count3 = 0;
								    for(presentation_detail pre3 : preList3) {
								    	
								    	if(Strings.isNotBlank(pre3.getLabel_ms_form())) {
								    		String prefixName3 = pre3.getPrefix() + "_" + pre3.getName();
								    		String pfx3 = "lhdnm-cor";
								    		String name3 = pre3.getName();
								    		if(pre3.getPrefix().contains("lhdnm-sme")) {
								    			tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre3.getName())).findFirst().orElse(null);
										    	if(Objects.isNull(tsconcept)) {
										    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre3.getName())).findFirst().orElse(null);
											    	if(Objects.isNull(tse)) {
											    		continue;
											    	} else {
											    		name3 = tse.getName();
											    		pfx3 = "../enumerations/" + tse.getPrefix();
											    		prefixName3 = tse.getPrefix() + "_" + name3;
											    	}
										    	} else {
										    		name3 = tsconcept.getName();
										    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
										    			pfx3 = "../enumerations/" + tsconcept.getPrefix();
										    		} else {
										    			pfx3 = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
										    		}				    		
										    		prefixName3 = tsconcept.getPrefix() + "_" + name3;
										    	}
									    	} else {
									    		tax_enum te;
										    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre3.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
										    	if(Objects.isNull(tconcept)) {
										    		te = taxEnumList.stream().filter(tcl -> pre3.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
											    	if(Objects.isNull(te)) {
											    		continue;
											    	} else {
											    		name3 = te.getName();
											    		pfx3 = "../enumerations/" + te.getPrefix();
											    		prefixName3 = te.getPrefix() + "_" + name3;
											    	}
										    	} else {
										    		name3 = tconcept.getName();
										    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
										    			pfx3 = "../enumerations/" + tconcept.getPrefix();
										    		} else {
										    			pfx3 = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
										    		}				    		
										    		prefixName3 = tconcept.getPrefix() + "_" + name3;
										    	}
									    	}
									    	
									    	String labelName3 = "label_" + name3;
									    	String labelText3 = StringEscapeUtils.escapeXml(pre3.getLabel_ms_form());
									    	
									    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre3.getName())).findAny().orElse(null) == null) {
									    		tempArray.add(pre3.getName());
									    		writer.newLine();
											    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx3 + "_" + dtString + ".xsd#" + prefixName3 + "\" xlink:label=\"" + name3 + "\" xlink:title=\"" + name3 + "\"/>");
											    writer.newLine();
											    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName3 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName3 + "\" xml:lang=\"en\" id=\"" + labelName3 + "\">" + labelText3 + "</link:label>");
											    writer.newLine();
											    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name3 + "\" xlink:to=\"" + labelName3 + "\" xlink:title=\"label: " + name3 + " to " + labelName3 + "\"/>");
											    
									    	} else {
									    		writer.newLine();
											    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName3 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName3 + "\" xml:lang=\"en\" id=\"" + labelName3 + "\">" + labelText3 + "</link:label>");
											    writer.newLine();
											    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name3 + "\" xlink:to=\"" + labelName3 + "\" xlink:title=\"label: " + name3 + " to " + labelName3 + "\"/>");
											    
									    	}
									    	
									    	if(Strings.isNotBlank(pre3.getLabel_ms_document())) {
									    		if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre3.getName())).findAny().orElse(null) == null) {
										    		tempArray2.add(pre3.getName());
										    		count3++;
											    	String labelDoc3 = "label_" + pre3.getName() + "_" + count3;
											    	String docText3 = StringEscapeUtils.escapeXml(pre3.getLabel_ms_document());
											    	writer.newLine();
												    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc3 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc3 + "\" xml:lang=\"en\" id=\"" + labelDoc3 + "\">" + docText3 + "</link:label>");
												    writer.newLine();
												    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name3 + "\" xlink:to=\"" + labelDoc3 + "\" xlink:title=\"label: " + name3 + " to " + labelDoc3 + "\"/>");
												    
										    	}
									    		
										    }
								    		
								    	}
									    if(!pre3.getChildren().isEmpty()) {
									    	List<presentation_detail> preList4 = pre3.getChildren();
									    	int count4 = 0;
										    for(presentation_detail pre4 : preList4) {
										    	
										    	if(Strings.isNotBlank(pre4.getLabel_ms_form())) {
										    		String prefixName4 = pre4.getPrefix() + "_" + pre4.getName();
										    		String pfx4 = "lhdnm-cor";
										    		String name4 = pre4.getName();
										    		if(pre4.getPrefix().contains("lhdnm-sme")) {
										    			tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre4.getName())).findFirst().orElse(null);
												    	if(Objects.isNull(tsconcept)) {
												    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre4.getName())).findFirst().orElse(null);
													    	if(Objects.isNull(tse)) {
													    		continue;
													    	} else {
													    		name4 = tse.getName();
													    		pfx4 = "../enumerations/" + tse.getPrefix();
													    		prefixName4 = tse.getPrefix() + "_" + name4;
													    	}
												    	} else {
												    		name4 = tsconcept.getName();
												    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
												    			pfx4 = "../enumerations/" + tsconcept.getPrefix();
												    		} else {
												    			pfx4 = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
												    		}				    		
												    		prefixName4 = tsconcept.getPrefix() + "_" + name4;
												    	}
											    	} else {
											    		tax_enum te;
												    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre4.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
												    	if(Objects.isNull(tconcept)) {
												    		te = taxEnumList.stream().filter(tcl -> pre4.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
													    	if(Objects.isNull(te)) {
													    		continue;
													    	} else {
													    		name4 = te.getName();
													    		pfx4 = "../enumerations/" + te.getPrefix();
													    		prefixName4 = te.getPrefix() + "_" + name4;
													    	}
												    	} else {
												    		name4 = tconcept.getName();
												    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
												    			pfx4 = "../enumerations/" + tconcept.getPrefix();
												    		} else {
												    			pfx4 = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
												    		}				    		
												    		prefixName4 = tconcept.getPrefix() + "_" + name4;
												    	}
											    	}
											    	
											    	String labelName4 = "label_" + name4;
											    	String labelText4 = StringEscapeUtils.escapeXml(pre4.getLabel_ms_form());
											    	
											    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre4.getName())).findAny().orElse(null) == null) {
											    		tempArray.add(pre4.getName());
											    		writer.newLine();
													    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx4 + "_" + dtString + ".xsd#" + prefixName4 + "\" xlink:label=\"" + name4 + "\" xlink:title=\"" + name4 + "\"/>");
													    writer.newLine();
													    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName4 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName4 + "\" xml:lang=\"en\" id=\"" + labelName4 + "\">" + labelText4 + "</link:label>");
													    writer.newLine();
													    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name4 + "\" xlink:to=\"" + labelName4 + "\" xlink:title=\"label: " + name4 + " to " + labelName4 + "\"/>");
													    
											    	} else {
											    		writer.newLine();
													    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName4 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName4 + "\" xml:lang=\"en\" id=\"" + labelName4 + "\">" + labelText4 + "</link:label>");
													    writer.newLine();
													    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name4 + "\" xlink:to=\"" + labelName4 + "\" xlink:title=\"label: " + name4 + " to " + labelName4 + "\"/>");
													    
											    	}
											    	
											    	if(Strings.isNotBlank(pre4.getLabel_ms_document())) {
											    		if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre4.getName())).findAny().orElse(null) == null) {
												    		tempArray2.add(pre4.getName());
												    		count4++;
													    	String labelDoc4 = "label_" + pre4.getName() + "_" + count4;
													    	String docText4 = StringEscapeUtils.escapeXml(pre4.getLabel_ms_document());
													    	writer.newLine();
														    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc4 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc4 + "\" xml:lang=\"en\" id=\"" + labelDoc4 + "\">" + docText4 + "</link:label>");
														    writer.newLine();
														    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name4 + "\" xlink:to=\"" + labelDoc4 + "\" xlink:title=\"label: " + name4 + " to " + labelDoc4 + "\"/>");
														    
												    	}
											    		
												    }
										    		
										    	}
										    	
										    	if(!pre4.getChildren().isEmpty()) {
											    	List<presentation_detail> preList5 = pre4.getChildren();
											    	int count5 = 0;
												    for(presentation_detail pre5 : preList5) {
												    	
												    	if(Strings.isNotBlank(pre5.getLabel_ms_form())) {
												    		String prefixName5 = pre5.getPrefix() + "_" + pre5.getName();
												    		String pfx5 = "lhdnm-cor";
												    		String name5 = pre5.getName();
												    		if(pre5.getPrefix().contains("lhdnm-sme")) {
												    			tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre5.getName())).findFirst().orElse(null);
														    	if(Objects.isNull(tsconcept)) {
														    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre5.getName())).findFirst().orElse(null);
															    	if(Objects.isNull(tse)) {
															    		continue;
															    	} else {
															    		name5 = tse.getName();
															    		pfx5 = "../enumerations/" + tse.getPrefix();
															    		prefixName5 = tse.getPrefix() + "_" + name5;
															    	}
														    	} else {
														    		name5 = tsconcept.getName();
														    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
														    			pfx5 = "../enumerations/" + tsconcept.getPrefix();
														    		} else {
														    			pfx5 = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
														    		}				    		
														    		prefixName5 = tsconcept.getPrefix() + "_" + name5;
														    	}
													    	} else {
													    		tax_enum te;
														    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre5.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
														    	if(Objects.isNull(tconcept)) {
														    		te = taxEnumList.stream().filter(tcl -> pre5.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
															    	if(Objects.isNull(te)) {
															    		continue;
															    	} else {
															    		name5 = te.getName();
															    		pfx5 = "../enumerations/" + te.getPrefix();
															    		prefixName5 = te.getPrefix() + "_" + name5;
															    	}
														    	} else {
														    		name5 = tconcept.getName();
														    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
														    			pfx5 = "../enumerations/" + tconcept.getPrefix();
														    		} else {
														    			pfx5 = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
														    		}				    		
														    		prefixName5 = tconcept.getPrefix() + "_" + name5;
														    	}
													    	}
													    	
													    	String labelName5 = "label_" + name5;
													    	String labelText5 = StringEscapeUtils.escapeXml(pre5.getLabel_ms_form());
													    	
													    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre5.getName())).findAny().orElse(null) == null) {
													    		tempArray.add(pre5.getName());
													    		writer.newLine();
															    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx5 + "_" + dtString + ".xsd#" + prefixName5 + "\" xlink:label=\"" + name5 + "\" xlink:title=\"" + name5 + "\"/>");
															    writer.newLine();
															    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName5 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName5 + "\" xml:lang=\"en\" id=\"" + labelName5 + "\">" + labelText5 + "</link:label>");
															    writer.newLine();
															    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name5 + "\" xlink:to=\"" + labelName5 + "\" xlink:title=\"label: " + name5 + " to " + labelName5 + "\"/>");
															    
													    	} else {
													    		writer.newLine();
															    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName5 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName5 + "\" xml:lang=\"en\" id=\"" + labelName5 + "\">" + labelText5 + "</link:label>");
															    writer.newLine();
															    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name5 + "\" xlink:to=\"" + labelName5 + "\" xlink:title=\"label: " + name5 + " to " + labelName5 + "\"/>");
															    
													    	}
													    	
													    	if(Strings.isNotBlank(pre5.getLabel_ms_document())) {
													    		if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre5.getName())).findAny().orElse(null) == null) {
														    		tempArray2.add(pre5.getName());
														    		count5++;
															    	String labelDoc5 = "label_" + pre5.getName() + "_" + count5;
															    	String docText5 = StringEscapeUtils.escapeXml(pre5.getLabel_ms_document());
															    	writer.newLine();
																    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc5 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc5 + "\" xml:lang=\"en\" id=\"" + labelDoc5 + "\">" + docText5 + "</link:label>");
																    writer.newLine();
																    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name5 + "\" xlink:to=\"" + labelDoc5 + "\" xlink:title=\"label: " + name5 + " to " + labelDoc5 + "\"/>");
																    
														    	}
													    		
														    }
												    		
												    	}
												    	
												    	if(!pre5.getChildren().isEmpty()) {
													    	List<presentation_detail> preList6 = pre5.getChildren();
													    	int count6 = 0;
														    for(presentation_detail pre6 : preList6) {
														    	
														    	if(Strings.isNotBlank(pre6.getLabel_ms_form())) {
														    		String prefixName6 = pre6.getPrefix() + "_" + pre6.getName();
														    		String pfx6 = "lhdnm-cor";
														    		String name6 = pre6.getName();
														    		if(pre6.getPrefix().contains("lhdnm-sme")) {
														    			tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre6.getName())).findFirst().orElse(null);
																    	if(Objects.isNull(tsconcept)) {
																    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre6.getName())).findFirst().orElse(null);
																	    	if(Objects.isNull(tse)) {
																	    		continue;
																	    	} else {
																	    		name6 = tse.getName();
																	    		pfx6 = "../enumerations/" + tse.getPrefix();
																	    		prefixName6 = tse.getPrefix() + "_" + name6;
																	    	}
																    	} else {
																    		name6 = tsconcept.getName();
																    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
																    			pfx6 = "../enumerations/" + tsconcept.getPrefix();
																    		} else {
																    			pfx6 = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
																    		}				    		
																    		prefixName6 = tsconcept.getPrefix() + "_" + name6;
																    	}
															    	} else {
															    		tax_enum te;
																    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre6.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																    	if(Objects.isNull(tconcept)) {
																    		te = taxEnumList.stream().filter(tcl -> pre6.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																	    	if(Objects.isNull(te)) {
																	    		continue;
																	    	} else {
																	    		name6 = te.getName();
																	    		pfx6 = "../enumerations/" + te.getPrefix();
																	    		prefixName6 = te.getPrefix() + "_" + name6;
																	    	}
																    	} else {
																    		name6 = tconcept.getName();
																    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
																    			pfx6 = "../enumerations/" + tconcept.getPrefix();
																    		} else {
																    			pfx6 = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
																    		}				    		
																    		prefixName6 = tconcept.getPrefix() + "_" + name6;
																    	}
															    	}
															    	
															    	String labelName6 = "label_" + name6;
															    	String labelText6 = StringEscapeUtils.escapeXml(pre6.getLabel_ms_form());
															    	
															    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre6.getName())).findAny().orElse(null) == null) {
															    		tempArray.add(pre6.getName());
															    		writer.newLine();
																	    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx6 + "_" + dtString + ".xsd#" + prefixName6 + "\" xlink:label=\"" + name6 + "\" xlink:title=\"" + name6 + "\"/>");
																	    writer.newLine();
																	    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName6 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName6 + "\" xml:lang=\"en\" id=\"" + labelName6 + "\">" + labelText6 + "</link:label>");
																	    writer.newLine();
																	    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name6 + "\" xlink:to=\"" + labelName6 + "\" xlink:title=\"label: " + name6 + " to " + labelName6 + "\"/>");
																	    
															    	} else {
															    		writer.newLine();
																	    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName6 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName6 + "\" xml:lang=\"en\" id=\"" + labelName6 + "\">" + labelText6 + "</link:label>");
																	    writer.newLine();
																	    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name6 + "\" xlink:to=\"" + labelName6 + "\" xlink:title=\"label: " + name6 + " to " + labelName6 + "\"/>");
																	    
															    	}
															    	
															    	if(Strings.isNotBlank(pre6.getLabel_ms_document())) {
															    		if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre6.getName())).findAny().orElse(null) == null) {
																    		tempArray2.add(pre6.getName());
																    		count6++;
																	    	String labelDoc6 = "label_" + pre6.getName() + "_" + count6;
																	    	String docText6 = StringEscapeUtils.escapeXml(pre6.getLabel_ms_document());
																	    	writer.newLine();
																		    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc6 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc6 + "\" xml:lang=\"en\" id=\"" + labelDoc6 + "\">" + docText6 + "</link:label>");
																		    writer.newLine();
																		    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name6 + "\" xlink:to=\"" + labelDoc6 + "\" xlink:title=\"label: " + name6 + " to " + labelDoc6 + "\"/>");
																		    
																    	}
															    		
																    }
														    		
														    	}
														    	
														    	if(!pre6.getChildren().isEmpty()) {
															    	List<presentation_detail> preList7 = pre6.getChildren();
															    	int count7 = 0;
																    for(presentation_detail pre7 : preList7) {
																    	
																    	if(Strings.isNotBlank(pre7.getLabel_ms_form())) {
																    		String prefixName7 = pre7.getPrefix() + "_" + pre7.getName();
																    		String pfx7 = "lhdnm-cor";
																    		String name7 = pre7.getName();
																    		if(pre7.getPrefix().contains("lhdnm-sme")) {
																    			tax_sme_concept tsconcept = tsconceptList.stream().filter(ts -> ts.getName().equalsIgnoreCase(pre7.getName())).findFirst().orElse(null);
																		    	if(Objects.isNull(tsconcept)) {
																		    		tax_sme_enum tse = tseList.stream().filter(tss -> tss.getName().equalsIgnoreCase(pre7.getName())).findFirst().orElse(null);
																			    	if(Objects.isNull(tse)) {
																			    		continue;
																			    	} else {
																			    		name7 = tse.getName();
																			    		pfx7 = "../enumerations/" + tse.getPrefix();
																			    		prefixName7 = tse.getPrefix() + "_" + name7;
																			    	}
																		    	} else {
																		    		name7 = tsconcept.getName();
																		    		if(tsconcept.getPrefix().equalsIgnoreCase("lhdnm-sme-enum")) {
																		    			pfx7 = "../enumerations/" + tsconcept.getPrefix();
																		    		} else {
																		    			pfx7 = "../../../../def/ic/" + tsconcept.getPrefix() + "-cor";
																		    		}				    		
																		    		prefixName7 = tsconcept.getPrefix() + "_" + name7;
																		    	}
																	    	} else {
																	    		tax_enum te;
																		    	tax_concept tconcept = taxConceptList.stream().filter(tcl -> pre7.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																		    	if(Objects.isNull(tconcept)) {
																		    		te = taxEnumList.stream().filter(tcl -> pre7.getName().equalsIgnoreCase(tcl.getName())).findAny().orElse(null);
																			    	if(Objects.isNull(te)) {
																			    		continue;
																			    	} else {
																			    		name7 = te.getName();
																			    		pfx7 = "../enumerations/" + te.getPrefix();
																			    		prefixName7 = te.getPrefix() + "_" + name7;
																			    	}
																		    	} else {
																		    		name7 = tconcept.getName();
																		    		if(tconcept.getPrefix().equalsIgnoreCase("lhdnm-enum")) {
																		    			pfx7 = "../enumerations/" + tconcept.getPrefix();
																		    		} else {
																		    			pfx7 = "../../../../def/ic/" + tconcept.getPrefix() + "-cor";
																		    		}				    		
																		    		prefixName7 = tconcept.getPrefix() + "_" + name7;
																		    	}
																	    	}
																	    	
																	    	String labelName7 = "label_" + name7;
																	    	String labelText7 = StringEscapeUtils.escapeXml(pre7.getLabel_ms_form());
																	    	
																	    	if(tempArray.stream().filter(x -> x.equalsIgnoreCase(pre7.getName())).findAny().orElse(null) == null) {
																	    		tempArray.add(pre7.getName());
																	    		writer.newLine();
																			    writer.write("<link:loc xlink:type=\"locator\" xlink:href=\"" + pfx7 + "_" + dtString + ".xsd#" + prefixName7 + "\" xlink:label=\"" + name7 + "\" xlink:title=\"" + name7 + "\"/>");
																			    writer.newLine();
																			    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName7 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName7 + "\" xml:lang=\"en\" id=\"" + labelName7 + "\">" + labelText7 + "</link:label>");
																			    writer.newLine();
																			    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name7 + "\" xlink:to=\"" + labelName7 + "\" xlink:title=\"label: " + name7 + " to " + labelName7 + "\"/>");
																			    
																	    	} else {
																	    		writer.newLine();
																			    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelName7 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName7 + "\" xml:lang=\"en\" id=\"" + labelName7 + "\">" + labelText7 + "</link:label>");
																			    writer.newLine();
																			    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name7 + "\" xlink:to=\"" + labelName7 + "\" xlink:title=\"label: " + name7 + " to " + labelName7 + "\"/>");
																			    
																	    	}
																	    	
																	    	if(Strings.isNotBlank(pre7.getLabel_ms_document())) {
																	    		if(tempArray2.stream().filter(x -> x.equalsIgnoreCase(pre7.getName())).findAny().orElse(null) == null) {
																		    		tempArray2.add(pre7.getName());
																		    		count7++;
																			    	String labelDoc7 = "label_" + pre7.getName() + "_" + count7;
																			    	String docText7 = StringEscapeUtils.escapeXml(pre7.getLabel_ms_document());
																			    	writer.newLine();
																				    writer.write("<link:label xlink:type=\"resource\" xlink:label=\"" + labelDoc7 + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelDoc7 + "\" xml:lang=\"en\" id=\"" + labelDoc7 + "\">" + docText7 + "</link:label>");
																				    writer.newLine();
																				    writer.write("<link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + name7 + "\" xlink:to=\"" + labelDoc7 + "\" xlink:title=\"label: " + name7 + " to " + labelDoc7 + "\"/>");
																				    
																		    	}
																	    		
																		    }
																    		
																    	}
																    	
																    }
															    }
														    	
														    }
													    }
												    	
												    }
											    }
										    	
										    }
									    }
								    }
							    }
							    
						    }
					    }
				    }
			    	
			    }
			    
			    
			    writer.newLine();
			    writer.write("  </link:labelLink>");
			    writer.newLine();
			    writer.write("</link:linkbase>");
				
				isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
		return isCompleted;
	}

	public boolean writeFormulaDef(String path, String dtString, Long id, String type) {
		boolean isCompleted = false;
		String regex = "^\\s+";
		System.out.println("\n\nModal id: " + id);
		List<linkbase_form> linkbaseList1 = findByModalId(id);
		List<linkbase_form> linkbaseList = null;
		if(type.equalsIgnoreCase("lhdnm-sme")) {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		} else {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		}
		
		System.out.println("\n\nModal List: " + linkbaseList.size());
		for(linkbase_form form : linkbaseList) {
			String fname = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
				fname = fname.replaceAll(" ", "-");
//				fname = fname.replaceAll(".", "");
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/frm_lhdnm-" + fname + "_" + dtString + ".xml"))) {
				
				writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd http://xbrl.org/2008/generic http://www.xbrl.org/2008/generic-link.xsd http://xbrl.org/2008/formula http://www.xbrl.org/2008/formula.xsd http://xbrl.org/2008/label http://www.xbrl.org/2008/generic-label.xsd http://xbrl.org/2008/reference http://www.xbrl.org/2008/generic-reference.xsd http://xbrl.org/2008/assertion/consistency http://www.xbrl.org/2008/consistency-assertion.xsd http://xbrl.org/2008/assertion/existence http://www.xbrl.org/2008/existence-assertion.xsd http://xbrl.org/2008/assertion/value http://www.xbrl.org/2008/value-assertion.xsd http://xbrl.org/2008/filter/concept http://www.xbrl.org/2008/concept-filter.xsd http://xbrl.org/2010/message http://www.xbrl.org/2010/generic-message.xsd http://xbrl.org/2010/message/validation http://www.xbrl.org/2010/validation-message.xsd http://xbrl.org/2008/filter/dimension http://www.xbrl.org/2008/dimension-filter.xsd\" xmlns:ca=\"http://xbrl.org/2008/assertion/consistency\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:fn=\"http://www.w3.org/2005/xpath-functions\" xmlns:variable=\"http://xbrl.org/2008/variable\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:xfi=\"http://www.xbrl.org/2008/function/instance\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:cf=\"http://xbrl.org/2008/filter/concept\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:valm=\"http://xbrl.org/2010/message/validation\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:ea=\"http://xbrl.org/2008/assertion/existence\" xmlns:df=\"http://xbrl.org/2008/filter/dimension\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:validation=\"http://xbrl.org/2008/validation\" xmlns:formula=\"http://xbrl.org/2008/formula\" xmlns:msg=\"http://xbrl.org/2010/message\" xmlns:va=\"http://xbrl.org/2008/assertion/value\" xmlns:label=\"http://xbrl.org/2008/label\">");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"" + form.getLinkrole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/link\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-link.xsd#standard-link-role\"/>");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2010/role/message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/generic-message.xsd#standard-message\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/variable-filter\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/variable.xsd#variable-filter\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/variable-set\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/variable.xsd#variable-set\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/variable-set-filter\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/variable.xsd#variable-set-filter\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/boolean-filter\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/boolean-filter.xsd#boolean-filter\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2010/assertion-unsatisfied-message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/validation-message.xsd#assertion-unsatisfied-message\"/>");
			    writer.newLine();
			    writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"" + form.getLinkrole() + "\">");
			    
			    int vaCount = 0;
			    int fvCount = 0;
			    for(presentation_formula pre : form.getFormulaList()) {
			    	vaCount++;
			    	String formatted = String.format("%03d", vaCount);
			    	formatted = fname + "_va" + formatted;
			    	String vaLabel = "valueAssertion_" + vaCount;
			    	pre.setValue_assertion(formatted);
			    	pre.setValue_assertion_label(vaLabel);
			    	pfService.saveSingle(pre);
//			    	formatted = form.getName() + "_" + formatted;
			    	
			    	String[] variableList = pre.getTestItem().split("\\|", -1);
			    	String formulaExp = pre.getTest_expression();
//			    	System.out.println("\n\nVariable List: " + variableList.length);
			    	System.out.println("formulaExp: " + formulaExp);
			    	if(variableList.length > 0) {
			    		for(String va : variableList) {
			    			if(Strings.isNotBlank(va) && !va.matches("^[0-9]+$")) {
			    				System.out.println("VA value: " + va);
			    				String vs = va.replaceAll("\\*", "");
			    				String vaTrim = vs.replaceAll(regex, "");
				    			String toReplace = "$" + vs.trim();
				    			System.out.println("VA toReplace: " + toReplace);
				    			formulaExp = formulaExp.replaceAll(vaTrim, "\\$" + vaTrim);
			    			}			    			
			    		}
			    		System.out.println("formulaExp: " + formulaExp);
			    	}
			    				    	
			    	writer.newLine();
				    writer.write("    <va:valueAssertion xlink:type=\"resource\" xlink:label=\"" + vaLabel + "\" id=\"" + formatted + "\" test=\"" + formulaExp + "\" aspectModel=\"dimensional\" implicitFiltering=\"true\"/>");
				    				    
				    if(variableList.length > 0) {
				    	
				    	for(String va : variableList) {
				    		String vs = va.replaceAll("\\*", "");
				    		String tempVa = vs.trim();
				    		if(!tempVa.isEmpty() && !tempVa.equalsIgnoreCase("0")) {
				    			presentation_detail pd = new presentation_detail();
				    			String qname = "";
//					    		System.out.println("\n VA value 2: " + va);
					    		if(tempVa.contains("_")) {
					    			String frmName = tempVa.substring(0, tempVa.indexOf("_"));
//					    			System.out.println("\n frmName: " + frmName);
					    			String fieldName =  tempVa.substring(tempVa.indexOf("_") + 1, tempVa.length());
					    			System.out.println("\n fieldName: " + fieldName);
//					    			linkbase_form form2 = linkbaseList.stream().filter(lform -> frmName.equalsIgnoreCase(lform.getNamespace())).findAny().orElse(null);
					    			linkbase_form form2 = getByNamespace(frmName, id);
					    			if(form2 != null) {
					    				qname = findPresentationDetail(form2, fieldName);
					    				
//					    				linkbase_form getForm = getSingle(form2.getFrm_id());
//					    				pd = getForm.getPresentationList().stream()
//							    				  .filter(pdetail -> fieldName.equalsIgnoreCase(pdetail.getDataItem()))
//							    				  .findAny()
//							    				  .orElse(null);
//					    				pd = getForm.getPresentationList().stream().flatMap(e -> e.getChildren().stream()).filter(pdetail -> fieldName.equalsIgnoreCase(pdetail.getDataItem())).findAny().orElse(null);
					    			}
					    			
					    		} else {
					    			qname = findPresentationDetail(form, tempVa);
//					    			pd = form.getPresentationList().stream()
//						    				  .filter(pdetail -> va.equalsIgnoreCase(pdetail.getDataItem()))
//						    				  .findAny()
//						    				  .orElse(null);
//					    			pd = form.getPresentationList().stream().flatMap(e -> e.getChildren().stream()).filter(pdetail -> va.equalsIgnoreCase(pdetail.getDataItem())).findAny().orElse(null);
					    		}
					    		
					    		if(pd != null) {
					    			fvCount++;
						    		String faLabel = "factVariable_" + fvCount;
						    		String cnLabel = "conceptName_" + fvCount;
						    		
//					    			String qname = pd.getPrefix() + ":" + pd.getName();
					    			writer.newLine();
								    writer.write("    <variable:factVariable xlink:type=\"resource\" xlink:label=\"" + faLabel + "\" id=\"" + faLabel + "\" bindAsSequence=\"false\" fallbackValue=\"0\"/>");
							    	writer.newLine();
								    writer.write("    <variable:variableArc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/variable-set\" xlink:from=\"" + vaLabel + "\" xlink:to=\"" + faLabel + "\" order=\"" + fvCount + ".0\" name=\"" + tempVa + "\"/>");
								    writer.newLine();
								    writer.write("    <cf:conceptName xlink:type=\"resource\" xlink:label=\"" + cnLabel + "\" id=\"" + cnLabel + "\">");
								    writer.newLine();
								    writer.write("      <cf:concept>");
								    writer.newLine();
								    writer.write("        <cf:qname>" + qname + "</cf:qname>");
								    writer.newLine();
								    writer.write("      </cf:concept>");
								    writer.newLine();
								    writer.write("    </cf:conceptName>");
								    writer.newLine();
								    writer.write("    <variable:variableFilterArc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/variable-filter\" xlink:from=\"" + faLabel + "\" xlink:to=\"" + cnLabel + "\" order=\"1.0\" cover=\"true\" complement=\"false\"/>");
					    		}
				    		}
				    	}
				    	
				    }
				    
			    }
				
			    writer.newLine();
			    writer.write("  </gen:link>");
				writer.newLine();
			    writer.write("</link:linkbase>");
			    
			    isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
		
		return isCompleted;
	}
	
	public boolean writeFormulaSmeDef(String path, String dtString, Long id, String type) {
		boolean isCompleted = false;
		System.out.println("\n\nModal id: " + id);
		List<linkbase_form> linkbaseList1 = findByModalId(id);
		List<linkbase_form> linkbaseList = null;
		if(type.equalsIgnoreCase("lhdnm-sme")) {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		} else {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		}
		
		System.out.println("\n\nModal List: " + linkbaseList.size());
		for(linkbase_form form : linkbaseList) {
			String fname = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
				fname = fname.replaceAll(" ", "-");
//				fname = fname.replaceAll(".", "");
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/frm_lhdnm-" + fname + "_" + dtString + ".xml"))) {
				
				writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd http://xbrl.org/2008/generic http://www.xbrl.org/2008/generic-link.xsd http://xbrl.org/2008/formula http://www.xbrl.org/2008/formula.xsd http://xbrl.org/2008/label http://www.xbrl.org/2008/generic-label.xsd http://xbrl.org/2008/reference http://www.xbrl.org/2008/generic-reference.xsd http://xbrl.org/2008/assertion/consistency http://www.xbrl.org/2008/consistency-assertion.xsd http://xbrl.org/2008/assertion/existence http://www.xbrl.org/2008/existence-assertion.xsd http://xbrl.org/2008/assertion/value http://www.xbrl.org/2008/value-assertion.xsd http://xbrl.org/2008/filter/concept http://www.xbrl.org/2008/concept-filter.xsd http://xbrl.org/2010/message http://www.xbrl.org/2010/generic-message.xsd http://xbrl.org/2010/message/validation http://www.xbrl.org/2010/validation-message.xsd http://xbrl.org/2008/filter/match http://www.xbrl.org/2008/match-filter.xsd http://xbrl.org/2008/filter/period http://www.xbrl.org/2008/period-filter.xsd\" xmlns:ca=\"http://xbrl.org/2008/assertion/consistency\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:fn=\"http://www.w3.org/2005/xpath-functions\" xmlns:variable=\"http://xbrl.org/2008/variable\" xmlns:xfi=\"http://www.xbrl.org/2008/function/instance\" xmlns:cf=\"http://xbrl.org/2008/filter/concept\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:valm=\"http://xbrl.org/2010/message/validation\" xmlns:mf=\"http://xbrl.org/2008/filter/match\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:ea=\"http://xbrl.org/2008/assertion/existence\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:validation=\"http://xbrl.org/2008/validation\" xmlns:pf=\"http://xbrl.org/2008/filter/period\" xmlns:lhdnm-sme=\"http://www.hasil.gov.my/xbrl/" + dtString + "/lhdnm-sme\" xmlns:formula=\"http://xbrl.org/2008/formula\" xmlns:msg=\"http://xbrl.org/2010/message\" xmlns:va=\"http://xbrl.org/2008/assertion/value\" xmlns:label=\"http://xbrl.org/2008/label\">");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"" + form.getLinkrole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\"/>");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/link\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-link.xsd#standard-link-role\"/>");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2010/role/message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/generic-message.xsd#standard-message\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/variable-filter\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/variable.xsd#variable-filter\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/variable-set\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/variable.xsd#variable-set\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/variable-set-filter\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/variable.xsd#variable-set-filter\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/boolean-filter\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/boolean-filter.xsd#boolean-filter\"/>");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2010/assertion-unsatisfied-message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/validation-message.xsd#assertion-unsatisfied-message\"/>");
			    writer.newLine();
			    writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"" + form.getLinkrole() + "\">");
			    
			    int vaCount = 0;
			    int fvCount = 0;
			    for(presentation_formula pre : form.getFormulaList()) {
			    	vaCount++;
			    	String formatted = String.format("%03d", vaCount);
			    	formatted = fname + "_va" + formatted;
			    	String vaLabel = "valueAssertion_" + vaCount;
			    	pre.setValue_assertion(formatted);
			    	pre.setValue_assertion_label(vaLabel);
			    	pfService.saveSingle(pre);
//			    	formatted = form.getName() + "_" + formatted;
			    	
			    	String[] variableList = pre.getTestItem().split("\\|", -1);
			    	String formulaExp = pre.getTest_expression();
//			    	System.out.println("\n\nVariable List: " + variableList.length);
			    	System.out.println("formulaExp: " + formulaExp);
			    	if(variableList.length > 0) {
			    		for(String va : variableList) {
			    			if(Strings.isNotBlank(va) && !va.matches("^[0-9]+$")) {
			    				System.out.println("VA value: " + va);
				    			String toReplace = "$" + va.trim();
				    			System.out.println("VA toReplace: " + toReplace);
				    			formulaExp = formulaExp.replaceAll(va, "\\$" + va);
			    			}			    			
			    		}
			    		System.out.println("formulaExp: " + formulaExp);
			    	}
			    				    	
			    	writer.newLine();
				    writer.write("    <va:valueAssertion xlink:type=\"resource\" xlink:label=\"" + vaLabel + "\" id=\"" + formatted + "\" test=\"" + formulaExp + "\" aspectModel=\"dimensional\" implicitFiltering=\"true\"/>");
				    				    
				    if(variableList.length > 0) {
				    	
				    	for(String va : variableList) {
				    		String tempVa = va.trim();
				    		if(!tempVa.isEmpty() && !tempVa.equalsIgnoreCase("0")) {
				    			presentation_detail pd = new presentation_detail();
				    			String qname = "";
//					    		System.out.println("\n VA value 2: " + va);
					    		if(tempVa.contains("_")) {
					    			String frmName = tempVa.substring(0, tempVa.indexOf("_"));
//					    			System.out.println("\n frmName: " + frmName);
					    			String fieldName =  tempVa.substring(tempVa.indexOf("_") + 1, tempVa.length());
					    			System.out.println("\n fieldName: " + fieldName);
//					    			linkbase_form form2 = linkbaseList.stream().filter(lform -> frmName.equalsIgnoreCase(lform.getNamespace())).findAny().orElse(null);
					    			linkbase_form form2 = getByNamespace(frmName, id);
					    			if(form2 != null) {
					    				qname = findPresentationDetail(form2, fieldName);
					    				
//					    				linkbase_form getForm = getSingle(form2.getFrm_id());
//					    				pd = getForm.getPresentationList().stream()
//							    				  .filter(pdetail -> fieldName.equalsIgnoreCase(pdetail.getDataItem()))
//							    				  .findAny()
//							    				  .orElse(null);
//					    				pd = getForm.getPresentationList().stream().flatMap(e -> e.getChildren().stream()).filter(pdetail -> fieldName.equalsIgnoreCase(pdetail.getDataItem())).findAny().orElse(null);
					    			}
					    			
					    		} else {
					    			qname = findPresentationDetail(form, tempVa);
//					    			pd = form.getPresentationList().stream()
//						    				  .filter(pdetail -> va.equalsIgnoreCase(pdetail.getDataItem()))
//						    				  .findAny()
//						    				  .orElse(null);
//					    			pd = form.getPresentationList().stream().flatMap(e -> e.getChildren().stream()).filter(pdetail -> va.equalsIgnoreCase(pdetail.getDataItem())).findAny().orElse(null);
					    		}
					    		
					    		if(pd != null) {
					    			fvCount++;
						    		String faLabel = "factVariable_" + fvCount;
						    		String cnLabel = "conceptName_" + fvCount;
						    		
//					    			String qname = pd.getPrefix() + ":" + pd.getName();
					    			writer.newLine();
								    writer.write("    <variable:factVariable xlink:type=\"resource\" xlink:label=\"" + faLabel + "\" id=\"" + faLabel + "\" bindAsSequence=\"false\" fallbackValue=\"0\"/>");
							    	writer.newLine();
								    writer.write("    <variable:variableArc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/variable-set\" xlink:from=\"" + vaLabel + "\" xlink:to=\"" + faLabel + "\" order=\"" + fvCount + ".0\" name=\"" + tempVa + "\"/>");
								    writer.newLine();
								    writer.write("    <cf:conceptName xlink:type=\"resource\" xlink:label=\"" + cnLabel + "\" id=\"" + cnLabel + "\">");
								    writer.newLine();
								    writer.write("      <cf:concept>");
								    writer.newLine();
								    writer.write("        <cf:qname>" + qname + "</cf:qname>");
								    writer.newLine();
								    writer.write("      </cf:concept>");
								    writer.newLine();
								    writer.write("    </cf:conceptName>");
								    writer.newLine();
								    writer.write("    <variable:variableFilterArc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/variable-filter\" xlink:from=\"" + faLabel + "\" xlink:to=\"" + cnLabel + "\" order=\"1.0\" cover=\"true\" complement=\"false\"/>");
					    		}
				    		}
				    	}
				    	
				    }
				    
			    }
				
			    writer.newLine();
			    writer.write("  </gen:link>");
				writer.newLine();
			    writer.write("</link:linkbase>");
			    
			    isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
		
		return isCompleted;
	}
	
	public boolean writeFormulaLabelEn(String path, String dtString, Long id, String type) {
		boolean isCompleted = false;
		List<linkbase_form> linkbaseList1 = findByModalId(id);
		List<linkbase_form> linkbaseList = null;
		if(type.equalsIgnoreCase("lhdnm-sme")) {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		} else {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		}
		
		for(linkbase_form form : linkbaseList) {
			String fname = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
				fname = fname.replaceAll(" ", "-");
//				fname = fname.replaceAll(".", "");
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/frm_lab_lhdnm-" + fname + "-en_" + dtString + ".xml"))) {
				
				writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    if(type.equalsIgnoreCase("lhdnm-sme")) {
			    	writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd http://xbrl.org/2008/generic http://www.xbrl.org/2008/generic-link.xsd http://xbrl.org/2008/formula http://www.xbrl.org/2008/formula.xsd http://xbrl.org/2008/label http://www.xbrl.org/2008/generic-label.xsd http://xbrl.org/2008/reference http://www.xbrl.org/2008/generic-reference.xsd http://xbrl.org/2008/assertion/consistency http://www.xbrl.org/2008/consistency-assertion.xsd http://xbrl.org/2008/assertion/existence http://www.xbrl.org/2008/existence-assertion.xsd http://xbrl.org/2008/assertion/value http://www.xbrl.org/2008/value-assertion.xsd http://xbrl.org/2008/filter/concept http://www.xbrl.org/2008/concept-filter.xsd http://xbrl.org/2010/message http://www.xbrl.org/2010/generic-message.xsd http://xbrl.org/2010/message/validation http://www.xbrl.org/2010/validation-message.xsd http://xbrl.org/2008/filter/dimension http://www.xbrl.org/2008/dimension-filter.xsd http://xbrl.org/2010/filter/aspect-cover http://www.xbrl.org/2010/aspect-cover-filter.xsd\" xmlns:ca=\"http://xbrl.org/2008/assertion/consistency\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:fn=\"http://www.w3.org/2005/xpath-functions\" xmlns:variable=\"http://xbrl.org/2008/variable\" xmlns:xfi=\"http://www.xbrl.org/2008/function/instance\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:cf=\"http://xbrl.org/2008/filter/concept\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:valm=\"http://xbrl.org/2010/message/validation\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:ea=\"http://xbrl.org/2008/assertion/existence\" xmlns:asf=\"http://xbrl.org/2010/filter/aspect-cover\" xmlns:df=\"http://xbrl.org/2008/filter/dimension\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:validation=\"http://xbrl.org/2008/validation\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:formula=\"http://xbrl.org/2008/formula\" xmlns:msg=\"http://xbrl.org/2010/message\" xmlns:va=\"http://xbrl.org/2008/assertion/value\" xmlns:label=\"http://xbrl.org/2008/label\">");
			    } else {
			    	writer.write("<link:linkbase xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:bf=\"http://xbrl.org/2008/filter/boolean\" xmlns:ca=\"http://xbrl.org/2008/assertion/consistency\" xmlns:cf=\"http://xbrl.org/2008/filter/concept\" xmlns:df=\"http://xbrl.org/2008/filter/dimension\" xmlns:ea=\"http://xbrl.org/2008/assertion/existence\" xmlns:fn=\"http://www.w3.org/2005/xpath-functions\" xmlns:formula=\"http://xbrl.org/2008/formula\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:msg=\"http://xbrl.org/2010/message\" xmlns:va=\"http://xbrl.org/2008/assertion/value\" xmlns:validation=\"http://xbrl.org/2008/validation\" xmlns:valm=\"http://xbrl.org/2010/message/validation\" xmlns:variable=\"http://xbrl.org/2008/variable\" xmlns:xfi=\"http://www.xbrl.org/2008/function/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd http://xbrl.org/2008/generic http://www.xbrl.org/2008/generic-link.xsd http://xbrl.org/2008/formula http://www.xbrl.org/2008/formula.xsd http://xbrl.org/2008/label http://www.xbrl.org/2008/generic-label.xsd http://xbrl.org/2008/reference http://www.xbrl.org/2008/generic-reference.xsd http://xbrl.org/2008/assertion/consistency http://www.xbrl.org/2008/consistency-assertion.xsd http://xbrl.org/2008/assertion/existence http://www.xbrl.org/2008/existence-assertion.xsd http://xbrl.org/2008/assertion/value http://www.xbrl.org/2008/value-assertion.xsd http://xbrl.org/2008/filter/concept http://www.xbrl.org/2008/concept-filter.xsd http://xbrl.org/2008/filter/dimension http://www.xbrl.org/2008/dimension-filter.xsd http://xbrl.org/2010/message http://www.xbrl.org/2010/generic-message.xsd http://xbrl.org/2010/message/validation http://www.xbrl.org/2010/validation-message.xsd http://xbrl.org/2008/filter/boolean http://www.xbrl.org/2008/boolean-filter.xsd\">");
			    }
			    
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"" + form.getLinkrole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\" />");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2010/role/message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/generic-message.xsd#standard-message\" />");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2010/assertion-unsatisfied-message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/validation-message.xsd#assertion-unsatisfied-message\" />");
			    writer.newLine();
			    writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"" + form.getLinkrole() + "\">");
			    
			    int vaCount = 0;
			    for(presentation_formula pre : form.getFormulaList()) {
			    	vaCount++;
			    	String labelText = StringEscapeUtils.escapeXml(pre.getError_message_en());
			    	writer.newLine();
				    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"frm_lhdnm-" + fname + "_" + dtString + ".xml#" + pre.getValue_assertion() + "\" xlink:label=\"" + pre.getValue_assertion_label() + "\" />");
				    writer.newLine();
				    writer.write("    <msg:message xlink:type=\"resource\" xlink:label=\"label" + vaCount + "\" xlink:role=\"http://www.xbrl.org/2010/role/message\" xml:lang=\"en\" id=\"label" + vaCount + "\">" + labelText + "</msg:message>");
				    writer.newLine();
				    writer.write("    <gen:arc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2010/assertion-unsatisfied-message\" xlink:from=\"" + pre.getValue_assertion_label() + "\" xlink:to=\"label" + vaCount + "\" order=\"1.0\" />");
			    }
				
				writer.newLine();
			    writer.write("  </gen:link>");
			    writer.newLine();
			    writer.write("</link:linkbase>");
			    
			    isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
		
		return isCompleted;
	}
	
	public boolean writeFormulaSmeLabelEn(String path, String dtString, Long id, String type) {
		boolean isCompleted = false;
		List<linkbase_form> linkbaseList1 = findByModalId(id);
		List<linkbase_form> linkbaseList = null;
		if(type.equalsIgnoreCase("lhdnm-sme")) {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		} else {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		}
		
		for(linkbase_form form : linkbaseList) {
			String fname = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
				fname = fname.replaceAll(" ", "-");
//				fname = fname.replaceAll(".", "");
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/frm_lab_lhdnm-" + fname + "-en_" + dtString + ".xml"))) {
				
				writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    writer.write("<link:linkbase xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:bf=\"http://xbrl.org/2008/filter/boolean\" xmlns:ca=\"http://xbrl.org/2008/assertion/consistency\" xmlns:cf=\"http://xbrl.org/2008/filter/concept\" xmlns:df=\"http://xbrl.org/2008/filter/dimension\" xmlns:ea=\"http://xbrl.org/2008/assertion/existence\" xmlns:fn=\"http://www.w3.org/2005/xpath-functions\" xmlns:formula=\"http://xbrl.org/2008/formula\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:msg=\"http://xbrl.org/2010/message\" xmlns:va=\"http://xbrl.org/2008/assertion/value\" xmlns:validation=\"http://xbrl.org/2008/validation\" xmlns:valm=\"http://xbrl.org/2010/message/validation\" xmlns:variable=\"http://xbrl.org/2008/variable\" xmlns:xfi=\"http://www.xbrl.org/2008/function/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd http://xbrl.org/2008/generic http://www.xbrl.org/2008/generic-link.xsd http://xbrl.org/2008/formula http://www.xbrl.org/2008/formula.xsd http://xbrl.org/2008/label http://www.xbrl.org/2008/generic-label.xsd http://xbrl.org/2008/reference http://www.xbrl.org/2008/generic-reference.xsd http://xbrl.org/2008/assertion/consistency http://www.xbrl.org/2008/consistency-assertion.xsd http://xbrl.org/2008/assertion/existence http://www.xbrl.org/2008/existence-assertion.xsd http://xbrl.org/2008/assertion/value http://www.xbrl.org/2008/value-assertion.xsd http://xbrl.org/2008/filter/concept http://www.xbrl.org/2008/concept-filter.xsd http://xbrl.org/2008/filter/dimension http://www.xbrl.org/2008/dimension-filter.xsd http://xbrl.org/2010/message http://www.xbrl.org/2010/generic-message.xsd http://xbrl.org/2010/message/validation http://www.xbrl.org/2010/validation-message.xsd http://xbrl.org/2008/filter/boolean http://www.xbrl.org/2008/boolean-filter.xsd\">");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"" + form.getLinkrole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\" />");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2010/role/message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/generic-message.xsd#standard-message\" />");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2010/assertion-unsatisfied-message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/validation-message.xsd#assertion-unsatisfied-message\" />");
			    writer.newLine();
			    writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"" + form.getLinkrole() + "\">");
			    
			    int vaCount = 0;
			    for(presentation_formula pre : form.getFormulaList()) {
			    	vaCount++;
			    	String labelText = StringEscapeUtils.escapeXml(pre.getError_message_en());
			    	writer.newLine();
				    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"frm_lhdnm-" + fname + "_" + dtString + ".xml#" + pre.getValue_assertion() + "\" xlink:label=\"" + pre.getValue_assertion_label() + "\" />");
				    writer.newLine();
				    writer.write("    <msg:message xlink:type=\"resource\" xlink:label=\"label" + vaCount + "\" xlink:role=\"http://www.xbrl.org/2010/role/message\" xml:lang=\"en\" id=\"label" + vaCount + "\">" + labelText + "</msg:message>");
				    writer.newLine();
				    writer.write("    <gen:arc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2010/assertion-unsatisfied-message\" xlink:from=\"" + pre.getValue_assertion_label() + "\" xlink:to=\"label" + vaCount + "\" order=\"1.0\" />");
			    }
				
				writer.newLine();
			    writer.write("  </gen:link>");
			    writer.newLine();
			    writer.write("</link:linkbase>");
			    
			    isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
		
		return isCompleted;
	}
	
	public boolean writeFormulaLabelMs(String path, String dtString, Long id, String type) {
		boolean isCompleted = false;
		List<linkbase_form> linkbaseList1 = findByModalId(id);
		List<linkbase_form> linkbaseList = null;
		if(type.equalsIgnoreCase("lhdnm-sme")) {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm-sme")).collect(Collectors.toList());
		} else {
			linkbaseList = linkbaseList1.stream().filter(lform -> lform.getCategory().equalsIgnoreCase("lhdnm")).collect(Collectors.toList());
		}
		
		for(linkbase_form form : linkbaseList) {
			String fname = "";
			if (form.getName() != null) {
				fname = form.getName().toLowerCase();
				fname = fname.replaceAll(" ", "-");
//				fname = fname.replaceAll(".", "");
			}
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/frm_lab_lhdnm-" + fname + "-ms_" + dtString + ".xml"))) {
				
				writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			    writer.newLine();
			    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			    writer.newLine();
			    if(type.equalsIgnoreCase("lhdnm-sme")) {
			    	writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd http://xbrl.org/2008/generic http://www.xbrl.org/2008/generic-link.xsd http://xbrl.org/2008/formula http://www.xbrl.org/2008/formula.xsd http://xbrl.org/2008/label http://www.xbrl.org/2008/generic-label.xsd http://xbrl.org/2008/reference http://www.xbrl.org/2008/generic-reference.xsd http://xbrl.org/2008/assertion/consistency http://www.xbrl.org/2008/consistency-assertion.xsd http://xbrl.org/2008/assertion/existence http://www.xbrl.org/2008/existence-assertion.xsd http://xbrl.org/2008/assertion/value http://www.xbrl.org/2008/value-assertion.xsd http://xbrl.org/2008/filter/concept http://www.xbrl.org/2008/concept-filter.xsd http://xbrl.org/2010/message http://www.xbrl.org/2010/generic-message.xsd http://xbrl.org/2010/message/validation http://www.xbrl.org/2010/validation-message.xsd http://xbrl.org/2008/filter/dimension http://www.xbrl.org/2008/dimension-filter.xsd http://xbrl.org/2010/filter/aspect-cover http://www.xbrl.org/2010/aspect-cover-filter.xsd\" xmlns:ca=\"http://xbrl.org/2008/assertion/consistency\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:fn=\"http://www.w3.org/2005/xpath-functions\" xmlns:variable=\"http://xbrl.org/2008/variable\" xmlns:xfi=\"http://www.xbrl.org/2008/function/instance\" xmlns:lhdnm-enum=\"http://www.hasil.gov.my/xbrl/" + dtString + "/enum\" xmlns:cf=\"http://xbrl.org/2008/filter/concept\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:valm=\"http://xbrl.org/2010/message/validation\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:ea=\"http://xbrl.org/2008/assertion/existence\" xmlns:asf=\"http://xbrl.org/2010/filter/aspect-cover\" xmlns:df=\"http://xbrl.org/2008/filter/dimension\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:validation=\"http://xbrl.org/2008/validation\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:formula=\"http://xbrl.org/2008/formula\" xmlns:msg=\"http://xbrl.org/2010/message\" xmlns:va=\"http://xbrl.org/2008/assertion/value\" xmlns:label=\"http://xbrl.org/2008/label\">");
			    } else {
			    	writer.write("<link:linkbase xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:bf=\"http://xbrl.org/2008/filter/boolean\" xmlns:ca=\"http://xbrl.org/2008/assertion/consistency\" xmlns:cf=\"http://xbrl.org/2008/filter/concept\" xmlns:df=\"http://xbrl.org/2008/filter/dimension\" xmlns:ea=\"http://xbrl.org/2008/assertion/existence\" xmlns:fn=\"http://www.w3.org/2005/xpath-functions\" xmlns:formula=\"http://xbrl.org/2008/formula\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:msg=\"http://xbrl.org/2010/message\" xmlns:va=\"http://xbrl.org/2008/assertion/value\" xmlns:validation=\"http://xbrl.org/2008/validation\" xmlns:valm=\"http://xbrl.org/2010/message/validation\" xmlns:variable=\"http://xbrl.org/2008/variable\" xmlns:xfi=\"http://www.xbrl.org/2008/function/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd http://xbrl.org/2008/generic http://www.xbrl.org/2008/generic-link.xsd http://xbrl.org/2008/formula http://www.xbrl.org/2008/formula.xsd http://xbrl.org/2008/label http://www.xbrl.org/2008/generic-label.xsd http://xbrl.org/2008/reference http://www.xbrl.org/2008/generic-reference.xsd http://xbrl.org/2008/assertion/consistency http://www.xbrl.org/2008/consistency-assertion.xsd http://xbrl.org/2008/assertion/existence http://www.xbrl.org/2008/existence-assertion.xsd http://xbrl.org/2008/assertion/value http://www.xbrl.org/2008/value-assertion.xsd http://xbrl.org/2008/filter/concept http://www.xbrl.org/2008/concept-filter.xsd http://xbrl.org/2008/filter/dimension http://www.xbrl.org/2008/dimension-filter.xsd http://xbrl.org/2010/message http://www.xbrl.org/2010/generic-message.xsd http://xbrl.org/2010/message/validation http://www.xbrl.org/2010/validation-message.xsd http://xbrl.org/2008/filter/boolean http://www.xbrl.org/2008/boolean-filter.xsd\">");
			    }
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"" + form.getLinkrole() + "\" xlink:type=\"simple\" xlink:href=\"lhdnm-" + fname + "_" + dtString + ".xsd#" + fname + "\" />");
			    writer.newLine();
			    writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2010/role/message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/generic-message.xsd#standard-message\" />");
			    writer.newLine();
			    writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2010/assertion-unsatisfied-message\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2010/validation-message.xsd#assertion-unsatisfied-message\" />");
			    writer.newLine();
			    writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"" + form.getLinkrole() + "\">");
			    
			    int vaCount = 0;
			    for(presentation_formula pre : form.getFormulaList()) {
			    	vaCount++;
			    	String labelText = StringEscapeUtils.escapeXml(pre.getError_message_ms());
			    	writer.newLine();
				    writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"frm_lhdnm-" + fname + "_" + dtString + ".xml#" + pre.getValue_assertion() + "\" xlink:label=\"" + pre.getValue_assertion_label() + "\" />");
				    writer.newLine();
				    writer.write("    <msg:message xlink:type=\"resource\" xlink:label=\"label" + vaCount + "\" xlink:role=\"http://www.xbrl.org/2010/role/message\" xml:lang=\"en\" id=\"label" + vaCount + "\">" + labelText + "</msg:message>");
				    writer.newLine();
				    writer.write("    <gen:arc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2010/assertion-unsatisfied-message\" xlink:from=\"" + pre.getValue_assertion_label() + "\" xlink:to=\"label" + vaCount + "\" order=\"1.0\" />");
			    }
				
				writer.newLine();
			    writer.write("  </gen:link>");
			    writer.newLine();
			    writer.write("</link:linkbase>");
			    
			    isCompleted = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("fail to write file: " + e.getMessage());
			}
		}
		
		return isCompleted;
	}
	
	public String findPresentationDetail(linkbase_form form, String fieldName) {
		String fname = "";
		
		if(!form.getPresentationList().isEmpty()) {
			presentation_detail pd = form.getPresentationList().stream().filter(pdetail -> compareNullableString(pdetail.getDataItem(), fieldName)).findAny().orElse(null);
			if(pd != null) {
				fname = pd.getPrefix() + ":" + pd.getName();
				System.out.println("\n frmName1: " + fname);
			} else {
				List<presentation_detail> pdTemp = form.getPresentationList();
				for(presentation_detail pre : pdTemp) {
					if(!pre.getChildren().isEmpty()) {
						presentation_detail pd2 = pre.getChildren().stream().filter(pdetail -> compareNullableString(pdetail.getDataItem(), fieldName)).findAny().orElse(null);
						if(pd2 != null) {
							fname = pd2.getPrefix() + ":" + pd2.getName();
							System.out.println("\n frmName2: " + fname);
							break;
						} else {
							List<presentation_detail> pdTemp2 = pre.getChildren();
							for(presentation_detail pre2 : pdTemp2) {
								if(!pre2.getChildren().isEmpty()) {
									presentation_detail pd3 = pre2.getChildren().stream().filter(pdetail -> compareNullableString(pdetail.getDataItem(), fieldName)).findAny().orElse(null);
									if(pd3 != null) {
										fname = pd3.getPrefix() + ":" + pd3.getName();
										System.out.println("\n frmName3: " + fname);
										break;
									} else {
										List<presentation_detail> pdTemp3 = pre2.getChildren();
										for(presentation_detail pre3 : pdTemp3) {
											if(!pre3.getChildren().isEmpty()) {
												presentation_detail pd4 = pre3.getChildren().stream().filter(pdetail -> compareNullableString(pdetail.getDataItem(), fieldName)).findAny().orElse(null);
												if(pd4 != null) {
													fname = pd4.getPrefix() + ":" + pd4.getName();
													System.out.println("\n frmName4: " + fname);
													break;
												} else {
													List<presentation_detail> pdTemp4 = pre3.getChildren();
													for(presentation_detail pre4 : pdTemp4) {
														if(!pre4.getChildren().isEmpty()) {
															presentation_detail pd5 = pre4.getChildren().stream().filter(pdetail -> compareNullableString(pdetail.getDataItem(), fieldName)).findAny().orElse(null);
															if(pd5 != null) {
																fname = pd5.getPrefix() + ":" + pd5.getName();
																System.out.println("\n frmName5: " + fname);
																break;
															} else {
																List<presentation_detail> pdTemp5 = pre4.getChildren();
																for(presentation_detail pre5 : pdTemp5) {
																	if(!pre5.getChildren().isEmpty()) {
																		presentation_detail pd6 = pre5.getChildren().stream().filter(pdetail -> compareNullableString(pdetail.getDataItem(), fieldName)).findAny().orElse(null);
																		if(pd6 != null) {
																			fname = pd6.getPrefix() + ":" + pd6.getName();
																			System.out.println("\n frmName6: " + fname);
																			break;
																		} else {
																			List<presentation_detail> pdTemp6 = pre5.getChildren();
																			for(presentation_detail pre6 : pdTemp6) {
																				if(!pre6.getChildren().isEmpty()) {
																					presentation_detail pd7 = pre6.getChildren().stream().filter(pdetail -> compareNullableString(pdetail.getDataItem(), fieldName)).findAny().orElse(null);
																					if(pd7 != null) {
																						fname = pd7.getPrefix() + ":" + pd7.getName();
																						System.out.println("\n frmName7: " + fname);
																						break;
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		
		return fname;
	}
	
	public boolean compareNullableString(String str, String filterStr) {

	    boolean result = false;
	    
	    if(Strings.isNotBlank(str)) {
	    	result = str.equalsIgnoreCase(filterStr);
	    }

	    return result;
	}
	
}

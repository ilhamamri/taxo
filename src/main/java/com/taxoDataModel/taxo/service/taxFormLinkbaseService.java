package com.taxoDataModel.taxo.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.taxoDataModel.taxo.model.tax_data_modal;
import com.taxoDataModel.taxo.model.tax_form;
import com.taxoDataModel.taxo.model.tax_form_linkbase;
import com.taxoDataModel.taxo.repository.tax_form_linkbaseRepository;

@Service
public class taxFormLinkbaseService {
	
	@Autowired
	tax_form_linkbaseRepository repository;
	
	public void save(List<tax_form_linkbase> tList) {
		try {
			repository.saveAll(tList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveSingle(tax_form_linkbase form) {
		try {
			repository.save(form);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<tax_form_linkbase> getAll(){
		List<tax_form_linkbase> tList = new ArrayList<tax_form_linkbase>();
		try {
			tList = repository.findAll(Sort.by(Sort.Direction.ASC, "id"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tList;
	}

	public boolean writeDimensionDef(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/def_lhdnm-dim_" + dtString + ".xml"))) {
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			writer.newLine();
			writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			writer.newLine();
			writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
			writer.newLine();
			writer.write("  <link:roleRef roleURI=\"http://www.hasil.gov.my/role/defaults\" xlink:type=\"simple\" xlink:href=\"lhdnm-dim_" + dtString + ".xsd#lhdnm-defaults\"/>");
			writer.newLine();
			writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/int/dim/arcrole/dimension-default\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2005/xbrldt-2005.xsd#dimension-default\"/>");
			writer.newLine();
			writer.write("  <link:definitionLink xlink:type=\"extended\" xlink:role=\"http://www.hasil.gov.my/role/defaults\">");
			writer.newLine();
			writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../../../../def/ic/lhdnm-cor_" + dtString + ".xsd#lhdnm_TypeOfFundAxis\" xlink:label=\"TypeOfFundAxis\" xlink:title=\"TypeOfFundAxis\"/>");
			writer.newLine();
			writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"../../../../def/ic/lhdnm-cor_" + dtString + ".xsd#lhdnm_TotalMember\" xlink:label=\"TotalMember\" xlink:title=\"TotalMember\"/>");
			writer.newLine();
			writer.write("    <link:definitionArc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/int/dim/arcrole/dimension-default\" xlink:from=\"TypeOfFundAxis\" xlink:to=\"TotalMember\" xlink:title=\"definition: TypeOfFundAxis to TotalMember\" order=\"1.0\"/>");
			writer.newLine();
			writer.write("  </link:definitionLink>");
			writer.newLine();
			writer.write("</link:linkbase>");
			isCompleted = true;
			
		} catch (Exception e) {
			isCompleted = false;
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		
		return isCompleted;
	}
	
	public boolean writeDimension(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/lhdnm-dim_" + dtString + ".xsd"))) {
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			writer.newLine();
			writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			writer.newLine();
			writer.write("<xsd:schema targetNamespace=\"http://www.hasil.gov.my/xbrl/" + dtString + "/dimensions\" elementFormDefault=\"qualified\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:lhdnm-dim=\"http://www.hasil.gov.my/xbrl/" + dtString + "/dimensions\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:reference=\"http://xbrl.org/2008/reference\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/" + dtString + "\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
			writer.newLine();
			writer.write("  <xsd:annotation>");
			writer.newLine();
			writer.write("    <xsd:appinfo>");
			writer.newLine();
			writer.write("    <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"gla_lhdnm-dim-en_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			writer.newLine();
			writer.write("    <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"gla_lhdnm-dim-ms_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			writer.newLine();
			writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"def_lhdnm-dim_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/definitionLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
			writer.newLine();
			writer.write("      <link:roleType roleURI=\"http://www.hasil.gov.my/role/defaults\" id=\"lhdnm-defaults\">");
			writer.newLine();
			writer.write("        <link:definition>Axis: Defaults</link:definition>");
			writer.newLine();
			writer.write("        <link:usedOn>link:definitionLink</link:usedOn>");
			writer.newLine();
			writer.write("      </link:roleType>");
			writer.newLine();
			writer.write("    </xsd:appinfo>");
			writer.newLine();
			writer.write("  </xsd:annotation>");
			writer.newLine();
			writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/label\" schemaLocation=\"http://www.xbrl.org/2008/generic-label.xsd\"/>");
			writer.newLine();
			writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/reference\" schemaLocation=\"http://www.xbrl.org/2008/generic-reference.xsd\"/>");
			writer.newLine();
			writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/generic\" schemaLocation=\"http://www.xbrl.org/2008/generic-link.xsd\"/>");
			writer.newLine();
			writer.write("</xsd:schema>");
			isCompleted = true;
			
		} catch (Exception e) {
			isCompleted = false;
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		
		return isCompleted;
	}
	
	public boolean writeDimensionGlaEn(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/gla_lhdnm-dim-en_" + dtString + ".xml"))) {
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			writer.newLine();
			writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			writer.newLine();
			writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\">");
			writer.newLine();
			writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#standard-label\"/>");
			writer.newLine();
			writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/link\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-link.xsd#standard-link-role\"/>");
			writer.newLine();
			writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/element-label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#element-label\"/>");
			writer.newLine();
			writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"http://www.xbrl.org/2008/role/link\">");
			writer.newLine();
			writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"lhdnm-dim_" + dtString + ".xsd#lhdnm-defaults\" xlink:label=\"roleType\" xlink:title=\"roleType\"/>");
			writer.newLine();
			writer.write("    <label:label xlink:type=\"resource\" xlink:label=\"label\" xlink:role=\"http://www.xbrl.org/2008/role/label\" xlink:title=\"label\" xml:lang=\"en\">[999.999] Axis: Defaults</label:label>");
			writer.newLine();
			writer.write("    <gen:arc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/element-label\" xlink:from=\"roleType\" xlink:to=\"label\" xlink:title=\"user-defined: roleType to label\" order=\"1.0\"/>");
			writer.newLine();
			writer.write("  </gen:link>");
			writer.newLine();
			writer.write("</link:linkbase>");
			isCompleted = true;
			
		} catch (Exception e) {
			isCompleted = false;
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		
		return isCompleted;
	}
	
	public boolean writeDimensionGlaMs(String path, String dtString, tax_data_modal modal) {
		boolean isCompleted = false;
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/gla_lhdnm-dim-ms_" + dtString + ".xml"))) {
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			writer.newLine();
			writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
			writer.newLine();
			writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd\" xmlns:gen=\"http://xbrl.org/2008/generic\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:label=\"http://xbrl.org/2008/label\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\">");
			writer.newLine();
			writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#standard-label\"/>");
			writer.newLine();
			writer.write("  <link:roleRef roleURI=\"http://www.xbrl.org/2008/role/link\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-link.xsd#standard-link-role\"/>");
			writer.newLine();
			writer.write("  <link:arcroleRef arcroleURI=\"http://xbrl.org/arcrole/2008/element-label\" xlink:type=\"simple\" xlink:href=\"http://www.xbrl.org/2008/generic-label.xsd#element-label\"/>");
			writer.newLine();
			writer.write("  <gen:link xlink:type=\"extended\" xlink:role=\"http://www.xbrl.org/2008/role/link\">");
			writer.newLine();
			writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\"lhdnm-dim_" + dtString + ".xsd#lhdnm-defaults\" xlink:label=\"roleType\" xlink:title=\"roleType\"/>");
			writer.newLine();
			writer.write("    <label:label xlink:type=\"resource\" xlink:label=\"label\" xlink:role=\"http://www.xbrl.org/2008/role/label\" xlink:title=\"label\" xml:lang=\"ms\">[999.999] Axis: Defaults</label:label>");
			writer.newLine();
			writer.write("    <gen:arc xlink:type=\"arc\" xlink:arcrole=\"http://xbrl.org/arcrole/2008/element-label\" xlink:from=\"roleType\" xlink:to=\"label\" xlink:title=\"user-defined: roleType to label\" order=\"1.0\"/>");
			writer.newLine();
			writer.write("  </gen:link>");
			writer.newLine();
			writer.write("</link:linkbase>");
			isCompleted = true;
			
		} catch (Exception e) {
			isCompleted = false;
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		
		return isCompleted;
	}
	
	
	
	
	
}

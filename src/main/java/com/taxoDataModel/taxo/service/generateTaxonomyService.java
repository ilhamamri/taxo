package com.taxoDataModel.taxo.service;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import com.taxoDataModel.taxo.helper.taxConceptHelper;
import com.taxoDataModel.taxo.model.tax_concept;
import com.taxoDataModel.taxo.model.tax_data_modal;
import com.taxoDataModel.taxo.model.tax_enum;
import com.taxoDataModel.taxo.repository.tax_conceptRepository;

@Service
public class generateTaxonomyService {
	
	@Autowired
	private tax_conceptRepository repository;
	
	@Autowired
	taxDataModalService tdmService;

	@Autowired
	taxConceptService tcService;
	
	@Autowired
	taxEnumService teService;
	
	@Autowired
	taxSmeConceptService tscService;
	
	@Autowired
	taxSmeEnumService tseService;
	
	@Autowired
	taxFormService frmService;
	
	@Autowired
	taxSmeFormService smefrmService;
	
	@Autowired
	linkbaseFormService linkService;
	
	@Autowired
	taxFormLinkbaseService tfService;
	
//	@Autowired
//	taxConceptHelper tcHelper;
	
	public static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd"); 
	public static final String SAMPLE_TAXO_FOLDER = "./src/main/output/taxo";
	public static final String SAMPLE_ZIP_FOLDER = "./src/main/output/zip";
	public static final String REFERENCE_TAXO = "./src/main/references/lhdm_reference";
	private final Path fileStorageLocation = null;
	List<String> filesListInDir = new ArrayList<String>();

	public String writeFile(Long id) {
		boolean isCompleted = false;
		LocalDateTime now = LocalDateTime.now(); 
//		String dtString = dtf.format(now);
		String dtString = "2020-08-31";
		String taxoPath = SAMPLE_TAXO_FOLDER + "/lhdm_" + dtString;
		try {
		    
			String metaInfPath = taxoPath + "/META-INF";
			String metaInfOriginalPath = REFERENCE_TAXO + "/META-INF"; //copy catalog
			
			String hasilPath = taxoPath + "/www.hasil.gov.my";
			String xbrlPath = hasilPath + "/xbrl";
			String datePath = xbrlPath + "/" + dtString;
			
			String hasilOriginalPath = REFERENCE_TAXO + "/www.hasil.gov.my";
			String xbrlOriginalPath = hasilOriginalPath + "/xbrl";
			String dateOriginalPath = xbrlOriginalPath + "/2020-08-31";
			
			String defPath = datePath + "/def";
			String extPath = defPath + "/ext";
			String fdnPath = defPath + "/fdn";
			String icPath = defPath + "/ic";
			
			String defOriginalPath = dateOriginalPath + "/def";
			String extOriginalPath = defOriginalPath + "/ext"; //copy
			String fdnOriginalPath = defOriginalPath + "/fdn"; //copy
			String icOriginalPath = defOriginalPath + "/ic"; //copy ssmt & sme
			
			String repPath = datePath + "/rep";
			String lhdnmPath = repPath + "/lhdnm";
			String ssmPath = repPath + "/ssm";
			
			String repOriginalPath = dateOriginalPath + "/rep";
			String lhdnmOriginalPath = repOriginalPath + "/lhdnm";
			String ssmOriginalPath = repOriginalPath + "/ssm";
			
			String taxPath = lhdnmPath + "/tax";
			String taxSmePath = lhdnmPath + "/tax_sme";
			
			String taxOriginalPath = lhdnmOriginalPath + "/tax";
			String taxSmeOriginalPath = lhdnmOriginalPath + "/tax_sme";
			
			String taxDimensions = taxPath + "/dimensions";
			String taxEnumerations = taxPath + "/enumerations";
			String taxLabels = taxPath + "/labels";
			String taxLinkbases = taxPath + "/linkbases";
			
			String taxDimensionsOriginal = taxOriginalPath + "/dimensions"; //no copy
			String taxEnumerationsOriginal = taxOriginalPath + "/enumerations"; //copy gla
			String taxLabelsOriginal = taxOriginalPath + "/labels";
			String taxLinkbasesOriginal = taxOriginalPath + "/linkbases";
			
			String smeEnumerations = taxSmePath + "/enumerations";
			String smeLabels = taxSmePath + "/labels";
			String smeLinkbases = taxSmePath + "/linkbases";
			
			String smeEnumerationsOriginal = taxSmeOriginalPath + "/enumerations";
			String smeLabelsOriginal = taxSmeOriginalPath + "/labels";
			String smeLinkbasesOriginal = taxSmeOriginalPath + "/linkbases";
			
			String ssmCaPath = ssmPath + "/ca";
			String ssmMfrsPath = ssmPath + "/mfrs";
			String ssmPersPath = ssmPath + "/pers";
			
			String ssmCaOriginalPath = ssmPath + "/ca";
			String ssmMfrsOriginalPath = ssmPath + "/mfrs";
			String ssmPersOriginalPath = ssmPath + "/pers";
			
			File directory = createDirectory(taxoPath);
			File metaDirectory = createDirectory(metaInfPath);
			File hasilDirectory = new File(datePath);

			if (! hasilDirectory.exists()){
				hasilDirectory.mkdirs();
		        // If you require it to make the entire directory path including parents,
		        // use directory.mkdirs(); here instead.
		    } else {
		    	if(deleteDir(hasilDirectory)) {
		    		hasilDirectory.mkdirs();
		    	}		    	
		    }
			
			File defDir = createDirectory(defPath);
			File extDir = createDirectory(extPath);
			File fdnDir = createDirectory(fdnPath);
			File icDir = createDirectory(icPath);
			
			File repDir = createDirectory(repPath);
			File lhdnmDir = createDirectory(lhdnmPath);
			File ssmDir = createDirectory(ssmPath);
			
			File taxDir = createDirectory(taxPath);
			File taxSmeDir = createDirectory(taxSmePath);
			File taxDimensionsDir = createDirectory(taxDimensions);
			File taxEnumerationsDir = createDirectory(taxEnumerations);
			File taxLabelsDir = createDirectory(taxLabels);
			File taxLinkbasesDir = createDirectory(taxLinkbases);
			File smeEnumerationsDir = createDirectory(smeEnumerations);
			File smeLabelsDir = createDirectory(smeLabels);
			File smeLinkbasesDir = createDirectory(smeLinkbases);
			
			tax_data_modal modal = tdmService.getById(id);
			
		    isCompleted = tcService.writeTaxConcept(icPath, dtString, id);
		    if(isCompleted) {
		    	isCompleted = tcService.writeTaxConceptLabelEn(icPath, dtString, id);
		    	if(isCompleted) {
		    		isCompleted = tcService.writeTaxConceptLabelMs(icPath, dtString, id);
		    	} else {
			    	throw new RuntimeException("fail to write file: error at tax concept 2");
			    }
		    } else {
		    	throw new RuntimeException("fail to write file: error at tax concept 1");
		    }

		    if(isCompleted) {
		    	isCompleted = teService.writeTaxEnum(taxEnumerations, dtString, id);
		    	if(isCompleted) {
		    		isCompleted = teService.writeTaxEnumDef(taxEnumerations, dtString, id);
		    		if(isCompleted) {
		    			isCompleted = teService.writeTaxEnumLabEn(taxEnumerations, dtString, id);
		    			isCompleted = teService.writeTaxEnumLabMs(taxEnumerations, dtString, id);
		    			isCompleted = teService.writeTaxEnumGlaEn(taxEnumerations, dtString, id);
		    			isCompleted = teService.writeTaxEnumGlaMs(taxEnumerations, dtString, id);
		    		} else {
				    	throw new RuntimeException("fail to write file: error at tax enum 2");
				    }
		    	} else {
			    	throw new RuntimeException("fail to write file: error at tax enum 1");
			    }
		    } else {
		    	throw new RuntimeException("fail to write file: error at tax concept 3");
		    }
		    
		    if(isCompleted) {
		    	isCompleted = frmService.writeTaxFormEntry(taxPath, dtString, modal);
		    	if(isCompleted) {
		    		isCompleted = frmService.writeTaxFormLabelEn(taxLabels, dtString, modal);
				    isCompleted = frmService.writeTaxFormLabelMs(taxLabels, dtString, modal);
		    	} else {
			    	throw new RuntimeException("fail to write file: error at tax form entry 1");
			    }
		    } else {
		    	throw new RuntimeException("fail to write file: error at tax enum 3");
		    }
		    
		    if(isCompleted) {
		    	isCompleted = frmService.writeTaxFormLinkbase(taxLinkbases, dtString, modal);
		    	if(isCompleted) {
		    		isCompleted = frmService.writeTaxFormHeaderDef(taxLinkbases, dtString, modal);
		    		if(isCompleted) {
		    			isCompleted = frmService.writeTaxFormHeaderPre(taxLinkbases, dtString, modal);
		    			if(isCompleted) {
		    				isCompleted = frmService.writeTaxFormHeaderLabelEn(taxLinkbases, dtString, modal);
		    			    isCompleted = frmService.writeTaxFormHeaderLabelMs(taxLinkbases, dtString, modal);
		    			    isCompleted = frmService.writeFormulaDef(taxLinkbases, dtString, modal, "lhdnm");
		    			    isCompleted = frmService.writeFormulaLabelEn(taxLinkbases, dtString, modal, "lhdnm");
		    			    isCompleted = frmService.writeFormulaLabelMs(taxLinkbases, dtString, modal, "lhdnm");
		    			    
		    			} else {
		    		    	throw new RuntimeException("fail to write file: error at tax form header 3");
		    		    }
		    		} else {
				    	throw new RuntimeException("fail to write file: error at tax form header 2");
				    }
		    	} else {
			    	throw new RuntimeException("fail to write file: error at tax form header 1");
			    }
		    } else {
		    	throw new RuntimeException("fail to write file: error at tax form entry 2");
		    }
		    
		    if(isCompleted) {
		    	isCompleted = linkService.writeLinkbaseForm(taxLinkbases, dtString, id, "lhdnm");
		    	if(isCompleted) {
		    		isCompleted = linkService.writeLinkbaseDef(taxLinkbases, dtString, id, "lhdnm");
		    		if(isCompleted) {
		    			isCompleted = linkService.writeLinkbasePre(taxLinkbases, dtString, id, "lhdnm");
		    			if(isCompleted) {
		    				isCompleted = linkService.writeLinkbaseLabEn(taxLinkbases, dtString, id, "lhdnm");
		    			    isCompleted = linkService.writeLinkbaseLabMs(taxLinkbases, dtString, id, "lhdnm");
		    			} else {
		    		    	throw new RuntimeException("fail to write file: error at tax linkbase 3");
		    		    }
		    		} else {
				    	throw new RuntimeException("fail to write file: error at tax linkbase 2");
				    }
		    	} else {
			    	throw new RuntimeException("fail to write file: error at tax linkbase 1");
			    }
		    } else {
		    	throw new RuntimeException("fail to write file: error at tax form header 4");
		    }
		    
		    if(isCompleted) {
		    	isCompleted = linkService.writeFormulaDef(taxLinkbases, dtString, id, "lhdnm");
		    	if(isCompleted) {
		    		isCompleted = linkService.writeFormulaLabelEn(taxLinkbases, dtString, id, "lhdnm");
		    		isCompleted = linkService.writeFormulaLabelMs(taxLinkbases, dtString, id, "lhdnm");
		    	}
		    }
		    
		    if(!isCompleted) {
		    	throw new RuntimeException("fail to write file: error at tax linkbase 4");
		    }
		    
		    isCompleted = tscService.writeTaxConcept(icPath, dtString, id);
		    if(isCompleted) {
		    	isCompleted = tscService.writeTaxConceptLabelEn(icPath, dtString, id);
		    	if(isCompleted) {
		    		isCompleted = tscService.writeTaxConceptLabelMs(icPath, dtString, id);
		    	} else {
			    	throw new RuntimeException("fail to write file: error at tax sme concept 2");
			    }
		    } else {
		    	throw new RuntimeException("fail to write file: error at tax sme concept 1");
		    }
		    
		    if(isCompleted) {
		    	isCompleted = tseService.writeTaxEnum(smeEnumerations, dtString, id);
		    	if(isCompleted) {
		    		isCompleted = tseService.writeTaxEnumDef(smeEnumerations, dtString, id);
		    		if(isCompleted) {
		    			isCompleted = tseService.writeTaxEnumLabEn(smeEnumerations, dtString, id);
		    			isCompleted = tseService.writeTaxEnumLabMs(smeEnumerations, dtString, id);
		    			isCompleted = tseService.writeTaxEnumGlaEn(smeEnumerations, dtString, id);
		    			isCompleted = tseService.writeTaxEnumGlaMs(smeEnumerations, dtString, id);
		    		} else {
				    	throw new RuntimeException("fail to write file: error at tax sme enum 2");
				    }
		    	} else {
			    	throw new RuntimeException("fail to write file: error at tax sme enum 1");
			    }
		    } else {
		    	throw new RuntimeException("fail to write file: error at tax sme concept 3");
		    }
		    
		    if(isCompleted) {
		    	isCompleted = smefrmService.writeTaxFormEntry(taxSmePath, dtString, modal);
		    	if(isCompleted) {
		    		isCompleted = smefrmService.writeTaxFormLabelEn(smeLabels, dtString, modal);
				    isCompleted = smefrmService.writeTaxFormLabelMs(smeLabels, dtString, modal);
		    	} else {
			    	throw new RuntimeException("fail to write file: error at tax sme form entry 1");
			    }
		    } else {
		    	throw new RuntimeException("fail to write file: error at tax sme enum 3");
		    }
		    
		    if(isCompleted) {
		    	isCompleted = smefrmService.writeTaxFormLinkbase(smeLinkbases, dtString, modal);
		    	if(isCompleted) {
		    		isCompleted = smefrmService.writeTaxFormHeaderDef(smeLinkbases, dtString, modal);
		    		if(isCompleted) {
		    			isCompleted = smefrmService.writeTaxFormHeaderPre(smeLinkbases, dtString, modal);
		    			if(isCompleted) {
		    				isCompleted = smefrmService.writeTaxFormHeaderLabelEn(smeLinkbases, dtString, modal);
		    			    isCompleted = smefrmService.writeTaxFormHeaderLabelMs(smeLinkbases, dtString, modal);
		    			} else {
		    		    	throw new RuntimeException("fail to write file: error at tax sme form header 3");
		    		    }
		    		} else {
				    	throw new RuntimeException("fail to write file: error at tax sme form header 2");
				    }
		    	} else {
			    	throw new RuntimeException("fail to write file: error at tax sme form header 1");
			    }
		    } else {
		    	throw new RuntimeException("fail to write file: error at tax sme form entry 2");
		    }
		    
		    if(isCompleted) {
		    	isCompleted = linkService.writeLinkbaseSmeForm(smeLinkbases, dtString, id, "lhdnm-sme");
		    	if(isCompleted) {
		    		isCompleted = linkService.writeLinkbaseSmeDef(smeLinkbases, dtString, id, "lhdnm-sme");
		    		if(isCompleted) {
		    			isCompleted = linkService.writeLinkbaseSmePre(smeLinkbases, dtString, id, "lhdnm-sme");
		    			if(isCompleted) {
		    				isCompleted = linkService.writeLinkbaseLabEn(smeLinkbases, dtString, id, "lhdnm-sme");
		    			    isCompleted = linkService.writeLinkbaseLabMs(smeLinkbases, dtString, id, "lhdnm-sme");
		    			} else {
		    		    	throw new RuntimeException("fail to write file: error at tax linkbase 3");
		    		    }
		    		} else {
				    	throw new RuntimeException("fail to write file: error at tax linkbase 2");
				    }
		    	} else {
			    	throw new RuntimeException("fail to write file: error at tax linkbase 1");
			    }
		    } else {
		    	throw new RuntimeException("fail to write file: error at tax form header 4");
		    }
		    
		    if(isCompleted) {
		    	isCompleted = linkService.writeFormulaSmeDef(smeLinkbases, dtString, id, "lhdnm-sme");
		    	if(isCompleted) {
		    		isCompleted = linkService.writeFormulaLabelEn(smeLinkbases, dtString, id, "lhdnm-sme");
		    		isCompleted = linkService.writeFormulaLabelMs(smeLinkbases, dtString, id, "lhdnm-sme");
		    	}
		    }
		    
		    if(!isCompleted) {
		    	throw new RuntimeException("fail to write file: error at tax linkbase 4");
		    }
		    
		    isCompleted = frmService.writeTaxonomyPackage(metaInfPath, dtString, modal);
		    
		    //copy file
		    isCompleted = copyFile(metaInfOriginalPath + "/catalog.xml", metaInfPath + "/catalog.xml");
		    
		    isCompleted = copyFile(extOriginalPath + "/ifrs-cor_2012-03-29.xsd", extPath + "/ifrs-cor_2012-03-29.xsd");
		    isCompleted = copyFile(extOriginalPath + "/lab_ifrs-en_2012-03-29.xml", extPath + "/lab_ifrs-en_2012-03-29.xml");
		    
//		    isCompleted = copyFile(fdnOriginalPath + "/lhdnm-fdn_2020-08-31.xsd", fdnPath + "/lhdnm-fdn_"+ dtString +".xsd");
		    isCompleted = frmService.writeLhdnmFdn(fdnPath, dtString, modal);
		    isCompleted = copyFile(fdnOriginalPath + "/ssmt-fdn_2012-12-31.xsd", fdnPath + "/ssmt-fdn_2012-12-31.xsd");
		    
//		    isCompleted = copyFile(icOriginalPath + "/lab_lhdnm-sme-en_2020-08-31.xml", icPath + "/lab_lhdnm-sme-en_2020-08-31.xml");
//		    isCompleted = copyFile(icOriginalPath + "/lab_lhdnm-sme-ms_2020-08-31.xml", icPath + "/lab_lhdnm-sme-ms_2020-08-31.xml");
		    isCompleted = copyFile(icOriginalPath + "/lab_ssmt-en_2012-12-31.xml", icPath + "/lab_ssmt-en_2012-12-31.xml");
//		    isCompleted = copyFile(icOriginalPath + "/lhdnm-sme-cor_2020-08-31.xsd", icPath + "/lhdnm-sme-cor_2020-08-31.xsd");
		    isCompleted = copyFile(icOriginalPath + "/ssmt-cor_2012-12-31.xsd", icPath + "/ssmt-cor_2012-12-31.xsd");
		    
		    isCompleted = tfService.writeDimensionDef(taxDimensions, dtString, modal);
		    isCompleted = tfService.writeDimension(taxDimensions, dtString, modal);
		    isCompleted = tfService.writeDimensionGlaEn(taxDimensions, dtString, modal);
		    isCompleted = tfService.writeDimensionGlaMs(taxDimensions, dtString, modal);
		    
		    isCompleted = copyAllFile(ssmOriginalPath, ssmPath);
//		    
//		    isCompleted = linkService.writeFormulaDef(taxLinkbases, dtString);
//		    isCompleted = linkService.writeFormulaLabelEn(taxLinkbases, dtString);
//		    isCompleted = linkService.writeFormulaLabelMs(taxLinkbases, dtString);
		    
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		return taxoPath;
	}
	
	private boolean writeTaxConcept() {
		boolean isCompleted = false;
		LocalDateTime now = LocalDateTime.now(); 
		String dtString = dtf.format(now);
		try(BufferedWriter writer = new BufferedWriter(new FileWriter("lhdnm-cor_" + dtString + ".xsd"))) {
		    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		    writer.newLine();
		    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
		    writer.newLine();
		    writer.write("<xsd:schema targetNamespace=\"http://www.hasil.gov.my/xbrl/2020-08-31\" attributeFormDefault=\"unqualified\" elementFormDefault=\"qualified\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/2020-08-31\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\">");
		    writer.newLine();
		    writer.write("<xsd:annotation>");
		    writer.newLine();
		    writer.write("<xsd:appinfo>");
		    writer.newLine();
		    writer.write("<link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-en_2020-08-31.xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    writer.newLine();
		    writer.write("<link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-ms_2020-08-31.xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    writer.newLine();
		    writer.write("</xsd:appinfo>");
		    writer.newLine();
		    writer.write("</xsd:annotation>");
		    writer.newLine();
		    writer.write("<xsd:import namespace=\"http://www.xbrl.org/2003/instance\" schemaLocation=\"http://www.xbrl.org/2003/xbrl-instance-2003-12-31.xsd\"/>");
		    writer.newLine();
		    writer.write("<xsd:import namespace=\"http://www.xbrl.org/dtr/type/non-numeric\" schemaLocation=\"http://www.xbrl.org/dtr/type/nonNumeric-2009-12-16.xsd\"/>");
		    writer.newLine();
		    writer.write("<xsd:import namespace=\"http://www.xbrl.org/dtr/type/numeric\" schemaLocation=\"http://www.xbrl.org/dtr/type/numeric-2009-12-16.xsd\"/>");
		    writer.newLine();
		    writer.write("<xsd:import namespace=\"http://xbrl.org/2005/xbrldt\" schemaLocation=\"http://www.xbrl.org/2005/xbrldt-2005.xsd\"/>");
		    
		    
		    List<tax_concept> tList = tcService.getAll();
		    
		    for(tax_concept tc : tList) {
		    	writer.newLine();
		    	writer.write("<xsd:element name=\"" + tc.getName() + "\" id=\"" + tc.getConcept_id() + "\" type=\"" + tc.getType() + "\" substitutionGroup=\"" + tc.getSubstitution_group() + "\" abstract=\"" + tc.isAbstract_col() + "\" nillable=\"" + tc.isNillable() + "\" xbrli:periodType=\"" + tc.getPeriod_type() + "\"/>");
		    }
		    
		    writer.newLine();
		    writer.write("</xsd:schema>");
		    
		    isCompleted = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		return isCompleted;
	}
	
	private boolean writeTaxEnum() {
		boolean isCompleted = false;
		LocalDateTime now = LocalDateTime.now(); 
		String dtString = dtf.format(now);
		try(BufferedWriter writer = new BufferedWriter(new FileWriter("lhdnm-cor_" + dtString + ".xsd"))) {
			
			List<tax_enum> enumList = teService.getAll();
			List<tax_concept> tcList = tcService.getAllByPrefix("lhdnm-enum");
			
		    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		    writer.newLine();
		    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
		    writer.newLine();
		    writer.write("<xsd:schema targetNamespace=\"http://www.hasil.gov.my/xbrl/2020-08-31\" attributeFormDefault=\"unqualified\" elementFormDefault=\"qualified\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/2020-08-31\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\">");
		    writer.newLine();
		    writer.write("  <xsd:annotation>");
		    writer.newLine();
		    writer.write("    <xsd:appinfo>");
		    writer.newLine();
		    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"gla_lhdnm-enum-en_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    writer.newLine();
		    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"gla_lhdnm-enum-ms_" + dtString + ".xml\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    writer.newLine();
		    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"def_lhdnm-enum_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/definitionLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    writer.newLine();
		    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-enum-en_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    writer.newLine();
		    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-enum-ms_" + dtString + ".xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    
		    for(tax_enum te : enumList) {
		    	writer.newLine();
		    	writer.write("      <link:roleType roleURI=\"" + te.getLinkrole() + "\" id=\"" + te.getEnum_id() + "\">");
		    	writer.newLine();
		    	writer.write("        <link:definition>" + te.getDefinition_en() + "</link:definition>");
		    	writer.newLine();
		    	writer.write("        <link:usedOn>link:definitionLink</link:usedOn>");
		    	writer.newLine();
		    	writer.write("      </link:roleType>");
		    }
		    
		    writer.write("    </xsd:appinfo>");
		    writer.newLine();
		    writer.write("  </xsd:annotation>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://www.xbrl.org/2003/instance\" schemaLocation=\"http://www.xbrl.org/2003/xbrl-instance-2003-12-31.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://xbrl.org/2014/extensible-enumerations\" schemaLocation=\"http://www.xbrl.org/2014/extensible-enumerations.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://www.xbrl.org/dtr/type/non-numeric\" schemaLocation=\"http://www.xbrl.org/dtr/type/nonNumeric-2009-12-16.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://xbrl.org/2005/xbrldt\" schemaLocation=\"http://www.xbrl.org/2005/xbrldt-2005.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/label\" schemaLocation=\"http://www.xbrl.org/2008/generic-label.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/reference\" schemaLocation=\"http://www.xbrl.org/2008/generic-reference.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://xbrl.org/2008/generic\" schemaLocation=\"http://www.xbrl.org/2008/generic-link.xsd\"/>");

		    for(tax_concept tc : tcList) {
		    	String id = tc.getPrefix() + "_" + tc.getName();
		    	boolean headUsable = false;
		    	if(tc.getEnum_headUsable() != null) {
		    		headUsable = true;
		    	}
		    	writer.newLine();
		    	writer.write("  <xsd:element name=\"" + tc.getName() + "\" id=\"" + id + "\" type=\"" + tc.getType() + "\" enum:headUsable=\"" + headUsable + "\" substitutionGroup=\"" + tc.getSubstitution_group() + "\" enum:linkrole=\"" + tc.getEnum_linkrole() + "\" enum:domain=\"" + tc.getEnum_domain() + "\" abstract=\"" + tc.isAbstract_col() + "\" nillable=\"" + tc.isNillable() + "\" xbrli:periodType=\"" + tc.getPeriod_type() + "\"/>");
		    }
		    
		    for(tax_enum te : enumList) {
		    	String id = te.getPrefix() + "_" + te.getName();
		    	writer.newLine();
		    	writer.write("  <xsd:element name=\"" + te.getName() + "\" id=\"" + id + "\" type=\"nonnum:domainItemType\" substitutionGroup=\"xbrli:item\" abstract=\"false\" nillable=\"true\" xbrli:periodType=\"duration\"/>");
		    }
		    
		    writer.newLine();
		    writer.write("</xsd:schema>");
		    
		    isCompleted = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		return isCompleted;
	}
	
	public File createDirectory(String path) {
		
//		if (! directory.exists()){
//        directory.mkdir();
//        // If you require it to make the entire directory path including parents,
//        // use directory.mkdirs(); here instead.
//    }
		
		File directory = new File(path);
		if (! directory.exists()){
	        directory.mkdir();
	    }
		
		return directory;
	}
	
	public static boolean deleteDir(File dir) {
	    if (dir.isDirectory()) {
	        String[] children = dir.list();
	        for (int i=0; i<children.length; i++) {
	            boolean success = deleteDir(new File(dir, children[i]));
	            if (!success) {
	                return false;
	            }
	        }
	    }
	    return dir.delete();
	}

	public String zipFolder(String taxoPath) throws IOException {
		boolean isCompleted = false;
//		File file = new File("/Users/pankaj/sitemap.xml");
//        String zipFileName = "/Users/pankaj/sitemap.zip";
        
        File dir = new File(taxoPath);
        String zipDirName = SAMPLE_ZIP_FOLDER + "/" + dir.getName() + ".zip";
        File testFile = new File(zipDirName);
        
        if(testFile.exists()) {
        	testFile.delete();
        }
        
//        zipSingleFile(file, zipFileName);
        
        zipDirectory(dir, zipDirName);
        return zipDirName;
	}
	
	private void zipDirectory(File dir, String zipDirName) {
        try {
            populateFilesList(dir);
            //now zip files one by one
            //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipDirName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            for(String filePath : filesListInDir){
//                System.out.println("Zipping "+filePath);
                //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
                ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length()+1, filePath.length()));
                zos.putNextEntry(ze);
                //read the file and write to ZipOutputStream
                FileInputStream fis = new FileInputStream(filePath);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                zos.closeEntry();
                fis.close();
            }
            zos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	private void populateFilesList(File dir) throws IOException {
        File[] files = dir.listFiles();
        for(File file : files){
            if(file.isFile()) filesListInDir.add(file.getAbsolutePath());
            else populateFilesList(file);
        }
    }
	
	private static void zipSingleFile(File file, String zipFileName) {
        try {
            //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipFileName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            //add a new Zip Entry to the ZipOutputStream
            ZipEntry ze = new ZipEntry(file.getName());
            zos.putNextEntry(ze);
            //read the file and write to ZipOutputStream
            FileInputStream fis = new FileInputStream(file);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            
            //Close the zip entry to write to zip file
            zos.closeEntry();
            //Close resources
            zos.close();
            fis.close();
            fos.close();
            System.out.println(file.getCanonicalPath()+" is zipped to "+zipFileName);
            
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
	
	public Resource loadFileAsResource(String fileName) throws Exception {
		try {
			System.out.println("File Path "+fileName);
			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				throw new FileNotFoundException("File not found " + fileName);
			}
		} catch (MalformedURLException ex) {
			throw new FileNotFoundException("File not found " + fileName);
		}
	}

	private boolean copyFile(String originalPath, String copiedPath) throws Exception {
		boolean completed = false;
		File original = new File(originalPath);
		File copied = new File(copiedPath);
		InputStream is = null;
	    OutputStream os = null;
	    try {
	        is = new FileInputStream(original);
	        os = new FileOutputStream(copied);
	        byte[] buffer = new byte[1024];
	        int length;
	        while ((length = is.read(buffer)) > 0) {
	            os.write(buffer, 0, length);
	        }
	        completed = true;
	    } finally {
	        is.close();
	        os.close();
	    }
		return completed;
	}
	
	private boolean copyAllFile(String originalPath, String copiedPath) throws Exception {
		boolean completed = false;
		File original = new File(originalPath);
		File copied = new File(copiedPath);
//		InputStream is = null;
//	    OutputStream os = null;
	    try {
	    	FileUtils.copyDirectory(original, copied);
//	        is = new FileInputStream(original);
//	        os = new FileOutputStream(copied);
//	        byte[] buffer = new byte[1024];
//	        int length;
//	        while ((length = is.read(buffer)) > 0) {
//	            os.write(buffer, 0, length);
//	        }
	        completed = true;
	    } finally {
//	        is.close();
//	        os.close();
	    }
		return completed;
	}
	
	public boolean createZipFile(String zipFileName, String fileOrDirectoryToZip) {
		boolean dirName = false;
        BufferedOutputStream bufferedOutputStream = null;
        ZipArchiveOutputStream zipArchiveOutputStream = null;
        OutputStream outputStream = null;
        try {
            Path zipFilePath = Paths.get(zipFileName);
            outputStream = Files.newOutputStream(zipFilePath);
            bufferedOutputStream = new BufferedOutputStream(outputStream);
            zipArchiveOutputStream = new ZipArchiveOutputStream(bufferedOutputStream);
            File fileToZip = new File(fileOrDirectoryToZip);

            addFileToZipStream(zipArchiveOutputStream, fileToZip, "");
            dirName = true;

            zipArchiveOutputStream.close();
            bufferedOutputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dirName;
    }
	
	private void addFileToZipStream(ZipArchiveOutputStream zipArchiveOutputStream, File fileToZip, String base) throws IOException {
        String entryName = base + fileToZip.getName();
        ZipArchiveEntry zipArchiveEntry = new ZipArchiveEntry(fileToZip, entryName);
        zipArchiveOutputStream.putArchiveEntry(zipArchiveEntry);
        if(fileToZip.isFile()) {
            FileInputStream fileInputStream = null;
            try {
                fileInputStream = new FileInputStream(fileToZip);
                IOUtils.copy(fileInputStream, zipArchiveOutputStream);
                zipArchiveOutputStream.closeArchiveEntry();
            } finally {
                IOUtils.closeQuietly(fileInputStream);
            }
        } else {
            zipArchiveOutputStream.closeArchiveEntry();
            File[] files = fileToZip.listFiles();
            if(files != null) {
                for (File file: files) {
                    addFileToZipStream(zipArchiveOutputStream, file, entryName + "/");
                }
            }
        }
    }
	
}

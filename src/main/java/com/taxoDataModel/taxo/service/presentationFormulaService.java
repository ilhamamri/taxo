package com.taxoDataModel.taxo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.taxoDataModel.taxo.model.presentation_formula;
import com.taxoDataModel.taxo.repository.presentation_formulaRepository;

@Service
public class presentationFormulaService {
	
	@Autowired
	presentation_formulaRepository repository;
	
	public void save(List<presentation_formula> tList) {
		try {
			repository.saveAll(tList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveSingle(presentation_formula pf) {
		try {
			repository.save(pf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<presentation_formula> getAll(){
		return repository.findAll();
	}
	
	public List<presentation_formula> getByDataItem(String data_item) {
		return repository.findByDataItem(data_item);
	}

}

package com.taxoDataModel.taxo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.taxoDataModel.taxo.model.presentation_detail;
import com.taxoDataModel.taxo.model.presentation_formula;
import com.taxoDataModel.taxo.model.tax_enum;
import com.taxoDataModel.taxo.repository.presentation_detailRepository;

@Service
public class presentationDetailService {
	
	@Autowired
	presentation_detailRepository repository;
	
	public static final List<String> listOfRegex = new ArrayList<String>() {{
		add(" + ");
		add("+");
		add(" - ");
		add(" != ");
		add("!=");
		add(" = ");
		add("=");
		add(" * ");
		add("*");
		add(" div ");
		add("sum(");
		add("min(");
		add("(");
		add(")");
		add("/!+$/=");
		add(" is not ");
		add(" less than or equal to ");
		add(" greater than or equal to ");
		add(" lesser than or equal to ");
		add(" equal to ");
		add(" string-length ");
		add(" string ");
		add(" year-from-date ");
		add("if ");
		add(" then ");
		add(" else ");
		add(" true()");
		add(" and ");
		add(" or ");
		add(" ge ");
		add(" le ");
		add(" gt ");
		add(" lt ");
		add(" true ");
		add(" not ");
		add(" empty ");
		add(",");
		add("1. ");
		add("2. ");
		add("3. ");
		add("4. ");
		add("5. ");
		add("6. ");
		add("7. ");
		add("8. ");
		add("9. ");
		add("10. ");
	}};
	
	public void save(List<presentation_detail> tList) {
		try {
			repository.saveAll(tList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<presentation_detail> getAll(){
		return repository.findByParentIdIsNull();
	}
	
	public presentation_detail getByDataItem(String data_item) {
		return repository.findByDataItem(data_item);
	}
	
	public presentation_detail tableMapping(presentation_detail preObj, int index, String obj) {
		presentation_detail tEnum = preObj;
		
		switch (index) {
		case 0:
			tEnum.setPrefix(obj);
			break;
		case 1:
			tEnum.setName(obj);
			break;
		case 2:
			tEnum.setLabel_en(obj);
			break;
		case 3:
			tEnum.setLabel_ms(obj);
			break;
		case 4:
			tEnum.setLabel_en_form(obj);
			break;
		case 5:
			tEnum.setLabel_ms_form(obj);
			break;
		case 6:
			tEnum.setLabel_en_document(obj);
			break;
		case 7:
			tEnum.setLabel_ms_document(obj);
			break;
		case 8:
			tEnum.setDataItem(obj);
			break;
//		case 9:
//			tEnum.setTest_expression(obj);
//			break;
//		case 10:
//			tEnum.setError_message_en(obj);
//			break;
//		case 11:
//			tEnum.setError_message_ms(obj);
//			break;
		default:
		}
		
		return tEnum;
	}
	
	public presentation_formula tableMappingFormula(presentation_formula preObj, int index, String obj) {
		presentation_formula tEnum = preObj;
		
		switch (index) {
		case 8:
			tEnum.setDataItem(obj);
			break;
		case 9:
			tEnum.setTest_expression(obj);
			String myString = obj;
			for(String str : listOfRegex) {
				if(myString.contains(str)) {
					myString = myString.replace(str, " | ");
					myString = myString.replace(" ", "");
//					myString = myString.replace("\\b" + str + "\\b", " | ");
				}
			}
			tEnum.setTestItem(myString);
			break;
		case 10:
			tEnum.setError_message_en(obj);
			break;
		case 11:
			tEnum.setError_message_ms(obj);
			break;
		default:
		}
		
		return tEnum;
	}

	
}

package com.taxoDataModel.taxo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.taxoDataModel.taxo.model.tax_concept;
import com.taxoDataModel.taxo.model.tax_data_modal;
import com.taxoDataModel.taxo.pojo.responsePojo;
import com.taxoDataModel.taxo.service.excelToDBService;
import com.taxoDataModel.taxo.service.generateTaxonomyService;
import com.taxoDataModel.taxo.service.linkbaseFormService;
import com.taxoDataModel.taxo.service.taxDataModalService;
import com.taxoDataModel.taxo.service.taxEnumService;
import com.taxoDataModel.taxo.service.taxFormService;

@CrossOrigin("http://localhost:8080")
@Controller
@RequestMapping("/api/taxo")
@SpringBootApplication(exclude = { ErrorMvcAutoConfiguration.class })
public class taxoController {

	@Autowired
	excelToDBService es;
	
	@Autowired
	generateTaxonomyService gs;
	
	@Autowired
	taxDataModalService tdmService;
	
	@Autowired
	taxEnumService teService;
	
	@Autowired
	linkbaseFormService formService;
	
	@Autowired
	taxFormService tfService;

	@PostMapping("/upload")
	public String uploadFile(Model model, @RequestParam("excel") MultipartFile multipartFile) {
//		tax_data_modal dataModal, @RequestParam("excel") MultipartFile multipartFile
		
		String message = "";
		responsePojo resp = new responsePojo();

		if (es.hasExcelFormat(multipartFile)) {
			try {
				Long id = es.saveFile(multipartFile.getOriginalFilename(), multipartFile);

				message = "Uploaded the file successfully: " + multipartFile.getOriginalFilename();
				List<tax_data_modal> dmList = tdmService.getAll();
				model.addAttribute("listFile", dmList);
//				resp.setSuccess(es.readExcel(fileName));
//		resp.setForm(es.readExcel());
				resp.setMessage("Successs");
				return "upload_excel";
			} catch (Exception e) {
				message = "Could not upload the file: " + multipartFile.getOriginalFilename() + "!";
				resp.setMessage(message);
//				return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(resp);
				return "upload_excel";
			}
		} else {
			message = "" + multipartFile.getOriginalFilename() + " is not an Excel File! Please upload a correct file!";
			resp.setMessage(message);
//			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(resp);
			return "upload_excel";
		}

//	    message = "Please upload an excel file!";
//	    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
	}

	@GetMapping("/write34234")
	public ResponseEntity<responsePojo> writeFile() {
		String message = "";
		responsePojo resp = new responsePojo();
		
		resp.setMessage(gs.writeFile(1L));
//		resp.setMessage("Successs");
		return ResponseEntity.status(HttpStatus.OK).body(resp);
		
	}
	
	@GetMapping("/getList")
	public ResponseEntity<responsePojo> getList() {
		String message = "";
		responsePojo resp = new responsePojo();
		
//		resp.setTaxEnumList(teService.getAll());
//		resp.setTformList(tfService.getAll());
		resp.setForm(formService.getSingle(108L));
//		resp.setTdm(tdmService.getById(2L));
		resp.setSuccess(true);
		resp.setMessage("Successs");
		return ResponseEntity.status(HttpStatus.OK).body(resp);
		
	}
}

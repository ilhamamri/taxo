package com.taxoDataModel.taxo.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.taxoDataModel.taxo.model.tax_data_modal;
import com.taxoDataModel.taxo.pojo.responsePojo;
import com.taxoDataModel.taxo.service.excelToDBService;
import com.taxoDataModel.taxo.service.generateTaxonomyService;
import com.taxoDataModel.taxo.service.taxDataModalService;
 
@Controller
public class MainController {
	
	public static final String SAMPLE_ZIP_FOLDER = "./src/main/output/zip/";
	
	@Autowired
	excelToDBService es;
	
	@Autowired
	taxDataModalService tdmService;

	@Autowired
	generateTaxonomyService gs;
	
	@GetMapping("/register")
    public String showForm(Model model) {
//        User user = new User();
//        model.addAttribute("user", user);
//         
//        List<String> listProfession = Arrays.asList("Developer", "Tester", "Architect");
//        model.addAttribute("listProfession", listProfession);
		List<tax_data_modal> dmList = tdmService.getAll();
		model.addAttribute("listFile", dmList);
         
        return "upload_excel";
    }
	
	@PostMapping("/upload")
	public String uploadFile(Model model, @RequestParam("excel") MultipartFile multipartFile) {
		String message = "";
		responsePojo resp = new responsePojo();

		if (es.hasExcelFormat(multipartFile)) {
			try {
				Long id = es.saveFile(multipartFile.getOriginalFilename(), multipartFile);
				
				if(id > 0) {
					message = "Uploaded the file successfully: " + multipartFile.getOriginalFilename();
					List<tax_data_modal> dmList = tdmService.getAll();
					model.addAttribute("listFile", dmList);
					resp.setSuccess(es.readExcel(id));
					resp.setMessage("Successs");
					return "upload_excel";
				} else {
					resp.setMessage("Failed");
					return "upload_excel";
				}

			} catch (Exception e) {
				message = "Could not upload the file: " + multipartFile.getOriginalFilename() + "!";
				resp.setMessage(message);
				return "upload_excel";
			}
		} else {
			message = "" + multipartFile.getOriginalFilename() + " is not an Excel File! Please upload a correct file!";
			resp.setMessage(message);
			return "upload_excel";
		}
	}
	
	@GetMapping("/write/{id}")
	public ResponseEntity<Resource> writeFile(@PathVariable(name = "id") Long id, HttpServletRequest request) {
		String message = "";
		responsePojo resp = new responsePojo();
		
//		resp.setSuccess(gs.writeFile());
		try {
			
//			ZipFileCompressUtils zipFileCompressUtils = new ZipFileCompressUtils();
			
			
			String dirName = gs.writeFile(id);
//			String zipDirName = gs.zipFolder(dirName);
			
			File dirz = new File(dirName);
			String zipDirName2 = SAMPLE_ZIP_FOLDER + "/" + dirz.getName() + ".zip";
			
			boolean completed = gs.createZipFile(zipDirName2, dirName);
			
			if(completed) {
				System.out.println("ok ");
			}
			
			File dir = new File(zipDirName2);
			String fileName = dir.getName();
			System.out.println("filename "+fileName);
			Resource resource = null;
			if (fileName != null && !fileName.isEmpty()) {
				try {
//					resource = gs.loadFileAsResource(fileName);
					Path path = Paths.get(SAMPLE_ZIP_FOLDER + fileName);
					resource = new UrlResource(path.toUri());
				} catch (Exception e) {
					e.printStackTrace();
				}

				// Try to determine file's content type
				String contentType = null;
				try {
					contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
				} catch (IOException ex) {
//					 logger.info("Could not determine file type.");
				}
				// Fallback to the default content type if type could not be determined
				if (contentType == null) {
					contentType = "application/octet-stream";
				}
				return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
						.header(HttpHeaders.CONTENT_DISPOSITION,
								"attachment; filename=\"" + resource.getFilename() + "\"")
						.body(resource);
			} else {
				return ResponseEntity.notFound().build();
			}
//			resp.setMessage("Successs");
//			return ResponseEntity.status(HttpStatus.OK).body(resp);
		} catch (Exception e) {
            e.printStackTrace();
//            resp.setMessage("Failed");
            return ResponseEntity.notFound().build();
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resp);
        }
	}
	
}

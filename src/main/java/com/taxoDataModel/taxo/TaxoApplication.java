package com.taxoDataModel.taxo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaxoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaxoApplication.class, args);
	}

}

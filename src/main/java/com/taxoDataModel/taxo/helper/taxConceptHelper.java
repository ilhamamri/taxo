package com.taxoDataModel.taxo.helper;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.taxoDataModel.taxo.model.tax_concept;
import com.taxoDataModel.taxo.repository.tax_conceptRepository;
import com.taxoDataModel.taxo.service.taxConceptService;
import com.taxoDataModel.taxo.service.taxEnumService;

public class taxConceptHelper {
	
	@Autowired
	private tax_conceptRepository repository;

	@Autowired
	taxConceptService tcService;
	
	public static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd"); 
	public static final String SAMPLE_TAXO_FOLDER = "./src/main/output";
	
	public boolean writeTaxConcept(String path) {
		boolean isCompleted = false;
		LocalDateTime now = LocalDateTime.now(); 
		String dtString = dtf.format(now);
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/lhdnm-cor_" + dtString + ".xsd"))) {
		    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		    writer.newLine();
		    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
		    writer.newLine();
		    writer.write("<xsd:schema targetNamespace=\"http://www.hasil.gov.my/xbrl/2020-08-31\" attributeFormDefault=\"unqualified\" elementFormDefault=\"qualified\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/2020-08-31\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\">");
		    writer.newLine();
		    writer.write("  <xsd:annotation>");
		    writer.newLine();
		    writer.write("    <xsd:appinfo>");
		    writer.newLine();
		    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-en_2020-08-31.xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    writer.newLine();
		    writer.write("      <link:linkbaseRef xlink:type=\"simple\" xlink:href=\"lab_lhdnm-ms_2020-08-31.xml\" xlink:role=\"http://www.xbrl.org/2003/role/labelLinkbaseRef\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\"/>");
		    writer.newLine();
		    writer.write("    </xsd:appinfo>");
		    writer.newLine();
		    writer.write("  </xsd:annotation>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://www.xbrl.org/2003/instance\" schemaLocation=\"http://www.xbrl.org/2003/xbrl-instance-2003-12-31.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://www.xbrl.org/dtr/type/non-numeric\" schemaLocation=\"http://www.xbrl.org/dtr/type/nonNumeric-2009-12-16.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://www.xbrl.org/dtr/type/numeric\" schemaLocation=\"http://www.xbrl.org/dtr/type/numeric-2009-12-16.xsd\"/>");
		    writer.newLine();
		    writer.write("  <xsd:import namespace=\"http://xbrl.org/2005/xbrldt\" schemaLocation=\"http://www.xbrl.org/2005/xbrldt-2005.xsd\"/>");
		    
		    
		    List<tax_concept> tList = tcService.getAll();
		    
		    for(tax_concept tc : tList) {
		    	writer.newLine();
		    	writer.write("  <xsd:element name=\"" + tc.getName() + "\" id=\"" + tc.getConcept_id() + "\" type=\"" + tc.getType() + "\" substitutionGroup=\"" + tc.getSubstitution_group() + "\" abstract=\"" + tc.isAbstract_col() + "\" nillable=\"" + tc.isNillable() + "\" xbrli:periodType=\"" + tc.getPeriod_type() + "\"/>");
		    }
		    
		    writer.newLine();
		    writer.write("</xsd:schema>");
		    
		    isCompleted = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		return isCompleted;
	}

	public boolean writeTaxConceptLabelEn(String path) {
		boolean isCompleted = false;
		
		LocalDateTime now = LocalDateTime.now(); 
		String dtString = dtf.format(now);
		
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/lab_lhdnm-en_" + dtString + ".xsd"))) {
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		    writer.newLine();
		    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
		    writer.newLine();
		    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-" + dtString + ".xsd\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/\" + dtString + \"\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
		    writer.newLine();
		    writer.write("  <link:labelLink xlink:type=\"extended\" xlink:role=\"http://www.xbrl.org/2003/role/link\">");
		    
		    List<tax_concept> tList = tcService.getAll();
		    String corName = "lhdnm-cor_" + dtString + ".xsd#";
		    for(tax_concept tc : tList) {
		    	if(tc.getPrefix().equalsIgnoreCase("lhdnm")) {
		    		String labelName = "label_" + tc.getName();
		    		writer.newLine();
		    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\""+ corName + tc.getConcept_id() +"\" xlink:label=\"" + tc.getName() + "\" xlink:title=\"" + tc.getName() + "\"/>");
		    		writer.newLine();
		    		writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"en\" id=\"" + labelName + "\">" + tc.getLabel_en() + "</link:label>");
		    		writer.newLine();
		    		writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + tc.getName() + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + tc.getName() + " to " + labelName + "\"/>");
		    	}		    	
		    }

		    writer.newLine();
		    writer.write("  </link:labelLink>");
		    writer.newLine();
		    writer.write("</link:linkbase>");
		    
		    
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		
		return isCompleted;
	}
	
	public boolean writeTaxConceptLabelMs(String path) {
		boolean isCompleted = false;
		
		LocalDateTime now = LocalDateTime.now(); 
		String dtString = dtf.format(now);
		
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/lab_lhdnm-ms_" + dtString + ".xsd"))) {
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		    writer.newLine();
		    writer.write("<!-- Lembaga Hasil Dalam Negeri -->");
		    writer.newLine();
		    writer.write("<link:linkbase xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-" + dtString + ".xsd\" xmlns:lhdnm=\"http://www.hasil.gov.my/xbrl/\" + dtString + \"\" xmlns:link=\"http://www.xbrl.org/2003/linkbase\" xmlns:num=\"http://www.xbrl.org/dtr/type/numeric\" xmlns:nonnum=\"http://www.xbrl.org/dtr/type/non-numeric\" xmlns:xbrldt=\"http://xbrl.org/2005/xbrldt\" xmlns:xl=\"http://www.xbrl.org/2003/XLink\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
		    writer.newLine();
		    writer.write("  <link:labelLink xlink:type=\"extended\" xlink:role=\"http://www.xbrl.org/2003/role/link\">");
		    
		    List<tax_concept> tList = tcService.getAll();
		    String corName = "lhdnm-cor_" + dtString + ".xsd#";
		    for(tax_concept tc : tList) {
		    	if(tc.getPrefix().equalsIgnoreCase("lhdnm")) {
		    		String labelName = "label_" + tc.getName();
		    		writer.newLine();
		    		writer.write("    <link:loc xlink:type=\"locator\" xlink:href=\""+ corName + tc.getConcept_id() +"\" xlink:label=\"" + tc.getName() + "\" xlink:title=\"" + tc.getName() + "\"/>");
		    		writer.newLine();
		    		writer.write("    <link:label xlink:type=\"resource\" xlink:label=\"" + labelName + "\" xlink:role=\"http://www.xbrl.org/2003/role/label\" xlink:title=\"" + labelName + "\" xml:lang=\"ms\" id=\"" + labelName + "\">" + tc.getLabel_ms() + "</link:label>");
		    		writer.newLine();
		    		writer.write("    <link:labelArc xlink:type=\"arc\" xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/concept-label\" xlink:from=\"" + tc.getName() + "\" xlink:to=\"" + labelName + "\" xlink:title=\"label: " + tc.getName() + " to " + labelName + "\"/>");
		    	}		    	
		    }

		    writer.newLine();
		    writer.write("  </link:labelLink>");
		    writer.newLine();
		    writer.write("</link:linkbase>");
		    
		    
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("fail to write file: " + e.getMessage());
		}
		
		return isCompleted;
	}
	
}
